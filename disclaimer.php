<!DOCTYPE HTML>
<html>
    <head>
        <title>Disclaimer - SaintSeiyaSigma.com</title>
        <meta name="description" content="Este es el Disclaimer de SaintSeiyaSigma.com" />
        
        <?php
        include 'template/head.php';
        ?>
        
        
    </head>
    <body class="disclaimer">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">Disclaimer</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include 'template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <header>
                                <h2>SaintSeiyaSigma.com</h2>
                            </header>
                            <section>
                                <header>
                                    <h3>Disclaimer SaintSeiyaSigma.com</h3>
                                </header>
                                <p>
                                    Todas las marcas registradas en el contenido del portal pertenecen a sus respectivos dueños.
                                </p>
                            </section>
                        </article>
                    </div>
                </div>
            </div>
        </div>


        <!-- Features -->
        <?php
        include 'template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include 'template/footer.php';
        ?>

    </body>
</html>