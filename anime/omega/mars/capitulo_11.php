<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 11 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 11: ¡Proteger a Aria! ¡El ataque de Sonia, la perseguidora!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_10.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_12.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/5463836C398E48DE" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Luego de la explosión provocada ante la colisión de los poderes de Seiya de Sagitario y Marte, Kōga
                                    y sus compañeros se encuentran en caminos separados. Yuna y Aria se refugian en un pueblo 
                                    cercano para pasar la noche. Al día siguiente mientras paseaban Yuna esconde a Aria en la
                                    Iglesia, al sentir el cosmo enemigo. Ella es atacada por una Marciana de alto rango, Sonia 
                                    del Avispón, hija de Marte, y dos Caballeros de Plata, Johan de Cuervo y Miguel de Perros
                                    Cazadores; pero por petición de su padre, Sonia regresa al santuario. Antes de esto Sōma
                                    aparece y la reconoce como la asesina de su padre, pero esta se retira, y empieza la 
                                    pelea con los Caballeros de Plata. Cuando estaban por perder Kōga aparece y en equipo 
                                    vencen a Johan de Cuervo y escapan del otro Caballero de Plata. Mientras Marte ahora
                                    reconocido ante los Caballeros como el Patriarca, proclama ante todo el santuario                                                       
                                    que los 5 Caballeros de Bronce han secuestrado a Athena y han destruido las 12 
                                    casas del Santuario, da la orden para que los maten y la traigan devuelta. Ahora para
                                    destruir la Torre de Babel; Kōga, Sōma, Yuna y Aria avanzan por territorio silvestre
                                    hacia las misteriosas Ruinas del Viento.  
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
