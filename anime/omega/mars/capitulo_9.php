<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 9 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 9: ¡Fatídico encuentro! ¡El impactante santo dorado!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_8.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_10.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/6D758C266AAF1127" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga conoce a Haruto en la celda, este se muestra como el Caballero de Lobo, ambos logran
                                    escapar de prisión, para encontrarse con una Palestra sumida en la oscuridad.
                                    Mientras tanto, Marte sostiene un encuentro con Ionia y la Athena falsa,
                                    destruyendo parte del santuario. Por otro lado Geki y los santos aprendices 
                                    hacen frente a los Marcianos, Kōga y Haruto se reúnen con Yuna, Ryuhō y Sōma
                                    , partiendo rumbo al santuario. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
