<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 40 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 40: ¡La determinación de Sonia! ¡La cadena del destino están rotas!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="../../omega.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_2.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/968120AD94873748" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga, Yuna y Sōma llegan a la octava casa, la casa de Escorpio, adentro les espera la recién ascendida a Caballero Dorado Femenino Sonia de Escorpio, después de mediar palabras, Sonia ataca con una enorme fuerza brutal que manda a volar a los Caballeros de Bronce, rompiéndole el casco a Yuna, Sonia se dispone a rematar a Yuna pero Sōma se lo impide, atacando a Sonia y alejándola de los demás caballeros pero Sonia se defiende usando su técnica Agujas Carmesí a lo que Sōma responde con su técnica Fuego ardiente del León Menor, Sōma no solo la ataca sino que también incendia un pequeño terreno y les pide a Kōga y a Yuna que sigan adelante, que su meta es llegar al templo del Patriarca y salvar a Athena, así Kōga y Yuna salen de la casa de Escorpio, quedándose Sōma peleando con Sonia, empezando la pelea Sonia ataca con una serie de golpes y patadas, estrellando a Sōma contra la pared pero a este se le cae la Cloth-Stone de la Cruz del Sur de su padre, Sonia lo recoge y la destruye, provocando la rabia de Sōma; Sonia y Sōma se atacan incansablemente pero Sonia por un momento obtiene la victoria, Sōma le dice a Sonia que él había visto dentro de su corazón, ella estaba sufriendo, ella estaba confundida, incluso ahora que viste la Armadura de Escorpio, su cosmos esta tan desequilibrado que no puede vencer un Caballero de Bronce Sonia se molesta y le estrella su cabeza contra el suelo una y otra vez, y a punto de rematarlo Sōma desvía el ataque, y se levanta, cuestionandola acerca de las acciones que ella y su padre Marte están haciendo, Sonia lo vuelve a atacar e iba a usar el mismo golpe con que mato a Kazuma pero Sōma la detiene; Sonia al parecer ha caído en una especie de desequilibrio mental y explota su cosmos hacia el infinito para realizar una técnica llamada Torbellino Gigante Antares pero la Armadura de Escorpio abandona el cuerpo de Sonia, dejando a Sonia rodeada de un incontrolable y descomunal cosmos, Sōma para salvarla despierta el Séptimo sentido reventándose el casco de su armadura, Sonia está siendo consumida por su propio cosmos-elemento pero Sōma logra salvarla aunque demasiado tarde, todo el cuerpo de Sonia posee graves quemaduras y en sus últimos momentos de agonía, Sonia recuerda a lo que fue su familia y muere. El fuego de Libra se ha consumido. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
