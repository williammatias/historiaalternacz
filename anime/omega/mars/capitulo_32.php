<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 32 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 32: ¡El verdadero terror! ¡La atmósfera sobrenatural de Cáncer</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_31.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_32.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/287643729B223500" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Yuna es tele-transportada a la cuarta casa, la casa de Cáncer, adentro le espera el Caballero Dorado Schiller de Cáncer; el ambiente es bastante tétrico y repugnante, hay ataúdes por todas partes, y haciendo su aparición el Caballero de Cáncer presentándose ante Yuna. Empezando la batalla, Yuna sin mediar palabras ataca a Schiller con todas sus fuerzas pero este repela el ataque con una sola mano, es entonces que Schiller lanza su primer ataque, Marioneta de Cadáveres, y de los ataúdes empiezan a salir caballeros muertos que fueron asesinados por el caballero de Cáncer, los cadáveres atacan a Yuna, ella usando una de sus técnicas los manda a volar pero los cadáveres aun se levantan y empiezan a perseguir a Yuna es entonces que Schiller le envía una sorpresa, las mejores amigas de Yuna, Arne de Liebre y Komachi de Grulla que están en estado de Premortem la atacan sin cesar, pero Yuna no quiere atacarlas, Schiller ataca a Yuna con su técnica Caída al Inframundo (una ilusión al mundo de los muertos) pero esta se levanta es entonces que Schiller envía a todos los muertos a atacarla pero el Caballero Dorado se detiene ya que siente el cosmos de la casa de Tauro apagarse a lo que Yuna también se sorprende, entonces vuelve a atacar al Caballero de Cáncer pero este repele el ataque y la ataca con su técnica Ondas Infernales enviando a Yuna a la Colina del Inframundo. Por otro lado Kōga aun sigue peleando con el Caballero Dorado, Harbinger le aplica la fractura de la rótula pero Kōga siente el cosmos de Yuna y levantándose lentamente ya que Harbinger le ha roto los cinco huesos metacarpianos de ambas manos, y poco a poco elevando su cosmos hasta que logra semi-despertar el Séptimo Sentido y a continuación ambos caballeros elevan sus cosmos al máximo y atacando con sus mejores técnicas, al parecer Harbinger gana pero cae de rodillas debido al uso extremo de su cosmos, en cuanto a Kōga sale de la casa de Tauro hacia la siguiente casa; Harbinger se ríe admirando el valor y la tenacidad de Kōga. El fuego de Tauro se ha consumido. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
