<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 34 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 34: ¡El umbral entre la vida y la muerte! ¡La batalla en el Inframundo!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_33.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_35.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/626F08B07935506C" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga y Ryūhō llegan a la casa de Cáncer, Schiller se presenta ante ellos y admite, fríamente, ser el responsable de enviar a Yuna al mundo de los muertos; por otro lado, Yuna se encuentra con las almas de aquellas personas que murieron quemadas en su aldea a causa de la guerra, dichas almas que aun no encuentran el descanso eterno siguen gritando por sus vidas, Yuna horrorizada, huye de ese lugar. Ryūhō le cuenta a Kōga que para que Yuna salga de la Colina del Inframundo Schiller tiene que morir, tal y como lo hizo su padre Shiryū de Dragón con Mascara de Muerte de Cáncer, Kōga y Ryūhō empiezan el ataque pero Schiller los detiene con una sola mano, Schiller furioso ataca con sus Ondas Infernales a Kōga y a Ryūhō, estos usan sus mejores técnicas para defenderse pero Kōga hace algo inesperado, agarra a Schiller y se lo lleva al mundo de los muertos; en la Colina del Inframundo, Schiller enfadado, ataca a Kōga por haberlo llevado al mundo de los muertos, Kōga cae rodeado de las almas que Marte ha asesinado sin embargo Yuna encuentra a Kōga y le dice que Aria posiblemente este allí pero Schiller dice que la nueva Athena está sufriendo una muerte peor que el de las almas mismas, al escuchar esto Kōga debido al atmósfera que hay en el mundo de los muertos empieza a despertar su cosmos de Oscuridad e increíblemente su cosmos de Oscuridad sobrepasa los poderes del Caballero Dorado tanto así que los demás Caballero Dorados sienten el cosmos de Kōga, Schiller horrorizado no puede defenderse de los ataques de Kōga pero es Yuna quien lo detiene y gracias a sus palabras Kōga vuelve a la normalidad pero cae inconsciente, Shiller decide matar a Kōga pero Yuna se lo impide es entonces que el Caballero de Cáncer utiliza su técnica Réquiem del Inframundo después de atacar a Yuna, Shiller se dirige a donde Kōga para darle el golpe final pero Yuna se levanta y despierta el séptimo sentido, ambos Caballeros elevan sus cosmos al máximo y Schiller ataca con su técnica Danza de los Espíritus pero Yuna contra-ataca con su técnica Explosión Brillante, Schiller pierde la pelea y muere al caer en el pozo de la Colina del Inframundo; Kōga y Yuna salen del mundo de los muertos y regresan a la casa de Cáncer junto con Ryūhō. El fuego de Géminis se ha consumido.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
