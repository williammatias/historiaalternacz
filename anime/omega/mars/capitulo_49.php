<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 49 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 49: ¡Proteger a Aria! ¡El ataque de Sonia, la perseguidora!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_48.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_50.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/95B06D4DDD42884E" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    En la Tierra solo falta el Santuario de Athena en Grecia para que se consuma, los Caballeros Dorados están aguantando con todo su ser y con todo sus cosmos para retardar la destrucción de la Tierra; Eden ataca a Apsu pero este bloquea sus golpes, Eden ataca rápidamente con su Danza del Trueno pero Apsu neutraliza fácilmente la técnica pero Eden eleva su cosmos y lo vuelve a atacar ferozmente con su misma técnica enviando a Apsu lejos de Athena, enseguida Eden lo ataca con su técnica Furia del trueno pero Apsu no tuvo ningún rasguño, Eden y Apsu se atacan entre ellos en una pelea cuerpo a cuerpo y, Eden consigue lo que quería, alejar lo más lejos que se pueda a Apsu de Athena, y lo estrella contra la pared de un cráter, Apsu eleva su cosmos de oscuridad y ataca a Eden sin que este se hubiese dado cuenta e implantándole en su mano izquierda y en su pecho la Herida de Oscuridad (la misma Herida de Oscuridad que los Caballeros Legendarios poseen en sus cuerpos) e impidiendo que Eden encienda su cosmos, los demás Caballeros de Bronce sienten que el cosmos de Eden a disminuido considerablemente pero deciden quedarse y proteger a Athena, Apsu ataca a Eden con un golpe tras otro pero a este no le queda más que esquivarlos mientras pone en marcha su estrategia en detener a Apsu, haciendo caer descomunales pilares de roca en él debido a los golpes fallidos, seguidamente y a pesar del dolor de la Herida de Oscuridad Eden eleva su cosmos hacia el Séptimo Sentido (mientras que la herida se expande aún más) atacando con su mejor técnica Orión Devastador, mientras que Apsu solo se defiende con una esfera de energía oscura absorbiendo la técnica de Eden e implosionando en frente de él pero Medea tele-transporta a su hijo a momentos de morir en la enorme explosión y, Apsu nuevamente emprende su marcha donde Athena; Medea ahora custodia el Báculo de Aria que esta enterrada en un enorme Cristal, Eden se dispone a buscar el Báculo pero su madre se niega a dárselo paralizando el cuerpo de Eden, este neutraliza la técnica y se ve obligado a atacar a su propia madre con su técnica Orión Devastador pero el Báculo de Aria rompe la Prisión de Cristal e irónicamente protege a Medea, Eden agarra el Báculo (mientras se ve como la Herida de Oscuridad de su mano se ha expandido hasta por casi todo su antebrazo y de su pecho a cubierto gran parte de ello) y se dirige donde los demás Caballeros; Apsu vuelve a llegar donde Athena, los Caballeros de Bronce Ryūhō de Dragón, Sōma de León Menor y Haruto de Lobo están dispuestos a entregar su vida como Caballeros de Athena pero Yuna de Águila duda en pelear en contra de "Kōga" estos elevan sus cosmos hacia el séptimo sentido y cada uno ataca con sus poderosas técnicas pero Apsu neutraliza dichas técnicas y además de destruirle el escudo de Dragón de Ryūhō les deja en ellos la Herida de Oscuridad, Sōma que se niega a caer y a punto de hacer algo para detener a Apsu este lo vuelve a atacar ferozmente con la herida de oscuridad y Sōma cae inconsciente, Yuna horrorizada no puede creer lo que ha visto pero aún así decide no moverse y ataca a Apsu pero Yuna también cae con una Herida de Oscuridad en toda su pierna izquierda, Apsu avanza donde Athena pero Eden ha llegado con el Báculo de Aria en sus manos la luz del Báculo expulsa a Apsu del cuerpo de Kōga, Apsu en su forma intangible ataca a Eden pero Medea protege a su hijo y recibe el poderoso ataque matándola al instante pero aún así Apsu ataca a Eden rompiéndole su casco y destruyendo gran parte de su armadura y además le deja otra herida de oscuridad en casi todo su cuerpo para luego destruir el Báculo de Aria y regresar al cuerpo de Kōga. Los fragmentos del Báculo de Aria caen en lo profundo del subsuelo. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
