<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 31 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 31: ¡La encrucijada del destino! ¡El enigma de Géminis!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_30.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_32.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/4A1804B982A50605" width="600" height="360" frameborder="0" scrolling="no"></iframe>                                <p>
                                        La Tierra ha empezado ha sufrir los planes de Marte (en Brasil la estatua de Cristo Redentor se ha empezado ha desfragmentar); por otro lado, Ryūhō es tele-transportado a la tercera casa, la casa de Géminis, aunque por un momento había intentado regresar a la casa de Tauro recordó las palabras de Kōga "perdamos lo que perdamos, ¡debemos seguir adelante!" fue entonces que Ryūhō decide entrar a la casa de Géminis, adentro le espera el Caballero Dorado Femenino Paradox de Géminis; Ryūhō sorprendido de que el Caballero Dorado sea femenino, ya que esta tiene el rostro descubierto le pregunta a Ryūhō si ¿sabes lo que significa cuando un caballero femenino tiene el rostro descubierto? así que se pone a pensar que si un hombre ve su rostro solo hay dos opciones amarlo o matarlo y para sorpresa de este, Paradox decide amarlo y lo abraza sin que Ryūhō se diera cuenta de sus movimientos, Ryūhō se suelta y sigue adelante ya que Paradox se niega a pelear, es entonces que Paradox se mueve rápidamente para bloquearle la salida a Ryūhō dispuesta a pelear, Ryūhō le lanza una serie de ataques pero el Caballero de Géminis las esquiva muy fácilmente, es entonces que Ryūhō de Dragón le lanza su técnica Dragón Naciente pero sorprendentemente, Paradox de Géminis hace la misma técnica, el Dragón Naciente y por ser caballero dorado la hace mucho más poderosa venciendo así a Ryūhō. Sin embargo Ryūhō se levanta a lo que Paradox le lanza otra técnica Encrucijada espejismo mostrándole a Ryūhō un futuro paralelo si decide rendirse, causando la furia de Ryūhō y haciendo arder su cosmos sale de la Encrucijada espejismo, y realiza su técnica el Dragón Naciente a lo que Paradox contra-ataca con la misma técnica pero el cosmos de Ryūhō es tan fuerte que al parecer le gana y hiere al caballero de Géminis, es entonces que la segunda personalidad de Paradox emerge y ataca sin piedad al Caballero de Bronce, Ryūhō horrorizado, no puede defenderse ante los ataques tan seguidos de Paradox del Odio y cae inconscientemente. 
                                    </p>
                                </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
