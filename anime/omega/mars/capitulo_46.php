<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 46 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 46: ¡Kōga y Eden! ¡Joven cosmos, asesina la Oscuridad!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_45.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_47.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/A253DB710A4CFA9D" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Eden se enfrenta directamente a Marte, atacándolo con un golpe y que Marte esquiva muy fácilmente, Eden emplea una técnica pero Kōga decide participar a lo que Eden se lo impide abriéndole paso para que llegue hasta el Báculo de Athena (Niké), e impedir la ambición de Marte; Kōga llega hasta donde el Báculo e intenta quitarlo pero anteriormente Medea había colocado un Campo de Fuerza alrededor del Báculo en la que solo Medea o Marte se le puede acercar; Kōga que intenta una y otra vez acercase al Báculo es expulsado por el campo de fuerza, por otro lado Marte que se encuentra peleando contra su hijo Eden es este quien niega a Marte como su padre y que detendrá sus ambiciones, Marte al escuchar esto se llena de ira y su cosmos de oscuridad consume completamente el cuerpo de Ludwig, dándole una nueva apariencia y aplastando a Eden con una fuerza descomunal, Eden cae inconsciente y Marte se dirige a donde Kōga y una vez allí lo ataca con su Resplandor Escarlata de Gungnir pero Kōga se defiende con su Meteoro de Pegaso y directamente contra Marte pero este desvanece la técnica con una sola mano, e inmediatamente Marte ataca a Kōga con su técnica Tormenta Escarlata pero Kōga se niega a caer y Marte lo vuelve a atacar con su técnica Meteoro Escarlata y esta vez Kōga cae casi inconsciente, Kōga se niega a rendirse y a duras cuestas se pone de pie y ataca una vez mas a Marte con su Cometa de Pegaso pero Marte refleja la técnica de Kōga y se la devuelve con el doble de poder, Kōga a punto de caer al precipicio es salvado por Eden y juntos unen fuerzas para sacar el Báculo pero Marte se los impide, Marte hace crecer su cosmos de oscuridad nublando la vista de los Caballeros, Eden que recordó el entrenamiento que tuvo con Micenas de Leo le guía a Kōga a través de la oscuridad, Marte los ataca con su técnica Resplandor Escarlata de Gugnir y Kōga contra-ataca con su Meteoro de Pegaso, Marte vuelve a atacar con su Meteoro Escarlata y a continuación Eden contra-ataca con su Danza del Trueno, luego Marte ataca otra vez con su Tormenta Escarlata pero ambos Caballero esquivan la técnica y con la luz de Aria ambos Caballero llegan a donde se encuentra Marte y a punto de atacarlo Eden siente el corazón de Ludwig clamando a gritos ser salvado para luego Marte repelerlos con su cosmos, ambos Caballeros aun tienen fuerzas para ponerse en pie y atacar nuevamente a Marte, este vuelve a emplear las mismas técnicas que uso anteriormente pero Kōga y Eden ya saben que hacer ante dichas técnicas llegando nuevamente a donde esta Marte y este a punto de atacarlos es Eden quien usándose a si mismo como escudo, Marte le da un golpe que aparentemente pudo haberlo matado pero Eden pudo lograr esquivarlo y convenciendo a Marte de haberlo asesinado, Marte queda aturdido ya que Ludwig estaba apaciguando el cosmos de Marte, y fue justo en ese momento que Kōga ataca con todas sus fuerzas a Marte con su Cometa de Pegaso, Marte quien demasiado tarde recupera el conocimiento recibe directamente la técnica destruyéndolo casi por completo, ambos Caballeros están completamente exhaustos y Ludwig quien ha recuperado casi completamente el control de su cuerpo le pide perdón y se despide de su hijo Eden, empleando el poco cosmos del Dios de la Guerra para sacar el Báculo pero resulta ser demasiado tarde el último fuego, el fuego de Piscis se ha extinguido y Ludwig quien a punto de sacar el Báculo es impactado por un enorme relámpago que vino directamente desde Marte destruyendo el terreno donde se encuentran Kōga y Eden. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
