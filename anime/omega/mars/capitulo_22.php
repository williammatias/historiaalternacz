<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 22 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 22: ¡Los sentimientos hacia mis amigos! ¡El orgullo de los santos y el camino de un ninja!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_21.php.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_23.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe width="720" height="405" src="http://rutube.ru/video/embed/5500062" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>
                                </div>
                                 <p>
                                                        Haruto regresa a su aldea acompañado voluntariamente de Ryūhō, pero Haruto desaparece a la vista de su amigo, Ryūhō perdido
                                                        en el bosque se enfrenta a un ninja resultando ser el líder del clan Fuji y padre de Haruto, este decide 
                                                        llevar a Ryūhō a su aldea y le cuenta la historia de como su hijo decidió ser un caballero traicionando a la vez
                                                        a su gente (los ninjas odian a los caballeros por traer desgracias a sus tierras) y culpando a Yoshitomi (maestro
                                                        y hermano mayor de Haruto). Por otro lado, Haruto se encuentra en el lugar donde fue entrenado por Yoshitomi y 
                                                        empieza a recordar la partida de su maestro rumbo a palestra explicándole a Haruto que desea ser un Caballero, 
                                                        después de un tiempo Yoshitomi regresa herido a su aldea pidiendo la ayuda a sus camaradas pero ellos le niegan 
                                                        la ayuda y lo expulsan de la aldea, Yoshitomi molesto se esconde entre los bosques pero es Haruto quien lo ayuda 
                                                        con sus heridas y le habla acerca de los planes que tiene Ionia con Palestra pero son interrumpidos por un cosmos
                                                        acercándose y apareciendo de la nada el Caballero de Plata de Reloj con la misión de acabar con la vida de Yoshitomi
                                                        de Lobo, Haruto huye del campo de batalla y en una confrontación de cosmos el terreno explota declarando así la
                                                        derrota de Yoshitomi mientras que el Caballero de Plata se retira, Haruto regresa y ve muerto a su hermano, este 
                                                        agarra el Cristal de la Armadura de Lobo, entrenándose y convirtiéndose así en el nuevo portador de la armadura. 
                                                        De vuelta a la realidad, fue gracias al padre de Haruto que Ryūhō pudo encontrar el lugar donde posiblemente estaba 
                                                        Haruto, pero entonces los ninjas atacan a Ryūhō y a Haruto pero este se deja atacar preguntándose a si mismo "¿quien 
                                                        soy ninja o Caballero?" pero es Ryūhō quien lo hace despertar, cuando los ninjas deciden atacar solamente a Ryūhō,
                                                        entonces Haruto decide ser los dos un Caballero-Ninja salvando así la vida de su amigo, es entonces que aparece el 
                                                        padre de Haruto aceptando a su hijo como un Caballero. 
                                                    </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
