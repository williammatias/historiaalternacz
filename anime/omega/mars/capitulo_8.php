<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 8 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 8: ¡Fatídico encuentro! ¡El impactante santo dorado!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_7.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_9.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/A0E3F49F0A64DF12" width="600" height="360" frameborder="0" scrolling="no"></iframe>                                
                                </div>
                                <p>
                                    Kōga siente un cosmos, cree que es del Athena y va corriendo hacia donde se encuentra 
                                    ella, llevándose la sorpresa de que es una temerosa chica que Ionia de Capricornio 
                                    revela como la verdadera Diosa Athena y también revelándole al Caballero 
                                    su lealtad hacia Marte, lo cual los dos caballeros se enfrentan viéndose
                                    el nivel del Caballero de Oro que derrota fácilmente al joven Caballero
                                    de Bronce. Kōga despierta y se encuentra en una celda junto al misterioso
                                    Caballero de Bronce Haruto de Lobo. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
