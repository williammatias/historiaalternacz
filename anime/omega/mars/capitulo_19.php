<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 19 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 19: ¡El secreto de los cinco picos! ¡Aparece padre, el espíritu de lucha de Shiryū!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_18.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_20.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/6BD35052C7F88D76" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Después de conseguir el Cristal de Fuego, Aria y el grupo de jóvenes caballeros (Kōga, Yuna y Haruto) llegan hasta el reactor del agua,
                                    que se ubica en la cascada Colmillo de Dragón cerca de los antiguos Cinco Picos de China. Ryūhō que después de haberse 
                                    recuperado en la aldea donde se encontraba Shun, se dirige inmediatamente hasta aquella cascada y aparece el Caballero 
                                    de Plata Mirfak de Perseo, que en su escudo porta a Medusa, ya que quien mire directamente este escudo quedará petrificado, 
                                    quedando Kōga, Yuna y Haruto en ese estado, mientras que Aria permanece escondida. Inicialmente la batalla le es difícil a 
                                    Ryūhō debido a que sufre la misma enfermedad del corazón que sufría su padre Shiryū de Dragón, sumando el hecho de que 
                                    Mirfak le recuerda que el anterior Caballero de Plata de Perseo (Algol) fue vencido por Shiryū, tras haber perdido su 
                                    vista como sacrificio. Ryūhō logra vencer a Mirfak y el efecto medusa desaparece, dejando a los demás a la normalidad, 
                                    quedando agotado por la batalla. Estando en frente del reactor, el grupo es incapaz de entrar, debido a que el flujo de 
                                    la cascada es capaz de aplastar a cualquiera que entre, ante esto, Shiryū (que custodiaba la Armadura de Libra para
                                    evitar que caiga en manos de Marte, y que Shiryū se negaba a aceptar la voluntad de la armadura) envía 
                                    la Armadura Dorada de Libra para ayudar a los jóvenes caballeros. Ryūhō y Aria juntos extraen el cristal del agua
                                    y justo después de ese momento desde los bosques aparece Genbu, un Caballero Dorado que reclama la Armadura de Libra 
                                    como propia y que la propia armadura sonaba aceptando a Genbu como su nuevo portador y atacando a los jóvenes con su 
                                    poderoso cosmos sin siquiera colocarse su armadura pero es distraído por un enorme Cosmos. Es Shiryū que, desde los 
                                    Cinco Picos, abre sus ojos y eleva su cosmos hasta al mismo nivel que el Caballero de Libra, y lo hace dubitar. 
                                    Al Shiryū hacerlo, su Herida de Oscuridad (que Marte en la última batalla le lanzó) se expande por casi todo su 
                                    cuerpo. Genbu decide irse con la Armadura de Libra, ya que reconoce que si pelea con Shiryū no sería una batalla fácil. 
                                    Ryūhō sintiéndose decepcionado, le promete a su padre retornar con la Armadura Dorada de Libra y curarlo de sus heridas de Oscuridad.
                                    Finalmente Haruto se separa una vez mas del grupo y en ésta ocasión es Ryūhō quien lo acompaña, mientras que Aria, Yuna y Kōga siguen
                                    su camino hacia las Ruinas del Relámpago. 
                                </p>    
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
