<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 18 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 17: ¡Tenemos que protegerlos! ¡El reparador de armaduras y el legendario mineral!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_16.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_18.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/C51FEC47C60E21B3" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Siguiendo su camino hacia las Ruinas del Fuego Aria y el grupo de jóvenes caballeros 
                                    (Kōga, Yuna y Sōma) cruzan las montañas y se encuentran con Raki, una pequeña niña de 
                                    la misma raza que los maestros Shion, Mu entre otros; y al igual que ellos, Raki maneja
                                    la telequinesis (luego de atacar a Kōga). Luego de ver que todos son caballeros, 
                                    la niña continua el camino con todos ellos, entablando una especial amistad con Aria.
                                    Mientras tanto, Sonia es notificada de que no han encontrado al grupo rebelde, con lo
                                    que el Caballero de Plata Dore de Cerbero se ofrece voluntariamente a traer a Aria de 
                                    vuelta a la Torre de Babel. Raki lleva a los caballeros hasta una cueva donde se encuentra 
                                    el mineral y el polvo estelar necesarios para reparar las armaduras. Seguidamente, el
                                    Caballero de Plata los interrumpe, atacando a los Caballeros de Bronce con su técnica,
                                    siendo parcialmente derrotados. Raki tiembla de miedo y se esconde detrás de Aria, quien
                                    enciende su cosmos de luz para protegerla. Es Kōga quien se levanta de suelo, elevando su 
                                    cosmos hasta el mismo nivel que el Caballero de Plata y vence a Dore con su Meteoro de Pegaso.
                                    Posteriormente Raki repara las armaduras de los Caballeros de Bronce y se despide de todos 
                                    ellos, en especial de Aria. Finalmente, Raki se siente acongojada, y entra en escena su mentor
                                    Kiki, discípulo del legendario Mu de Aries, viendo en los jóvenes caballeros una nueva luz de esperanza. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
