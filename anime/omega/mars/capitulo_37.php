<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 37 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 37: ¡El inamovible guardián! ¡El santo dorado de Virgo!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_36.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_38.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/2279BBBF8408873E" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kiki de Aries todavía sigue peleando contra el ejército de Marcianos en su templo pero Harbinger de Tauro llega para ayudar al Caballero de Aries; admitiendo que tanto él como los algunos Caballeros Dorados ahora están en contra de sus planes. Por otro lado, Sōma y Haruto llegan a la sexta casa, la casa de Virgo, pero el enorme cosmos del Caballero de Virgo los arrastra adentro y se presenta ante ellos (con los ojos cerrados) como Fudō de Virgo, Fudō ya de por si, sin siquiera vestir su armadura posee un cosmos enorme y además posee dos armas una Espada Flamígera y una Cuerda Sagrada; Haruto empieza la pelea atacando al Caballero de Virgo, pero este está protegido por una especie de campo de energía, impidiendo que cualquier ataque simple lo toque; Sōma también lo intenta pero el mismo campo de energía lo repele, entonces Fudō usa su técnica Invocación del Señor de la Destrucción haciendo que Sōma y Haruto ardan en llamas pero ellos pensando en sus amigos se levantan y se prepara para atacar pero rápidamente Fudō les lanza otra técnica, Condena Mundana, y la cuerda empieza a hacer una atadura en sus cuerpos impidiendo que se muevan y una vez atados les vuelven a lanzar la técnica Invocación del Señor de la Destrucción, ellos se intenta zafar de la cuerda sagrada pero no lo logran y hacen que la cuerda les apriete aun mas, a puntos de morir son salvados por Kōga, Yuna y Ryūhō y, gracias a Yuna deshace las cuerdas; los Caballero de Bronce se han vuelto a reunir y atacan con sus mejores técnicas a Fudō, primeramente Yuna y Ryūhō que han despertado el Séptimo Sentido lo atacan pero solo consiguieron por un momento desfigurar el campo de energía de Fudō y este emplea su siguiente técnica Ōm mandando a Yuna y a Ryūhō a volar, después en un ataque combinado de Sōma y Haruto contra Fudō, a este no le pasa nada y vuelve a usar su técnica Ōm estrellándolos contra el suelo, quedando solamente Kōga, este usa su técnica Meteoro de Pegaso pero sus golpes son devueltos contra del Caballero de Bronce, es entonces que Fudō vuelve a usar su técnica Condena Mundana y la cuerda vuelve a aparecer atando fuertemente a los Caballeros de Bronce, estos intentan zafarse pero Fudō hace que las cuerdas ardan como el fuego y emplea su siguiente técnica, Liberación del Samsara y por un momento los caballeros de Bronce caen en el piso, pero por salvar al mundo hacen arder sus cosmos rompiendo las cuerdas que los ataban, Fudō (empieza a llorar y abriendo los ojos) ha entendido bien los ideales de los caballeros de bronce y como Caballero Dorado va a poner a prueba esos ideales, invocando la cloth-stone de Virgo, ahora Fudō viste la armadura dorada de Virgo y su cosmos ha aumentado descomunalmente, y con ese cosmos emplea su técnica Descención del Iluminado y ahí mismo su otra técnica Kān incrustandole una lanza en sus corazones y estrellándolos contra el suelo y para rematarlos utiliza su última técnica en ellos Tenma kōfuku pero son salvados gracias al hijo de Marte y Caballero de Bronce, Eden de Orión. El fuego de Leo se ha consumido. 
                                </p> 
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
