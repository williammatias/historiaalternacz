<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 39 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 39: ¡Reunión en Libra! ¡Choque, Dorado vs Dorado!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_38.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_40.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/00746882A9EE73D4" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga es expulsado de las escaleras, pero las armas de Libra lo salvan de caer al precipicio, tele-transportándolo a él y a los demás caballeros, incluyendo a Tokisada, a la casa de Libra, allí el Caballero Dorado Genbu de Libra admite su lealtad hacia Athena y les pide perdón a los Caballeros de Bronce debido al último encuentro que tuvieron, Acuario (llamado así debido a que Tokisada no esta en sus cabales como para realizar alguna técnica conscientemente) al escuchar eso, decide atacar a Genbu (desatándose así la batalla de los mil días) con su técnica Golpe de tiempo ralentizando los movimientos de Genbu pero este duplica la velocidad de sus movimientos haciendo que la técnica de Acuario no tenga efecto en él; ambos Caballeros Dorados elevan sus cosmos para que luego Acuario usara de nuevo su técnica Golpe de tiempo para ralentizar aun mas la velocidad de sus ataques pero Genbu usa una técnica a la Velocidad de la luz llamada Impacto real de Rozan un poderoso golpe que estrella a Acuario contra el suelo creando un enorme cráter; Genbu le habla a Ryūhō que todas sus técnicas las aprendió en los Antiguos Cinco Picos de China y que su maestro fue Dohko de Libra, Genbu fue el último estudiante de Dohko, el condiscípulo de Shiryū y el segundo heredero de la armadura de Libra, ya que el heredero de la armadura es Shiryū pero debido a su actual condición física, Shiryū tuvo que renunciar a su Armadura de Libra, quedándose Genbu con la armadura; Acuario se levanta y regenera sus fuerzas rápidamente usando su técnica Regresión del tiempo y vuelven a atacarse entre ellos, Genbu se está agotando pero Acuario vuelve a usar su técnica Regresión del tiempo para luego atacarlo con una series de golpes, y rematarlo con su técnica Cronos ejecución pero Genbu sale ileso y le da un fuerte golpe en la cara a Acuario, este trata de usar su Regresión del tiempo pero no puede dado que esta exhausto para seguir usando sus técnicas al igual que Genbu, ambos están exhaustos, pero Acuario obliga dolorosamente a Tokisada a explotar su cosmos para luego usar la técnica Cronos ejecución pero Genbu contrarresta esa técnica con su Ascensión celestial de Rozan esta técnica absorbe la técnica de Acuario y la devuelve con el doble del poder en contra de su enemigo, y Tokisada vuelve a recuperar la conciencia pero queda gravemente herido, pero Medea hace algo precipitado y trata de destruir el único camino que tienen los Caballeros de Bronce de llegar hasta el templo del Patriarca, destruyendo las doce casas, Genbu emplea sus últimas fuerzas para impedir que las doce casas sean destruidas, pero Tokisada que está sufriendo los efectos secundarios, aun se vuelve a levantar y emplea una técnica suicida llevándose consigo a Ryūhō y a Haruto, mientras que el fuego de Virgo se ha consumido. Anteriormente, durante la pelea entre Genbu y Acuario, Medea asciende a su hijastra Sonia a Caballero Dorado de Escorpio. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
