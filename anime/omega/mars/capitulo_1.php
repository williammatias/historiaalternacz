<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 1 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 1: ¡La vida que fue salvada por Seiya! ¡Renace la leyenda de los santos!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="../../omega.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_2.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/78F3EA9526C590BA" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Trece años después (cerca del Siglo XXI), de la era de Saint Seiya: La
                                    Saga de Hades-Campos Elíseos, Marte el Dios del Fuego y la Guerra desciende
                                    a la Tierra y ataca en pleno Santuario con la intención de instaurar un nuevo 
                                    orden y gobernar así el Planeta, Atenea y el bebé Kōga están en peligro. Seiya
                                    Primer Caballero Legendario, ahora que es el Caballero Dorado de Sagitario 
                                    aparece para luchar, salvando a Atenea y al infante Kōga, pero recibiendo él
                                    y los demás Caballeros a su vez la maldición de la Herida de Oscuridad. Atenea
                                    preocupada por la nueva crisis establece un centro de entrenamiento para sus
                                    futuros Caballeros, palestra. Kōga es criado por Saori pero él no sabe que ella
                                    es Atenea siendo entrenado desde su infancia por Shaina de Ofiuco para convertirse 
                                    en el futuro Caballero de Pegaso. Él descubre que Saori es Atenea, cuando ella 
                                    vuelve a ser atacada por Marte, el Dios de la Guerra, quien en su primera aparición 
                                    dejó un maldición a Atenea, con lo cual cada vez sus poderes disminuyen, debilitando 
                                    así su cuerpo. Con el paso del tiempo, Marte escapa de la lava donde fue lanzado, al
                                    volver a aparecer ataca en la Isla Huerfano al Sur del Santuario y le deja la Herida
                                    de Oscuridad a Shaina de Ofiuco. Kōga para rescatar a Atenea, despierta su cosmos y 
                                    logra ponerse la Armadura de Pegaso, atacando con todo su cosmos, pero no fue suficiente 
                                    para detener a Marte y este secuestra a Atenea, desde entonces pasaría a ser llamado Kōga de Pegaso. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
