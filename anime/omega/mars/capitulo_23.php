<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 23 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 23: ¡Invadiendo el campo enemigo! ¡Los jóvenes santos, juntos otra vez!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_22.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_24.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/FF3B10B0B362B6D4" width="600" height="360" frameborder="0" scrolling="no"></iframe>                                </div>
                                <p>
                                    Los Caballeros de Bronce (Kōga, Yuna, Haruto y Ryūhō) se dirigen al castillo de Marte para rescatar a Aria,
                                    en el cual él no estaba allí en ese momento, dado que el castillo tenia poca vigilancia (casi todos los 
                                    Caballeros de Plata habían salido). Se dividen en dos grupos, por un lado Yuna y Kōga; y por el otro 
                                    Haruto y Ryūhō, estos últimos se encuentran con Sonia de Avispón, logran escapar de ella fingiendo sus 
                                    muertes, dado que la prioridad era encontrar a Aria. El primer grupo (Kōga y Yuna) son interceptados
                                    por un Caballero de Plata Enéada de Escudo, al cual los ataques combinados de Kōga y Yuna resultan ser 
                                    inútiles ante el enorme escudo de Enéada, pero de la nada aparece Sōma, que derrota fácilmente de una 
                                    sola técnica al Caballero de Plata de Escudo. Sōma les empieza a contar como después de un duro
                                    entrenamiento con el que fue el Caballero de Bronce de Unicornio, Jabu, obteniendo así una nueva 
                                    técnica llamada Bombardero del León Menor, pero luego Sōma se vuelve a separar del grupo para
                                    buscar a Sonia. Para cuando Kōga y Yuna llegan a la habitación de Aria, Haruto y Ryūhō ya estaban
                                    ahí, y según sus averiguaciones Aria se fue junto con Eden hacia las Ruinas del Cristal del Relámpago, 
                                    dado que ella se lo había pedido. Mientras que Sōma se encuentra con Sonia, pero en medio de la 
                                    fuerte pelea es interrumpida por el grito de Kōga que llega junto con los demás caballeros.
                                    En el momento en el que Sonia se estaba escapando deja caer la Piedra de la Armadura de la Cruz
                                    del Sur y se va diciendo que dejara la pelea para después pero que ella definitivamente la 
                                    terminaría; Sōma se dirige hacia sus amigos pero pisa la Piedra de la Armadura de su padre Kazuma,
                                    lo recoge sorprendido de encontrarla ahí. Ahora los Caballeros de Bronce (Kōga, Yuna, Haruto, Ryūhō y Sōma)
                                    se dirigen hacia la última de las Ruinas, las Ruinas del Cristal del Relámpago. 
                                </p> 
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
