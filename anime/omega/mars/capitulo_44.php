<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 44 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 44: ¡Por mis amigos! !La fuerza oculta en Kōga!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_43.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_45.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/A253DB710A4CFA9D" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    El fuego de Acuario se ha extinguido, los Caballeros de Bronce solamente tienen una hora para llegar al templo del Patriarca y detener la ambición de Marte, Kōga vuelve a despertar el cosmos de oscuridad pero como recientemente ha vuelto a despertar el séptimo sentido puede controlarlo a medias y, gracias a ese cosmos atraviesa la barrera de Agua Silenciosa que había colocado Amor, y derrota a uno de los Berserkers de un solo golpe, Amor presiona y provoca a Kōga para que haga estallar su cosmos de oscuridad y funciona, Kōga se ve rodeado de un descomunal cosmos de oscuridad, Ryūhō, Sōma y Haruto tratan de detener a Kōga pero no logran conseguir nada, pero es Yuna quien vuelve a despertar su séptimo sentido para apaciguar el cosmos de Kōga y lo logra; Kōga vuelve a entrar en razón, pero esta vez son Ryūhō, Sōma y Haruto quienes les hacen frente a los Berserkers restantes para abrirle a Kōga una brecha en su camino, elevan sus cosmos hasta el séptimo sentido y dándoles una gran batalla, en medio de la pelea Kōga huye ante la mirada de Amor cuando este estaba a punto de atacarlo pero es Yuna quien lo detiene y Kōga de Pegaso logra escapar de la casa de Piscis. Ryūhō, Sōma y Haruto empleando sus mejores técnicas ponen fin a su batalla contra los Berserkers ante la mirada de sorpresa de Amor pero los tres Caballeros caen perdiendo el conocimiento, Amor se dispone a rematarlos pero Yuna se lo impide, Amor a punto de atacar a Yuna es Eden de Orión quien llega para pelear contra su tío Amor, ambos Caballeros dan una buena pelea pero Amor de Piscis le confiesa a su sobrino que él es el asesino de Micenas de Leo y lo provoca diciéndole que Micenas fue un pésimo rival que se hacia llamar Caballero Dorado de Leo, "El rey de la selva" Eden enfadado ataca a Amor pero este bloquea los golpes fácilmente, y le da un golpe certero en el estomago de Eden, este vuelve a atacarlo pero Amor bloquea sus movimientos esposandolo contra la pared, Amor se dispone a salir de su templo a buscar a Kōga pero Yuna se lo impide, Amor ataca a Yuna pero esta se levanta y eleva su cosmos, Eden logra zafarse de la técnica de Amor y también se dispone a atacar a su tío pero Yuna decide atacar a Amor para abrirle camino a Eden y, en medio de la explosión Eden de Orión también logra salir de la casa de Piscis para dirigirse al templo del Patriarca. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
