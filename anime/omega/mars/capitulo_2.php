<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 2 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 2: ¡Salida! ¡Los Santos de una Nueva Era!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_1.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_3.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/56FF10A2B10CCF97" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga está preocupado por la desaparición de Atenea y se encuentra frustrado por su 
                                    fracaso en protegerla a ella y a su maestra Shaina que también lo consuela y le dice
                                    que a partir de ahora comienza su búsqueda por Atenea. Kōga con la ayuda del viejo
                                    Tatsumi cruza en lancha el otro lado del mar y está decidido a dominar su cosmos para 
                                    rescatar a Atenea. Desesperado encontrará la ayuda y consejo en su nuevo amigo, el 
                                    Caballero de Bronce Sōma de León Menor. Sōma le habla a Kōga sobre Palestra y salen rumbó
                                    para allá, pero en el camino tienen un ligero altercado, en el que Sōma ataca a Kōga al 
                                    descubrir que este era el Caballero de Pegaso. Kōga comparte junto a su nuevo amigo y, 
                                    ambos pasan juntos la noche en medio del bosque. Justo en medio de su descanso son atacados
                                    por un sirviente de Marte, Ordukia de Mantis, sin embargo, Kōga y Sōma visten sus armaduras 
                                    y lo atacan con sus técnicas al enemigo, pero Kōga no puede controlar los movimientos de su
                                    armadura siendo un inútil a la hora de pelear, en un momento inesperado Kōga enciende su 
                                    cosmos y a duras penas aparentemente derrota a Ordukia, pero este se levanta huye del campo
                                    de batalla para luego ser derrotado por Sonia de Avispón. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
