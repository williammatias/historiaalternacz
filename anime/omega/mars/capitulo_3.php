<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 3 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 3: La ley de la máscara! ¡El santo del viento aparece!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_2.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_4.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/BDE0E352C289CFAD" width="600" height="360" frameborder="0" scrolling="no"></iframe>                                
                                </div>
                                <p>
                                    Kōga y Sōma llegan a Palestra, un sitio de entrenamiento
                                    de los caballeros. Aparece Geki, quien es ahora maestro de
                                    Sōma, siendo recibidos por caballeros aprendices que los miran 
                                    con hostilidad. Kōga y su nueva amiga un Caballero de Bronce 
                                    Femenino Yuna de Águila se reúnen para ver las constelaciones
                                    y hablar sobre un código que rige sobre los santos femeninos.
                                    Yuna (alentada por Kōga) se enfrenta a Spear de Dorado, 
                                    rompiendo la regla del uso de la máscara, siguiendo su 
                                    propio camino como Caballero Femenino.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
