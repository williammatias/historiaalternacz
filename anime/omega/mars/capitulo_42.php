<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 42 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 42: ¡El Santo dorado traidor! ¡Ionia vs Kōga</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_41.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_43.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/EF1F20D4F25C78A1" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga y Yuna llegan a la décima casa, la casa de Capricornio, adentro les espera el Caballero Dorado traidor y el director de Palestra (ahora en ruinas) Ionia de Capricornio, para sorpresa de Kōga y Yuna, Ionia se encuentra alabando la estatua de Athena, Ionia cuenta que cuando él era joven ha sido el guardián y protector de Athena desde tiempos inmemoriales y antes del nacimiento de la penúltima Athena, se había retirado como guardián y como Caballero Dorado de Capricornio cediéndole su armadura a Shura pero cuando presenció el nacimiento de Saori decidió volver a ser solamente el guardián de Athena, él fundo la primera Palestra (debido a la perdida de muchos caballeros que poco a poco han muerto durante la batalla en contra de Seiya) para aquellos hombres que deseen portar el titulo de Portador de Armadura con la condición de que abandonaran sus intereses personales y que su prioridad sea proteger a Athena, pero la voluntad de Ionia se ha ido poco a poco corrompiendo debido a los últimos acontecimientos durante la batalla contra Hilda de Polaris, Poseidón y Hades; Ionia creyó que sus estudiantes no eran lo suficientemente fuertes como para dar la talla de caballeros, los forzó a entrenamientos exhaustivos hasta que hubo una revuelta de estudiantes, pero Ionia comete un terrible pecado, asesina a todos sus estudiantes y la Palestra queda totalmente en ruinas, Ionia voluntariamente y sin resistencia decide encarcelarse en Cabo Sunión, en la cárcel decidió escribir en libros lo que pensaba que era ser un Caballero, hasta que la propia Saori en persona (después de la última batalla contra Marte) le pide a Ionia que vuelva a vestir la armadura de Capricornio y sea el director de la segunda Palestra, Ionia (ya viejo) acepta, tiempo después Ionia recibe la visita de Medea y le dice que todas las reencarnaciones de Athenas están maldecidas con el sufrimiento eterno, y que para aliviar el dolor de Athena es entregarla a Marte, Ionia como un verdadero caballero de Capricornio entendió el sufrimiento de todas las Athenas y acepto tal propuesta. Kōga molesto enciende su cosmos y ataca a Ionia pero este se defiende con su técnica Lenguaje de Dominación y Kōga detiene su ataque, Yuna va en su ayuda pero Ionia le ordena que ataque a Kōga y ella obedece, luego Ionia le ordena que Kōga haga lo mismo con Yuna, Kōga lucha por no obedecer y se ve obligado a romperse el brazo para desviar el ataque, Ionia rápidamente le ordena a Kōga que despertara el cosmos de oscuridad para luego decirle que estallara su cosmos, Ionia provoca a Kōga para que la voluntad de este se perdiera en la oscuridad pero fueron los cosmos de las dos Athenas (Saori y Aria) quienes apaciguan el descomunal cosmos de oscuridad de Kōga, Ionia se ve forzado ha usar su mejor técnica Fuerza de Juventud, el caballero dorado rejuvenece y su fuerza muscular, velocidad y cosmos se duplican, atacando a Kōga con todo su ser, este no puede ser capaz de siquiera defenderse, esa descomunal fuerza le destruye el casco de su armadura, Kōga está perdiendo poco a poco sus 5 sentidos ya ni siquiera es capaz de sentir dolor hasta que despierta el séptimo sentido y lo ataca con su técnica Cometa de Pegaso, la armadura de Capricornio abandona a Ionia y este muere debido a los efectos secundarios de su técnica, los Caballeros de Bronce se vuelven a reunir y siguen su camino a la siguiente casa. El fuego de Sagitario se ha consumido. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
