<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 14 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 14: ¡Reunión en mi ciudad natal! ¡Maestra y discípulo enfrentándose en los campos de nieve!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_13.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_15.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
<iframe src="http://www.putlocker.com/embed/B2FAA0EDDC59AC9C" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Después de conseguir el Cristal Cosmos del Viento, el grupo (Kōga, Sōma, Yuna y Aria) siguen su camino hacia las siguientes
                                    ruinas y llegan a la ciudad natal de Yuna. En ese lugar nació y creció Yuna pero los jóvenes caballeros sienten el
                                    cosmos de un caballero acercarse, Yuna decidida se queda para pelear con el caballero; el caballero se revela como
                                    la maestra de Yuna, Pavlin de Pavo Real, quien la entreno desde temprana edad. Haciendo caso omiso de los intentos
                                    de Yuna por convencerla, su maestra la ataca sin piedad debido a que ha violado La Ley de Mascara. Luego de que
                                    Yuna le demostrara su verdadero poder, Pavlin le dice que su verdadera intención era revelarle que Saori está 
                                    viva pero en manos de Marte y que éste estaba planeando algo con ella y también que estaba allí para protegerla 
                                    de otros caballeros que en realidad estaban buscando a Yuna y a sus compañeros para asesinarlos y traer a Aria
                                    de vuelta donde Marte. Es en ese momento cuando llegan tres Caballeros de Plata para cumplir con su cometido: 
                                    Sham de Flecha, Balazo de Retículo y Almaaz de Auriga. Instantáneamente Pavlin de Pavo Real se quita su máscara 
                                    señalándole a los tres Caballeros de Plata que al haber visto su rostro los tendrá que matar, y a Yuna
                                    le ordena que debe seguir su destino de proteger a Aria y salvar a la verdadera Athena, y Pavlin decide 
                                    enfrentar a estos Caballeros hasta el instante en que ella al parecer muere. Por su parte Yuna obedeciendo 
                                    su destino tal como lo dijo su maestra, se marcha y llega hacia donde está el grupo y le entrega el mensaje 
                                    a Kōga: "Athena... Saori Kido está viva, Marte presenta a Aria como la nueva Athena; mientras mantiene cautiva a Saori Kido". 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
