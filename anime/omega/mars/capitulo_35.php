<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 25 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 35: ¡El puño del León! ¡La batalla más triste de Eden!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_34.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_36.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/9DC8BEDE119FF8F1" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Marte empieza a convocar los poderes del Báculo de Athena, para atraer así al planeta Marte y convertirlo en el nuevo mundo (que sustituiría al planeta Tierra), pero Eden está dudando acerca de tal proposición y también acerca de las acciones que su padre Marte ha hecho al haber asesinado a Aria, Eden se siente culpable y se reclama a si mismo porque siendo tan fuerte (tal y como lo fue su predecesor Jaga de Orión) no pudo hacer nada para defenderla, pero el cosmos de Aria (transformada en un pétalo de rosa blanca) aparece frente a los ojos de Eden, y lo guía hacia unas colinas lejos del castillo, pero se encuentra con su hermana Sonia de Avispón y ella intenta hacerle superar su tristeza por Aria a golpes y cachetadas; Sonia le iba a aplicar una de sus técnicas a su hermano pero el Caballero Dorado Micenas de Leo se lo impide y recibe el golpe por Eden, Micenas le pide a Sonia que se lo deje a cargo, que él se va a encargar que Eden reaccione a lo que Sonia acepta, Micenas se lleva a Eden cerca del Castillo y con su cosmos crea un Coliseo, parecido al Coliseo romano, y le pide a Eden que del primer golpe pero este no se siente seguro de hacerlo a lo que Micenas lo ataca con su cosmos alumbrando todo el Coliseo, pero Eden solo se defiende; Micenas le dice a Eden que se vuelva fuerte y elevando su cosmos al máximo, manda a volar a Eden pero este furioso lo ataca con todas sus fuerzas y le recrimina que todos esperan mucho de él, pero él ya no tiene nada por quien proteger, y su cosmos desaparece ya que cree que hasta su padre comete "errores", Micenas sorprendido ante tales palabras pierde la razón e intenta hacer reaccionar a su estudiante con un golpe de Relámpago pero rápidamente el cosmos de Aria aparece y tele-transporta a Eden al lugar donde se había encontrado con su hermana y lo sigue guiando hasta un lago en donde se refleja las estrellas, Eden de niño quería llevar a Aria a ese lugar para que dejara de estar triste; el cosmos de Aria hace aparece el zarcillo roto que Eden le había regalado, tal objeto todavía sigue brillando con el cosmos de Aria, y hace que todo alrededor del lago brille y salgan flores, Eden ve la imagen de Aria cuando era niña sonriendo en ese campo de flores y el Caballero de Orión llora de la felicidad y decide proteger todo aquello que Aria le hace feliz y a continuación Aria aparece sujetándolo de la mano. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
