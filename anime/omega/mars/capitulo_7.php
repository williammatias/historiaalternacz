<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 7 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 7: ¡El puño de un amigo! ¡Golpea, Meteoro de Pegaso!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_6.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_8.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/A0E3F49F0A64DF12" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    El torneo de los caballeros continúa, Kōga entrena en la noche y
                                    recuerda como hizo lazos con sus amigos, de pronto se acerca Geki 
                                    recordando como fue vencido por el cosmos de Seiya. Ya de vuelta en
                                    el torneo Yuna enfrenta a Güney de Delfín donde gana el caballero 
                                    femenino de viento. Ahora es el turno de Sōma y Kōga, quienes
                                    pelean motivados, pero Kōga mas aún por las palabras que recordó 
                                    de Geki, logra vencer a su amigo con el Meteoro de Pegaso.  
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
