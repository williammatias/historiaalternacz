<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 10 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 10: ¡Rescate suicida! ¡El otro santo dorado!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_9.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_11.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/6FE0265593D38FFC" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Los jóvenes Caballeros de Bronce entran a la Torre de Babel, Kōga va para rescatar a la 
                                    joven declarada como Athena, cual su verdadero nombre es Aria. Los Caballeros tratan de 
                                    escapar junto con Aria pero aparece otro Caballero Dorado llamado Mycenes de Leo, lo 
                                    cual por su rango es mucho más fuerte que los jóvenes y los Caballeros de Bronce no 
                                    logran hacerle frente. En el momento más inapropiado y oscuro para Kōga, Marte aparece 
                                    para rematar a Pegaso; y nuevamente aparece Seiya de Sagitario para salvarlo, lo cual
                                    aumenta la esperanza de lucha para Kōga de Pegaso. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
