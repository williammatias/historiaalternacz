<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 51 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 51: ¡Brilla, Kōga! ¡La batalla final entre Luz y Oscuridad!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_50.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="../pallas/capitulo_52.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/D6D9AE068B78AE14" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    El espíritu de Apsu regresa a su cuerpo y cuelga a Athena de las manos en la cima de una pirámide fantasmagórica, Kōga que está vistiendo la Armadura Dorada de Sagitario llega al Mundo de la Oscuridad; Kōga corre hacia donde esta Athena pero se encuentra con el temible Dios de la Oscuridad el cual Kōga procede a atacarlo pero Apsu bloquea la técnica con una sola mano y ataca a Kōga destruyéndole la otra ala de su armadura, Apsu seduce a Kōga con el cosmos de oscuridad pero Kōga rechaza dicho cosmos y hace arder su cosmos de Luz pero Apsu lo deja inconsciente y lo encierra para luego irse donde Athena, Kōga despierta y recuerda el sacrificio de Athena al cuidarlo desde bebé; mientras tanto los restantes caballeros encomiendan a Kōga que salve a Athena y Seiya, al ver esto, dice que ellos son la Luz de la Esperanza. Kōga hace estallar su cosmos usando su técnica Meteoro de Pegaso saliendo de ese encierro. Por otro lado, Apsu le reclama a Athena que desde la era mitológica ella y los otros Dioses le arrebataron su mundo y lo expulsaron a él del Olímpo, Kōga que corrió y llegó hasta donde Athena, Apsu lo ataca con una serie de flechas de oscuridad pero Kōga esquiva cada uno de los ataques de Apsu y luego lo ataca con su Meteoro de Pegaso pero el Dios Apsu no sufre daño alguno y con su cosmos hace retorcer el cuerpo de Kōga y lanzarlo contra el suelo, Apsu procede a atacar a Athena pero Kōga lo detiene y poco a poco va elevando su cosmos y ataca a Apsu, este esquiva el ataque pero aún así logró rozarle y herir su rostro, Apsu enfadado, no puede creer que un simple "Joven Caballero" lo haya herido, agarra a Kōga y le prende en llamas todo su brazo derecho, este del dolor cae por las escaleras de la pirámide y a punto de caer al precipicio se sostiene del otro brazo, Apsu le brinda una segunda oportunidad de entregarse a la oscuridad a cambio le devolverá el brazo, por supuesto Kōga se niega y prefiere vivir con ese dolor, este hace arder su cosmos brillando entre la oscuridad y salvándose a sí mismo, los Jóvenes Caballeros que sintieron el cosmos de Kōga se levantan una vez mas para encender sus cosmos (a pesar de que la Herida de Oscuridad que se vuelve a expandir) y enviárselo a Kōga; de la Armadura de Sagitario solo quedan los guantes, las botas y parte del abdomen, el resto de la armadura se ha destruido, a continuación los cosmos de todos sus amigos llegan hasta él, lo bañan en cosmos y Kōga concentra todo ese cosmos en su brazo quemado para emplear la técnica Meteoro de Pegaso, Apsu para demostrar su superioridad no se mueve de ahí y trata de contener la técnica de Kōga pero no lo logra, Apsu por primera vez en su vida ha quedado herido de gravedad, el impacto es tan fuerte que todo el mundo de oscuridad se deforma y las cadenas de Athena se rompen y cae al precipicio, Kōga vuela hasta donde Athena y la atrapa a tiempo, este emprende rápidamente la huida pero el Dios de la Oscuridad estalla en ira y vuela hasta donde Kōga, ambos se atacan fuertemente pero Apsu estrella a Kōga contra la pared y hace aparecer la Herida de Oscuridad en Athena y luego la expande rápidamente por todo su cuerpo pero Kōga la salva, Apsu lo agarra por la cabeza pero Kōga se suelta y logra sacar a Athena del mundo de oscuridad que se está desmoronando, quedándose los dos solos en una pelea sin cuartel, Kōga resiste todo lo que puede y cuando estaba a punto de morir es salvado por el Báculo de Aria que brillo con todo su ser, debilitando a Apsu y aumentando el cosmos de Kōga, este le propina unos fuertes golpes al Dios Apsu destruyéndolo por completo. El mundo de oscuridad ha colapsado; en Marte y en La Tierra están volviendo a su estado natural. El planeta Rojo está regresando a su órbita mientras que Athena los tele-transporta a todos de vuelta a la Tierra. Kōga que, gracias a la Armadura de Sagitario no ha muerto, se encuentra flotando en el espacio. El espíritu de Aria se aparece ante él y, con una sonrisa, le agradece por haber cumplido con su deseo; lo tele-transporta de vuelta a la Tierra. En la Tierra ha empezado a caer una lluvia de Cosmos de Luz curándolos a todos de las Heridas de Oscuridad y apareciendo ante los ojos de todos Kōga de Pegaso. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
