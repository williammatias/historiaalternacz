<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 50 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 50: ¡Háganselo llegar a Seiya! ¡El deseo de los Jóvenes Santos!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_49.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_51.php.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/AC783E778C632BA5" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga ha cedido ante la oscuridad, toda esperanza de recuperar a Kōga se está perdiendo, con el Báculo destruido ya no hay nada ni nadie que pueda detener a Apsu, y éste con su cosmos de oscuridad ha creado alas para poder volar, todos los Caballeros de Bronce gravemente heridos e incapaces de pelear a excepción de Yuna que, ha pedido de Athena (quien a estado sintiendo todo lo sucedido), decide atacar a Apsu con su Tornado Divino pero éste la expulsa brutalmente contra el suelo. El último fragmento del Báculo de Aria cae en lo profundo del subsuelo y su brillo deja ver unas cadenas. Yuna, herida, aún así se niega a rendirse y vuelve a atacarlo con su técnica Guaraña explosiva pero no le hace ni el más mínimo daño, Apsu la vuelve a atacar para que esta vez no se vuelva a levantar, Yuna clama por Kōga y Apsu para callarla decide crucificarla con la Herida de Oscuridad. Apsu ha llegado al fin donde Athena pero ésta clama con lágrimas en los ojos por Kōga. Cuando Apsu procede a asesinarla, las plegarias y los deseos de los Jóvenes Caballeros viajan y se juntan con los fragmentos del Báculo en las cavernas subterráneas donde se divisaron aquellas cadenas, lo que resulta ser una prisión. Un poderoso cosmos muy familiar hace temblar todo el terreno en donde se encuentran todos. Es Seiya de Sagitario quien ha estado encadenado todo este tiempo el que, gracias a los deseos de los jóvenes guerreros es liberado y aparece ante Apsu protegiendo a Athena. Seiya también clama por Kōga de Pegaso pero este, al parecer, ya ha caído en la oscuridad. Entonces Seiya decide atacar (aún a pesar de la Herida de Oscuridad que está en casi todo su cuerpo) a Apsu con su Meteoro de Pegaso pero el Dios de la Oscuridad viaja entre los cientos de golpes y se burla en la cara de Seiya de su técnica; Apsu y Seiya se atacan entre ellos deformando el terreno donde se encuentran y destruyéndole a Seiya el ala izquierda de su Armadura. Apsu a punto de darle el golpe final a Seiya es salvado por Yuna de Águila que eleva su cosmos al séptimo sentido pero la Herida de Oscuridad se vuelve a expandir. Yuna le recuerda a Kōga-Apsu que fueron Athena y Seiya quienes cuidaron de él desde bebé. Seiya se admira al saber que Koga también posee grandes amigos como los tuvo él en el pasado y le dice a Yuna que ha llegado el momento de salvar a Kōga, ambos elevan sus cosmos al máximo y atacan con sus mejores técnicas a Apsu pero este fácilmente las neutraliza para luego atacar a Seiya y luego a Yuna volviéndola a crucificar, pero Seiya la salva rápidamente. Seiya sigue clamando por Kōga y entonces decide elevar su cosmos al séptimo sentido y arriesgarlo el todo por el todo en su Meteoro de Pegaso, mientras que Apsu también lo ataca pero quien gana es Seiya a costa de que la Herida de Oscuridad se expandiera por sus dos brazos, parte de su torso, parte de su pierna derecha, tobillo y pie izquierdos, la mente de Kōga ha despertado y empezado a luchar contra Apsu quien está teniendo una crisis de identidad; Yuna se abalanza donde Kōga y aclama a gritos por este; Kōga al fin ha despertado y expulsa a Apsu de su cuerpo generando una enorme explosión que destruye completamente la Armadura de Pegaso; todos, agotados, están contentos de que Kōga haya recuperado la conciencia pero el espíritu de Apsu antes de regresar a su cuerpo original vuelve a secuestrar a Athena; Seiya quien está muy mal herido le presta a Kōga la Armadura Dorada de Sagitario para que salve a Athena ya que Kōga no ha sido lastimado por la oscuridad y puede entrar en ella. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
