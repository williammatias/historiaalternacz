<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 16 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 16: ¡En la estrella con lado del destino! ¡La forma de vivir de un Santo!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_15.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_17.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/EC67BE16F34C0762" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Después de conseguir el Cristal de Tierra, los jóvenes Caballeros (Kōga, Yuna, Sōma) y Aria llegan a un pequeño 
                                    bosque en donde se encuentran con sirvientes del escuadrón hormiga de Marte, para proteger a Aria, deciden huir
                                    de la confrontación, encontrándose poco después con Haruto. En medio de la huida, Yuna deja caer el monedero 
                                    que les dio Shun. Poco después llegan a una ciudad repleta de turistas. Al no tener dinero, los jóvenes se
                                    lamentan por no poder dirigirse hasta el siguiente reactor, luego se encuentran con Paulo, un anciano que
                                    les ofrece llevarlos hasta el otro lado de la ciudad en su lancha, a cambio de que trabajen para él, en
                                    la pensión que administra junto con su esposa Ann. Los jóvenes resultan ser poco eficientes para el 
                                    trabajo duro (Kōga y Aria rompen los platos, Sōma coquetea con las clientas, Yuna al tratar de secar
                                    la ropa la rompe con una de sus técnicas y Haruto espanta a los huéspedes al hacer la limpieza). 
                                    Paulo reconsidera que permanezcan para ayudarles, pero su esposa piensa lo contrario, despidiéndolos.
                                    Los jóvenes piden una segunda oportunidad, indicando que usarán sus "talentos especiales", fracasando
                                    nuevamente y siendo despedidos por segunda vez. Posteriormente, el escuadrón hormiga de Marte irrumpe
                                    frente a la pensión de Paulo y Ann, y los caballeros no tienen más remedio que luchar, vistiendo sus 
                                    armaduras y derrotando así a los marcianos. Tanto como Paulo y Ann como los turistas creen que la 
                                    reciente batalla fue parte de un gran espectáculo que le daría mucha publicidad a la pensión, a lo
                                    que Sōma inmediatamente le responde a Paulo, exigiendo el pago de su trabajo, con lo que Paulo 
                                    acepta y lleva a los jóvenes en su lancha para que así continúen con su viaje. De camino en la
                                    lancha, los jóvenes comprenden el sentido que lleva ser un caballero, y a la vez, tuvieron un divertido día. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
