<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 12 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 12: ¡El cosmo heredado! ¡Shun, el santo legendario!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_11.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_13.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/1DFA2C039646E563" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    En su camino hacia las Ruinas del Viento se encuentran con Miguel de Perros Cazadores que
                                    ataca sin piedad, pero en medio de la pelea deciden separarse; quedándose Pegaso 
                                    para pelear con Perros Cazadores y es salvado gracias al Tercer Caballero Legendario
                                    Shun de Andrómeda, ordenándole a Miguel que se regrese al Santuario, y se lleva a
                                    Kōga a su aldea donde se encuentra a Ryūhō recuperándose. Ya en la aldea, Kōga le 
                                    pregunta a Shun todo lo que sepa sobre Marte y de la herida de oscuridad; entonces 
                                    Shun habla sobre la 1° invasión de Marte a la Tierra, apareciendo en ese instante 
                                    Seiya (vistiendo la armadura original de Sagitario), Shiryu de Dragón, Shun de
                                    Andrómeda, Hyoga de Cisne e Ikki de Fénix con sus armaduras originales, desatándose
                                    la batalla y en medio de la pelea un Meteorito extraño cae a la Tierra acabando así 
                                    con la batalla, Shun también habla acerca de la explosión del meteorito y de como
                                    se fusionó con las Armaduras de Bronce, las Armaduras de Plata, la Armadura Dorada 
                                    de Sagitario y la Armadura de Marte no solo dándoles nueva forma (las armaduras 
                                    doradas también cambiaron, ya no poseen la caja de Pandora, ahora son Piedras
                                    de Armadura) sino también dándoles el poder de controlar los elementos. Sin embargo
                                    Marte (ahora con nueva Armadura y nuevos poderes) regresa a la Tierra, ataca a
                                    Saori y al bebe Kōga en el Santuario y es Seiya (con nueva Armadura y poderes)
                                    quien nuevamente los salva, siendo Marte herido por Seiya, y los 5 Caballeros
                                    con sus nuevas armaduras en un contraataque final para acabar con Marte, atacan 
                                    juntos pero son heridos incluyendo a Saori por la herida de Oscuridad de Marte; 
                                    imposibilitándolos de sus cosmos y armaduras; y es Marte quien huye de la batalla 
                                    ya que a Seiya no le afecto tanto el cosmos de Oscuridad. De vuelta a la actualidad; 
                                    Miguel que había estado escuchando todo pelea contra Shun, Kōga y Ryūhō, Shun
                                    desesperado al ver a Pegaso siendo golpeado sin piedad por Perros Cazadores arriesga
                                    su vida y eleva su cosmos apareciendo por un breve momento la nueva Armadura de 
                                    Andrómeda y atacando a Miguel con su Cadena Nebular, y es Kōga que con su meteoro 
                                    de Pegaso derrota y destruye la armadura de Miguel, ya con el camino libre Kōga se 
                                    dirige hacia las Ruinas del Viento mientras que Ryūhō se queda para recuperarse. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
