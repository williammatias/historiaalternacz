<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 20 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 20: ¡Por el bien de Aria! ¡El golpe furioso eléctrico de Eden!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_19.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_21.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/FF3B10B0B362B6D4" width="600" height="360" frameborder="0" scrolling="no"></iframe>     
                                </div>
                                <p>
                                    Después de obtener el Cristal del Agua, los jóvenes caballeros Kōga, Yuna junto con Aria continúan su
                                    viaje rumbo hacia el último reactor, las ruinas de Cristal del Relámpago en donde Shaina 
                                    (que también controla el Relámpago), maestra de Kōga, les espera; Eden estando en las Ruinas del Santuario,
                                    se le viene a la mente como fue que conoció a su padre, Marte y de como juró ante él hacerse fuerte, 
                                    pero recibiendo una desagradable noticia de como Sonia regreso herida al Santuario, Eden decidido se 
                                    fue a traer de vuelta a Aria; en otra parte se descubre que Athena (atrapada junto a un gran árbol 
                                    negro siendo despojada poco a poco de su cosmos) se encuentra secuestrada en el planeta rojo Marte,
                                    Aria que desde tan lejos sintió por un momento el cosmos de Athena (sin saber que era de ella); 
                                    los tres pasaron la noche en una montaña hablando de lo feliz que se sentía Aria y de los amigos
                                    que ha hecho. Aria con su cosmos de Luz devuelve a la vida la vegetación a su alrededor pero 
                                    aparece el hijo de Marte y Caballero de Bronce Eden de Orión, llenando el cielo con nubes de
                                    relámpago, la batalla principia con Yuna, dejándola inconsciente lanzándole un enorme trueno
                                    directamente al cuerpo de esta, Kōga que va a su rescate, se dispone a pelear lanzándole sus
                                    Meteoro de Pegaso, Eden esquiva muy fácilmente sus cientos de puños y lanzándole a Kōga otro
                                    poderoso ataque creando un cráter en el suelo, Eden dispuesto a llevarse a Aria, es Kōga quien 
                                    se vuelve a levantar haciendo arder su cosmos tanto así que por un momento su Armadura de
                                    Bronce brilla tan intensamente como una Armadura de Plata y atacándolo con todas sus fuerzas,
                                    Eden, furioso le da un tremendo golpe en la cara a Kōga hundiéndolo en los suelos y quemando
                                    el bosque a su alrededor (dejando en claro la diferencia de sus cosmos) pero es Aria quien pone
                                    fin a la batalla al decidir irse con Eden de nuevo hacia las Ruinas del Santuario y dándole a
                                    Kōga y a Yuna: "Gracias por haberme protegido".  
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
