<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 5 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 5: ¡Pruebas de selección! ¡Desafío en el campo de la muerte!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_4.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_6.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/E78EEA232EA78F7C" width="600" height="360" frameborder="0" scrolling="no"></iframe>                                
                                </div>
                                <p>
                                    Los aprendices de caballeros son llevados hasta un bosque a 
                                    manera de entrenamiento, donde son organizados por parejas
                                    (Kōga y Sōma; Yuna y Ryuhō) que en el camino encuentran 
                                    obstáculos (elementos opuestos a los que manejan), 
                                    teniendo que superarlos. Sōma tiene recuerdos de su 
                                    padre durante la misión de entrenamiento, hecho que
                                    le ayuda a derribar parte de un muro de hielo con su
                                    elemento. Kōga y Sōma son los últimos en llegar al
                                    punto de encuentro designado por Geki. Finalmente
                                    aparece el enigmatico Caballero de Bronce Edén de Orion.  
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
