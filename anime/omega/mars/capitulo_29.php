<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 29 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 29: ¡El comienzo de una nueva batalla! ¡Los doce Templos del Zodiaco!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="../../omega.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_2.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/F6C1F87E2A8B4FB0" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Con la muerte de la nueva Athena, Aria, el Patriarca Marte usa los poderes del Báculo para crear las 
                                    nuevas 12 casas del santuario que rodean la Torre de Babel y sumir a la Tierra en una oscuridad,
                                    gracias a Aria, los jóvenes caballeros son tele-transportados cerca del primer templo, con sus corazones rotos, Kōga decidido motiva a los demás a cumplir la última voluntad de Aria y deciden entrar a la casa, la casa de Aries, adentro les espera el último reparador de armaduras y Caballero Dorado Kiki de Aries (maestro de Raki y discípulo de Mu de Aries). Kiki los detiene solo para reparar sus armaduras (admitiendo que él solo le es leal a Athena), empezando primeramente con Kōga de Pegaso y mientras le reparaba su armadura les habla a todos acerca de los poderes de los Caballeros Dorados que llega más allá de los 5 sentidos ordinarios (Gusto, Oído, Olfato, Tacto y Vista), todos deben llegar más allá del Sexto Sentido hasta despertar aunque sea por un instante el Séptimo Sentido además les habla de que los ataques de los Caballeros Dorados viajan a la Velocidad de la luz y que solamente dispondrán de 12 horas para llegar hasta la Cámara del Patriarca y salvar a la Tierra, Kiki le dice a Kōga que "solo él podrá vencer a Marte ya que él posee la Armadura que se opone a los Dioses". Una vez terminado con la armadura de Kōga, este se sorprende de ver su armadura resplandeciente e inmediatamente se dirige a la siguiente casa, la casa de Tauro, los demás caballeros le siguen pero Kiki los detiene ya que aun no ha reparado sus armaduras, Kōga llega a la segunda casa, adentro le espera el Caballero Dorado Harbinger de Tauro, por otro lado Kiki empieza con las reparaciones de las armaduras de los jóvenes caballeros pero éste siente la presencia de un ejercito de Marcianos acercándose a su templo (ya que Marte junto con Medea descubren la traición de este) entonces el Caballero Dorado eleva su cosmos para acabar rápidamente con las reparaciones; una vez finalizada las reparaciones saca a los Caballeros de Bronce de su templo para quedarse él solo a pelear contra el ejército de Marcianos; por otro lado, Kōga eleva su cosmos y pelea fuertemente (recordando los últimos momentos que paso junto con Aria) contra Harbinger usando sus Meteoro de Pegaso, Harbinger se sorprende por un instante al ver la técnica de la leyenda pero contra-ataca con su técnica en la que solo los Caballeros de Tauro pueden usar, Gran Cuerno, Kōga sale expulsado de esa poderosa técnica y se escucha como sus huesos se rompen y al parecer se escucha como su corazón se desgarra ante la mirada atónita de los demás jóvenes caballeros que llegaron tarde.
                                </p> 
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
