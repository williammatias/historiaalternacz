<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 41 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 41: ¡La ambición de Tokisada! ¡El Campeón más allá del tiempo!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_40.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_42.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/B02F21EEC04516F9" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Eden llega a la casa de Escorpio, encontrándose con su hermana muerta, Eden le pide a Sōma que siga adelante, por otro lado, Marte le reclama a Medea por las acciones tan precipitadas al haberle entregado una Armadura Dorada, sin embargo Medea no mostró ningún arrepentimiento, ni piedad, después de ser la causante de la muerte de su hijastra. Ryūhō y Haruto aun están vivos pero se encuentran en un universo paralelo llamada El fin de los tiempos, un universo en la que el presente, el pasado y el futuro están juntos y, quien caiga allí jamas podrá salir; Tokisada de Acuario (que ya ha recuperado su voluntad) también sigue con vida y no solo eso también ha recuperado toda su fuerza, Ryūhō y Haruto empiezan la pelea lanzando sus mejores técnicas pero Tokisada las bloquea con una sola mano pero no antes recibiendo un rasguño en su cara, pero este vuelve a curar sus heridas con su técnica Regresión del tiempo por otro lado Kōga y Yuna llegan a la novena casa, mientras ven como se extingue el fuego de Escorpio, solo quedan menos de 4 horas para que la Tierra sea envuelta en la Oscuridad, en la casa de Sagitario, el guardián de esa casa es Seiya de Sagitario pero este no se encuentra, solo esta el mensaje de Aioros: A los caballeros que están aquí, les encomiendo a Athena; en Marte, Athena poco a poco se está convirtiendo en un pilar humano. Volviendo a donde están Ryūhō y Haruto, estos siguen peleando contra Tokisada, este les lanza un ataque pero Ryūhō lo bloquea con su escudo, destruyendo al instante el casco de su armadura y cae inconsciente; por otro lado, un furioso Micenas de Leo, que acaba de descubrir que quien está detrás de todo este caos (lo de Marte, la muerte de Sonia, entre otras cosas) es Medea pero ella está tranquila y admite ser la culpable, y a punto de matarla el Caballero Dorado Amor de Piscis se lo impide, Micenas se sorprende de conocer al Caballero de Piscis, ya que nadie ha sabido de su existencia, Amor admite estar al tanto de todo lo de Medea, ya que ella es su hermana, ambos caballeros se disponen a pelear pero el cosmos de Oscuridad de Amor es enorme, matando así al Caballero de Leo, todos los Caballeros del Santuario sintieron el cosmos de Micenas. Haruto sigue peleando contra Tokisada, este se burla de Yoshitomi llamándolo un fracaso de caballero, Haruto se enfada y despierta el Séptimo sentido, Tokisada también eleva su cosmos y usa su técnica Cronos ejecución esta técnica le está robando el cosmos a todo alrededor incluido a Ryūhō, a Haruto y, al mismo Tokisada, pero Haruto anula la Cronos ejecución de Tokisada, salvando así a Ryūhō y muriendo Tokisada de Acuario, Genbu sintió el cosmos de Haruto y envía una de las armas de Libra a buscarlos, sacándolos de ese universo. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
