<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 24 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 24: ¡Reúnanse de nuevo! ¡Vayan a la última ruina!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_23.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_25.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/DDDC489F048833DE" width="600" height="360" frameborder="0" scrolling="no"></iframe>                                </div>
                                <p>
                                    Aria y Eden ya se encuentran en las Ruinas del Cristal de Relámpago, mientras que los
                                    Caballeros de Bronce se dirigen hacia allá. En el camino se encuentran con Shaina, quien
                                    a solas, le cuenta a Kōga como momentos después de la caída del Meteorito nacen dos bebés 
                                    uno fue Kōga y la otra fue Aria, quedándose Saori con Kōga mientras que Marte se queda con 
                                    Aria decidiéndose así el destino de estos. Llegando a las ruinas son interceptados por 3
                                    Caballeros de Plata Menkar de Ballena, Bartschius de Jirafa y Bayer de Boyero; quedándose 
                                    a pelear Ryūhō de Dragón con Bayer de Boyero, Haruto de Lobo con Bartschius de Jirafa y 
                                    finalmente Yuna de Águila y Sōma de León Menor con Menkar de Ballena mientras que Kōga de Pegaso
                                    y Shaina de Ofiuco avanzan hacia al Reactor. Aria y Eden están esperándolos, empezando un 
                                    segundo encuentro entre pegaso y orión por otro lado Shaina se sorprende al sentir el cosmos 
                                    de Aria que posee el mismo cosmos que Saori, pero Aria tiene que tomar una decisión, quedarse
                                    y detener a Eden o ir con Shaina y destruir las ruinas y así traicionar la amabilidad de Eden. 
                                    Aunque lo lastimara decide ir y finalmente con la ayuda de Shaina lo destruye obteniendo el 
                                    Cristal de Relámpago. Eden con el corazón herido y derrotado ante la elección de Aria de 
                                    hacerle caso a Kōga, se enfurece, queriendo pelear contra Shaina, esta eleva su cosmos 
                                    apareciendo su Armadura de Plata pero también aparece alrededor de su cuerpo la Herida de
                                    Oscuridad (debido a la última pelea que tuvo contra Marte) pero son salvadas por Kōga, 
                                    terminando con una confrontación entre las mejores técnicas de los dos, en lo cual Aria
                                    interfiere metiéndose en el medio, pero de la luz nace la oscuridad que los absorbe a los 
                                    3 y a los demás caballeros de bronce (Shaina es la única que se logra zafar), cayendo hacia 
                                    unas Ruinas ocultas, las Ruinas del Cristal de la Oscuridad, la cual es vista desde lejos 
                                    por Sonia (que también cae hacia las ruinas). En Marte, el Dios de la Guerra proclama que es
                                    hora de poner en marcha su plan, la luz se vuelve oscuridad.   
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
