<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 36 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 36: ¡Orgullo sublime! ¡El Puño Real de Micenas!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_35.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_37.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/7C4C28BE303D3E67" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Sōma y Haruto llegan a la quinta casa, la casa de Leo, adentro les espera el Caballero Dorado Micenas de Leo, los Caballeros de Bronce empiezan la pelea con sus mejores técnicas pero no son rivales para Micenas quien los detiene con un solo dedo, Micenas solamente con elevar su cosmos los estrellan contra los suelos a Sōma y a Haruto, este le pide una explicación acerca de porque su lealtad a Marte siendo tan poderoso, Micenas empieza a recordar el momento en el que estaba hablando con Marte (antes de la caída del meteorito) y le responde a Haruto "Marte me contó su historia acerca de que quiere cambiar al mundo, para vivir en un mundo sin guerras, ni muertes" Micenas conmovido ante una historia falsa, acepta unirse a su causa pero no sin antes, de que Marte mostrara su verdadero rostro y respondiendo al nombre de "Ludwig", sin embargo Micenas se pone a pesar (empezando a dudar) acerca de las últimas acciones de Marte, han dejado mucho que desear; el asesinato de la nueva Athena, las dudas que tiene Eden acerca de su padre, y los planes que tienen (Medea y Marte) si Eden no se hace fuerte. Volviendo a la pelea, Micenas los provoca diciéndoles que "mi lealtad es jurar obedecer a mis propias convicciones y faltar a ello es como faltar al deber que tiene todo aquel que viste una armadura", al oir esto Sōma y Haruto vuelven a atacar con sus mejores técnicas pero Micenas les responde con su técnica Rugido del Rey y los mandan a volar,Söma pudo ver como funciona su técnica a lo que Micenas le afirma y les habla acerca de como funciona exactamente, él crea de su propio cosmos una esfera lanzándolo al aire con una primera onda que paraliza el cuerpo de su oponente (escuchándose el rugido de un león) y luego una segunda onda de choque que los deja tendidos en el suelo, Micenas después de haberles contado como funciona su técnica, los vuelve provocar diciéndoles que nunca podrán deshacer su técnica, entonces estos lo vuelven a atacar pero Micenas les responde con su misma técnica, pero aun así se vuelven a levantar, Micenas admira su espíritu de batalla y se pone a analizar la situación y, toma la decisión de volver atacarlos con su misma técnica Rugido del Rey, pero aun así se siguen levantando, sin embargo los Caballeros de Bronce ya saben como neutralizar su técnica, elevan sus cosmos al máximo, pero Micenas rápidamente analiza la situación y también eleva su cosmos al máximo, apunto de lanzar su técnica y para sorpresa de este Sōma y Haruto logran neutralizar su técnica, es entonces que Micenas utiliza su otra técnica usando su cosmos-elemento Emblema del Rey, el poderoso ataque iba directamente a Sōma pero Haruto se atraviesa y usa una de sus técnicas para disminuir el ataque, Haruto queda herido y el casco de su armadura se ha roto; Sōma se enfurece por un momento y rápidamente lo ataca con su mejor técnica, a lo que Micenas no le da tiempo de defenderse y recibe la poderosa técnica de Sōma, obteniendo una grieta en la hombrera derecha su armadura. Por otro lado, Eden llorando lágrimas de sangre hace estallar su cosmos, enviándoles un mensaje a su padre Marte, a su madrastra Medea, a los Caballeros de Bronce y a Micenas de Leo, su participación en esta guerra santa como Caballero de Athena en contra de los planes de Marte, Micenas siente el cosmos despejado de dudas de Eden y lo acepta felizmente, entonces les abre el camino a Sōma y a Haruto, admitiendo su derrota en esta pelea. El fuego de Cáncer se ha consumido. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
