<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 47 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 47: ¡La única esperanza! ¡Un nuevo campo de batalla!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_46.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_48.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/D90B48FC5647FBD0" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    El apocalipsis en la Tierra ha comenzado, Yuna, Ryūhō, Sōma y Haruto se vuelven a levantar para pelear contra Amor pero los vuelve a atacar para luego dejarlos inconscientes, Kōga y Eden han caído en el Templo del Patriarca y buscan la manera de detener el avance del cosmos de la Tierra a Marte, Medea quien llega para ser tele-transportada a Marte pero antes le extrae todo el cosmos de oscuridad a Ludwig y, Amor que había llegado justo en ese momento le jura lealtad a su hermana Medea y esta le entrega el cosmos de oscuridad de Ludwig a él, luego Medea es tele-transportada a Marte y Amor se queda peleando contra Kōga y Eden, Amor los ataca con su cosmos de oscuridad al máximo y empleando la técnica Vals Sangriento y sorprendentemente Eden contra-ataca con puros golpes para luego atacarlo con su técnica Relámpago Fulminante deshaciendo la técnica de Amor, inmediatamente Amor eleva su cosmos de oscuridad para atacar a la velocidad de la luz con su Cañón Sangriento pero es Kōga quien responde a este ataque con su Meteoro de Pegaso, Amor hace estallar su cosmos de oscuridad esparciendo una nube de oscuridad alrededor y llevándose consigo a Kōga y a Eden. Yuna, Ryūhō, Sōma y Haruto a duras cuestas han llegado al Templo del Patriarca pero no pudieron hacer nada para evitar que se llevaran a Kōga y a Eden; el cuerpo de Athena está por ser consumido por el Gran Árbol, los Caballeros, Kōga y Eden han sido llevados por Amor a Marte, donde Athena y el Báculo se encuentran en ese planeta, el poderoso cosmos de oscuridad que Amor retiene en su cuerpo le está causando un enorme dolor a este, pero a pesar de ese dolor Amor se dispone a pelear incrementando su cosmos de oscuridad y empezando la gran batalla, Amor los ataca con su Réquiem de Oscuridad para luego seguir con su técnica Concierto de Gravedad, Kōga y Eden han quedado atrapados en esta técnica, y Amor puede controlar la gravedad de sus cuerpos, estrellándolos contra el suelo, Kōga se dispone a atacar pero Amor impide su golpe, Eden hace lo mismo pero Amor aumenta la gravedad en el cuerpo de Eden impidiendo que se mueva, para luego hacer lo mismo pero con el doble de gravedad en Kōga y atrayendo su cuerpo hacia Amor y este se dispone a darle el golpe final pero Kōga se logra zafar de la técnica elevando su cosmos al máximo para luego atacarlo con su técnica Meteoro de Pegaso pero Amor de Piscis rápidamente esquiva la técnica y atrapa a Kōga de Pegaso con la guardia baja y le entrega a este el enorme cosmos de oscuridad que su hermana Medea le había dado. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
