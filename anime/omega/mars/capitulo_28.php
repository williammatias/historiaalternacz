<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 28 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 28: ¡El ejército más poderoso! ¡Los Santos Dorados se reúnen!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_27.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_29.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/D595F1266856E543" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                    <p>
                                        Básicamente este episodio es un resumen de lo sucedido entre los capítulos del 1 al 27, y cerca del
                                        final del episodio, aparece Ionia de Capricornio, diciéndole a Marte que los Doce Caballeros Dorados
                                        ya se han reunido en la Torre de Babel. Posterior a ello, el Patriarca Marte, que ahora posee el 
                                        Báculo de Athena, cubre la Tierra con un halo de oscuridad. Los jóvenes caballeros dispondrán de 
                                        12 horas para salvar al mundo y cumplir la última voluntad de Aria, detener las ambiciones de Marte. 
                                    </p>
                                </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
