<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 48 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 48: Reúnanse, amigos! ¡El cosmos desbordante de Kōga!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_47.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_49.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/CA5764CA9662CED4" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    La muerte y la putrefacción ha comenzado poco a poco a consumir la Tierra, todos los Caballeros Dorados supervivientes se reúnen en la casa de Libra para ayudar a Yuna, Ryūhō, Sōma y Haruto a tele-transportarlos a Marte; Kōga que había recibido el cosmos de oscuridad que Amor le entregó, está sucediendo un efecto contrario en él, ahora Kōga esta rebosante de un enorme cosmos de luz. Amor ha quedado totalmente sorprendido ante tal situación pero rápidamente eleva su cosmos de oscuridad y ataca a Kōga con su Vals Sangriento pero sorprendentemente Kōga desvía la técnica con una sola mano y lo ataca con su Meteoro de Pegaso y es Amor quien sale herido, éste lo ataca con sus dos técnicas Réquiem de Oscuridad y Concierto de Gravedad pero Kōga fácilmente logra neutralizar dicha técnica. Amor no puede creer tal situación, y se comunica telepatícamente con su hermana Medea pidiéndole explicaciones acerca la situación pero Medea no le responde a su hermano menor Amor; este estalla en ira al descubrir la traición de su hermana mayor y decide destruir todo lo que es importante para ella. Aparece Eden que al fin logra neutralizar la técnica, y Amor con una enorme fuerza brutal lo ataca y luego ataca a Kōga. Yuna, Ryūhō, Sōma y Haruto quienes han llegado a Marte gracias a los Caballeros Dorados Kiki de Aries, Harbinger de Tauro, Genbu de Libra y Fudo de Virgo; juntos empiezan a buscar a Kōga y a Eden escuchando un enorme estruendo cerca, por otro lado Amor y Kōga aún siguen peleando cuando en medio de la pelea Kōga siente la presencia de Athena y se dirige a ella, este se encuentra con Athena pero Medea juega con la mente de Kōga y le hace creer que Athena ha sido impactada por una lanza y matándola al instante, Kōga quien se a creído tal mentira, estalla en furia y todo el cosmos de oscuridad lo rodea, Kōga trata de controlar su cosmos pero no lo logra perdiendo el control de su mente y cuerpo, Apsu, el Dios sumerio de la Oscuridad ha poseído el cuerpo de Kōga, este ataca a Amor con un golpe a la velocidad de la luz, matándolo al instante, el resto de los Caballeros de Bronce quienes han llegado para ver la muerte de Amor, ahora Apsu se dirige hacia donde Athena pero los Caballeros de Bronce se lo impiden y lo atacan con sus mejores técnicas para que Kōga despierte pero es inútil, cayendo ante el enorme poder de Apsu, este está llegando donde Athena, pero el Caballero de Bronce, Eden de Orión, se interpone en su camino protegiendo a Athena. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
