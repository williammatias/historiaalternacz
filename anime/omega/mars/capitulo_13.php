<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 13 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 13: ¡El mensaje de Seiya! ¡A ustedes, les encomiendo a Atenea!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_12.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_14.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/CB1B54E410076EC9" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Eden llega a las Ruinas del Santuario y observa desde una torre como la Torre de Babel va absorbiendo 
                                    poco a poco los cosmos, y en ese instante aparece Marte ordenándole a Eden que empiece a entrenar 
                                    con Mycenes de Leo para convertirse en uno de los Caballeros Dorados y Eden admitiendo que desde
                                    temprana edad ha estado entrenando con Caballeros de Bronce y Caballeros de Plata para hacerse
                                    más fuerte y también revela que Eden es hijo de Marte y hermano de Sonia de Avispón en otra parte
                                    se puede ver a Saori amarrada en un árbol negro siendo poco a poco drenada de su cosmos. Kōga, 
                                    Sōma, Yuna y Aria llegan a las Ruinas del Viento encontrándose con un enorme tornado de arena
                                    que está enviando el cosmos de viento a la torre de babel y es gracias a Yuna que ella y Kōga
                                    entran al interior del tornado para destruir el Generador de Viento pero les espera un Caballero 
                                    de Plata traidor a Athena Fly de Mosca atacando a Yuna de Águila y a Kōga de Pegaso con Virus de
                                    Arena por un momento siendo vencidos, pero es gracias a Kōga que eleva su cosmos hasta al maximo 
                                    y en una feroz batalla es Kōga de Pegaso quien vence a Fly de Mosca y es Yuna de Aguila que junto
                                    con Aria destruyen el Nucleo de Viento y brindándole un nuevo poder a Yuna. Solos y cansados los 
                                    jóvenes Caballeros ven hacia el cielo la Constelación de Pegaso recibiendo una nueva luz de 
                                    esperanza, un mensaje de Seiya de Sagitario: "Jóvenes Caballeros; ha llegado la hora de
                                    encomendarles a Athena, crean en ustedes mismos y en su Constelación Guardiana, salven a 
                                    Athena con sus propias fuerzas ¡y derroten a Marte! ¡Vayan por el camino de la luz!"; el
                                    mensaje de Seiya se desvanece y los jóvenes siguen su camino. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
