<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 15 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 15: ¡Los Colmillos Venenosos se aproximan! ¡Las segundas ruinas rodeado de intriga!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_14.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_16.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
<iframe src="http://www.putlocker.com/embed/4EF1BD73EE588C51" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Los jóvenes caballeros (Kōga, Yuna, Sōma) y Aria se dirigen hacia el reactor de la tierra y en el camino se encuentran con
                                    Haruto quien les dice que hay una aldea cercana para que descansen y que "un sujeto muy divertido está detrás de ustedes" 
                                    resultando ser Ichi de Hidra, un camarada que luego los traicionaría volviéndose un sirviente mas de Marte, a cambio 
                                    de que su armadura pasara de bronce a plata. En la aldea conocen a un pequeño niño que los guiaría hasta unas Ruinas
                                    donde se encontraba el reactor, posteriormente son atacados por el Caballero de Plata Michelangelo de Buril, quien 
                                    los ataca con sus golems. Durante la batalla Ichi, ahora santo de plata de Hidra, ataca a Kōga, Yuna y Sōma, luego
                                    Kōga cuestiona el porque le sirve a Marte, e Ichi se justifica ante Kōga, diciéndole que "quería brillar tanto como 
                                    alguna vez lo hicieron Seiya, Shiryū y Shun". Mientras tanto Haruto descubre que el Reactor de Tierra está debajo del
                                    templo, es entonces que junto con Aria y gracias a sus habilidades como ninja llegan hasta donde esta el Caballero de Plata
                                    de Buril y ordena la retirada, negándose a pelear; finalmente Haruto, con la ayuda de Aria, destruyen el reactor 
                                    y Haruto logra obtener el Cristal de Tierra que controlaba dicho reactor. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
