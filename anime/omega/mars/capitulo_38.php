<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 38 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 38: ¡La heroica traición! ¡El espíritu determinado de batalla de Eden!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_37.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_39.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/ADA1B25F3D47E395" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Fudō cree que Eden ha llegado para ayudarlo en su batalla pero este lo niega todo y le dice que quiere pelear contra el Caballero Dorado más poderoso de todos, entonces Eden viste su armadura de Orión, planeando atacarlo elevando su cosmos al máximo y empezando la gran batalla, Fudō de Virgo le lanza su técnica Descención del Iluminado aplicándolo con su segunda técnica Kān y Eden de Orión recibe el ataque directamente y al mismo tiempo increíblemente neutraliza la técnica de Fudō para luego atacarlo pero el Caballero Dorado lo neutraliza rápidamente con su técnica Condena Mundana pero Eden se logra zafar muy fácilmente y lo ataca con su técnica Tornado Violento y sorprendentemente aunque por un instante logro mover a Fudō de su puesto; Fudō le pregunta ¿porque haces esto? a lo que Eden responde "lo hago porque me siento culpable, culpable de no haber protegido a Aria", Kōga le recrimina sus intenciones y le dice que pudo haberla defendido pero Eden le responde a Kōga que no tiene intenciones de pelear al lado de ellos y hace algo arbitrario, los expulsa de la casa de Virgo quedándose Eden solo con Fudō, reanudando otra vez la batalla, Fudō lo lleva al mundo de los Myō-ō y le lanza su técnica Supremacía del Mundo Superior una poderosa técnica que estrella a Eden contra el suelo creando un enorme cráter, Eden se vuelve a levantar y le muestra el zarcillo de Aria a Fudō, es entonces que para sorpresa de Fudō de Virgo, el Caballero de Bronce despierta el Séptimo Sentido y Fudō eleva su cosmos al maximo y lo vuelve a atacar con su Supremacía del Mundo Superior pero a Eden no le pasa nada y lo ataca con su Orión Devastador quedando Fudō fuera de combate. Por otro lado, los demás Caballeros de Bronce siguen su camino hacia la siguiente casa pero en medio del camino un Caballero Dorado que ha salido de su templo les impide seguir adelante, se trata de Tokisada de Acuario, anteriormente era Caballero de Plata de Reloj pero fue ascendido por Medea a Caballero de Acuario y, ataca violentamente a los Caballeros de Bronce y expulsando a Kōga de las escaleras. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
