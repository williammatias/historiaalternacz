<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 33 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 33: ¡La esencia del Cosmos! ¡El Séptimo Sentido!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_32.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_34.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe width="720" height="405" src="http://rutube.ru/video/embed/5500062" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>
                                </div>
                                <p>
                                    Kōga herido llega a la casa de Géminis, Ryūhō que estaba inconsciente despierta en los brazos de Paradox y se dispone a pelear con ella pero siente fuertemente los efectos de la anterior pelea contra Harbinger es entonces que Paradox aprovecha ese momento para torturar a Ryūhō con su Encrucijada Espejismo, mostrándole a Ryūhō el mismo futuro si decide rendirse a lo que este se niega a aceptar pero son interrumpidos por Kōga que llega con cierta dificultad; Kōga ataca a Paradox con su Resplandor de Pegaso pero esta la repele con una sola mano, Ryūhō le dice a Kōga que Paradox puede adivinar el próximo movimiento de su enemigo (Clarividencia), Kōga se dispone atacar con su Meteoro de Pegaso pero Paradox usa su técnica Muro de la fortuna, Kōga eleva su cosmos pero Paradox no puede hacer nada debido a que esta fatigada con la última pelea que tuvo contra el Caballero de Dragón perdiendo la contienda pero instantáneamente aparece su otra personalidad, Paradox del odio, y sin que Kōga se diera cuenta le acierta salvajemente un golpe en el estomago, Kōga es incapaz de defenderse de todos los ataques de Paradox, Kōga cae inconsciente y Paradox del Odio se prepara para darle el golpe final pero Ryūhō la detiene pero Paradox del Odio lo golpea fuertemente dejándolo semi-inconsciente pero le perdona la vida ya que es alguien preciado para su otra personalidad. Ryūhō empieza a recordar las palabras que le dijo Shun: Para obtener el séptimo sentido tienes que perder tus 5 sentidos; Paradox lo vuelve a atacar con su Encrucijada Espejismo mostrándole a Ryūhō las decisiones que ella tuvo que tomar si quería convertirse en Caballero como ponerse la mascara, reprimir su amor hacia Shiryū, y callar al saber de la infiltración de Marcianos en el Santuario, Ryūhō la recrimina por haber elegido siempre el camino fácil y le pregunta si: ¿era esto lo que realmente querías?, Paradox enfadada empieza a odiar a Ryūhō es entonces que aparece Paradox del Odio y lo ataca con su técnica Destino Final despojándole de sus 5 sentidos Ryūhō se ve envuelto en una oscuridad infinita y gracias al cosmos de Kōga, Ryūhō despierta su séptimo sentido y le da una buena pelea al Caballero de Géminis, Paradox sorprendida no puede creer que un simple Caballero de Bronce haya obtenido el séptimo sentido y teniendo una confrontación de personalidades, Ryūhō eleva su cosmos y la ataca con su mejor técnica Los Cien Dragones y es Paradox quien cae inconsciente. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
