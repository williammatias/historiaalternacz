<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 30 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 30: ¡El maravilloso poder! ¡El santo del templo del Toro Dorado!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_29.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_30.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/46B25F844C50289C" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Los Caballeros de Bronce (Yuna, Haruto, Sōma y Ryūhō) empiezan la batalla contra el Caballero Dorado Harbinger de Tauro pero estos no pueden hacer nada contra el enorme poder de Harbinger y sin embargo él les señala a cada uno de ellos que parte de los huesos les van a romper empezando por Sōma que le dice que le va a romper el trapecio y el segundo metacarpiano, luego sigue con Haruto que también le dice que le va a romper la tercera y sexta costilla, con Yuna le va a romper la Clavícula y con Ryūhō el Hueso esfenoides y la Mandíbula, luego de haberlos señalado empieza a atacarlos y sin aplicar su fuerza solo con su cosmos-elemento les rompe los huesos a cada uno de ellos pero los jóvenes caballeros aun se siguen levantando y juntos planean como derrotar al caballero de tauro sin embargo Harbinger los ataca con su poderosa técnica Cuerno Mayor y los jóvenes caen con esta técnica pero aun así se levantan y lo vuelven a atacar pero el caballero dorado aplica la misma técnica en ellos ya que quiere escuchar la melodía de como sus espíritus se quiebra, pero para sorpresa de Harbinger es Kōga quien se levanta y eleva su cosmos alumbrando la casa de Tauro y dándole pelea a Harbinger, y en un intento por defender a sus amigos Kōga con su Meteoro de Pegaso ataca a Harbinger pero este usa su Cuerno Mayor con lo que la técnica de Kōga apenas y lo resiste a continuación Harbinger eleva su cosmos al máximo alumbrando el nuevo santuario y abriendo una brecha en el espacio-tiempo envía a los jóvenes caballeros hacia otras casas quedándose solamente Kōga en el templo de Tauro mientras que el fuego de Aries se ha consumido.   
                                </p>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
