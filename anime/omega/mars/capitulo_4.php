<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 4 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 4: ¡Hijo de un héroe! ¡Ryūhō vs Kōga!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_3.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_5.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/BC0A3B7EC9A5653A" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga conoce al Caballero de Bronce Ryūhō de Dragón, hijo del 
                                    Segundo Caballero Legendario Shiryu de Dragón. Ryūhō se muestra
                                    como una persona bondadosa ante todos en el Palestra, tiempo
                                    después manifiesta querer enfrentarse a Kōga en batalla, obteniendo 
                                    la victoria, no sin antes observar el gran potencial de Kōga como
                                    Caballero de Pegaso, al finalizar la batalla Ryūhō decide extenderle su amistad a Kōga.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
