<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 26 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 26: ¡Recuerdos y venganza! ¡La trampa de las Ruinas de la Oscuridad!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_25.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_27.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/96363BD974409BAF" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Sōma de León Menor y Sonia de Avispón también caén cerca de las Ruinas del Cristal de la Oscuridad llegando
                                    directamente al salón del Amor y Odio viajando entre sus recuerdos del pasado; Sonia cae a lo que parece
                                    ser la recámara de un castillo, viéndose a si misma cuando era niña hablando con su madrastra, Medea, y
                                    recibiendo la orden de matar a Kazuma de la Cruz del Sur por ser uno de los líderes de los Caballeros que 
                                    se oponen ante Marte, pero Sonia titubea ante tal decisión; Sōma cae a un lugar donde paso los últimos
                                    momentos con su padre, viendo a Kazuma hablando con dos Caballeros de Plata acerca de los sucesos 
                                    recientes, la desaparición de Caballeros de Plata recién graduados en Palestra, y de como otros caballeros
                                    se han vuelto traidores; y viéndose a si mismo cuando era niño ayudando a su padre, pero Kazuma siente el 
                                    cosmos de alguien cerca y manda a su hijo a casa, Kazuma camina hasta donde se encuentra el cosmos, 
                                    apareciendo Sonia, empezando y acabando rápidamente la pelea a favor del Caballero de Plata, Kazuma, 
                                    pero este se niega a terminar la pelea dado que ella es una niña, comparándola que esta puede ser
                                    la hermana mayor de Sōma, Sonia humillada ataca por la retaguardia a Kazuma, este se da cuenta
                                    demasiado tarde recibiendo una apuñalada en el corazón y muriendo al instante, Sōma de León Menor 
                                    ve a Sonia llorar por haber cometido tal atrocidad, Sonia regresa al castillo a informarle a su 
                                    madrastra acerca de lo sucedido después regresa a su recámara a seguir llorando mientras Sōma de León Menor 
                                    la sigue viendo, es en ese momento que aparece la imagen de Marte para decirle a Sonia de Avispón que la
                                    desprecia como hija por ser débil, entonces Sonia se deja caer al fondo del Hades pero es salvada por 
                                    Sōma quien ha estado viendo todo (la mascara de Sonia se empieza a romper dejando ver su ojo derecho) 
                                    y ataca fuertemente a la ilusión de Marte, saliendo así del cosmos de oscuridad, Sonia se retira, 
                                    entrando Sōma al salón encontrándose con Haruto, Yuna y Ryūhō.  
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
