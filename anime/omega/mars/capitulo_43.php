<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 43 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 43: ¡La resurrección de los cuatro dioses de la guerra! !Intervención, en el último templo!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="../../omega.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_2.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/8EA5C074432D37E1" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Los Caballeros de Bronce finalmente llegan a la décima segunda y última de las casas, la casa de Piscis, adentro les espera el Caballero Dorado y hermano de Medea Amor de Piscis, el Caballero Dorado empieza el ataque con su técnica Agua Silenciosa separando a Kōga y a Yuna del resto del grupo y enseguida ataca a Kōga con su técnica Sentencia Diferida lanzándole unos barrotes de oscuridad y paralizandole los movimientos a este, Yuna por su parte ataca a Amor pero este simplemente la esquiva y le propone burlonamente a esta que sea su mujer en el nuevo mundo pero ella lo rechaza indiscutiblemente sin embargo Amor ataca a Ryūhō, Sōma y Haruto con su técnica Cristalización del Renacimiento reviviendo a los 4 Berserkers más poderosos que los propios Caballeros Dorados llamados los cuatro Dioses de la Guerra, sin embargo los Berserkers están restringidos de sus poderes debido a la última pelea que tuvieron con Athena cuando esta vestía su Armadura Divina, pero aun así son igual de poderosos. El fuego de Capricornio se ha extinguido, Amor le dice a Kōga que para salvarlos y salvarse este tiene que entregarse a su cosmos de oscuridad ya que los barrotes solo se destruyen con ese cosmos, Yuna vuelve a atacar a Amor con una serie de patadas pero este solo las esquiva; por otro lado, Ryūhō, Sōma y Haruto rápidamente han sido acorralados por los 4 Berserkers y a duras cuestas están luchando para defenderse de los ataques; Yuna trata de atacar a Amor pero este solamente bloquea los ataques con una sola mano e inclusive con un solo dedo detiene sus ataques, y la obliga a sentarse una y otra vez al lado de él a observar la pelea dejándola semi-inconsciente al recibir un ataque de Amor; Ryūhō, Sōma y Haruto están siendo pulverizados por los Berserkers y sus Armaduras de Bronce se están destruyendo con cada ataque que reciben, Yuna recupera la conciencia y se dispone a volver a atacar a Amor pero este pierde la paciencia y ataca a Yuna dejándola herida y destruyéndole gran parte de su Armadura, y le dice a Kōga que sino se entrega a su Oscuridad va a rematar a Yuna, Kōga se queda callado y no sabe que hacer, así que Amor procede a rematar a Yuna y Kōga se ve obligado a aceptar y entregarse a su cosmos de oscuridad, destruyendo los barrotes que lo apresaban y salvando de una muerte segura a Yuna. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
