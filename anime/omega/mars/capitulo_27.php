<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 27 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 27: ¡El final del camino! ¡La niña de la luz y los jóvenes!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_26.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_28.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/C363916AE8D25EB8" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Aria, Eden de Orión y Kōga de Pegaso también caen cerca de las Ruinas del Cristal de Oscuridad entrando
                                    al salón de Alma y Corazón viajando en lo más profundo de sus corazones descubren la verdad que tanto 
                                    se estaban negando; Eden de Orión cae en un lugar donde hay puros espejos de cristal, entre esos
                                    espejos aparece Aria (cuando era niña) y empieza a decirle la verdad a Eden que tanto ocultaba; 
                                    por otro lado Aria cae al mismo lugar que Eden y empieza a discutir con los reflejos de su corazón 
                                    y Kōga también cae al mismo lugar y entre los reflejos aparece Saori diciéndole a Kōga que su
                                    verdadero cosmos es Oscuridad por lo que este se horroriza; Aria discutiendo con los reflejos
                                    del espejo se deja llevar por el cosmos de oscuridad; Eden recuerda los momentos en que le 
                                    regalo un pendiente a Aria, pero también ve los momentos en que ella fue torturada por Marte, 
                                    es entonces que Eden se da cuenta del enorme error que había cometido con lo que sentía
                                    Aria y también se deja absorber por la oscuridad, Aria siente el cosmos de Eden extinguiéndose,
                                    entonces Aria despierta su cosmos de luz e ilumina la oscuridad encontrándose así con Eden,
                                    este le pide perdón a Aria por todo el dolor que le causó, ella acepta sus disculpas pero son
                                    interrumpidos por el grito desesperado de Kōga; Kōga ve la imagen de Marte torturando a Aria 
                                    por lo que Kōga empieza a despertar su verdadero cosmos, la oscuridad, pero Aria lo detiene y 
                                    elevando su cosmos salen al fin del salón llegando directamente al Reactor, Aria le responde a
                                    Kōga que "a pesar de que su cosmos es oscuridad, gracias a Saori pudo despertar la luz en su
                                    oscuridad", juntos destruyen el reactor y Kōga obtiene el Cristal de Oscuridad y con la luz
                                    de Aria el cristal paso a ser un Cristal de Luz. El verdadero Marte aparece decapitando la
                                    enorme estatua de la medusa y ataca a Aria absorbiendo su cosmos de luz por lo que Kōga lo 
                                    impide queriendo darle pelea a Marte a lo que este muestra su verdadera forma, los
                                    Caballeros de Bronce atacan con sus mejores técnicas al Dios de la Guerra pero este los 
                                    detiene muy fácilmente y los ataca violentamente, pero estos se vuelven a levantar, es entonces 
                                    que los Cristales de Agua, Tierra, Fuego, Viento, Relámpago y Luz bailan alrededor de Aria
                                    (ya que esta despertó su verdadero cosmos, el noveno cosmos, el cosmos de Athena), y de la 
                                    fusión de esos cristales aparece el Báculo de Aria, oponiéndose a Marte los caballeros de bronce
                                    lo atacan una y otra vez, haciendo brillar sus Armaduras de Bronce a Armaduras de Plata; en un 
                                    ataque desesperado y para sorpresa de todos, Marte ataca con un relámpago de oscuridad al
                                    corazón de Aria dejándola gravemente herida, Marte obtiene el Báculo de Aria y termina por
                                    absorberle todo el cosmos a Aria, después se retira junto con sus dos hijos. Aria se despide 
                                    de todos, feliz de haberlos conocido; finalmente Aria con el poco cosmos que le quedaba los
                                    envía fuera de las ruinas, cerca de la Torre de Babel. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
