<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 45 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 45: ¡El Indómito Dios de la Guerra! !Marte y Ludwig!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_44.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_46.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/A253DB710A4CFA9D" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    En la Tierra, a plena luz del día (en ciertos países) ya hay un Eclipse Rojo casi completo, tapando la luz del Sol; Kōga es el primero en lograr llegar hasta el Templo del Patriarca, Marte y Kōga vuelven a verse las caras después de tanto tiempo, Kōga exige que le devuelva a Saori y lo ataca con su técnica Meteoro de Pegaso, Marte detiene la técnica con una sola mano y lo ataca con su técnica Resplandor Escarlata de Gungnir dejando a Kōga en el suelo sin poder levantarse y a punto de darle la estocada final Eden que también ha llegado al Templo del Patriarca ataca a su padre oponiéndose a las ambiciones de este, Marte le dice a su hijo que desde que él nació se dedicó en cuerpo y alma, a que se cumpla su voluntad y no le importará arriesgar la vida si es necesario con tal de conseguirlo, Eden se niega a aceptar algo así ya que Aria nunca habría querido que la Tierra fuese destruida junto con sus habitantes, entonces Marte le cuenta su historia, la historia en la que su Receptor llamado Ludwig ha vivido; Ludwig era un hombre noble y amable que vivía tranquilamente junto con su hija Sonia y su primera esposa Misha, un día la pareja quedaron en salir y que se encontrarían en el teatro, a Ludwig se le hizo un poco tarde en llegar entonces Misha triste decide entrar sola, Ludwig ha llegado al teatro y a punto de entrar ocurre un atentado en la que Misha se vio involucrada, una fuerte explosión en el interior del teatro, dejando a muchas personas muertas entre ellas Misha, Ludwig enterró a su esposa en el cementerio; ya en su casa Ludwig tiene muchos sentimientos de odio y oscuridad encontrados (tristeza, incertidumbre, desesperación, venganza, etc.) entonces decide buscar a todos los que estuvieron involucrados el atentado y, masacrarlos uno a uno sin piedad, y así lo ha conseguido, pero este quería más y más, él decidió que el mundo necesitaba ser purgado, y fue ahí donde el Dios Marte posee el cuerpo de Ludwig dándole poderes para que este actuara a su voluntad. Marte invoca a los 4 Berserkers y juntos comienzan la carnicería destruyendo gran parte de la ciudad y asesinando a todas las personas cercanas, fue ahí donde los Caballeros Seiya de Sagitario, Shiryū de Dragón, Hyōga de Cisne, Shun de Andrómeda e Ikki de Fénix (vistiendo sus armaduras originales) junto con Athena que viste su Armadura Divina aparecen, y empieza la gran batalla Shiryū, Hyōga, Shun e Ikki pelean fuertemente contra los Berserkers mientras que Seiya pelea contra Marte pero fue que gracias a Athena que sella a los Berserkers y Marte que desvió la mirada es Seiya quien le da un fuerte golpe en la cara, Medea (que para ese entonces ya era su segunda esposa y la madre de su hijo Eden) invoca los poderes de una nebulosa a continuación el Meteorito negro que cae en la Tierra e iba directamente contra la Diosa era el cosmos del Dios de la Oscuridad, Athena usando su escudo evita que el Meteorito se le acerque, arrastrándola por media ciudad, fue en ese entonces que Athena en medio de su lucha se encuentra con dos bebes (Kōga y Aria) recientemente huérfanos de padre y madre (ya que estos fueron asesinados por los Berserkers y murieron protegiendo a sus hijos), Athena decidida a no ceder un paso mas eleva su cosmos hasta el Noveno Sentido para proteger a los bebes, el cosmos de oscuridad del meteorito y el cosmos de Athena hacen que todas las armaduras evolucionen, pero el meteorito implosiona en frente de Athena y pequeños fragmentos de meteoritos le caen a Kōga y a Marte, la radiactividad hace que la apariencia de este cambie radicalmente y posea la Armadura Galaxia y, a continuación Marte se roba a la bebe que heredo el cosmos de Athena (Aria) y Athena se queda con el bebe que posee el cosmos de oscuridad (Kōga), Marte también habla acerca de su segunda invasión en pleno Santuario y de como les dejo la Herida de Oscuridad a todos los que estuvieron ahí, también habló acerca de su tercera invasión en la Isla Huérfano y capturó a Athena para luego colocarla como pilar humano en Marte. Kōga y Eden después de haberlo escuchado, Eden se niega a aceptar la petición de su padre y lo vuelve a atacar pero a Marte se le rompe el casco dejando ver el verdadero rostro de Ludwig, las consecuencias de la radiación del Meteorito. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
