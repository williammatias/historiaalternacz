<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 21 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 21: ¡El pegaso sin alas! ¡El viaje después de la derrota!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_20.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_22.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/F8A9798240D99ED0" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Luego de que Aria acepta irse con Eden, Kōga ha probado el amargo sabor de la derrota, humillado y con 
                                    su armadura llena de roturas, Yuna despierta a Kōga, y este se lamenta tras no haberla protegido.
                                    Al ver esto, Yuna decide llevarlo al Valle de los Lamentos, lugar donde ella consiguió su armadura. 
                                    En el Valle de los Lamentos, Kōga se aleja de Yuna y experimenta alucinaciones en las que cree ver 
                                    a Marte. De repente Kōga pisa mal y cae al fondo del precipicio, poco después despierta en frente
                                    del 4° Caballero Legendario Hyōga de Cisne, quien le hace saber a Kōga que fue un incapaz de proteger 
                                    a quienes quería por sentirse débil. Hyōga enfatiza que su persona más querida (Natassia, su madre) 
                                    no la volverá a ver, pero que Kōga si volvería a ver a Aria, siempre y cuando este último quisiera.
                                    Mientras tanto, en las Ruinas del Santuario y ante la atónita mirada de Sonia, Eden le reclama a
                                    Marte por la manera en que está haciendo las cosas, y además de ello pide un poco mas de libertad 
                                    para Aria, con lo que Marte le responde que la gentileza es sinónimo de debilidad. Posteriormente
                                    Eden sostiene un encuentro breve con Aria, en donde se descubre que ambos serán los gobernantes del 
                                    nuevo mundo, hecho que Aria no comparte, pues ella recuerda la cálida compañía de los jóvenes santos 
                                    y lo divertido que fue. Eden insiste en que la comprenda, para luego colocarle un pendiente que le había
                                    dado desde que eran niños. Por otro lado, Hyōga le dice a Kōga que es muy débil y que así no podrá proteger
                                    a los que quiere, por lo que este se molesta y lo ataca con su Meteoro de Pegaso, Hyōga detiene con solo tres 
                                    dedos cada uno de los cientos de puños y le dice que si quiere recuperar a Aria debe encender su cosmos
                                    fuertemente, luego Hyōga eleva su cosmos mostrándose por un breve momento la nueva Armadura de Cisne y
                                    apareciendo la Herida de Oscuridad en su cuerpo, para luego atacarlo con su técnica Polvo de Diamante. 
                                    El recuerdo de Aria y la conexión de luz entre ellos, hace que Kōga encienda su verdadero cosmos y logre 
                                    volar como si fuera un pegaso, saliendo así del Valle de los Lamentos, al mismo tiempo, Aria se siente 
                                    tranquila y con más fé (por la conexión de luz con Kōga, quien hizo su catarsis) y le pide a Eden un deseo:
                                    que ambos se dirijan hacia el Reactor del Relámpago, mientras alguien los escucha. Finalmente, Kōga regresa 
                                    hasta donde está Yuna y sin vacilar, le dice que partan juntos para rescatar a Aria y vencer a Eden. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
