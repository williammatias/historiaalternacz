<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 58 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 58: ¡Llegan los Cuatro Grandes Reyes! ¡Atenea vs. Pallas, enfrentamiento completo!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_57.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_59.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/81E2F40006725C91" width="600" height="360" frameborder="0" scrolling="no"></iframe>   
                                </div>
                                <p>
                                    Los palasianos han empezado a atacar los países de alrededor del mundo Hawái, Brasil y Argentina son los que han sufrido más los estragos de Retraso del Tiempo pero principalmente en dicho país, en una base militar situado en el Desierto del Monte dos militares consternados que pudieron sobrevivir gracias a que sus cosmos estaban por encima del nivel humano, estos militares pedían ayuda por la radio cuando son sorprendidos por soldados palasianos, pero inesperadamente Harbinger de Tauro llega para salvarlos atacando a los palasianos con su técnica Gran Cuerno siendo derrotados rápidamente y además a uno de ellos el Caballero Dorado le pisa el brazo rompiéndole los huesos, pero Genbu de Libra aparece para decirle a los militares que hay un refugio muy cerca de ellos y regaña a Harbinger por haberse ido del Santuario para luego llevárselo de vuelta a Grecia, los soldados palasianos al ver a dos caballeros juntos deciden huir, en el Santuario Athena acompañada de Kiki de Aries, Fudō de Virgo, Seiya de Sagitario, Genbu de Libra y Harbinger de Tauro se lamenta por poner siempre en peligro a las personas, Harbinger regaña a su Diosa diciéndole que es muy tarde como para estarse lamentando ahora Athena con una sonrisa en la cara le da la razón a Harbinger, y le ordena a sus Caballeros Dorados que reúna a todos los soldados y caballeros en el coliseo, más tarde en ese mismo lugar, ya estaban reunidos (además de los Jóvenes Caballeros a excepción de Eden) Caballeros de Bronce como Spear de Dorado, Ichi de Hidra; Caballeros de Plata como Dorie de Cerbero, Mirfak de Perseo, Shaina de Ofiuco, Michelangelo de Buril; y los 5 Caballeros Dorados, al anochecer, Athena junto con su báculo, Niké, hace acto de presencia y todos los caballeros hacen reverencia pero Subaru al verla siente una enorme nostalgia, como si se conociesen desde tiempos mitológicos; Athena les habla a sus caballeros que La Tierra está siendo amenazada nuevamente, haciendo mención de la inundación de Poseidón, la invasión de Hades, ahora la Diosa del Amor, Palas, está amenazando con apoderarse de la Tierra y le pide a sus caballeros que la ayuden, pero Athena se debilita con el espiral en su brazo y esta por caer por las escaleras pero Seiya la atrapa, Genbu les explica a todos los caballeros acerca del espiral y les ordena a todos que vayan a los países donde el tiempo fue detenido y destruyan a los palasianos, abriendo el telón de una nueva guerra santa, mientras tanto en algún lugar, la pequeña Palas acompañada de Titán de la Espada Teogénesis, está jugando con una muñeca muy parecida a Athena que Europa de Chakram le regaló, pero Palas le exige a Titán ver a su hermana Athena y apareciendo ante ellos 3 palasianos de primer nivel llamados Hyperion de la Espada de la Destrucción, Galia de la Espada de la Guerra y Egeón de la Espada del Trueno, ellos junto con Titán le juran lealtad a la Diosa Palas; al día siguiente, los jóvenes caballeros parten por caminos separados, despidiéndose entre ellos para luego separarse, Kōga y Subaru, llegan a una cuidad en Grecia muy cerca del Santuario, donde el tiempo se ha detenido, Kōga siente una fuerte presencia palasiana a su alrededor y de un solo golpe destruye la pared de un edificio y dentro de ella aparece todo un batallón de soldados palasianos, ambos caballeros visten sus armaduras, mientras que batallones de palasianos van saliendo de los edificios aledaños formando todo un ejercito de soldados, pero es Kōga quien se encarga de la mayoría de ellos y aparece ante los caballeros Tarvos del Lucero del Alba, ambos se disponen a pelear pero Tarvos vio en los ojos de Kōga la sed de batalla, y lo compara con él mismo, Kōga se niega a creerle y lo ataca pero Tarvos esquiva cada ataque que le lanza y lo ataca con su arma pero Subaru lo defiende recibiendo él el impacto, pero del edificio que esta al lado de Kōga sale gateando un bebé y Tarvos al verlo lo ataca cobardemente con su arma pero Kōga protege al bebé y se lo entrega a Subaru, el Caballero de Pegaso esta enfurecido y hace arder su cosmos, Tarvos lo ataca con su técnica Golpe Meteoro de Hierro mientras que Kōga lo ataca con su técnica Meteoro de Pegaso destruyéndole a Tarvos su lucero del alba, su cronotech y dejándolo a él herido, Tarvos escapa devolviéndole a esa ciudad el transcurso del tiempo, mientras que Kōga y Subaru buscan otro país donde haya palasianos. 
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
