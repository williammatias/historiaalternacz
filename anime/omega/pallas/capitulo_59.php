<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 59 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 59: ¡El vínculo entre hermanos! ¡Shun de Andrómeda, se une a la batalla!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_58.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_60.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/75D0A140C543DE79" width="600" height="360" frameborder="0" scrolling="no"></iframe>                                </div>
                                <p>
                                    Un ataque en una aldea, deja a la mayoría de los aldeanos bajo los efectos de Retraso de Tiempo el cual lo está causando Halimede sin embargo dos hermanos supervivientes llamados Rei y Philip que se habían escondido al momento del ataque huyen de la aldea pero Halimede los ve y los empieza a perseguir, los hermanos quedan acorralados, de un lado esta un acantilado y del otro Halimede con su batallón de soldados palasianos, este a punto de atacarlos es sorprendido por el ataque de las cadenas de Shun de Andrómeda, el caballero protege a los muchachos y acorrala a Halimede, Shun le habla al palasiano y le dice que si deja la aldea y a sus habitantes tal cual como estaban le iba a perdonar la vida, Halimede acepta pero solo para usar su Retraso de Tiempo en Philip para luego salir huyendo, Sōma, Yuna y Ryūhō llegan solo para ver la aldea totalmente devastada y se encuentran con Halimede que huye del campo de batalla, Shun guía a Sōma, Yuna, Ryūhō y Rei hacía un refugio escondido entre las montañas en la cual solo unos pocos aldeanos han sido rescatados por Shun, pero Rei esta bastante decepcionado de que el caballero haya permitido que el palasiano haya congelado en el tiempo a su hermano mayor y se va del refugio, Shun entiende a la perfección los sentimientos de Rei, ya que él también tiene un hermano llamado Ikki de Fénix al cual no sabe nada de su existencia pero cada vez que se pone la mano en el corazón siente que aún su hermano está vivo; Ray se encuentra en el acantilado en donde su hermano se encuentra, Yuna lo encuentra y trata de hablar con Rei pero este esta muy resentido con los caballeros y huye, en medio del bosque Rei se consigue con Halimede y el palasiano le propone un trato, si le trae la cloth-stone de Andrómeda, Halimede le devuelve a su hermano, Rei acepta y más tarde, a la madrugada cuando todos estaban durmiendo, Rei se acerca al dormitorio de Shun, le arrebata la cloth-stone y corre al lugar pautado, Shun se había dado cuenta de las intenciones de Rei, lo sigue hasta el acantilado, para ver como Rei le entrega la cloth-stone de Andrómeda a Halimede y empieza la batalla donde Halimede tiene la ventaja rápidamente, Yuna que siente el cosmos de Shun, se despierta solo para ver que él y Rei no se encuentran y sale del refugio a buscarlos, Shun intenta conseguir su cloth-stone pero Halimede se lo impide estrellándolo contra el suelo, Shun se levanta y le pide a Rei que vaya donde su hermano, el Caballero de Andrómeda sin siquiera tener puesta su armadura hace arder su cosmos para atacar a Halimede pero este lo convence que si lo ataca Philip nunca volverá a la normalidad, Shun se retracta y cambio le pide a Halimede que si regresa a Philip a la normalidad, Shun le entregara la vida, Halimede no discute ante una oferta así, entonces el palasiano empieza a atacarlo con su arma, la Barra Triple, Yuna llega para ayudar a Shun pero él se lo impide, mientras que Halimede ataca sin piedad a Shun, Rei que está viendo todo se siente culpable de lo que hizo, y le pide ayuda a su hermano, el espíritu de su hermano le habla y le dice que sea valiente, que él siempre estará a su lado, Rei se arma de valor y ataca a Halimede para arrebatarle la cloth-stone de Andrómeda y entregársela a Shun, Halimede lo golpea mientras que Yuna y Shun se preparan para atacarlo, Halimede pierde la paciencia y usa su técnica Desastre Triple en contra de Shun y Ray, Yuna intenta repeler la técnica con su Tornado Divino pero no logra hacerle nada, saliendo ella perjudicada, Ray le entrega en sus manos a Shun la cloth-stone de Andrómeda y Shun la usa para proteger a Rei pero ambos salieron ilesos de la poderosa técnica y la cloth-stone se rompe dándole una nueva forma a su armadura, vistiendo a Shun y atacando a Halimede con su técnica Cadena Nebular destruyéndole a Halimede su barra triple, Halimede no le queda de otra que huir, mientras les advierte que el tiempo aún no ha vuelto a la normalidad, al día siguiente, los chicos se despiden de Shun ya que él se queda para proteger a los aldeanos y dar caza a Halimede. 
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
