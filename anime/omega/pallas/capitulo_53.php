<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 53 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 53: ¡Reunión! ¡Sōma, que la llama de tu espíritu arda!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_52.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_54.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/E713FEF4F8D5CABD" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Con la evolución de la armadura de Pegaso, ahora Kōga se ve obligado a cargar la Caja de Pandora, Tatsumi envía a Kōga en barco hacia el reconstruido Santuario pero un infiltrado Subaru intenta golpear a Kōga pero este lo esquiva muy fácilmente, una vez que llegaron a la entrada del Santuario, en el coliseo donde Seiya había obtenido la Armadura de Pegaso, son recibidos por Soldados Rasos del Santuario pero su líder Shaina de Ofiuco los detiene, permitiéndole la entrada a Kōga solamente, Subaru intenta entrar pero Shaina se lo impide, por otro lado, Kōga llega directamente al Templo de Atenea y empieza a hablar con ella acerca de los Palasianos, de la nueva guerra que se aproxima, de la joya en espiral que apareció en ambos brazos de las Diosas, que las une y que ese paso a Athena le quedaría un año de vida; Athena le ordena amablemente a Kōga que busque y le avise a todos los Caballeros acerca de esta nueva amenaza, Kōga quería quedarse y cuidar de Athena pero Seiya de Sagitario lo regaña y le dice que los Caballeros Dorados tienen la responsabilidad de resguardar la seguridad de Athena, los Caballeros de Plata tienen la misión de buscar el escondite de Palas y los pocos Caballeros de Bronce tienen la misión de buscar a todos los Caballeros que están desplegados alrededor del mundo y prepararse para la guerra, entonces Kōga decide irse, Subaru se encuentra peleando contra Shaina para que lo deje entrar pero esta no da su brazo a torcer, el Caballero de Acero se molesta y muestra un poco de su cosmos lo suficiente como para poner en guardia a Shaina pero cuando iban a pelear en serio, Kōga los interrumpe para luego irse y Subaru le sigue. En Palestra, Sōma se encuentra dando clases a los novatos para ser futuros Caballeros, sus estudiantes emocionados les hacen muchas preguntas a Sōma pero Ichi de Hidra aparece vistiendo su Armadura de Bronce y reta a Sōma a una pelea pero Geki lo detiene con un golpe en la cabeza y le dice que las peleas entre Caballeros están prohibidas, en la hora de descanso Sōma recibe en Palestra (después de un año sin verse), a Kōga y a Subaru, Kōga ve a extraños Caballeros entrenando, Geki le explica que esos son los Caballeros de Acero que llegaron recientemente por parte de la Fundación Graude, sus armaduras son muy diferentes a las que los Caballeros visten pero la voluntad de proteger es la misma, Sōma le pregunta a Kōga acerca de la gran caja que carga, este le responde que es su nueva armadura y Subaru se presenta ante todos como uno de los Caballeros de Acero; en ese momento Loge de Brionac, un Palasiano de Tercer Nivel ataca a los Soldados Rasos que protegían la entrada de la puerta y luego destruye la entrada, vino con la intención de matar a todos los estudiantes a Caballeros, Loge le corta la cabeza a la estatua de Athena y envía a sus siervos las Abejas Palasianas a atacar a los estudiantes, Kōga viste su nueva Armadura de Pegaso y destruye las abejas, Sōma también se une a la pelea, viste su armadura y ataca a las otras abejas, Subaru tampoco se queda atrás y también empieza a vestir su Armadura de Acero y todos juntos pelean contra las abejas, Loge que con la ayuda de su armadura llamada Cronotech detiene el tiempo, esta técnica solo funciona con aquellos individuos que poseen bajo cosmos, al resto de los caballeros que no le surtieron efecto son atacados por las abejas, Ichi de Hidra en un ataque sorpresa intenta atacar a Loge pero las abejas a pesar de su armadura lo paralizan, Sōma decide atacar a Loge y le pide a Kōga que se encargue de todas las abejas, Sōma ataca a Loge pero este lo esquiva y le devuelve el golpe para luego lanzarlo al aire y desde ahí atacarlo con su técnica Danza de Agujas, Sōma se lamenta el haber perdido el tiempo en la enseñanza y no preocuparse en su entrenamiento, por otro lado Kōga y Subaru están rodeados de Abejas Palasianas y este quiere emprender una huida estratégica, Kōga se niega a hacerlo, él confía en Sōma; Loge que estaba a punto de matar a un caballero femenino, Sōma se lo impide, Loge lo ataca con su arma pero Sōma apenas consiguió detenerlo, casi destruyéndole su armadura, este eleva su cosmos y lo ataca con su técnica Bombardero del León Menor pero a Loge no le hace ningún rasguño, es más ni siquiera pudo que moverlo y el Palasiano le devuelve el golpe dejando a Sōma en el suelo, Loge se dispone a matar a un Caballero de Acero pero Sōma se lo impide, entonces Loge lo ataca con su lanza pero Kōga que junto con Subaru habían destruido todas las abejas, detiene su arma con una sola mano, Loge ataca a Kōga pero este se defiende con su Meteoro de Pegaso, hiriéndolo y para luego huir, represándolos a todos a la normalidad, Sōma le pregunta a su amigo como es que su armadura se volvió tan fuerte, Geki le explica acerca de las 5 armaduras que han sido bañadas en la Sangre de Athena, Sōma le dice que Geki que se retira de la enseñanza para volver a cumplir como Caballero de Athena. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
