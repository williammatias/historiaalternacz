<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 75 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 75: ¡El encuentro destinado! ¡Géminis, otra vez!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_74.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_76.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe width="420" height="315" src="//www.youtube.com/embed/aoSlcUjJBYk" frameborder="0" allowfullscreen></iframe>                                </div>
                                <p>
                                    Paradox se encuentra encarcelada en Cabo Sunión, un lugar en la que solo el poder de un Dios la puede liberar, Galia se aparece ante Paradox para reclutarla y con el poder de su espada sagrada no solo libera a Paradox sino que parte a la mitad la tanto la isla como el mar alrededor, Galia le ofrece unirse a las fuerzas de Palas y Paradox acepta convirtiéndose así en un Palasiana de Segundo Nivel, por otro lado en los alrededores del Palacio Belda, los jóvenes caballeros siguen avanzando hacia al castillo de Palas mientras que Atenea junto con sus caballeros dorados avanzan por otro lado, Kōga, Subaru y Ryūhō repentinamente se encuentran con una densa neblina y luego este último desaparece, Kōga confía en que a Ryūhō no le pasará nada y junto con Subaru siguen su camino, Ryūhō aparece en otra parte de los alrededores del Palacio Belda y se encuentra con una conocida, una Palasiana de Segundo Nivel llamada Paradox de Jano Escarlata, Paradox inmediatamente ataca a Ryūhō golpeándolo directamente en el estomago para luego atacarlo con su técnica Jano Escarlata pero Ryūhō se protege usando su escudo, Paradox esta decidida a vengarse de Ryūhō por haberla vencido la última vez, así que vuelve a atacarlo pero Ryūhō esquiva cada ataque para luego atacarla con su técnica Dragón Naciente pero Paradox anula la técnica con solo tocarlo, Paradox admite que antes ella había sido vencida debido a que no peleó en serio para luego atacarlo con su técnica Fin del Mundo, Ryūhō recibe la técnica directamente y cae contra el suelo, este le recrimina a Paradox su alianza con Palas y el porque de su traición, Paradox le contesta que Atenea, odio a Saori Kido solo por el hecho de haber nacido como Diosa, ser querida y protegida por todos y luego golpea a Ryūhō para luego volverlo a atacar con su técnica Encrucijada Espejismo, tele-transportando a Ryūhō al "espacio", Paradox lo vuelve a atacar con su técnica Fin del Mundo pero es detenida por el nuevo caballero dorado de Géminis, Integra de Géminis, la hermana gemela de Paradox la ataca rompiéndole su arma y Paradox responde al ataque rompiéndole la máscara dorada y dejando ver el rostro de Integra, el caballero dorado femenino felicita a Ryūhō por resistir ante los ataques de Paradox pero repentinamente Paradox cambia su color de cabello, enciende su propio cosmos y ataca a Integra con una gran brutalidad, esta intenta resistir a los ataques de su hermana para luego elevar su cosmos y emparejar la pelea, ambas hermanas pelean al unisono y ninguna da su brazo a torcer sin embrago Paradox eleva todavía más su cosmos haciendo que Integra a duras penas pueda resistir, Paradox se prepara para darle el golpe final pero Ryūhō participa en la pelea al lado de Integra debido a la gran determinación que Integra tiene, ambos caballeros elevan sus cosmos y juntos atacan a Paradox y, Ryūhō completa el golpe con su técnica Dragón Naciente sin embrago, Paradox no sufre ningún rasguño todo lo contrario posee un cosmos enorme y ataca con su técnica Destino Final pero Integra anula la técnica de su hermana y luego usa su mejor técnica Explosión de Galaxias, Paradox recibe la técnica directamente pero rápidamente huye jurando vengarse. 
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
