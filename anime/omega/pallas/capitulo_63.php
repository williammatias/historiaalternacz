<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 63 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 63: ¡Seiya, líder de la línea de fuego! ¡La decisión de Atenea!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_62.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_64.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/3A246B334CD01A27" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Con Genbu de Libra muerto, todos los caballeros de Palestra le rinden ofrendas, Geki da las últimas palabras y le entrega la Cloth-stone de Libra a Ryūhō y este promete algún día entregárselo a su padre, al día siguiente, los ánimos en toda Palestra están muy decaídos tanto para las personas rescatadas como para los caballeros, los jóvenes caballeros le preguntan a Geki acerca del cosmos omega pero este no sabe nada y ahora con las puertas de la entrada principal destruidas los caballeros de acero han pasado a vigilar la entrada, Subaru se queja acerca de la labor que tienen los caballeros de acero pero es regañado por sus maestros Ban y Nachi, inmediatamente un enorme cosmos de oscuridad vuelve a rodear Palestra y esta vez se trata de Hati de Daga quien comanda a una segunda ola de enormes ejércitos de palasianos, Hati ataca a los caballeros de acero dejándolos contra el suelo, los jóvenes caballeros llegan vistiendo sus armaduras y junto con Subaru le hacen frente al ejército de palasianos, cuando el enorme ejército cada vez se acercaba más, quien aparece para pelear contra ellos es el caballero dorado Seiya de Sagitario, los palasianos han comenzado a flaquear pero Hati los amenaza con matarlos si llegasen a retroceder, es entonces que los palasianos van firmes contra Seiya pero este usa la técnica que heredó de Aioros de Sagitario Trueno Atómico y derrota al primer ejército pero luego una segunda ola va en contra del caballero dorado pero este vuelve a usar la misma técnica derrotándolos al instante, Hati se dispone a pelear contra Seiya pero en toda Palestra comienza a brillar con un enorme cosmos de luz y Athena llega para proteger a sus caballeros, Hati trata de no perder la oportunidad de atacarla pero el enorme cosmos de Athena lo paraliza y Hati ordena la retirada, ahora en toda Palestra alberga la alegría y la felicidad, al anochecer, Athena le pide perdón a Kōga por haberlo metido de nuevo en esta guerra, pero a Kōga no le importa ya que aceptó su destino como Caballero de Athena, más tarde Athena emplea su cosmos para que la joya reaccione a su cosmos y funciona, la misma joya que la Diosa Palas tiene en su brazo reacciona al cosmos de Athena y, Palas entra en trance, teniendo un breve encuentro con su hermana mayor; gracias a eso Athena descubre la ubicación de Palas, se sitúa en el Palacio Belda ubicado al oeste de la India. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
