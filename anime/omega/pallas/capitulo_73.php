<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 73 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 73: ¿¡Lágrimas de Caballo Menor!? ¡Despertando a dos Cloths!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_72.php.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_74.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/AFFB305AFED8193C" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Cada vez más caballeros llegan para pelear contra los palasianos, Subaru que se había desmayado despierta de una pesadilla y se encuentra nuevamente en el refugio al lado de Selene mientras que la armadura de caballo se encuentra en su caja de pandora, Kōga le explica el acceso por las alcantarillas al Palacio Belda fue destruido mientras que el resto de los jóvenes caballeros pelean al lado de los caballeros de acero contra ejércitos de soldados palasianos para abrir camino, Subaru va hacia donde esta su armadura y enciende su cosmos poniendo nervioso a Kōga mientras que Eden también esta de la misma manera y tratando de entender el porque Subaru despertó su nueva armadura sin despertar el séptimo sentido, Athena que despertó, se pone de pie y con sus caballeros dorados se dirige a la estatua de Atenea, por otro lados en el Palacio Belda, Raki también llega al Palacio Belda por ordenes de Kiki de Aries pero se encuentra con Halimede de Barra Triple, este trata de atraparla pero se sorprende al ver los poderes de Raki e inmediatamente llega Kōga con Subaru, Raki se sorprende al ver a dos constelaciones hermanas peleando juntos, Halimede se prepara para atacar con su nueva arma que fue un regalo de Titám, llamada Devorada Fantasma, un arma que equivale a las armas de palasianos de segundo nivel, Subaru ataca a Halimede pero su armadura se lo impide e instantáneamente pierde su brillo y el palasiano lo golpea con su arma, Kōga también lo ataca con golpes y luego ataca con su técnica Meteoro de Pegaso pero el arma de Halimede no sufre ningún daño, este contra-ataca con su técnica Fantasma Brutal pero Kōga debido a su cansancio no logra esquivar el ataque y lo recibe directamente, la armadura de pegaso también esta en su limite y poco a poco se destruye, Subaru trata de ayudarlo pero la armadura de caballo no lo deja moverse libremente, Raki trata de ayudarlo, tocando su armadura pudo sentir que la armadura de caballo esta llorando debido a que sus anteriores portadores murieron creyendo que su nuevo portador también morirá, en Grecia, Athena junto con sus caballeros llegan a donde esta la estatua y Athena se prepara para invocar su Kamei, ella misma se hace sangrar su mano derecha y la coloca a los pies de la estatua, de repente el cielo se oscurece completamente y luego de una gran luz resplandeciente la estatua de Atenea se hace miniatura apareciendo en las manos de Athena, la Diosa se dirige a donde esta Harbinger de Tauro y se la entrega al caballero dorado para que sea el custodio de su armadura hasta que llegue el momento de entregársela, Harbinger esta completamente sorprendido y avergonzadamente acepta la misión, en el Palacio Belda, Kōga ya no le quedan fuerzas para pelear contra Halimede recibiendo grandes cantidades de castigo mientras que su armadura poco a poco se va deshaciendo, Raki ayuda en todo lo que puede a Subaru hasta que él entiende lo que le pasa a su armadura, Halimede se prepara para darle el golpe final a Pegaso pero Subaru interviene pero como todavía no puede moverse libremente trata de convencer a su armadura para que no se rinda y al mismo tiempo recibe fuerte golpes de la arma de Halimede, este se ríe de Subaru mientras que él enciende su cosmos y logra convencer a la constelación de Caballo Menor, la armadura vuelve a recuperar su brillo y Halimede ataca con su técnica Fantasma Brutal, el caballero de bronce eleva su cosmos y de un solo golpe anula la técnica de Halimede, este usa su mejor técnica llamada Grito Fantasma pero Subaru estalla su cosmos y ataca con su técnica Tempestad Pléyades, destruyendo la técnica de Halimede, su arma, su cronotech y matando así a Halimede, los demás jóvenes caballeros pelean arduamente contra incontables soldados palasianos pero ellos también han llegado a su limite pero poco tiempo después, llegan el resto de los caballeros dorados escoltando a Athena, los caballeros dorados acaban con todos los soldados palasianos rápidamente, el cosmos de Athena se hace sentir por todos lados, Shun de Andrómeda, Hyōga de Cisne, Shiryū y Seiya de Sagitario también sienten su cosmos, con la presencia de Athena al campo de batalla todos los caballeros presentes recuperan su espíritu de lucha, además el Doctor Asamori envía a los Caballeros Legendarios de Acero Shō de la Armadura Celeste, Daichi de la Armadura Terrestre y Ushio de la Armadura Marina al Palacio Belda.  
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
