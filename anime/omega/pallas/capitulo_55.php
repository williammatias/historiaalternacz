<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 55 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 55: ¡Lo único irreemplazable! ¡Dragón, despierta!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_54.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_56.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/E48B21A175230818" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga, Sōma, Yuna y Subaru están cerca de llegar a los Antiguos Cinco Picos en China para avisarle a Ryūhō acerca de los nuevos enemigos que han llegado pero son interceptados por un batallón de solados de Palas que están decididos a pelear, en la Gran Cascada del Monte Rozan, Ryūhō está siendo entrenado intensamente por su padre Shiryū con el objetivo de cambiar el flujo de la cascada sin hacer uso de su armadura, Ryūhō lo intenta una y otra vez pero no lo consigue, Shiryū regaña a su hijo diciéndole que a pesar de ser un caballero su corazón ahora siente dudas de seguir el sendero de un caballero, Ryūhō se pone a pensar al pie de la cascada acerca del riguroso entrenamiento que le a puesto su padre, su madre Sunrei se le acerca para hablar con su hijo, Ryūhō le dice a su madre que él regresó a casa para vivir una vida pacifica al lado de sus padres, que su deseo nunca fue ser un caballero, él solo acepto serlo para que su padre regresara a la normalidad, así que no entiende el porque del entrenamiento de su padre, Sunrei le dice que su padre como caballero siempre a estado en constante peligro pero nunca a dudado en ayudar y creer en sus amigos, Ryūhō vuelve con el entrenamiento y cambiar el flujo de la cascada pero aún no lo consigue llegando al punto de quedar exhausto para luego tomar otro descanso abandonando la cloth-stone en su casa mientras Shiryū ve como su hijo se va sin su armadura, Ryūhō vuelve al pie de la cascada a seguir pensado pero es interceptado por un Palasiano de Tercer Nivel llamado Halimede de Barra Triple cargando su arma la Barra Triple fue con la misión de asesinar a los Caballeros del Dragón, atacando con su arma a Ryūhō y este sale herido, Ryūhō se levanta e intenta pelear pero sin su armadura no consigue hacerle nada, Sunrei que fue enviada por Shiryū le entrega la cloth-stone a su hijo, Halimede lanza su barra contra Sunrei pero Ryūhō se interpone para recibir el daño este intenta invocar su armadura pero no responde, Halimede se acerca y le da de pisotones en la espalda a Ryūhō y este suelta la cloth-stone, Halimede estuvo a punto de destruirlo pero Ryūhō se pone en medio, el palasiano le empieza a golpear una y otra vez con su arma en la espalda, Ryūhō eleva su cosmos al máximo apareciendo en su espalda el Tatuaje del Dragón e invoca su armadura y esta vez la armadura responde al cosmos de Ryūhō, el nuevo poder de su armadura rompe la cloth-stone dándole una nueva forma a su armadura, el enorme cosmos de Ryūhō (sin siquiera vestir su armadura) detiene y cambia el flujo de la gran cascada, apareciendo ante él la nueva armadura que empieza a vestirle, Halimede lo ataca con su arma pero la nueva armadura no sufre ningún rasguño, Halimede lo vuelve a atacar con su técnica Tiro de Ráfaga pero Ryūhō solo usa su escudo para protegerse para luego atacarlo con su Dragón Naciente y Halimede sale herido para luego este salir huyendo, los demás caballeros han llegado para ayudar a Ryūhō pero este se empieza a reír. Más tarde, Ryūhō que ahora está cargando la Caja de Pandora se despide de sus padres y se reúne con sus amigos para luego separarse, Sōma, Yuna y Ryūhō se van al Santuario para reunirse con los demás caballeros, Kōga y Subaru se dirigen a Japón a buscar a Haruto de Lobo. 
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
