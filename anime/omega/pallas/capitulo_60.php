<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 60 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 60: ¡La estrella de acero! ¡Subaru, abraza tu espíritu de lucha de acero!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_59.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_61.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/25732AB9E0CBFC76" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Un ejército de soldados Palasianos comandados por Hati de Daga, atacan y destruyen el cuartel general de los Caballeros de Acero pero afortunadamente no había nadie en el cuartel, al día siguiente, Kōga y Subaru llegan a las ruinas del cuartel general pero son emboscados por un ejército de soldados Palasianos pero ambos caballeros visten sus armaduras y derrotan a la gran mayoría del ejército y apareciendo entre ellos Hati (que todavía padece de los efectos de la técnica de Haruto) que ataca a Subaru con su Lluvia Sangrienta dañando la armadura de Subaru y dejándola totalmente inservible, Hati quien ha llegado para vengarse del asesino de Dioses, lo ataca con Lluvia Sangrienta mientras que Kōga se defiende con su Meteoro de Pegaso, Hati queda con una herida en el rostro y por el momento decide retirarse con sus soldados pero al poco instante llegan un grupo de Caballeros de Acero y entre ellos esta el mejor amigo de Subaru Erna, quien los guía hacia el campamento provisional de los Caballeros de Acero, en el campamento son recibidos por los líderes de los Caballeros de Acero, Ban y Nachi ambos antiguos caballeros de León Menor y Lobo, que le reclaman a Subaru por haberse escapado del cuartel general pero Subaru les pide que le den otra armadura ya que la suya esta destruida, causando que Ban se moleste aún más con él, Subaru se cansa y huye del campamento para reflexionar un poco, Erna le explica a Kōga que Subaru siempre fue así, él llego al cuartel general en medio de una tormenta, y era el único de su clase que se esforzaba para ser más fuerte que nadie pero era bien anti-social hasta que escuchó las proezas de como Kōga y sus amigos derrotaron a los 12 Caballeros Dorados y derrotaron al Dios Apsu, Kōga al escuchar eso busca a Subaru para hablar con él, pero son emboscados por una segunda ola de ejércitos de soldados Palasianos comandados por Hati, todo el campamento dan las alarmas de guerra para luego aparecer un escuadrón de Caballeros de Acero comandados por Ban y Nachi, los tres comandantes dan la orden de atacar, Kōga viste su armadura y pelea con Hati este ataca a Kōga lanzándole una de sus Dagas pero a Kōga no le pasa nada, Subaru que se siente inútil al no poseer una armadura pero ayuda a su amigo Erna que estaba siendo estrangulado por un Palasiano, Ban y Nachi que están rodeados de soldados Palasianos elevan sus cosmos, y los atacan con sus técnicas Bombardero de León Menor y Aullido Mortal, Hati hace uso del Retraso del Tiempo afectando a muchos Caballeros de Acero, sin embargo Kōga lo ataca sabiendo que venciéndolo todos regresaran a la normalidad pero antes de recibir el golpe, Hati se defiende lanzándole una de sus dagas a Kōga, sorprendentemente la daga perforó el pecho de la armadura de Kōga, Hati enseguida lo ataca con su técnica Dagas Sangrientas, Kōga que logró sacarse la daga de su pecho no logra esquivar la técnica de Hati quedando herido en el suelo, Subaru intenta atacarlo pero Hati le da un fuerte golpe en el estómago, dejándolo clavado en el suelo, Ban y Nachi también van a atacarlo pero caen rápidamente, Subaru siente las consecuencias de las debilidades del ser humano y Hati procede a matarlo, Erna es el único que queda pero este está aterrorizado, luego se arma de valor y embiste a Hati para luego darle la espalda para salvar a su amigo, Hati le lanza su daga hiriendo por la espalda a Erna, Kōga al ver eso se vuelve a levantar para pelear contra Hati, Erna antes de morir le encomienda que haga uso de su armadura y con ella proteja a todos, para luego morir en los brazos de su mejor amigo, Subaru invoca la armadura de Erna y una vez teniéndola puesta Subaru que esta enfadado posee un cosmos muy fuerte, todos están sorprendidos, Subaru hace estallar su cosmos que esta cerca de despertar el séptimo sentido y ataca con su técnica Impacto Pléyades, Hati se defiende con su técnica Lluvia de Dagas pero esta técnica se hace nula ante la poderosa técnica de Subaru que golpea a Hati fuertemente pero antes de morir se tele-transporta para huir pero regresa a todos a la normalidad, al amanecer, todos le hacen una tumba a Erna y se despiden de él para luego todos juntos partir rumbo a Palestra. 
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
