<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 61 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 61: ¡Un gran enfoque del ejército! ¡La batalla para defender Palestra!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_60.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_62.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/1963A517227A8E16" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga y Subaru acompañados de Ban, Nachi y los demás caballeros de acero después de una larga caminata por el bosque llegan por fin a Palestra pero se encuentran con un ambiente extraño, muchas personas de diferentes aldeas que han sido salvadas por los caballeros están refugiadas en Palestra, Sōma, Yuna y Ryūhō que también se encuentran en Palestra le explican todo lo sucedido a Kōga y a Subaru y, en eso llega el Maestro Geki presumiendo ante los aldeanos del gran poder militar que ahora posee Palestra pero Geki mientras seguía alardeando se lleva a Kōga, Subaru, Sōma, Yuna, Ryūhō, Ban y Nachi a la recámara del Director y allí les habla con la verdad acerca de lo preocupante de la situación en la que se encuentra Palestra, en eso llega el comandante de los caballeros y caballero dorado Genbu de Libra, él siente el cosmos de la Armadura de Pegaso y le pide a Kōga que desvista su armadura, Kōga acepta y Genbu inspecciona minuciosamente la armadura, él vio la rasgadura en la armadura e inmediatamente supo como fue que sucedió, de igual manera les pide a Sōma, Yuna y Ryūhō que desvistan sus armaduras, Genbu siente el cosmos de cada una de ellas y notó que las armaduras están muy heridas, que solo se embriagaron con su poder y, les da un fuerte regaño a los jóvenes caballeros, más tarde en los jardines de Palestra, los jóvenes caballeros reflexionan acerca de lo sucedido pero a Kōga le molestó mucho que Genbu lo regañara, Subaru llega pero solo para burlarse de los jóvenes caballeros a lo cual irónicamente también recibe un fuerte regaño de parte de Ban y Nachi, por otro lado Genbu y Geki también discuten acerca de lo sucedido, acerca de la situación en la que se encuentra Palestra y ambos están de acuerdo en que se merecían ese regaño, además Genbu felicita a Geki por haberles enseñado bien a sus pupilos y Geki lo acepta con mucho orgullo pero en medio de la conversación aparece Haruto de Lobo que había capturado a un soldado Palasiano y lo interrogó, Haruto le explica a Genbu que todo formaba parte de un gran plan, todos los Palasianos que atacaron los pueblos y ciudades de todo el mundo se dejaron derrotar a propósito para que los caballeros se llevaran a las personas hacia un refugio que en este caso seria Palestra, Genbu preocupado, supo que sus sospechas eran ciertas e inmediatamente un descomunal ejército de soldados Palasianos rodea Palestra, el primer grupo de soldados lo comanda Tebe de Martillo, el cual empiezan a cruzar el largo puente de piedra, todos los caballeros como Bayer de Boyero, Miguel de Perros Cazadores, Mirfak de Perseo, Menkar de Ballena, Hooke de Compás, Bartschius de Jirafa, Ichi de Hidra, Güney de Delfín, Spear de Dorado, Dalí de Corona Boreal, Arne de Liebre, Komachi de Grulla, Michelagelo de Buril y los Caballeros de Acero están preparados y listos para pelear, sin embargo Genbu de Libra les ordena a todos que nadie salga de Palestra, los jóvenes caballeros se sorprenden ante tal orden pero es Genbu quien sale de Palestra y se enfrenta él solo al primer ejercito de soldados, derrotándolos a todos rápidamente, luego se enfrenta a Tebe de Martillo quien ataca al caballero dorado con su técnica Martillo Demoníaco, Genbu retiene la técnica con una sola mano y luego se la devuelve hiriendo a Tebe pero al mismo tiempo le enseña a los jóvenes caballeros 4 lecciones importantes que su maestro Dohko de Libra le enseño a él, entre ellas no depender de fuerza prestada, los jóvenes caballeros se dan cuenta de su error, pero en la batalla aparece el comandante del ejército de Palasianos, un Palasiano de Segundo Nivel y discípulo de Hyperion llamado Aegir del Brazo Fantasma y le pide a Thebe que se retire del campo de batalla, la pelea da inicio y Genbu comienza el ataque usando su técnica Furia Real del Monte Rozan pero Aegir se defiende con su técnica Mano Psiónica para luego atacar con su técnica Garra Fantasma creando en la zona en donde se encuentra Genbu y fuerte campo de fuerza, aplastando a Genbu pero este resiste con todas sus fuerzas, luego hace arder su cosmos y se levanta poco a poco, para luego hacer estallar su cosmos y destruir el campo de fuerza que lo rodeaba y al mismo tiempo el guantelete de Aegir, Hyperion que desde el castillo de Pallas estaba observando todo lo sucedido siente una enorme vergüenza de ser el maestro de Aegir y le envía su arma la Espada de la Destrucción a Aegir para que elimine de una vez por todas a Genbu, este siente un enorme cosmos de oscuridad emanando de la espada mientras que Aegir está preparado para la segunda ronda. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
