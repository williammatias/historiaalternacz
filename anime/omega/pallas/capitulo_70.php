<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 70 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 70: ¡La cloth destructiva! ¡El ataque del Pallasite vagabundo!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_69.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_71.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/47876FC987325AAD" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Menkar de Ballena y otros caballeros se enfrentan al palasiano Miller de Guante Alquimia pero no son capaces de siquiera vencerlo y como si fuera poco Miller ataca a los caballeros, destruyendole completamente la armadura de plata a Menkar y robandole las armaduras a los otros caballeros, la noche ha llegado y aunque la pelea sigue, Haruto convence a Kōga, Ryūhō, y Subaru de buscar refugio y descansar pero Haruto siente como los cosmos de Menkar y de los demás caballeros desaparecen, al día siguiente, gracias a los conocimientos de Haruto, los jóvenes caballeros evitan encontrarse con algún palasiano pero recorriendo los jardines de la ciudad Haruto siente la presencia de alguien acercarse sin saber por donde viene hasta que tarde se da cuenta que quien ataca desde arriba es el palasiano Miller de Guante Alquimia usando su técnica Detección de Elementos en Subaru dañándole su armadura de acero y dejándolo inconsciente, Haruto ataca con su arma ninja pero Miller lo esquiva rápidamente y con su arma le destruye todo el protector izquierdo del brazo, Miller un palasiano que no necesita vestir su cronotech, se presenta ante los jóvenes caballeros y de su bolsillo saca las cloth-stone de otros caballeros con los que él a peleado, dejando a todos sorprendidos, Kōga impulsivamente ataca a Miller pero él esquiva el golpe y le da una fuerte patada en la espalda estrellándolo contra el suelo, Ryūhō y Haruto van en su ayuda pero él solo juega con ellos y esquiva todos sus golpes, Miller muy confiado les propone dejarlos ir si al menos uno de ellos les logra dar un golpe, Haruto ataca usando su endan y se llevan a Subaru para esconderse y disminuyen sus cosmos para no ser detectados pero aun así Miller puede sentir la tensión de la batalla y puede disminuir su tensión para pasar desapercibido, llegando así a donde Haruto y lo ataca, estrellándolo contra la pared, Miller se prepara para darle el golpe final a Haruto pero Ryūhō viene a ayudarlo usando su escudo de dragón para protegerse y luego ataca con su técnica Dragón Naciente, Miller puede ver la técnica con claridad y viajar a través de ella, Miller esta sorprendido que el escudo de Ryūhō no se rompiera, los jóvenes caballeros atacan en equipo pero Miller esquiva cada golpe y usa su arma para atacar a Haruto pero Ryūhō usa su escudo para protegerlo, Miller como se puede mover a la velocidad de la luz, es capaz de multiplicarse así mismo y confundir a los caballeros, Miller ataca pero Haruto descubre al autentico y le lanza su kunai pero Miller destruye el arma y su objetivo principal siempre fue el escudo de Ryūhō, atacándolo directamente y logrando destruirle el escudo de dragón de Ryūhō y dejándolo inconsciente, Kōga lo ataca con su Meteoro de Pegaso pero Miller también puede ver a través de la técnica y esquivar cada golpe, desapareciendo ante la vista de pegaso para luego aparecer desde arriba y destruirle toda la hombrera derecha de su armadura, Haruto vuelve a usar su endan para esconderse y volver a planear otra estrategia de ataque, Haruto se tranquiliza y recuerda el entrenamiento con su padre Yoshimi acerca de las tensiones en la batalla y gracias a eso Haruto logra disminuir su tensión en la batalla hasta lograr llegarle por detrás a Miller pero cuando estaba a punto de matarlo vuelve a aparecer su tensión de pelea y Miller siente a Haruto, atacándolo rápidamente con su técnica Detección de Elementos destruyendole todo el pecho y el casco de su armadura, de la armadura de Haruto solo queda sus guantes, su cinturón y las botas, sin embargo, Haruto agarra a Miller y eleva su cosmos para usar su técnica Golpe del Lobo Desesperado, logrando así darle el primer golpe a Miller, el palasiano sale volando y cae estrellado contra un edificio, Ryūhō recupera el conocimiento y junto con Kōga se reúnen con Haruto pero Miller quien sale ileso admite su derrota pero furioso, se niega a dejarlos ir mostrando su verdadero poder pero los jóvenes caballeros están completamente agotados como para seguir peleando y en medio de la pelea aparece Europa para detener la batalla, tranquiliza a Miller y este decide retirarse junto con Europa. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
