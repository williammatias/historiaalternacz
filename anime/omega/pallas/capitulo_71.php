<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 71 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 71: ¿¡La cloth maldita!? ¡El santo de Caballo Menor!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_70.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_72.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/85A032D387C86C3E" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Los jóvenes caballeros se han vuelto a separar, Metone de Brillo Máximo se siente humillado por perder contra Yuna y Sōma, Europa de Corte Segador aparece y le entrega un reloj de arena negra llamado El Anti-Límite para que Metone adquiera más poder, Yuna y Sōma se encuentran con caballeros de acero que están peleando contra soldados palasianos y deciden ayudarlos, Sōma quiere enfrentarse a ellos directamente pero Yuna se lo impide ya que quiere planear una estrategia, los soldados palasianos siguen atacando sin cesar pero son derrotados por Ymir de Brillo Máximo y su hermano Metone de Brillo Máximo, él pone en practica el anti-límite, rompiendo el reloj y arrebatandole todo el cosmos de su hermana a tal punto de asesinarla para luego él absorber todo ese cosmos, Metone sufre una pequeña metamorfosis y, su poder se a incrementado comparándose con Palasianos de Segundo Nivel a pesar de ser un Palasiano de Tercer Nivel, los caballeros de acero tratan de retenerlo pero todos caen heridos rápidamente, Yuna ordena la retirada a los caballeros de acero ya que Metone solo quiere vencer a Yuna y Sōma, los caballeros de acero se van, Yuna y Sōma llaman la atención de Metone para llevarlo a un lugar más abierto, una vez allí, Sōma usa su técnica Fuego Ardiente del León Menor pero Metone fácilmente anula la técnica, el palasiano enloquece y hace explotar su cosmos, su cosmos es tan fuerte que empieza a rasguñar la armadura de Yuna y Sōma, el caballero de León Menor se lanza él solo contra Metone pero este le da un fuerte zarpazo a Sōma dejándolo herido, Metone se prepara para darle el golpe final pero Yuna interviene dándole una patada sin embargo Metone detiene la patada con una sola mano y aplica una gran fuerza para agrietar la bota derecha de su armadura para luego lanzarla, Yuna cae inconsciente pero Sōma la hace reaccionar, Metone vuelve a atacar a Yuna y Sōma pero el caballero protege a Yuna de Metone, ambos guerreros se atacan cuerpo a cuerpo pero Sōma es herido en el hombro derecho debido a un zarpazo de Metone, ambos caballeros aunque estén heridos se apoyan el uno al otro para defenderse de los ataques de Metone, este ataca a Yuna con su técnica Garra Salvaje Doble pero el caballero femenino se defiende con su técnica Tornado Tormenta y no solo eso con su técnica eleva a Sōma y, él con su enorme cosmos usa su técnica combinada llamada Tormenta Bombardera de León Menor dejando a Metone gravemente herido, su arma y su cronotech destruidas, Metone en medio del fuego intenta levantarse pero el espíritu de su hermana Ymir aparece ante él para cobrar venganza, atacando a Metone y regresarle el "favor" que le hizo. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
