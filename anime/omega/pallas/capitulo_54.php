<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 54 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 54: ¡Fortalecer mi coraje! ¡Cloth, revive!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_53.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_55.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/5F3473470AFC8A1B" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga y Sōma compiten corriendo entre ellos junto con Subaru para llegar a Jamir a que Kiki de Aries repare la armadura de León Menor pero Subaru que no esta tan bien entrenado paran para tomar un descanso, por otro lado Yuna ha llegado a Jamir y pide que Kiki de Aries repare su armadura de Águila pero quien sale a recibirla alegremente es la pequeña discípula de Kiki, Raki, y detrás de ella aparece Kiki y la hace pasar adentro de la Torre para comenzar con las reparaciones, una vez finalizada las reparaciones Yuna invoca la armadura pero esta no responde a los llamados, Kiki le dice que su armadura puede sentir las "dudas" que la albergan a ella por eso es que su armadura no responde a sus llamados, Yuna acepta la verdad y se va a hacia las montañas a pensar y detrás de ella la sigue Raki, Yuna cuenta que después de la guerra con Marte, Yuna regresa a su tierra natal en donde la guerra en su país había terminado y las personas poco a poco regresaban a sus casas, ella se acostumbró a esos días felices y tranquilos que paso con su gente; Al día siguiente Raki se lleva a Yuna muy temprano hacia un lugar el cual es muy preferido por Raki, una vez allí son interceptados por dos hermanos gemelos Palacianos de Tercer Nivel llamados Ymir de Zarpa y Metone de Zarpa, Yuna intenta una vez más invocar su armadura pero aún no responde a sus llamados, Kōga, Sōma y Subaru finalmente llegan a Jamir pero Kiki envía a Kōga a que ayude a Yuna que esta en peligro, al oír esto Kōga corre hacia donde esta Yuna y detrás de él le sigue Subaru, Sōma se queda para que Kiki empiece con las reparaciones, los Palacianos atacan a Yuna y las dudas en ella se hacen más fuertes perdiendo fácilmente el encuentro, los hermanos se dirigen a ella para darle el golpe final pero Raki usando su telequinesis mueve piedras muy grandes para salvarla, Metone usa su Retraso de Tiempo para paralizar a Raki y seguir atacando a Yuna, Kiki terminó con la armadura de Sōma y este invoca su armadura, el nuevo poder de la armadura rompe la cloth-stone dándole una nueva figura a la armadura de León Menor e inmediatamente Sōma se dirige a donde esta Yuna, en el campo de batalla Yuna está aterrada de pelear a lo que Ymir procede a darle el golpe final pero Kōga se lo impide a tiempo, atacándola pero ambos hermanos atacan a Kōga pero este sabe defenderse muy bien, Kōga le recrimina a Yuna el no saber siquiera defenderse a pesar de ser un Caballero Femenino, Yuna con lágrimas en los ojos solamente responde que siente miedo, miedo de seguir sufriendo a la hora de pelear, Metone para "aliviar su dolor" la ataca pero Kōga se lo impide y la convence de seguir peleando para defender lo quiere, Yuna invoca una vez más su armadura y esta vez la armadura responde, el nuevo poder de su armadura rompe su cloth-stone dándole una nueva forma a su armadura, en ese momento, Ymir ataca a Raki pero Yuna la protege, Kōga y Yuna están preparados para pelear pero Sōma llega para devolverle el favor a su amigo, Ymir y Metone usan su técnica Doble Garra Salvaje en Sōma y Yuna pero estos ni siquiera tuvieron necesidad de moverse para esquivar el ataque ya que sus nuevas armadura no tuvieron ningún rasguño, ambos caballeros atacan con sus mejores técnicas a los gemelos y estos caen vencidos, los gemelos Palasianos deciden huir devolviendo a Raki a la normalidad, más tarde, Yuna se despide de Raki y ahora dos nuevas Cajas de Pandora se unen al viaje. 
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
