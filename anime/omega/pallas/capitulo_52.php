<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 52 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 52: ¡La nueva Cloth! ¡Vuela, nuevo Pegaso!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="../mars/capitulo_51.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_53.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/9B5929F8992C6C70" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Un año después de la Guerra Santa contra el Dios de la Oscuridad Apsu, los días de paz en la Tierra han regresado por un tiempo, Athena que se encuentra descansando en el Templo de Atenea, en el recién reconstruido Santuario, despertó de su sueño perturbador en la cual vio a todos sus Caballeros supervivientes peleando contra los guerreros Los Palasianos acompañados de su Diosa Palas y, para evitar que esta nueva guerra surja Athena envía a Seiya de Sagitario a asesinar con una daga dorada (la misma daga que Saga había intentado usar para asesinar a la pequeña Athena) a la Diosa Palas, Seiya se sorprende al ver que el recipiente de la Diosa era una pequeña niña que esta estaba al tanto de todo lo sucedido y acepta ser asesinada por el Caballero Dorado, Seiya que a punto de lograr su cometido se niega a cumplir con su misión ya que nunca en su vida a maltratado a un niño pero de la nada aparece un Palasiano de Primer Nivel llamado Titán de la Espada Teogénesis sirviente de la Diosa que ataca a Seiya y hace dormir a la niña, y en su brazo derecho de la Diosa Palas y Athena aparecen una joya especial que las une, para luego ambos desaparecer ante los ojos de Seiya, este se lamenta al no haber cumplido con su misión. Kōga regresa a la Isla Huérfano y sigue entrenando cuando se le acerca el viejo Tatsumi y le quiere entregar los restos de la cloth-stone de Pegaso a Kōga pero este no la acepta dado que asume que ya no la necesita porque que la guerra ha terminado sin embargo recibe una inesperada y desagradable visita de Subaru quien lo ataca pero Kōga se niega a pelear y de la nada aparece otro Palasiano de Tercer Nivel llamado Tarvos del Triturador de Estrellas cargando su arma Lucero del Alba y, quiere atacar a Kōga pero Subaru se lo impide, viste su Armadura de Acero y ataca a Tarvos con todas sus fuerzas, Tatsumi corre hacia donde esta Kōga, le explica todo acerca de los Caballeros de Acero y le pide que vuelva a vestir su armadura de Pegaso pero este se niega a usarlo ya que no se perdona que en la guerra contra Apsu haya herido a sus amigos, Tarvos que no sufre ningún rasguño ataca con su técnica Meteoro Ionico, el impacto manda a volar a todos incluyendo a Tatsumi, Kōga preocupado, corre donde Tatsumi y este le vuelve a pedir a Kōga que vista su armadura y pelee contra el Palasiano, esta vez Kōga acepta la cloth-stone y hace arder su cosmos, la cloth-stone responde al cosmos de Kōga y a continuación la armadura de Pegaso evoluciona y empieza a vestir a Kōga, ambos muestran una gran pelea pero Kōga resulta ser el ganador y Tarvos se ve obligado a huir. En otro lado Kiki de Aries junto con Raki se encuentran en Jamir rindiendo ofrendas a todos los Caballeros de Bronce, Plata y Oro que han muerto en la anterior guerra, sin embargo reciben la visita de un Palasiano de Segundo Nivel llamado Dione que vino con la intención de matar al único reparador de armaduras rápidamente Kiki lo ataca con su técnica Extinción de Luz Estelar pero Dione también huye. En otra parte en algún lugar, Palas descansa en su trono y los Palasianos han empezado poco a poco a reunirse alrededor de ella, y entre ellos esta el Caballero de Bronce Eden de Orión.  
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
