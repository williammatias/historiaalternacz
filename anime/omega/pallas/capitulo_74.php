<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 74 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 74: ¡La batalla de Kiki! ¡Los amigos que superan las generaciones!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_73.php.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_75.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/4860D5136AE769B3" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Cada vez más ejércitos de soldados palasianos atacan el puesto donde el reparador de armaduras va a llegar, Shaina de Ofiuco,
                                    Ichi de Hidra, Ban y Nachi son los que protegen la entrada pero Kiki de Aries llega y derrota a todos los soldados palasianos, 
                                    ya cuando todo esta más calmado, Kiki termina con las reparaciones de la armadura de Hidra, por otro lado los jóvenes caballeros 
                                    también se dirigen para allá siendo guiados por Raki, sin embargo se encuentran con un batallón de soldados palasianos pero Subaru
                                    y Eden los derrotan fácilmente ya que las armaduras de ellos son las menos dañadas, una vez que llegan al puesto son recibidos por
                                    Geki vistiendo su armadura de acero, este los guía hasta que llegan donde Kiki de Aries, los jóvenes caballeros le entregan sus
                                    armaduras respectivamente, Kiki empieza a analizarlos y regaña a cada uno por hacer mal uso de las armaduras pero en especial
                                    a Kōga, Sōma, Yuna y Ryūhō, Subaru empieza a elogiar a Kiki por su habilidad pero en cambio él esta consternado en como es que
                                    Subaru pudo hacer evolucionar la armadura de Caballo Menor pero Haruto interrumpe la conversación avisando que soldados 
                                    palasianos se acercan comandados por el palasiano de segundo nivel, Dione de Plasma Serpiente, el palasiano reta a Kiki 
                                    a que salga a pelear y Kiki acepta pero el caballero de plata femenino Shaina se lo impide y le encarga que repare todas
                                    las armaduras, Shaina junto con Ban, Nachi, Ichi y Geki, se encargan de una parte de soldados palasianos, Dione envía a 
                                    más soldados palasianos pero en ese momento llegan los caballeros legendarios de acero, Shō de la Armadura Celeste, Daichi 
                                    de la Armadura Terrestre y Ushio de la Armadura Marina, estos caballeros de acero se enfrentan directamente contra Dione, 
                                    el palasiano ataca con su técnica Plasma Serpiente pero los caballeros de acero la esquivan fácilmente y Shō usa su armadura
                                    para absorber el ataque para luego todos juntos atacar con el Huracán de Acero, muchos soldados palasianos salen volando 
                                    pero a Dione no le pasa nada, por otro lado, Kiki empieza con las reparaciones de todas las armaduras y Dione junto con 
                                    una gran parte soldados palasianos atacan pero caballeros de bronce, caballeros de plata, caballeros legendarios y 
                                    caballeros de acero defiende la recamara donde se encuentra Kiki junto con los jóvenes caballeros, Dione le reprocha
                                    a Kiki el que se esconda mientras otros pelean su batalla pero Kiki al fin entiende la posición en la que se encontraba
                                    antes Mu de Aries y finaliza con las reparaciones para luego salir y enfrentarse a Dione retándolo esta vez a una pelea 
                                    de uno contra uno, Dione confiado acepta y empieza a darle una serie de latigazos pero Kiki sin moverse recibe cada golpe del 
                                    arma de Dione, el palasiano se prepara para darle el golpe final aumentando su poder y usando su técnica Plasma Serpiente Poder 
                                    Máximo pero Kiki rápidamente se tele-transporta y con un solo dedo toca la cronotech de Dione, destruyéndola al instante, Kiki
                                    le afirma a Dione que un reparador de armaduras también sabe el punto débil de ellas, Dione ordena a su ejército destruir a Kiki
                                    pero el caballero dorado usa su técnica Extinción de Luz Estelar enviando al ejército y a Dione a otra dimensión, más tarde, Kiki 
                                    junto con los demás caballeros se quedan para esperar a otros caballeros que quieran reparar sus armaduras mientras que los jóvenes 
                                    caballeros se marchan, Eden se queda hablando con Kiki acerca de las sospechas que tienen con Subaru, encargándole a Eden que lo
                                    mantenga vigilado, todos se despiden mientras que Kiki les desea suerte a los jóvenes caballeros el espíritu de Mu de Aries aparece 
                                    detrás de Kiki de Aries, por otro lado en el santuario, el Caballero Dorado de Géminis se prepara para pelear. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
