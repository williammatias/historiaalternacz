<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 62 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 62: ¡La batalla mortal de Genbu! ¡la Espada Sagrada vs. la Espada de Libra!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_61.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_63.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/EB03D5E8EAF6FCB8" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Genbu siente el enorme cosmos de oscuridad que emana de la Espada de la Destrucción, la espada que es muy parecida a Ascalon le pertenece a Hyperion y ahora esta en manos de Aegir, este apenas desenvainó la espada y en toda Palestra empezó a temblar, Aegir abanico la hoja de la espada contra Genbu pero el caballero dorado se confió y recibió el golpe directamente, estrellándolo contra las gruesas puertas de Palestra, destruyéndolas y quedando inconsciente, los jóvenes caballeros van en su ayuda, visten sus armaduras y Kōga intenta golpearlo pero Aegir detiene el golpe con un solo dedo, los jóvenes caballeros se disponen a atacar todos juntos pero Genbu los detiene y les ordena que se retiren, que Aegir es su oponente, Aegir sorprendido, eleva el cosmos de la espada y utiliza la técnica Distracción del Tiempo, todos los seres vivientes en Palestra así sean los individuos de cosmos fuerte se les detuvo el tiempo pero los jóvenes caballeros incluyendo a Subaru (a pesar de ser un Caballero de Acero) apenas han conseguido librarse un poco de esa técnica sin embargo no pueden moverse, Genbu enfurecido, no le queda más opción que usar una de las armas de Libra y desenvainar la espada de Libra para poder aumentar más su cosmos e igualar el cosmos de la Espada de la Destrucción, ambos guerreros pelean fuertemente con sus espadas pero aún así Genbu no puede resistir cada impacto que recibe la Espada de Libra del enorme poder de la Espada de la Destrucción y el caballero dorado termina exhausto, Aegir que se dio cuenta rápidamente lo ataca y Genbu cae estrellado contra el suelo pero aún se levanta, Aegir vuelve a abanicar su espada una y otra vez pero Genbu se levanta con cada ataque que recibe, Aegir eleva el cosmos de la espada y ataca una vez más a Genbu pero este recibe el corte de la espada, cortándolo desde el hombro hasta casi la mitad del cuerpo, sin embargo Genbu aún sigue con vida y retiene la espada con el cuerpo, Aegir trata de sacarla pero no puede; el Caballero de Libra emplea lo que le queda de cosmos y con la ayuda de la espada de Libra lo eleva hacia el infinito para poder destruir la Espada de la Destrucción, expulsa la espada de Hyperion y usa su más poderosa técnica Ascension Celestial de Rozan, pero sorprendentemente el enorme cosmos de oscuridad de la espada hace nula la técnica de Genbu de Libra pero aún así consigue causarle una pequeña grieta a la espada en cambio una de las espadas de Libra (la que él usó) fue destruida, Hyperion tele-transporta su Espada de la Destrucción regresando a todos a la normalidad y se conformó con el hecho de que al menos asesino al caballero de Libra Genbu, por otro lado, Aegir no lo piensa así, cae en la desesperación y se dispone a atacar al Asesino de Dioses con su técnica Garra Fantasma Clónica, pero Kōga se defiende con su Meteoro de Pegaso, destruyéndole a Aegir su cronotech y su arma, el resto de los soldados palasianos que estuvieron viéndolo todo huyen del lugar, Genbu en sus momentos de agonía se despide de todos y les pide que crean en sus cosmos ya que algún día ellos heredaran el máximo cosmos, el cosmos omega, para luego fallecer, Athena que siente el cosmos de Genbu desvanecerse le agradece por todo lo que ha hecho. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
