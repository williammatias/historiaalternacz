<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 66 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 66: ¡La lucha de acero! ¡Los guerreros sin nombre!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_65.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_67.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/224D3B39943A6607" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Los jóvenes caballeros se han separado, Sōma y Subaru luchan contra soldados palasianos para abrirles paso a los caballeros de acero, dos Caballeros de Acero en especifico, Emma y Kerry y, otros caballeros de acero han quedado acorralados por los soldados palasianos que llegaban sin cesar, viéndose en la necesidad de escapar del lugar, muchos caballeros de acero han caído en la batalla quedando solamente Emma y Kerry sin embargo no dura mucho su suerte ya que se han quedado rodeados de soldados palasianos sin embargo Sōma y Subaru llegan para salvarlos derrotando a todos los soldados palasianos alrededor de ellos, mientras caminaban para llegar al Palacio Belda, el panorama es atroz, hay cadáveres de caballeros de acero y soldados palasianos por todas partes y Emma le hecha la culpa a Sōma y Subaru por no llegar antes a salvar a sus compañeros, pero en ese momento aparece Loge de Brionac con su ejército de abejas y enseguida empieza la revancha entre Sōma y Loge mientras que las abejas también participan en la batalla atacando a Sōma pero los tres Caballeros de Acero Emma, Kerry y Subaru se encargan de ellas usando sus Flechas de Acero, Loge le ordena a sus abejas que ataquen a los Caballeros de Acero, las abejas embisten a Emma y esta queda herida, Sōma les ordena que se retiren de la batalla dándole la espalda a su enemigo, Loge aprovecha el momento y usa su técnica Último Aguijón de Abeja, y una abeja roja aparece y se le incrusta en su armadura, causando una gran explosión y dejando a Sōma herido, destruyendole la hombrera derecha y el casco de la armadura, Loge vuelve a atacar para darle el golpe final lanzandole su lanza pero Kerry le arroja una granada de humo a Loge para que Subaru se lleve cargando a Sōma y huyan del lugar, escondidos en un edificio en ruinas, Sōma descansa de la pelea y les dice a los caballeros de acero que él no contaba con que ellos estuvieran ahí causando que Emma se moleste aun más, Kerry les ordena a Subaru y Emma que vigilen afuera, mientras que él se queda y le cuenta su historia y la de Emma, pero en medio de la discusión aparece en la ventana una de las abejas de Loge mientras que soldados palasianos rodean el edificio y Loge les ordena que entren pero Sōma aun estando herido se prepara para atacar vistiendo su armadura, Loge les ordena a sus abejas a atacarlo pero Subaru aparece concentrando su cosmos y usando su técnica Impacto Pléyades contra las abejas, Loge consternado por la muerte de sus abejas usa su técnica Último Aguijón de Abeja pero Emma aparece y ataca a la abeja roja con Flecha de Acero destruyendo la abeja, para luego entre ellos encargarse del resto de las abejas y soldados palasianos, por otro lado, Sōma empieza a concentrar todo su cosmos en él, mientras que Kerry aparece detrás de Loge y lo atrapa con los brazos y, les ordena a Emma y Subaru hacer el Huracán de Acero causándole rasguños a Loge, mientras que Sōma esta preparado para hacer la técnica Bombardero del León Menor prendiendo a Loge en llamas y dejándolo totalmente herido e inconsciente, todos descansan de la batalla pero una abeja roja aparece entre los escombros para atacar por la espalda a Sōma, pero Kerry se atraviesa, la abeja se le incrusta en su armadura y es él quien recibe la gran explosión, dejándolo gravemente herido y su armadura de acero totalmente inservible, en sus momentos de agonía Kerry mira la foto de su familia en su collar para luego fallecer, más tarde, Emma carga a su compañero y le hace sepultura en el jardín de la iglesia también en ruinas para luego todos juntos despedirse de Kerry.  
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
