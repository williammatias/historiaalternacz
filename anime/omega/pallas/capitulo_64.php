<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 64 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 64: ¡Santos, hacía adelante! ¡El camino inaccesible al Palacio Belda!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_63.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_65.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/E7A25B48AE52F8DD" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Seiya regresa al Santuario con Athena desmayada en sus brazos, llegando al Templo de Athena son recibidos por los caballeros dorados, Kiki de Aries, Harbinger de Tauro y Fudō de Virgo, Seiya deja a Athena descansar en su templo, les cuenta a todos lo sucedido y le pide a los caballeros dorados que cuiden de ella mientras que él se va con la daga dorada al Palacio Belda, los jóvenes caballeros también se dirigen para allá pero Haruto decide irse por su cuenta por otro lado, la pequeña Diosa Palas esta ansiosa por ver a su hermana pero es regañada por Titán por haber salido de su habitación sin permiso y regresa inmediatamente pero Egeon, Hyperion y Galia aparecen para avisarle que ellos no tienen la obligación de cuidar de su Diosa asi que deciden retirarle su ayuda si algo le llegase a pasar para luego desaparecer, Titán entra al castillo y se encuentra con el Palasiano de Tercer Nivel llamado Cyrene de Guaraña, le ordena a este que asesine a los jóvenes caballeros que se acercan, Cyrene inmediatamente obedece y ataca a los jóvenes caballeros que estaban cruzando un puente de madera, Haruto llega para salvarlos impidiendo que Cyrene siguiera atacando el puente, gracias a Haruto todos logran cruzar el puente pero Subaru se queda para "ayudar" a Haruto atacando a Cyrene, este ataca a Subaru mientras que el caballero de acero estuvo a punto de caer por el acantilado, Haruto no le queda más opción que usar una bomba de humo y ayudar a Subaru a subir, Cyrene aprovecha el momento y termina de destruir el puente haciendo que Subaru cayera al acantilado mientras que Cyrene se esconde en el bosque, Haruto se lanza a buscarlo, encontrándolo desmayado en el río y lo ayuda a despertarse pero rápidamente aparece Cyrene acompañado de su hermano gemelo y Palasiano de Tercer Nivel Greip de Guaraña, este pelea contra Haruto mientras que Subaru pelea contra Cyrene, este usa su técnica Cortador de Tormenta y Subaru queda herido, Haruto va en su ayuda pero Cyrene usa su técnica Exclusión Diabólica y su hermano Greip también ataca con su técnica Ventisca Diabólica, Haruto carga a Subaru y esquiva la técnica, aprovechando la explosión de las técnicas huyen del campo de batalla pero dejan un rastro de sangre, al anochecer, Haruto prepara medicina con hierbas para la herida de Subaru, más tarde son encontrados por los hermanos Greip y Cyrene, mientras que a Haruto y a Subaru no les queda de otra que huir, los palasianos empiezan a perseguirlos, atacándolos incontables veces pero los caballeros no dejan de correr, Subaru que poco a poco se esta agotando debido a la herida que tiene, cae al suelo, los palasianos los alcanzan y Haruto se separa de Subaru, este sigue corriendo hasta el final del bosque y Cyrene procede a rematarlo pero Greip se lo impide ya que él quiere matar a Subaru, ambos hermanos se pelean por su "presa" mientras que Haruto aprovecha el momento para atacar a Greip con su técnica Garra de Lobo, Cyrene trata de atacarlo pero Subaru se lo impide, los palasianos usan sus mejores técnicas cada quien contra su oponente pero son Haruto y Subaru quienes ganan la contienda obligando a los palasianos a huir, al día siguiente, Seiya acompañado de los caballeros de plata Bartschius de Jirafa, Menkar de Ballena y Miguel de Perros Cazadores llegan a las puertas de la ciudadela, Seiya utiliza su técnica Meteoro de Pegaso contra los soldados palasianos que protegían la entrada mientras que llegaban los jóvenes caballeros y un rato mas tarde llegan Haruto y Subaru; Seiya para romper la entrada a la ciudadela eleva su cosmos y con sus brazos crea la constelación de Sagitario y usa su técnica Trueno Atómico destruyendo las gruesas puertas. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
