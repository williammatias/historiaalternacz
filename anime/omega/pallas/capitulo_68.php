<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 68 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 68: ¡Kōga y Pallas! ¡Encuentro en el campo de batalla!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_67.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_69.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/F13851F8B432A098" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    La guerra continua, cada vez más Caballeros de Acero y Soldados Palasianos van cayendo, en el Palacio Belda, la pequeña Diosa Palas esta deseosa de salir fuera del palacio pero Titán no lo desea así que manda a su Diosa a dormir pero en cuanto Titán sale de la habitación Palas prepara todo para escapar del palacio sin que nadie se diese cuenta, por otro lado en los alrededores del palacio, una sobreviviente huye desesperadamente de soldados palasianos que vienen persiguiéndola pero es acorralada por los mismos soldados que llegan para asesinarla sin embargo los jóvenes caballeros llegan para salvarla derrotando a todos los soldados palasianos pero Kōga ve a una niña jugando por los callejones de la cuidad y decide averiguar, persiguiéndola por todos lados hasta llegar al jardín de una biblioteca, Kōga se encuentra con la niña, esta admira el brillo y el cosmos de la armadura, Kōga se presenta ante ella como caballero de Athena sin saber que la niña es la Diosa Palas pero soldados palasianos que lo habían seguido, acorralan a Kōga, este carga a la niña y escapa para reunirlos con los demás refugiados, sin embargo son muchos los soldados que lo persiguen lanzandoles sus armas y acorralándolo a donde quiera que vaya, Kōga no le queda de otra que hacerles frente, así que salta a los callejones de la ciudad y esconde a la pequeña niña en uno de los edificios, Kōga hace llamar la atención de todos los soldados palasinos para derrotarlos con su Meteoro de Pegaso, la niña para ver la pelea de Kōga sale del edificio pero es vista por un soldado palasiano, la niña con solo mirarlo dejó al soldado perplejo y este la reconoce al instante pero antes de que pudiera decir algo es derrotado por Kōga que llega para salvarla, la pequeña Diosa le agradece pero como si nada decide irse a jugar por los alrededores de la cuidad, Kōga va a perseguirla, atrapándola para luego llevársela cargada hacia los demás refugiados; en la Palacio Belda, Titán entra en la habitación y descubre que su Diosa no se encuentra allí, saliendo inmediatamente en su búsqueda, Kōga llega a donde están sus amigos que han rescatado a más sobrevivientes para ser escoltados hacia la salida por Caballeros de Acero, la pequeña niña no quiere irse ya que ella quiera estar con Kōga pero este la obliga a irse con los refugiados, la niña se enfada haciendo explotar su descomunal cosmos y presentadose ante todos como Palas, Diosa del amor, todos han sentido el cosmos de la Diosa, Seiya de Sagitario que derrota a todos los soldados en su camino con su Trueno Atómico se dirige inmediatamente para allá pero Titán es quien llega primero para proteger a su Diosa, los jóvenes caballeros los rodean y se preparan para atacar elevando sus cosmos pero Titán se presenta ante ellos como el palasiano de Primer Nivel Titán de la Espada Teogénesis, mostrando ante todos su Génesistector y el poder abrumador de su espada, despojando a todos los jóvenes caballeros de sus cosmos y dejándolos inmóviles, la Diosa Palas le da la opción a Kōga de abandonar a Athena y jurarle lealtad pero Kōga se niega rotundamente es entonces que Palas le ordena a Titán acabar con la vida de Pegaso, cuando Titán estaba a punto de cumplir con su cometido llega Seiya para salvarlo, lanzandole una de sus técnicas Flecha Dorada de Sagitario a Titán pero este no sufre ningún rasguño, Seiya que esta vez esta seguro de atacar a la Diosa Palas vuelve a usar la técnica Flecha Dorada de Sagitario pero Titán bloquea el paso de la flecha con su espada para luego Palas usando su propio cosmos se la devuelve a Seiya pero Kōga logra moverse y usarse a si mismo como escudo humano para proteger a Seiya, la flecha destruye parte de la hombrera izquierda de la armadura de Kōga y dejandole una profunda herida cerca de la clavícula, Titán se dispone a pelear contra Seiya y Kōga pero Palas quiere irse y Titán obedece desapareciendo ante los ojos de todos, más tarde en el Palacio Belda, Palas descansa profundamente en su cama para luego aparecer ante Titán, Hyperion, Galia y Egeón volviéndoles a ofrecer su ayuda. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
