<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 56 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 56: ¡Resuena en mi corazón! ¡El grito de Haruto!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_55.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_57.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/3BBC09D5EB0BCABF" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga y Subaru han llegado a Japón pero no saben por donde empezar a buscar hasta que Subaru ve un cartel de música en donde Haruto aparece, ambos caballeros van al concierto en donde se está presentando Haruto y este se da cuenta de que su amigo Kōga esta viéndolo, Haruto se toma un descanso mientras habla con Kōga y Subaru, Kōga quiere llevarlo a Jamir a que repare su armadura para después reclutarlo ya que Athena necesita de sus caballeros otra vez, pero Haruto dice que antes de regresar a Japón, él ya había ido a Jamir a reparar su armadura y también dijo que no quiere pelear porque no tiene la necesidad de hacerlo, ya que ya ha vengado la muerte de su hermano, Subaru se molesta y lo ataca pero Haruto se defiende atacándolo también dejándolo en el suelo para luego dejarles en claro que su camino es ahora la música e irse; en su camerino Haruto descansa pero su conciencia empieza a atacarlo haciéndole ver que él es un Caballero de Athena sin embargo Haruto se niega a creerlo, Haruto se dirige al escenario a cantar pero Kōga y Subaru que estaban cerca de los alrededores sienten la presencia de un batallón de guerreros palasianos comandados por un palasiano de Tercer Nivel llamado Hati de Daga, los caballeros se colocan sus armaduras y derrotan fácilmente a los palasianos, Hati los ataca rodeándolos en circulo alrededor de ellos de cuchillos y los ataca una y otra vez, los caballeros intentan atacar pero caen rápidamente, Kōga eleva su cosmos y junto con Subaru atacan a Hati pero descubren que el palasiano está peleando con los ojos cerrados, él palasiano explica que no tiene necesidad de abrirlos ya que sus agudos oídos pueden escuchar hasta el flujo sanguíneo, luego ataca a Kōga y a Subaru dejándolos rápidamente tirados en el suelo, Haruto que estaba sintiendo el cosmos de la pelea abandona el escenario para ir a ayudar a sus amigos, Hati que escucha a Haruto acercarse lo ataca con sus armas pero Haruto los esquiva fácilmente y ataca a Hati viendo que este está peleando con los ojos cerrados, Hati lo ataca dejándolo también en el suelo, Hati para provocar a Haruto usa su técnica palasiana Retraso de Tiempo, las personas de los alrededores que ven una luz acercarse huyen aterradas pero no llegan lejos quedando hasta los músicos (compañeros de Haruto) petrificados, Haruto se enfada y eleva su cosmos al máximo, su armadura responde al cosmos de Haruto el nuevo poder de su armadura rompe la cloth-stone dándole una nueva forma a su armadura, vistiendo a Haruto y peleando cuerpo a cuerpo contra Hati pero este bloquea sus golpes con los ojos cerrados, sin embargo como su velocidad aumentó Haruto le da un fuerte golpe en la cara para luego atacarlo con su técnica Piedras Rodantes, pero a Hati no le sucede nada ya que viaja entre las rocas, atacando a Haruto con su técnica Lluvia Sangrienta cayendo herido al suelo, Haruto le pide ayuda a Kōga y este acepta, Haruto eleva su cosmos y aúlla con la misma fuerza al igual que un lobo, Hati que escucha los aullidos de Haruto queda totalmente aturdido y Kōga eleva rápidamente su cosmos al máximo y aprovecha el momento para atacarlo con su Meteoro de Pegaso, pero un palasiano de Segundo Nivel llamado Rea (que había estado escondido entre el público) interfiere, conteniendo la técnica de Kōga con una sola mano, que vino solamente para probar dicha técnica del Asesino de Dioses, Kōga de Pegaso, pero ambos palasianos deciden irse, Haruto de Lobo vuelve a aceptar su camino como Caballero de Athena y decide regresar al Santuario junto con Kōga y Subaru. 
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
