<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 72 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 72: ¡La cloth, heredara! ¡Subaru de Caballo Menor, nace!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_71.php.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_73.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/908F568AEE7C60D0" width="600" height="360" frameborder="0" scrolling="no"></iframe>                                </div>
                                <p>
                                    Celeris despierta repentinamente de su sueño, cuando conoció por primera vez a Kitalpha de Caballo Menor, dirigiéndose hacia las alcantarillas para despedirse de los jóvenes caballeros pero Subaru que esta preocupado por Celeris empieza a dudar en querer irse y dejar a Celeris solo, así que a mitad de camino, Subaru les avisa a los jóvenes caballeros que se va a quedar para ayudar a Celeris y emprende el viaje de regreso, Hati de Daga que lo había estado escuchando todo ataca el refugio en donde se encuentra Celeris, obligandolo a salir del refugio a pesar de que no se a recuperado de sus heridas, Subaru preocupado llega al encuentro, Celeris le cuenta a Subaru acerca de como obtuvo la Armadura de Caballo Menor, quien su anterior portador fue Kitalpha, que murió protegiendo a Celeris y Selene pero antes le enseña a Celeris la técnica con la que derroto a todo un ejercito de soldados palasianos y la armadura acepta a Celeris como su nuevo portador, Hati aparece y ataca repentinamente a Subaru y Celeris, por otro lado en las alcantarillas, los jóvenes caballeros se consiguen con los gemelos palasianos de tercer nivel Greip del Alma Asesina y Cyrene del Alma Asesina empezando así la batalla, Celeris y Subaru atacan juntos a Hati pero este defiende con su dagas estrellando a ambos contra el suelo, Hati se acerca rápidamente a Celeris y lo ataca con sus dagas sin cesar, dejando a Celeris gravemente herido y su armadura a empezado a agrietarse, Subaru corre a donde esta Celeris y lo protege atacando a Hati, este bloquea el golpe con su dagas, Subaru eleva su cosmos y usa su técnica Impacto Pléyades mientras que Hati usa su técnica Velocidad Sangrienta, al principio Subaru iba ganando pero Hati rápidamente usa su técnica Lluvia de Dagas haciendo que la técnica de Subaru se anule, hiriéndolo y su armadura de acero queda totalmente destruida, Hati se prepara para darle el golpe final a Subaru pero Celeris se levanta y embiste con una gran fuerza a Hati, el palasiano se levanta sorprendido de que Celeris pueda todavía moverse a pesar de sus heridas, Hati furioso, fusiona todas sus dagas y usa su mejor técnica Huracán Sangriento, Celeris eleva todo su cosmos y le enseña a Subaru la técnica que heredó de Kitalpha Máxima Explosión de Supernova, anulando las armas, técnica y matando a Hati con la enorme explosión de la técnica de Celeris pero también el caballero de bronce recibe parte del daño, un poco más tarde, Celeris quien tiene todo su cuerpo ensangrentado le hace entrega a Subaru la cloth-stone de Caballo Menor, despidiéndose de él para luego fallecer, por otro lado en las alcantarillas, la pelea de los jóvenes caballeros continua, Cyrene usa su técnica Exclusión Diabólica mientras que Greip usa su técnica Ventisca Diabólica en contra de los jóvenes caballeros, ambos palasianos disfrutan causar confusión a los caballeros pero Subaru llega para pelear contra los palasianos, la mano herida de Subaru baña en sangre la cloth-stone logrando así despertar la nueva armadura de Caballo Menor, la armadura empieza a vestir a Subaru, naciendo así Subaru de Caballo Menor, el nuevo caballero de bronce le hace frente él solo a los dos palasianos, ambos palasianos usan su técnica Guadaña Mortal pero Subaru no sufre daño alguno, ambos palasianos empiezan a sentir la desesperación y usan su mejor técnica Tempestad Gemela Diabólica mientras que Subaru usa la técnica que heredó de Celeris Máxima Explosión de Supernova, lanzándola contra los gemelos palasianos y matándolos al instante, sin embargo, cuando Subaru celebraba su victoria, la armadura de Caballo Menor rechaza a su nuevo portador.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
