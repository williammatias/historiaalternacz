<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 65 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 65: ¡Romper la puerta de la muralla de hierro! ¡La lanza de Pegaso y el escudo de Dragón!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_64.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_66.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/752BBF558BF916F8" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Con las puertas de la entrada a la ciudadela destruidas cada vez más caballeros llegan para ayudar, Seiya de Sagitario ahora es el líder de las fuerzas de ataque y junto a él están caballeros de plata, bronce y acero, en su recorrido por la cuidad se dan cuenta de que las estructuras de los edificios fueron construidas por el hombre y cuando se fijan bien dentro de ellos, hay personas que están bajo los efectos de Retraso de Tiempo, sin embargo soldados palasianos salen de todos los edificios de los alrededores formando un batallón, Seiya les solicita que los dejen pasar y los soldados en respuesta les lanzan sus armas, Seiya con un solo dedo detiene todas las armas y los ataca con su técnica Meteoro de Pegaso para así por su propia cuenta avanzar para que la gran mayoría de los soldados lo sigan y abrirles paso a los caballeros, todos los caballeros toman otro camino y en su largo recorrido se encuentran con el palasiano de Tercer Nivel y discípulo de Titán Tarvos de Lucero de Alba que ahora posee una cronotech más resistente que la anterior,Tarvos reta a los jóvenes caballeros a que lo ataquen pero son los Caballeros de Acero quienes deciden hacerlo usando sus armas Flechas de Acero logran acertarle a Tarvos pero este sale totalmente ileso, Tarvos responde con su técnica Hipermeteoro de Hierro y la gran mayoría de los caballeros de acero salen heridos a excepción de los jóvenes caballeros, Tarvos quien parece una enorme muralla de hierro, es Kōga de Pegaso quien decide quedarse y ajustar cuentas pendientes con Tarvos, Subaru a regañadientes se va con los demás, ayudan a los heridos y toman otro camino, Kōga y Tarvos se atacan entre ellos con todas sus fuerzas, Kōga usa su técnica Meteoro de Pegaso pero esta vez la cronotech de Tarvos no sufre daño alguno, este responde con su técnica Hipermeteoro de Hierro, Kōga trata de esquivar la técnica pero logra lastimarlo, Tarvos no desaprovecha el tiempo y le lanza su enorme arma, la lucero de alba pero Ryūhō llega justo para salvarlo usando su escudo, Kōga le recrimina el haber regresado, pero Ryūhō le pide a su amigo que piense con la cabeza fría, es entonces que Ryūhō recuerda la historia de la lanza y el escudo y se lo cuenta a Kōga para que él sea la "lanza" mientras que Ryūhō es el "escudo", Tarvos se enfurece y hace arder el cosmos de su arma para luego atacar con su técnica Hipermeteoro de Hierro, Ryūhō se encarga de la defensa, usando su escudo detiene cada uno de los ataques de Tarvos, mientras que Kōga responde con su Meteoro de Pegaso, ambos caballeros empiezan a trabajar en equipo haciendo que Tarvos llegue al punto de perder la paciencia y use su poderosa técnica Bazuca Meteoro, Ryūhō vuelve a usar su escudo para evitar que Kōga reciba directamente el poderoso ataque y contra-ataca con su técnica Dragón Naciente destruyendo así su arma, sin darse cuenta Kōga aparece en frente de él y lo ataca con su técnica Cometa de Pegaso volviéndole a destruir su cronotech y luego emplea todo su cosmos para usar su técnica Destello rodante de Pegaso dejando a Tarvos prácticamente muerto; por otro lado Galia le expresa sus "condolencias" a Titán por la perdida de su discípulo, pero en ese entonces la Diosa Palas tiene una pequeña metamorfosis en la que "crece" un poco más y aumentando descomunalmente su cosmos. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
