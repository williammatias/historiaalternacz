<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 67 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 67: Increíble cosmos, Subaru! ¡La misión de Eden!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_66.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_68.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/C0906DB544C97740" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Subaru y Sōma corren por las calles para llegar al Palacio Belda pero Subaru se adelanta y alcanza a Kōga pero ambos se esconden de un gran ejército de soldados palasianos que se interponen en su camino, Subaru pierde los estribos y se lanza contra los soldados palasianos, derrotando al enorme ejército él solo pero aun así su sed de batalla no se calma siendo Sōma y Ryūhō quienes lo detienen; por otro lado, Eden de Orión también se encuentra de paso debido a que su sangre de semidiós lo a guiado hasta ese lugar pero se encuentra con pequeños grupos de soldados palasianos a quien Eden los derrota fácilmente, en su recorrido se encuentra con el palasiano de Segundo Nivel Europa de Corte Segador quien fue enviado por Titán para buscar y asesinar a los jóvenes caballeros pero se encuentra con caballeros de acero quienes atacan al palasiano pero este se divierte con ellos derrotándolos fácilmente y a uno de ellos lo agarra del cuello ahorcándolo, Eden y Europa intercambian palabras pero lo que más aturdió a Eden fue que le dijo la verdad en que él que aun vive bajo la sombra de su padre y esta buscando la respuesta que le de sentido a su vida, Eden provocado por las palabras de Europa lo ataca pero este fácilmente esquiva su ataque y sigue provocando a Eden este invoca su armadura pero justo en ese momento llega Seiya de Sagitario y empieza la pelea contra Europa, ambos guerreros tienen la misma fuerza y pueden moverse a la velocidad de la luz pero Europa decide detener la pelea ya que tiene que cumplir una orden y necesitaría de mucho tiempo para derrotar a Seiya para luego el palasiano desaparecer, Eden le cuestiona en como él siendo un simple humano puede igualar a los Dioses, Seiya responde que lo hace para proteger a la gente que quiere y, que su respuesta la encontrara pronto para luego irse y seguir su camino hacia Palacio Belda; Europa que llega levitando, se encuentra con los jóvenes caballeros y es Subaru quien impulsivamente lo ataca, Europa le responde lanzandole su arma las cuchillas segadoras pero sorprendentemente Subaru las esquiva muy fácilmente Europa se molesta, volviéndolo a intentar y esta vez logra darle, dejando a Subaru inconsciente, Sōma y Ryūhō atacan rápidamente a Europa con una serie de golpes pero Europa esquiva cada golpe fácilmente para luego lanzarle a cada uno su arma dejándolos también inconscientes pero se da cuenta que ellos solo eran la distracción solo para que Kōga de Pegaso atacara con su Meteoro de Pegaso por detrás de él, Europa finge haber recibido la técnica pero solo para sorprenderlo por detrás de Kōga y echarle en cara que el haber derrotado fácilmente a Aegir del Brazo Fantasma fue porque él ya había agotado todas sus fuerzas en la batalla contra Genbu de Libra, Kōga cae en las provocaciones de Europa y trata de golpear a Europa pero este detiene el golpe con una sola mano y le demuestra que lo que dijo es cierto, apretando y agrietando muy fácil el puño derecho de la armadura para luego lanzarle su arma dejando a Kōga inconsciente, Europa se apresura en cumplir su misión en asesinar a Pegaso pero Eden se lo impide, invoca su nueva armadura de Orión y empieza la pelea contra Europa, usando su técnica Trueno Furioso pero Europa bloquea cada golpe usando su arma para luego Eden lo ataca con su técnica Orión Devastador recibiendo el golpe directamente sin embargo Europa que sale ileso se da cuenta de los planes de Eden y va de nuevo a asesinar a los jóvenes caballeros usando su técnica Cuchilla Terrible los jóvenes caballeros que apenas se están despertando es Eden quien bloquea la técnica usando su propio cuerpo, Europa le cuestiona su actitud en que si él es un semidiós debe de actuar más fríamente y no comportarse como un humano, Eden responde que para protegerlos él no desea el poder de un Dios, Subaru al escuchar las palabras de Eden, estalla en ira y explota su propio cosmos dejando a todos sorprendidos, un poderoso y descomunal cosmos muy parecido al cosmos de oscuridad rodea a Subaru y es él que neutraliza la técnica de Europa y lo golpea, Europa detiene el golpe con una sola mano pero aun así no es capaz de contener tan semejante golpe obligando al palasiano a arrodillarse ante un Caballero de Acero, Europa para detenerlo intenta asesinar a Subaru pero Eden rápidamente reacciona y ataca a Europa con su técnica Orión Exterminador dejando a Europa herido y obligandolo a retirarse de la batalla, el descomunal cosmos de Subaru desaparece y Eden confirma que lo que lo atrajo hasta ese lugar fue la sangre de Dios y el cosmos de Subaru, para luego este decidiera acompañar a los jóvenes caballeros para vigilar más de cerca a Subaru. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
