<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya Omega - Episodio 57 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="omega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya Omega</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 57: ¡Derrota a Pegaso! ¡Eden, el guerrero solitario!</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_56.php" class="icon icon-arrow-left"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_58.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/FC2BC29E04C59BAC" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Kōga, Subaru y Haruto llegan al Santuario por la noche pero inmediatamente Shaina le encomienda a Kōga y Subaru otra misión, la búsqueda y rescate del Caballero de Plata Enéada de Escudo que se encuentra en Italia ya que hay una gran actividad palasiana en ese lugar, en Italia, Enéada se encuentra peleando con Thebe de Martillo, Enéada se encuentra con una pequeña niña que se encuentra bajo los efectos de Retraso de Tiempo y decide protegerla del palasiano usando su enorme escudo, Thebe lo ataca con su técnica Martillo Demoníaco, creando un enorme cráter, rompiéndole el escudo a Enéada y dejándolo herido, Kōga y Subaru llegan a Italia pero en búsqueda de Enéada se encuentran con un ambiente devastador y en medio ven a un derrotado Enéada en el cual les dice a Kōga y a Subaru que es una trampa apareciendo inmediatamente un palasiano de Segundo Nivel llamado Europa de Corte Segador pero este solo los ataca mandándolos volar hacia un cementerio en donde se encuentra la lapida de Aria y al lado esta un palasiano de Tercer Nivel llamado Eden de Clava, Kōga le recrimina a Eden sus acciones acerca de haberse unido con los palasianos, Eden se queda callado sin responder una sola palabra aunque Europa le responde esa pregunta diciéndole que Eden se había unido a los palasianos porque él le propuso revivir a sus seres queridos a cambio de que se les uniera para que luego Eden atacara a Kōga y a Subaru estos se preparan para atacar pero Thebe de Martillo ataca a traición a Kōga, a Subaru y a Eden que vino solo con la intención de molestarlos, Thebe le propone un trato a Eden que si se les arrodilla no destruirá el cementerio, Eden acepta pero solo para darle un fuerte golpe en la cara de Thebe, el palasiano usa su técnica Meteoro Gigante en contra de Eden y este lo recibe directamente, a Eden no le pasa nada pero su cronotech queda totalmente destruida, Eden les dice a los palasianos que solo se les unió con la intención de ser un espía, él nunca creyó que los muertos revivirían para luego este encender su cosmos y despertar su nueva armadura de Orión, Eden ataca a Thebe con su técnica Danza del Trueno dejándolo inconsciente, Subaru intenta rematarlo pero Europa lo tele-transporta y desaparecen, más tarde, Eden se despide de Kōga y Subaru y, luego se despide de la lapida de Aria. 
                                </p>

                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
