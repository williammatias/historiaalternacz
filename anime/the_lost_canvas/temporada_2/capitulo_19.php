<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 19 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_19">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 19: La espada más fuerte</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_18.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_20.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/57ED5039F87C7BF5" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    El Cid, Santo dorado de Capricornio es enviado a una misón con la finalidad de liberar el alma de Sisifo que se encuentra atrapado en el mundo de los sueños (Yumekai), en esta misión conoce a los hijos de Hypnos que son dioses menores de los sueños (Ikelos, Phantasos, Morpheo y Oneiros), llega a luchar con el primero, utilizando su ataque cortante, pero El Cid no puede contra Ikelos que hace que su propio ataque corte su brazo derecho. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
