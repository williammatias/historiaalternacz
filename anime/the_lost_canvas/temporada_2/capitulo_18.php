<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 18 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_15">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 18: Sólo quiero que vivas</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_17.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_19.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/E64C4E88001A26EF" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Thanatos abre la hiperdimension donde el cuerpo del humano que utiliza es destruido junto con Manigoldo, pero el alma de Thanatos aun esta dispuesta a salir victoriosa e intenta poseer el cuerpo de Sage quien demuestra haber previsto esto y sella al Dios dentro de su cuerpo. El combate termina con dos grandes bajas en el ejército de Athena. Manigoldo se encuentra con Shion despidiendose de este.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
