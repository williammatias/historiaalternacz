<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 17 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_15">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 17: Basura</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_16.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_18.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/9F2244FB30B7271B" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Capitdo se enfrenta a Thanatos que despliega su poder de dios. Cuando Thanatos tiene el combate ganado es interrumpido por Sage (el gran patriarca), que ayuda a su discípulo a pelear contra Thanatos, a pesar de haber utilizado sellos impregnados con el poder de Athena, Thanatos aun se muestra muy superior. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
