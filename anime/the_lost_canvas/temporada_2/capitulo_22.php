<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 22 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_22">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 22: El camino de la lealtad</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_21.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_23.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/4FB6587AD192CBEB" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Oneiros ataca con su Guardian's Oracle (ガーディアンのオラクルGuardian's Oracle?) a los dos santos pero no causa efecto porque Athena ha llegado al Mukai para despertar a Sisifo. Los dos caballeros atacan en conjunto saliendo al mundo real logrando dañar un poco al dios que tiende a atacar de nuevo con su devastador poder; cerca de allí hay tres caballeros que inician el ataque al dios pero son fácilmente vencidos, teniendo la perdida de sus subordinados El Cid contraataca con ayuda de Tenma pero ningún ataque logra dañarlo, pero Tenma insiste ya que El Cid esta exhausto y fuera de combate por haber perdido mucha sangre.Tenma a punto de ser derrotado es salvado por Yato y Yuzuriha que logran ayudar y atacar junto con Tenma pero el poder de Oneiros (fusionado con los otros tres dioses derrotados) es muy superior.                
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
