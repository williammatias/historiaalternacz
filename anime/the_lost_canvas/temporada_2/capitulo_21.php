<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 21 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_15">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 21: Más allá de los sueños</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_20.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_22.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/94A2BB5983E3A5C2" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Ikelos enfrenta a El Cid, logrando el dios poder herir muchas veces al Santo, pero este tenia un plan desde el comienzo; consistía en dispersar su sangre para vencerlo. Mientras Tenma se enfrenta a si mismo, logra salir de su sueño viéndose con Morpheo, que lo ata con unas flores, los sueños de Pegaso hacen que su armadura se convierta en una armadura divina derrotando al dios. El Cid ve como el mundo de los Sueños se distorsiona y luego, acaba encontrándose con Tenma; con la misión de liberar de esa prisión de sueños a Sisifo, ataca sin ningún efecto, apareciendo Oneiros convocando a sus hermanos caídos, logra unirse para pelear contra los dos santos. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
