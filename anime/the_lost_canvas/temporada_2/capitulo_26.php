<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 26 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_26">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 26: Sé tú mismo</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_25.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="../../the_lost_canvas.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/6F1105F3C1149CB8" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Hakurei logar vencer a Hypnos utilizando el Sekishiki Tenryo Ha (精霊の行進 Marcha de los espíritus?) con la fuerza de sus Camaradas en la Guerra Santa anterior. Cuando está por disolver la barrera de Hades, el dios se presenta y asesina a Hakurei. Un enfurecido Shion ataca a Hades sin resultado alguno, ante la gran fuerza del dios del Inframundo, que desea matar a los dos santos, no obstante llegan a la batalla Dohko, Yato y Tenma, quienes tampoco pueden con la grandísima fuerza de Alone que ahora porta su Serplice de Hades, atacando a deseo a sus contrincantes. Dohko en desesperación logra atarcar a Hades pero también es inútil, elevando su cosmo hace que Tenma y los demás salgan del castillo de Hades, quedándose el a pelear. Tenma cree que Dohko ha caído en batalla y desea ser más fuerte. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
