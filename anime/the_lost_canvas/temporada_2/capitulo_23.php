<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 23 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_15">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 23: La Espada Sagrada</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_22.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_24.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/847FB53CCC4B264F" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Athena ve en el sueño de Sísifo que el siente culpa de haber separado a Sasha de Alone y Tenma, y por efecto de la guerra Santa contra Hades, quitándose el mérito de ser el santo de Sagitario su armadura se va de su cuerpo disparádole la flecha, logrando que su sangre se convierta en una Sapuri. Athena logra convencer al Santo de Sagitario que el no tuvo la culpa de los hechos aceicidos, le abraza y le hace recordar la promesa que le hizo ese día a Tenma logrando así recobrar su armadura dorada. Ya en el Santuario los dos mandan una flecha dorada para ayudar a El Cid que sin ganas de luchar se pone de pie usando su ataque logra dividir la flecha en cuatro logrando vencer a Oneiros, mas este no se da por vencido y se avalanza contra Tenma interviniendo El Cid para proteger a Tenma este se despide, al final de su vida el Santo de Capricornio logró utilizar Excalibur. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
