<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 20 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_20">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 20: La prisión de los Sueños</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_19.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_21.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/C0D5A93070F31347" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    El Cid es supuestamente vencido por Ikelos, pero aparece para vencer a Phantasos de un solo golpe. Mientras tanto Tenma y sus dos compañeros van por el bosque, siendo el caballero de Pegaso enviado a un sueño por Morpheo, viendo el pasado y al Santo de Sagitario, dándose cuenta que no es un sueño de verdad. Ikelos que ha sentido un remezón por la caída de Phantasos va a enfrentar a El Cid 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
