<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 24 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_15">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 24: Hora de una sangrienta batalla</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_23.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_25.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/92BFBF522447F712" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    La barrera en el castillo de Hades ha sido deshabilitada por Sísifo de Sagitario. Hakurei de Altar, haciéndose pasar por el patriarca, habla a las tropas de Athena para iniciar el ataque al lugar donde se encuentra Hades. Sin embargo él tiene otra misión, sellar al otro dios gemelo, Hypnos. Todos los santos se preparan para la última pelea y son teletransportados por Atla hacia el castillo. Shion y Dohko van por su cuenta desobedeciendo las órdenes de Sísifo, encontrándose con Tenma y sus amigos, mientras el antiguo Santo de Altar, Hakurei, se acerca más a la entrada del castillo, y también a su lucha contra Hypnos. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
