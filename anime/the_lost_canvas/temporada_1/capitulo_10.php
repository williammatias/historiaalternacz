<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 10 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 10: Advenimiento</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_9.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_11.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/5EE3DA93BF764880" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Dohko irrumpe en el combate entre Kagaho y Aldebarán e intenta atacar al espectro. Sin embargo Aldebarán le ordena detenerse ya que el aún no está muerto. Liberándose del Crucify Ankh (十字架アンクCrucify Ankh?), El Santo de Oro comienza a combatir de nuevo contra Kagaho, quién le lanza 3 Coronas Blast (クラウンブラストCoronas Blast?) sorprendiendo a Tauro, pero finalmente Aldebarán lo vence usando su potente Titan's Nova (タイタンの新星Titan's Nova?). Más Tarde al volver al Santuario, este es atacado por Hades que penetra en la barrera de Athena e inmoviliza a todos los Santos del Santuario. El Santo de Oro de Sagitario logra dispararle una flecha a Hades, pero éste la detiene súbitamente y la devuelve contra Sísifo alcanzando su corazón. En ese momento irrumpe Tenma con el rosario de las 108 cuentas, más que dispuesto para enfrentarse al dios de los muertos.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
