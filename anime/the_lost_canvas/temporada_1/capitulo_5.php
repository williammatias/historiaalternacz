<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 5 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 5: La rosa venenosa</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_4.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_6.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/984AE95E3A07B35A" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Hades se entera por Pandora de la derrota de Fedor de Mandrágora y de la posible resurrección de Pegaso; Tenma, Yato y Yuzuhira corren tratando de adrentarse a lo más profundo del infierno para cumplir la misión del Maestro de Jamir; mientras Minos y sus soldados son interceptados por Albafika con su jardín de rosas, entonces interviene Niobe de Deep quien lo ataca, Piscis al probar sus habilidades ve que no es problema ya que su sangre es un veneno mortal y lo vence; Minos utiliza su técnica para dispersar el jardín de rosas, luego controla los movimientos del Santo de oro y manda a sus espectros a atacar Rodorio, un pueblo cercano al Santuario, el Juez empieza a torturar a Albafika, pero él no pierde el espíritu.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
