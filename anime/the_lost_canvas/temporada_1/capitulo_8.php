<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 8 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 8: Un día con viento suave</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_7.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_9.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/687A75BD628B25A0" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Tenma cree haber golpeado a Asmita con su ataque, pero no era más que una ilusión, en realidad, lo que golpeó fue al Árbol sagrado de la Sabiduría que reacciona ante el cosmos de Tenma y deja caer sus últimos frutos, Asmita le dice que tome 108 de ellos y se los lleve al Maestro de Jamir. Tenma descubre que los espectros de Hades son inmortales y que el proposito de Asmita es que con la ayuda del maestro de Jamir, logren crear un rosario que sirva para sellar a los espectro y no vuelvan a recusitar más, sin embargo unos Espectros atacan la torre de Jamir y después de largo combate, son derrotados definitivamente por Asmita cuando este completa el rosario a costa de su propia vida. Este le encomienda a Tenma el cuidado de Athena.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
