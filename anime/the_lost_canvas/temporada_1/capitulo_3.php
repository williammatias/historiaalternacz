<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 3 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 3: Comienza la Guerra Santa</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_2.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_4.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/D6C26F3EB0D4398C" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Tenma y Yato han sido ascendidos a Santos de bronce e inmediatamente les es asingada la misión de ir a un pueblo florentino que ha sido atacado por los espectros, Tenma descubre de que se trata de su pueblo natal, el cual ha sido destruido. Es entonces cuando Tenma vuelve a encontrarse con Alone convertido en Hades, y después de un amargo combate, Tenma es "asesinado" por su viejo amigo y la ciudad terminada de arrasar. Después del colapso de la ciudad, Yato trata de ayudar en Tenma pero se encuentra con una joven llamada Yuzuhira.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
