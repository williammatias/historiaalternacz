<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 6 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 6: Funeral de flores</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_5.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_7.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/B51BD1F4EEF8BB2F" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    El combate entre Minos y Albafika continua su curso, para ese mommento interviene Agasha, una joven amiga del santo de piscis con el propostio de confrontar a Minos, este trata de matarla pero también aparece Shion para ayudar. Con la ayuda del santo de Aries, Albafika logra derrotar al al juez infernal pero muriendo en el mismo combate, valorando la belleza de las rosas. Hades se entera de los hechos y revive a los espectros muertos para que participen en la Guerra Santa de nuevo; mientras un enviado de Jamir viene al Santuario con noticias de que los espectros pueden revivir y la posibilidad de resucitar a Tenma en un día. 
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
