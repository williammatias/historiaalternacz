<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 11 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 11: Inalcanzable</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_10.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_12.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/62A5C232A0AA68CB" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Tenma está dispuesto a enfrentar a Alone en combate ya que éste hirió de gravedad a Sísifo e inmovilizó a los santos del Santuario. Pegaso junto con el Patriarca intenta sellar a Hades en la Gran Torre pero no lo logran ya que Pandora aparece, ésta encara a Sasha por nacer como hermana de Hades. Tenma, consternado, ve como Alone se retira junto a Pandora pero no está dispueto a dejarlo huir, aunque a pesar de sus esfuerzos éste se retira dejándole el mensaje de que todos morirán cuando termine la pintura que está haciendo: El Lienzo Perdido, una pintura que alberga a todas la almas humanas del mundo y que despliega en el cielo.  
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
