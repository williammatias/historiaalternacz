<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 13 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_13">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 13: El viaje</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_12.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="../temporada_2/capitulo_14.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/78BF301BF029C2F2" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Tenma está consternado por la muerte de Aldebarán y piensa en huir del Santuario, pero Manigoldo de Cáncer se lo impide y lo encierra en un calabozo. Sasha habla con el Patriarca de esto, puesto que Tenma es pieza clave en la Guerra Santa, pero éste huye con la ayuda de Yato y Yuzuriha (convertida en Santo de plata). Posteriormente se despide de Sasha y emprende su camino hacia los Bosques de la Muerte.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
