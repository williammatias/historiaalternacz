<!DOCTYPE html>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - Episodio 12 - SaintSeiyaSigma.com</title>        

        <?php
        include '../../../template/head.php';
        ?>
    </head>
    <body class="the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1 capitulos">

            <div class="container capitulo_1">
                <div class="row">
                    <div class="u8 skel-cell-mainContent" id="content">
                        <article id="main">
                            <div class="u12">
                                <h2>Episodio # 12: Sacrificios interminables</h2>
                                <div class="row navbuttons">
                                    <div class="u6">
                                        <a href="capitulo_11.php" class="icon icon-home"></a>
                                    </div>
                                    <div class="u6">
                                        <a href="capitulo_13.php" class="icon icon-arrow-right"></a>
                                    </div>
                                </div>    
                                <div class="video">
                                    <iframe src="http://www.putlocker.com/embed/6FCA7E0A125D6739" width="600" height="360" frameborder="0" scrolling="no"></iframe>
                                </div>
                                <p>
                                    Inicia la reconstrucción del Santuario, y Tenma empieza a entrenar junto con Aldebarán. Pandora, preocupada por la visita de Hades al Santuario, envía a dos asesinos a matar a Tenma pero no lo consiguen debido a que este es rescatado por Aldebarán; el Santo de Tauro decide enfrentar a los espectros a quienes logra derrotar con su técnica más poderosa, aunque sacrifica su vida para ello, Tenma trata de enfrentar a Kagaho que sobrevive, pero este lo rechaza y decide partir comentando que no obedecerá ninguna orden que no venga directamente del señor Hades.
                                </p>
                            </div>
                        </article>

                    </div>
                    <div class="u4" id="sidebar">
                        <!-- Sidebar -->
                        <?php
                        include '../../../template/aside.php';
                        ?>
                    </div>
                </div>


            </div>
        </div>

        <!-- Features -->
        <?php
        include '../../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../../template/footer.php';
        ?>
    </body>
</html>
