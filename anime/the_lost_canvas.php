<!DOCTYPE HTML>
<html>
    <head>
        <title>Saint Seiya The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="En la anterior guerra santa contra Hades ocurrida antes del Saint Seiya original donde se explica muchos cosas concernientes al mundo de Saint Seiya Los Caballeros del Zodiaco." />

        <link rel="stylesheet" href="../css/jquery-ui.css" />

        <?php
        include '../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/anime/SaintSeiyaTheLostCanvas.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/anime/SaintSeiyaTheLostCanvas.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/anime/SaintSeiyaTheLostCanvas.jpg"/>

        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
            $(function() {
                $("#accordion").accordion();
            });
        </script>


    </head>
    <body class="anime the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Saint Seiya The Lost Canvas</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <header>
                                <h2>Capitulos Disponibles</h2>
                                <span class="byline">
                                    <script type="text/javascript"><!--
                                        google_ad_client = "ca-pub-6835894736815485";
                                        /* SSLeaderBoard */
                                        google_ad_slot = "1201595250";
                                        google_ad_width = 728;
                                        google_ad_height = 90;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>
                                </span>
                            </header>
                            <div class="row">
                                <div class="u2">                                
                                    <script type="text/javascript"><!--
   google_ad_client = "ca-pub-6835894736815485";
                                        /* SSWideSkysraper */
                                        google_ad_slot = "2399126850";
                                        google_ad_width = 160;
                                        google_ad_height = 600;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>
                                </div>
                                <div class="u8">

                                    <div id="accordion">
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 1: Promesa</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_1.php">Ver Capitulo</a>
                                                    <p>
                                                        Tenma y Alone son dos niños huérfanos que viven en un pueblo italiano. Su vida se ve interrumpida cuando Pandora se encuentra con Alone y le dice que él es la reencarnación de Hades, mientras tanto Tenma se encuentra con Dohko de Libra, quien lo lleva al Santuario para convertirse en santo de Athena.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 2: El despertar de Hades</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_2.php">Ver Capitulo</a>
                                                    <p>
                                                        En el Santuario, Tenma se reencuentra con Sasha, la hermana menor de Alone, quien es la reencarnación de la diosa Atenea. Es atacado por Raimi de Worm, y acaba siendo vencido por Shion. Dos años más tarde Tenma, se convierte en el Santo de Pegaso, mientras tanto Alone es llevado a la catedral del bosque, donde el alma de Hades despierta.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 3: Comienza la Guerra Santa</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_3.php">Ver Capitulo</a>

                                                    <p>
                                                        Tenma y Yato han sido ascendidos a Santos de bronce e inmediatamente les es asingada la misión de ir a un pueblo florentino que ha sido atacado por los espectros, Tenma descubre de que se trata de su pueblo natal, el cual ha sido destruido. Es entonces cuando Tenma vuelve a encontrarse con Alone convertido en Hades, y después de un amargo combate, Tenma es "asesinado" por su viejo amigo y la ciudad terminada de arrasar. Después del colapso de la ciudad, Yato trata de ayudar en Tenma pero se encuentra con una joven llamada Yuzuhira.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 4: La pulsera de oración</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_4.php">Ver Capitulo</a>

                                                    <p>
                                                        Yuzuhira aparece ante Yato y el cuerpo moribundo de Tenma, entonces los lleva con el anciano de Jamir, él revive la cloth de Unicornio y los envía al Reino de los Muertos para recuperar el alma de Tenma. El santo de Pegaso se despierta capturado por Fedor de Mandrágora, Yato y Yuzuriha encuentran a Tenma e intentan liberarlo, pero son vencidos por Fedor. Durante la pelea, Tenma se libera y vence a Fedor. Por otro lado el Juez Minos y un grupo de espectros llegan al Santuario para atacar.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 5: La rosa venenosa</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_5.php">Ver Capitulo</a>

                                                    <p>
                                                        Hades se entera por Pandora de la derrota de Fedor de Mandrágora y de la posible resurrección de Pegaso; Tenma, Yato y Yuzuhira corren tratando de adrentarse a lo más profundo del infierno para cumplir la misión del Maestro de Jamir; mientras Minos y sus soldados son interceptados por Albafika con su jardín de rosas, entonces interviene Niobe de Deep quien lo ataca, Piscis al probar sus habilidades ve que no es problema ya que su sangre es un veneno mortal y lo vence; Minos utiliza su técnica para dispersar el jardín de rosas, luego controla los movimientos del Santo de oro y manda a sus espectros a atacar Rodorio, un pueblo cercano al Santuario, el Juez empieza a torturar a Albafika, pero él no pierde el espíritu.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 6: Funeral de flores</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_6.php">Ver Capitulo</a>
                                                    <p>
                                                        El combate entre Minos y Albafika continua su curso, para ese mommento interviene Agasha, una joven amiga del santo de piscis con el propostio de confrontar a Minos, este trata de matarla pero también aparece Shion para ayudar. Con la ayuda del santo de Aries, Albafika logra derrotar al al juez infernal pero muriendo en el mismo combate, valorando la belleza de las rosas. Hades se entera de los hechos y revive a los espectros muertos para que participen en la Guerra Santa de nuevo; mientras un enviado de Jamir viene al Santuario con noticias de que los espectros pueden revivir y la posibilidad de resucitar a Tenma en un día. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 7: El árbol de los frutos sagrados</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_7.php">Ver Capitulo</a>
                                                    <p>
                                                        Tenma, Yato y Yuzuriha continúan su camino hacía el infierno, el cual recorren con muchos riesgos y peligros hasta donde se encuentra con un Santo dorado el cual se trata del hombre más cercano a los dioses, Asmita de Virgo, quien le revela una desagradable sorpresa, la cual resulta que el Santo dorado ha decido cambiarse de bando por considerar a Athena "una deidad inútil", Tenma se enfurece y decide pelear contra Asmita, hasta que descubre, el verdadero porposito de este. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 8: Un día con viento suave</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_8.php">Ver Capitulo</a>
                                                    <p>
                                                        Tenma cree haber golpeado a Asmita con su ataque, pero no era más que una ilusión, en realidad, lo que golpeó fue al Árbol sagrado de la Sabiduría que reacciona ante el cosmos de Tenma y deja caer sus últimos frutos, Asmita le dice que tome 108 de ellos y se los lleve al Maestro de Jamir. Tenma descubre que los espectros de Hades son inmortales y que el proposito de Asmita es que con la ayuda del maestro de Jamir, logren crear un rosario que sirva para sellar a los espectro y no vuelvan a recusitar más, sin embargo unos Espectros atacan la torre de Jamir y después de largo combate, son derrotados definitivamente por Asmita cuando este completa el rosario a costa de su propia vida. Este le encomienda a Tenma el cuidado de Athena.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 9: Una gran estrella</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_9.php">Ver Capitulo</a>
                                                    <p>
                                                        Los espectros asesinados por Albafika de Piscis reviven y planean atacar el Santuario, pero son interrumpidos por Aldebarán de Tauro quien los vence con un sólo ataque. Mientras tanto, el espectro Kagaho de Bennu irrumpe en la zona buscando a Dohko, sin embargo con quien se encuentra es con Aldebarán y comienzan a combatir. Ambos oponentes se encuentran muy igualados pero Aldebarán empieza a hacer uso de su velocidad y fuerza enfureciendo al espectro.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 10: Advenimiento</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_10.php">Ver Capitulo</a>
                                                    <p>
                                                        Dohko irrumpe en el combate entre Kagaho y Aldebarán e intenta atacar al espectro. Sin embargo Aldebarán le ordena detenerse ya que el aún no está muerto. Liberándose del Crucify Ankh (十字架アンクCrucify Ankh?), El Santo de Oro comienza a combatir de nuevo contra Kagaho, quién le lanza 3 Coronas Blast (クラウンブラストCoronas Blast?) sorprendiendo a Tauro, pero finalmente Aldebarán lo vence usando su potente Titan's Nova (タイタンの新星Titan's Nova?). Más Tarde al volver al Santuario, este es atacado por Hades que penetra en la barrera de Athena e inmoviliza a todos los Santos del Santuario. El Santo de Oro de Sagitario logra dispararle una flecha a Hades, pero éste la detiene súbitamente y la devuelve contra Sísifo alcanzando su corazón. En ese momento irrumpe Tenma con el rosario de las 108 cuentas, más que dispuesto para enfrentarse al dios de los muertos.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 11: Inalcanzable</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_11.php">Ver Capitulo</a>
                                                    <p>
                                                        Tenma está dispuesto a enfrentar a Alone en combate ya que éste hirió de gravedad a Sísifo e inmovilizó a los santos del Santuario. Pegaso junto con el Patriarca intenta sellar a Hades en la Gran Torre pero no lo logran ya que Pandora aparece, ésta encara a Sasha por nacer como hermana de Hades. Tenma, consternado, ve como Alone se retira junto a Pandora pero no está dispueto a dejarlo huir, aunque a pesar de sus esfuerzos éste se retira dejándole el mensaje de que todos morirán cuando termine la pintura que está haciendo: El Lienzo Perdido, una pintura que alberga a todas la almas humanas del mundo y que despliega en el cielo.  
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 12: Sacrificios interminables</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_12.php">Ver Capitulo</a>
                                                    <p>
                                                        Inicia la reconstrucción del Santuario, y Tenma empieza a entrenar junto con Aldebarán. Pandora, preocupada por la visita de Hades al Santuario, envía a dos asesinos a matar a Tenma pero no lo consiguen debido a que este es rescatado por Aldebarán; el Santo de Tauro decide enfrentar a los espectros a quienes logra derrotar con su técnica más poderosa, aunque sacrifica su vida para ello, Tenma trata de enfrentar a Kagaho que sobrevive, pero este lo rechaza y decide partir comentando que no obedecerá ninguna orden que no venga directamente del señor Hades.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 13: El viaje</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_13.php">Ver Capitulo</a>
                                                    <p>
                                                        Tenma está consternado por la muerte de Aldebarán y piensa en huir del Santuario, pero Manigoldo de Cáncer se lo impide y lo encierra en un calabozo. Sasha habla con el Patriarca de esto, puesto que Tenma es pieza clave en la Guerra Santa, pero éste huye con la ayuda de Yato y Yuzuriha (convertida en Santo de plata). Posteriormente se despide de Sasha y emprende su camino hacia los Bosques de la Muerte.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 14: El bosque de la muerte</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_14.php">Ver Capitulo</a>
                                                    <p>
                                                        Tenma y sus dos amigos entran en un extraño bosque en el cual se dividen por la bandada de cuervos que estaban en los árboles, Tenma es recibido y atacado por sus amigos de la ciudad donde vivía, más tarde se descubre que eran manipulados por Nasu Veronica. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 15: Si pudiera volver a ese día</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_15.php">Ver Capitulo</a>
                                                    <p>
                                                        Yuzuriha y Yato tienen problemas en el bosque ya que son atacados por espectros, saliendo los dos bien librados de sus batallas se apresuran en unirse a Tenma que está luchando contra el espectro Nasu Veronica. Éste tiene las de ganar puesto que hace revivir a unos cadáveres, sin embargo irrumpe en escena Manigoldo de Cáncer. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 16: Dioses y peones</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_16.php">Ver Capitulo</a>
                                                    <p>
                                                        Manigoldo de Cáncer mantiene una pelea contra Veronica de Nasu, Manigoldo transporta a Veronica de Nassu a la colina de Yomutsu donde el combate termina. Más tarde, el Santo de Oro va hacia donde están los dioses gemelos Thanatos e Hypnos para confrontar al primero quien se inmuta ante su presencia y rápidamente es evidente la diferencia en el nivel de poder entre ambos. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 17: Basura</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_17.php">Ver Capitulo</a>
                                                    <p>
                                                        Capitdo se enfrenta a Thanatos que despliega su poder de dios. Cuando Thanatos tiene el combate ganado es interrumpido por Sage (el gran patriarca), que ayuda a su discípulo a pelear contra Thanatos, a pesar de haber utilizado sellos impregnados con el poder de Athena, Thanatos aun se muestra muy superior. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 18: Sólo quiero que vivas</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_18.php">Ver Capitulo</a>
                                                    <p>
                                                        Thanatos abre la hiperdimension donde el cuerpo del humano que utiliza es destruido junto con Manigoldo, pero el alma de Thanatos aun esta dispuesta a salir victoriosa e intenta poseer el cuerpo de Sage quien demuestra haber previsto esto y sella al Dios dentro de su cuerpo. El combate termina con dos grandes bajas en el ejército de Athena. Manigoldo se encuentra con Shion despidiendose de este.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 19: La espada más fuerte</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_19.php">Ver Capitulo</a>
                                                    <p>
                                                        El Cid, Santo dorado de Capricornio es enviado a una misón con la finalidad de liberar el alma de Sisifo que se encuentra atrapado en el mundo de los sueños (Yumekai), en esta misión conoce a los hijos de Hypnos que son dioses menores de los sueños (Ikelos, Phantasos, Morpheo y Oneiros), llega a luchar con el primero, utilizando su ataque cortante, pero El Cid no puede contra Ikelos que hace que su propio ataque corte su brazo derecho. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 20: La prisión de los Sueños</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_20.php">Ver Capitulo</a>
                                                    <p>
                                                        El Cid es supuestamente vencido por Ikelos, pero aparece para vencer a Phantasos de un solo golpe. Mientras tanto Tenma y sus dos compañeros van por el bosque, siendo el caballero de Pegaso enviado a un sueño por Morpheo, viendo el pasado y al Santo de Sagitario, dándose cuenta que no es un sueño de verdad. Ikelos que ha sentido un remezón por la caída de Phantasos va a enfrentar a El Cid 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 21: Más allá de los sueños</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_21.php">Ver Capitulo</a>
                                                    <p>
                                                        Ikelos enfrenta a El Cid, logrando el dios poder herir muchas veces al Santo, pero este tenia un plan desde el comienzo; consistía en dispersar su sangre para vencerlo. Mientras Tenma se enfrenta a si mismo, logra salir de su sueño viéndose con Morpheo, que lo ata con unas flores, los sueños de Pegaso hacen que su armadura se convierta en una armadura divina derrotando al dios. El Cid ve como el mundo de los Sueños se distorsiona y luego, acaba encontrándose con Tenma; con la misión de liberar de esa prisión de sueños a Sisifo, ataca sin ningún efecto, apareciendo Oneiros convocando a sus hermanos caídos, logra unirse para pelear contra los dos santos. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 22: El camino de la lealtad</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_22.php">Ver Capitulo</a>
                                                    <p>
                                                        Oneiros ataca con su Guardian's Oracle (ガーディアンのオラクルGuardian's Oracle?) a los dos santos pero no causa efecto porque Athena ha llegado al Mukai para despertar a Sisifo. Los dos caballeros atacan en conjunto saliendo al mundo real logrando dañar un poco al dios que tiende a atacar de nuevo con su devastador poder; cerca de allí hay tres caballeros que inician el ataque al dios pero son fácilmente vencidos, teniendo la perdida de sus subordinados El Cid contraataca con ayuda de Tenma pero ningún ataque logra dañarlo, pero Tenma insiste ya que El Cid esta exhausto y fuera de combate por haber perdido mucha sangre.Tenma a punto de ser derrotado es salvado por Yato y Yuzuriha que logran ayudar y atacar junto con Tenma pero el poder de Oneiros (fusionado con los otros tres dioses derrotados) es muy superior.                
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 23: La Espada Sagrada</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_23.php">Ver Capitulo</a>
                                                    <p>
                                                        Athena ve en el sueño de Sísifo que el siente culpa de haber separado a Sasha de Alone y Tenma, y por efecto de la guerra Santa contra Hades, quitándose el mérito de ser el santo de Sagitario su armadura se va de su cuerpo disparádole la flecha, logrando que su sangre se convierta en una Sapuri. Athena logra convencer al Santo de Sagitario que el no tuvo la culpa de los hechos aceicidos, le abraza y le hace recordar la promesa que le hizo ese día a Tenma logrando así recobrar su armadura dorada. Ya en el Santuario los dos mandan una flecha dorada para ayudar a El Cid que sin ganas de luchar se pone de pie usando su ataque logra dividir la flecha en cuatro logrando vencer a Oneiros, mas este no se da por vencido y se avalanza contra Tenma interviniendo El Cid para proteger a Tenma este se despide, al final de su vida el Santo de Capricornio logró utilizar Excalibur. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 24: Hora de una sangrienta batalla</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_24.php">Ver Capitulo</a>
                                                    <p>
                                                        La barrera en el castillo de Hades ha sido deshabilitada por Sísifo de Sagitario. Hakurei de Altar, haciéndose pasar por el patriarca, habla a las tropas de Athena para iniciar el ataque al lugar donde se encuentra Hades. Sin embargo él tiene otra misión, sellar al otro dios gemelo, Hypnos. Todos los santos se preparan para la última pelea y son teletransportados por Atla hacia el castillo. Shion y Dohko van por su cuenta desobedeciendo las órdenes de Sísifo, encontrándose con Tenma y sus amigos, mientras el antiguo Santo de Altar, Hakurei, se acerca más a la entrada del castillo, y también a su lucha contra Hypnos. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 25: Hace muchas lunas</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_25.php">Ver Capitulo</a>
                                                    <p>
                                                        Hakurei se ha infiltrado por completo en el castillo, donde su confrontación final es contra el dios del sueño Hypnos. Los recuerdos del santo de Altar se presentan, cómo en la Guerra Santa anterior Athena pudo vencer el alma de Hades pero no a los dioses gemelos, siendo obligada a partir con ellos en el portal de las dimensiones. Ese rencor llevado por más de doscientos años hace que Hakurei empiece la pelea. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <h3><span class="icon icon-arrow-down"></span>Episodio # 26: Sé tú mismo</h3>
                                        <div>
                                            <div class="row">
                                                <div class="avance">
                                                    <a href="the_lost_canvas/temporada_1/capitulo_26.php">Ver Capitulo</a>
                                                    <p>
                                                        Hakurei logar vencer a Hypnos utilizando el Sekishiki Tenryo Ha (精霊の行進 Marcha de los espíritus?) con la fuerza de sus Camaradas en la Guerra Santa anterior. Cuando está por disolver la barrera de Hades, el dios se presenta y asesina a Hakurei. Un enfurecido Shion ataca a Hades sin resultado alguno, ante la gran fuerza del dios del Inframundo, que desea matar a los dos santos, no obstante llegan a la batalla Dohko, Yato y Tenma, quienes tampoco pueden con la grandísima fuerza de Alone que ahora porta su Serplice de Hades, atacando a deseo a sus contrincantes. Dohko en desesperación logra atarcar a Hades pero también es inútil, elevando su cosmo hace que Tenma y los demás salgan del castillo de Hades, quedándose el a pelear. Tenma cree que Dohko ha caído en batalla y desea ser más fuerte. 
                                                    </p>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="u2">
                                    <script type="text/javascript"><!--
 google_ad_client = "ca-pub-6835894736815485";
                                        /* SSWideSkysraper */
                                        google_ad_slot = "2399126850";
                                        google_ad_width = 160;
                                        google_ad_height = 600;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>

                                </div>

                            </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../template/footer.php';
        ?>

    </body>
</html>