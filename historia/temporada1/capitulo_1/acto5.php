<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap1_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto6.php';
            }
            function previous() {
                window.location = 'acto4.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/Inspiracion.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
            });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo1">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto5">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 1: El comienzo de la guerra Santa</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap1_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u6">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        <a target="_blank" href="../../../caballeros/athena/caballeros_de_bronce/jabu.php">Jabu</a>
                                        queda sorprendido del poder del Gigante y dice: “Tienes mucho poder… pero yo soy el 
                                        <a target="_blank" href="../../../caballeros/athena/caballeros_de_bronce.php">caballero de Bronce</a>
                                        del Unicornio y no moriré hasta que te aniquile”. <strong class="jabu">GALOPE DEL UNICORNIO!!!</strong> 
                                        se lanza hacia Liakos y este sorprendido por la velocidad del caballero recibe su golpe y cae al suelo. 
                                    </p>    
                                    <p>
                                        Liakos se levanta furioso, te acabaré Caballero de <a target="_blank" href="../../../dioses/athena.php">athena!!!</a> y corre hacia el para pegarle un puñetazo gigantesco, 
                                        Jabu se protege con sus manos pero debido a la fuerza del Gigante es lanzado a lo lejos y se golpea 
                                        con un edificio de Suecia. 
                                    </p>    
                                </div>
                                <div class="u6">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/unicornio.jpg" alt="Unicornio Bestia" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">                                
                                <div class="u6">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/galope_de_unicornio.jpg" alt="Galope de Unicornio" />
                                    </a>
                                </div>
                                <div class="u6">        
                                    <p>    
                                        El gigante se acerca a Jabu y decide aplastarlo como hizo con el caballero del Leoncillo. Liakos 
                                        dice: “Te aplastare enano!!!” pero de repente siente un gran dolor en su pie y no entiende por qué, 
                                        Liakos dice: “ahh!!!, tal vez… fue el <a target="_blank" href="../../../caballeros/athena/caballeros_de_bronce/ban.php">
                                            Caballero del Leoncillo</a> que me golpeo el pie antes de morir!!!”, 
                                    </p>    
                                    <p>   
                                        Jabu le responde que estaba seguro que su amigo no había muerto sin por lo menos haberle causado algún daño, 
                                        Jabu aprovecha el momento y se lanza hacia el Cielo y cuando viene bajando grita: “<strong class="jabu"> GALOPE DE UNICORNIO!</strong>” y 
                                        golpea fuertemente al Gigante hiriendolo todavía levemente. 
                                    </p>
                                </div>
                            </div>                        
                        </article>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Footer -->
    <?php
    include '../../../template/footer_ad.php';
    ?>    

</body>
</html>