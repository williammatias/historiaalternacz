<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap1_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto4.php';
            }
            function previous() {
                window.location = 'acto2.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/Peligro.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo1">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto3">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a target="_blank" id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 1: El comienzo de la guerra Santa</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap1_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u6">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        Mientras tanto, una extraño guerrero mitológico hace su aparición en Estocolmo, Suecia 
                                        causando daños en el palacio real. El gran maestro Shion envía a 2 
                                        <a target="_blank" href="../../../caballeros/athena/caballeros_de_bronce.php">caballeros de bronce</a> 
                                        (<a target="_blank" href="../../../caballeros/athena/caballeros_de_bronce/ban.php">Ban del Leoncillo</a>
                                        y <a target="_blank" href="../../../caballeros/athena/caballeros_de_bronce/jabu.php">Jabu del Unicornio</a>) para que se dirijan a este sitio rápidamente 
                                        y así evitar que el desconocido guerrero cause más daño.
                                    </p>
                                </div>
                                <div class="u3">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/ban.jpg" alt="Ban de Leoncillo" />
                                    </a>
                                </div>
                                <div class="u3">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/jabu.jpg" alt="Jabu de Unicornio" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="u4">
                                    <a target="_blank" href="#" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/liakos.jpg" alt="Gigante Liakos" />
                                    </a>
                                </div>
                                <div class="u8">                                   
                                    <p>
                                        Ban y Jabu llegan al lugar y se encuentran con un guerrero muy alto, tan alto que parece 
                                        que fuera un Gigante. Jabu le dice que se detenga y deje de destruir las propiedades, 
                                        el Gigante se da vuelta y se burla de Jabu y Ban por sus diminuto tamaño. Ban se lanza hacia 
                                        el Gigante con la intención de Golpearlo con sus puños pero el Gigante le pega una enorme 
                                        patada que deja al Leoncillo debilitado..
                                    </p>
                                </div>

                            </div>                        
                        </article>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Footer -->
    <?php
    include '../../../template/footer_ad.php';
    ?>    

</body>
</html>