<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap1_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto3.php';
            }
            function previous() {
                window.location = 'acto1.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/PandoraArpa.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo1">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto2">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a target="_blank" id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 1: El comienzo de la guerra Santa</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap1_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u8">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        Se puede escuchar el sonido suave y majestuoso de un arpa. Pandora lo estaba tocando cuando de repente 
                                        escucha la voz de  <a target="_blank" href="../../../dioses/hades.php">Hades</a> que le dice que se dirija a su aposento. Hades le explica a Pandora que existe 
                                        un mítico astro del mismo tamaño del Sol no muy lejos de nuestra Galaxia, y que estará muy ocupado 
                                        transportandolo para ubicarlo entre la tierra y el sol y así llenar el mundo de oscuridad; 
                                        así que pandora deberá encargarse mientras tanto de su ejército de <a target="_blank" href="../../../caballeros/hades/espectros.php">Espectros</a>.
                                    </p>
                                </div>

                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/pandora_tocando_arpa.jpg" alt="Pandora tocando su arpa" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/espectros.jpg" alt="Espectros de Hades" />
                                    </a>
                                </div>
                                <div class="u8">                                   

                                    <br />
                                    <blockquote>
                                        Los guerreros del ejercito de Hades son llamados Espectros. Sus Sapuris (Armaduras)
                                        corresponden a las 108 Estrellas Malignas.
                                    </blockquote>
                                </div>
                            </div>

                        </article>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Footer -->
    <?php
    include '../../../template/footer_ad.php';
    ?>    

</body>
</html>