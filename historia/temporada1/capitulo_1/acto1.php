<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap1_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto2.php';
            }
            function previous() {
                window.location = 'acto0.php';
            }
        </script>

        <script type="text/javascript">
//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/HadesInicioCapitulo.mp3"
                        });
                        play();
                    },
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo1">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a target="_blank" id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 1: El comienzo de la guerra Santa</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap1_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u6">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        La mayor amenaza que ha vivido el mundo esta a punto de comenzar.
                                        <a target="_blank" href="../../../caballeros/athena/caballeros_dorados/dohko.php">Dohko de Libra</a> en los 5 picos en China 
                                        lo presiente, se pone de pie lentamente y pronuncia las siguientes palabras observando el sello de 
                                        <a target="_blank" href="../../../dioses/athena.php">Athena</a>
                                        que tenía a Hades encerrado: “El Momento ha llegado”.

                                    </p>
                                </div>

                                <div class="u6">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/dohko_en_5_picos.jpg" alt="Dohko en los 5 picos" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="u6">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/hades.jpg" alt="Hades y sus espectros" />
                                    </a>
                                </div>
                                <div class="u6">
                                    <blockquote>El Dios del mundo de los muertos <a target="_blank" href="../../../dioses/hades.php">Hades</a> 
                                        ha despertado con malas intenciones junto con sus <a target="_blank" href="../../../caballeros/hades/espectros.php">108 Espectros</a>.</blockquote>
                                </div>
                            </div>
                        </article>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>

<!-- Footer -->
<?php
include '../../../template/footer_ad.php';
?>    

</body>
</html>