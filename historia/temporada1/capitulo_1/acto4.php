<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap1_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto5.php';
            }
            function previous() {
                window.location = 'acto3.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/LuchaDificil.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo1">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto4">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 1: El comienzo de la guerra Santa</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap1_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u7">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                                    <p>
                                        El Gigante se presenta como Liakos de Escarlata uno de los 9 Gigantes de la Mitología, 
                                        a lo que <a target="_blank" href="../../../caballeros/athena/caballeros_de_bronce/jabu.php">Jabú</a>
                                        responde: “uno de los 9 gigantes… es decir que los gigantes han despertado nuevamente…” 
                                        Liakos le comenta que los Gigantes han sido liberados nuevamente para destruir el planeta. 
                                    </p>
                                </div>
                                <div class="u5">
                                    <a href="#" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/liakos.jpg" alt="Liakos de Escarlata" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="u5">
                                    <a href="#" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo1/ban_se_levanta.jpg" alt="Ban se levanta" />
                                    </a>
                                </div>
                                <div class="u7">   
                                    <p>
                                        <a target="_blank" href="../../../caballeros/athena/caballeros_de_bronce/ban.php">Ban del leoncillo</a>
                                        se comienza a levantar y le dice al Gigante que sus 6 años de entrenamiento 
                                        para convertirse en caballero no fueron solo para morir asi no mas. Ban hace el ataque. 
                                        <strong class="ban">BOMBARDEO DEL LEONCILLO</strong> lanzándose con sus piernas hacia la Cabeza del Gigante pero Liakos levanta 
                                        el puño y lo golpea hasta que Ban toca el suelo. Liakos le dice que su poder es como el de una hormiga
                                        comparado con el suyo y lo aplasta completamente con su pie derecho causándole la muerte instantáneamente.
                                    </p>
                                </div>
                            </div>                        
                        </article>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Footer -->
    <?php
    include '../../../template/footer_ad.php';
    ?>    

</body>
</html>