<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap4_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto4.php';
            }
            function previous() {
                window.location = 'acto2.php';
            }
        </script>

        <script type="text/javascript">
//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
//                            mp3: "http://www.saintseiyasigma.com/music/HadesInicioCapitulo.mp3"
                            mp3: "http://localhost/historiaalternacz/music/108Espectros.mp3"
                        });
                        play();
                    },
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo4">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto3">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a target="_blank" id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo4">Capítulo 4: Batalla en el inframundo</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap4_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u8">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        Mientras tanto... Un espectro se dirige hacia Giudecca. Entra al templo y va caminando hacia el aposento donde se 
                                        encuentra Pandora. Pandora pregunta quién es el que ha llegado y el espectro responde: 
                                        Soy Radamanthys de Wyvern, uno de los 3 jueces del Infierno; Vengo a comunicarle que los 
                                        caballeros de Athena se encuentran en la primera prisión pero no están muertos!
                                    </p>
                                </div>
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_4/radamanthys.jpg" alt="Radamanthys de Wyvern" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_4/pandora.jpg" alt="Pandora" />
                                    </a>
                                </div>
                                <div class="u8">
                                    <p>
                                        Pandora le responde que ya lo sabe pero que está completamente segura que Lune de Balrog 
                                        los matará y los enviará a la prisión correpondiente a sus pecados para que sufran por 
                                        toda la eternidad. 
                                    </p>
                                    <p>
                                        Radamanthys le comenta a Pandora que Athena es una tonta porque no ha enviado a sus 
                                        caballeros mas poderosos, Los Caballeros Dorados y que los caballeros de plata no 
                                        tienen ninguna oportunidad en el infierno.
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>

                </div>
            </div>

        </div>
    </div>

</div>

<!-- Footer -->
<?php
include '../../../template/footer_ad.php';
?>    

</body>
</html>