<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap4_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto1.php';
            }
            function previous() {
                window.location = '../../../historia.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/TituloCapitulo.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia">

        <!-- Main -->
        <div id="historia" class="wrapper style1 capitulo4">

            <div class="container acto0">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a target="_blank" id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo4">Capítulo 4: Batalla en el inframundo</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap4_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                           
                            <div class="row">
                                <div class="u6">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <h3>
                                        Caballeros del Zodiaco - Historia Alterna
                                    </h3>
                                    <h3>
                                        Chapter 1: Infierno
                                    </h3>
                                    <h3>
                                        Capítulo 4: Batalla en el inframundo.
                                    </h3>
                                </div>

                                <div class="u6">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_3/albiore_vs_lune.jpg" alt="Hades VS Athena" />
                                    </a>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

            </div>
        </div>


        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>    

    </body>
</html>