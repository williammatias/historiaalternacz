<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap3_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto4.php';
            }
            function previous() {
                window.location = 'acto2.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/APuntoDePartir.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo2">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto3">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capitulo 3: Entrando al Reino de los Muertos</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap3_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                               <div class="u5">
                                     <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                                    <img src="../../../images/historia/temporada1/capitulo_3/puerta_infierno.jpg" alt="Puerta del Infierno" />
                                </div>
                                <div class="u7">
                                    <div class="row">
                                        <div class="u2">
                                            <a href="#" class="image featured">
                                                <img src="../../../images/historia/temporada1/capitulo_3/aracne_rostro.jpg" alt="Aracne de Tarantula" />
                                            </a>
                                        </div>
                                        <div class="u10">                                   
                                            <p><a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/aracne.php">Aracne de Tarantula</a>: - "Estamos ante la puerta del infierno"</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="u2">
                                            <a href="#" class="image featured">
                                                <img src="../../../images/historia/temporada1/capitulo_2/jamian_rostro.png" alt="Jamian del Cuervo" />
                                            </a>
                                        </div>
                                        <div class="u10">
                                            <p><a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/jamian.php">Jamian del cuervo</a>: - "¿Pero qué quiere decir este mensaje que está inscrito en esta puerta?"</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="u2">
                                            <img src="../../../images/historia/temporada1/capitulo_3/agora_rostro.jpg" alt="Rostro de Agora de Loto" />
                                        </div>
                                        <div class="u10">
                                            <p><a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/agora.php">Agora de Loto</a> - "Aquel ser que entre deberá abandonar toda esperanza."
                                                "Estamos en el Reino de los muertos, donde la gente que ha muerto sufre aquí por toda la eternidad y nadie se salva de su castigo"
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="u2">
                                            <img src="../../../images/historia/temporada1/capitulo_2/babel_rostro.jpg" alt="Rostro de Babel de Centauro" />
                                        </div>
                                        <div class="u10">
                                            <p><a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/babel.php">Babel de Centauro</a> - "No estoy de acuerdo con que abandonemos la esperanza."
                                                "¡Destruiremos el Monstractor y protegeremos la tierra!"</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                </div>

            </div>
        </div>


        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>    

    </body>
</html>