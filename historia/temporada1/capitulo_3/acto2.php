<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap3_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto3.php';
            }
            function previous() {
                window.location = 'acto1.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/HablandoParaTenerCuidado.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo2">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto0">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capitulo 3: Entrando al Reino de los Muertos</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap3_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u8">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        <a target="_blank" href="../../../caballeros/athena/caballeros_dorados/mu.php">Mu de Aries</a> transporta a los <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata.php">caballeros de plata</a> hasta la puerta del infierno. 
                                        Les dice que deben tener mucho cuidado en el inframundo y que existen muchos lugares
                                        aun desconocidos para la misma <a target="_blank" href="../../../dioses/athena.php">Athena</a>.
                                        Mu les explica a los santos que él asi como todos los caballeros dorados
                                        tiene la orden de permanecer en el Santuario y que se debe marchar.
                                    </p>
                                </div>
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_3/mu_hablando.jpg" alt="Mu de Aries" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_3/inframundo.jpg" alt="Inframundo" />
                                    </a>
                                </div>
                                <div class="u8">

                                    <br />
                                    <blockquote>
                                        El infierno es el Reino de los Muertos donde son juzgadas las almas en las distintas prisiones. 
                                        Es el habitáculo de todos los espectros incluyendo al Señor Oscuro Hades. 
                                        Se compone de ocho prisiones, tres valles, diez fosos y cuatro esferas.
                                    </blockquote>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

            </div>
        </div>


        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>    

    </body>
</html>