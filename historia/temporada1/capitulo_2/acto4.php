<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap2_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto5.php';
            }
            function previous() {
                window.location = 'acto3.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/ContandoProblemas.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo2">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto0">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 2: El poder de un caballero plateado</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap2_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u2">
                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <a href="#" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/jamian_rostro.png" alt="Rostro de Jamian del Cuervo" />
                                    </a>
                                </div>
                                <div class="u4">                                   
                                    <p>Jamian del Cuervo: - “¿Cómo llegaremos hasta el infierno?”</p>
                                </div>
                                <div class="u3">
                                    <a href="#" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/dohko.jpg" alt="Viejo Maestro Dohko" />
                                    </a>
                                </div>
                                <div class="u3">
                                    <p>Dohko de Libra: - “Mu de Aries los llevará hasta allá”.                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="u3">
                                    <img src="../../../images/historia/temporada1/capitulo_2/babel_rostro.jpg" alt="Rostro de Babel de Centauro" />
                                </div>
                                <div class="u3">
                                    <p>Babel de Centauro - "¿Enviaran caballeros dorados junto con nosotros?"</p>
                                </div>
                                <div class="u3">
                                    <img src="../../../images/historia/temporada1/capitulo_2/patriarca.jpg" alt="Patriarca" />
                                </div>
                                <div class="u3">
                                    <p>Patriarca Shion - "Los caballeros dorados no puede abandonar el santuario por ningún motivo."</p>
                                </div>
                            </div>
                    </div>
                </div>
                </article>
            </div>
        </div>

    </div>
</div>


<!-- Footer -->
<?php
include '../../../template/footer_ad.php';
?>    

</body>
</html>