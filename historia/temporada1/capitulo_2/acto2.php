<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap2_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto3.php';
            }
            function previous() {
                window.location = 'acto1.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/PeleaVictoriaBuenos.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo2">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto0">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 2: El poder de un caballero plateado</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap2_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">

                                <div class="u4">
                                    <a href="#" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/gorgona_demoniaca.png" alt="Gorgona Demoniaca" />
                                        <img src="../../../images/historia/temporada1/capitulo_2/liakos_enojado.jpg" alt="Liakos enojado" />
                                    </a>
                                </div>
                                <div class="u4">                                   
                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        Liakos corre hacia <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/argol.php">Argol</a>
                                        y trata de golpearlo con su enorme puño pero Argol salta y lanza su ataque: 
                                        “<strong class="jabu">GORGONA DEMONÍACA</strong>”, he infinidad de serpientes atacan al Dios Gigante dañandolo severamente y 
                                        queda sangrando por muchas partes de su cuerpo. 

                                        Liakos no puede creer como un caballero de Athena ha sido capaz de hacerle esto a un Dios. 
                                        Liakos se enfurece en demasía y se lanza a atacar a Argol. 
                                    </p>
                                    <p>
                                        Argol utiliza su escudo de Medusa, 
                                        el Gigante lo mira y Queda convertido en piedra. Argol dice: te rompere en mil pedazos!!! “GORGONA DEMONÍACA” 
                                        y el Dios Gigante fue partido en muchísimos pedazos. Argol recoje a Jabu y lo lleva hacia un hospital.
                                    </p>
                                </div>
                                <div class="u4">
                                    <a href="#" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/escudo_medusa.jpg" alt="Escudo de Medusa" />
                                    </a>
                                </div>

                            </div>
                    </div>

                    </article>
                </div>
            </div>

        </div>
    </div>


    <!-- Footer -->
    <?php
    include '../../../template/footer_ad.php';
    ?>    

</body>
</html>