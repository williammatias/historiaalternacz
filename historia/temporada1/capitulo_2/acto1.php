<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap2_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto2.php';
            }
            function previous() {
                window.location = 'acto0.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/LlegoUnFuerte.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo2">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto0">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 2: El poder de un caballero plateado</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap2_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u8">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        En el inframundo Pandora se da cuenta que un Dios Gigante esta peleando en la tierra y dice: 
                                        “Los Dioses Gigantes ya han despertado…”.
                                    </p>
                                    <p>
                                        Liakos de Escarlata el Dios gigante se burla del tamaño de su nuevo contrincante y le dice: 
                                        “¿Quién eres diminuto caballero?” - “Soy el <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata.php">caballero de plata</a>,
                                        <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/argol.php">Argol de Perseo</a> y estoy aquí para derrotarte, 
                                        a decir verdad estaba deseoso de pelear y darle uso a mi escudo”. 
                                        Liakos dice: “Voy a derrotar a los caballeros de Athena uno por uno”. Argol dice: “No lo creo”.
                                    </p>

                                </div>
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/argol_perseo.jpg" alt="Hades VS athena" />
                                    </a>
                                </div>                                  
                                <hr/>
                            </div>
                            <div class="row">
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php" class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/88Caballeros.jpg" alt="88 Caballeros de Atena" />
                                    </a>
                                </div>
                                <div class="u8">
                                    <blockquote>
                                        Los caballeros de Athena son 88, los cuales corresponden a las 88 constelaciones que tiene nuestro planeta
                                        tierra. Existen 3 Rangos principales entre los caballeros de Athena. 
                                        Oro, Plata y Bronce, de los cuales 12 son de Oro, 24 son de Plata y 48 son de bronce, 
                                        aunque existen 4 caballeros más de los cuales no se sabe el rango. 
                                        Los caballeros dorados son los más fuertes, seguidos por los de plata y terminando con los de bronce.
                                    </blockquote>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>

            </div>
        </div>


        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>    

    </body>
</html>