<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />


        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap2_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = '../capitulo_3/acto0.php';
            }
            function previous() {
                window.location = 'acto5.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/adsong.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia adpage">

        <!-- Main -->
        <div id="historia" class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 2: El poder de un caballero plateado</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap2_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                                        Fin del Capítulo. Continuará...
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <!-- Ad Page -->
                                <?php
                                include '../adtemplate.php';
                                ?>  
                            </div>                        
                        </article>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- Footer -->
    <?php
    include '../../../template/footer_ad.php';
    ?>    

</body>
</html>