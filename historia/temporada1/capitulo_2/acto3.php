<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />
                

        <?php
        include '../../../template/head.php';
        ?>

        <?php
        include './cap2_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = 'acto4.php';
            }
            function previous() {
                window.location = 'acto2.php';
            }

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/ContandoProblemas.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });

                var element = document.getElementById('historia')
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer(element).on("swiperight", function() {
                    previous();
                });

            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="chapter_infierno historia capitulo2">

        <!-- Main -->
        <div id="historia" class="wrapper style1 acto0">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u6">
                                        <a id="return" class="u1" href="../../../historia.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="temporada">Chapter Infierno</h3>
                                            <h3 class="capitulo1">Capítulo 2: El poder de un caballero plateado</h3>
                                        </div>
                                    </div>
                                    <div class="u3">
                                        <?php
                                        include './cap2_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2 next_previous">
                                        <button class="icon icon-arrow-left circled" onclick="previous();"></button>
                                        <button class="icon icon-arrow-right circled" onclick="next();"></button>
                                    </div>                           
                                </div>
                            </header>
                            <div class="row">
                                <div class="u6">                                   

                                    <div id="jquery_jplayer_1" class="jp-jplayer"></div>

                                    <br />
                                    <p>
                                        En la recámara del maestro se encuentran Shion y <a target="_blank" href="../../../caballeros/athena/caballeros_dorados/dohko.php">Dohko</a>
                                        conversando… Shion le dice a Dohko que esta es 
                                        la peor guerra que la humanidad ha enfrentado en su historia y que aun no sabe cómo es que van a salir
                                        victoriosos, Dohko le dice que deben tener fé y confiar en su <a target="_blank" href="../../../dioses/athena.php">Diosa Athena</a>. 
                                    </p>
                                   
                                </div>
                                <div class="u3">
                                    <a target="_blank" href="../../../galeria.php"  class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/patriarca.jpg" alt="Patriarca" />
                                    </a>
                                </div>
                                <div class="u3">
                                    <a target="_blank" href="../../../galeria.php"  class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/dohko.jpg" alt="Dohko de Libra Old" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="u8">
                                     <p>
                                         Los <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata.php">caballeros de plata</a>
                                         comienzan a llegar uno por uno <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/asterion.php">Asterion de Perros de Caza</a>, 
                                         <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/babel.php">Babel de Centauro</a>, 
                                         <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/misty.php">Misty de Lagarto</a>, 
                                         <a target="_blank" href="../../../caballeros/athena/caballeros_de_plata/argol.php">Argol de Perseo</a>
                                         y otros más.
                                    </p>
                                </div>
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php"  class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/caballeros_plata.jpg" alt="Caballeros de Plata" />
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="u4">
                                    <a target="_blank" href="../../../galeria.php"  class="image featured">
                                        <img src="../../../images/historia/temporada1/capitulo_2/dohko.jpg" alt="Viejo Maestro Dohko" />
                                    </a>
                                </div>
                                <div class="u8">
                                    <p>
                                        Dohko les explica la situación: “<a target="_blank" href="../../../dioses/hades.php">Hades</a> ha despertado y está trasladando un mítico astro que se encuentra cercano a nuestra galaxia para ubicarlo entre la tierra y el sol con lo cual dejaría nuestro planeta en completa oscuridad, nunca más sería de día y el mundo se convertiría en un caos”. Necesitamos que ustedes bajen al infierno y detengan a Hades! 
                                    </p>
                                </div>
                            </div>
                    </div>
                    </article>
                </div>
            </div>

        </div>
    </div>


    <!-- Footer -->
    <?php
    include '../../../template/footer_ad.php';
    ?>    

</body>
</html>