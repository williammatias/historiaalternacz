<!DOCTYPE HTML>
<html>
    <head>
        <title>Ejercitos - SaintSeiyaSigma.com</title>
        <meta name="description" content="Los Ejercitos de los cuales estan compuestos los Dioses..." />

        <?php
        include 'template/head.php';
        ?>
        
        
    </head>
    <body class="caballeros">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a target="_blank" href="#" id="logo">Caballeros</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include 'template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <header>
                                <h2><a target="_blank" href="#">Caballeros y Guerreros de los Dioses</a></h2>
                                <span class="byline">
                                    <script type="text/javascript"><!--
                                         google_ad_client = "ca-pub-6835894736815485";
                                        /* SSLeaderBoard */
                                        google_ad_slot = "1201595250";
                                        google_ad_width = 728;
                                        google_ad_height = 90;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>
                                </span>
                            </header>
                            <div class="row">
                                <section class="u6">
                                    <header>
                                        <h3>Caballeros de Oro</h3>
                                    </header>
                                    <a target="_blank" href="caballeros/athena/caballeros_dorados.php" class="image">
                                        <img src="images/galeria/big/caballeros_dorados/dorados-header.jpg" alt="Caballeros Dorados"/>
                                    </a> 
                                </section>
                                <section class="u6" style="margin-top: 2em;">
                                    <header>
                                        <h3>Caballeros de Plata</h3>
                                    </header>
                                    <a target="_blank" href="caballeros/athena/caballeros_de_plata.php" class="image">
                                        <img src="images/galeria/big/caballeros_de_plata/caballeros_de_plata.jpg" alt="Caballeros de Plata"/>
                                    </a> 
                                </section>
                            </div>
                            <div class="row">
                                <section class="u6">
                                    <header>
                                        <h3>Caballeros de Bronce</h3>
                                    </header>
                                    <a target="_blank" href="caballeros/athena/caballeros_de_bronce.php" class="image">
                                        <img src="images/galeria/big/caballeros_de_bronce/caballeros_de_bronce.jpg" alt="Caballeros de Bronce"/>
                                    </a> 
                                </section>
                                <section class="u6">
                                    <header>
                                        <h3>108 Espectros</h3>
                                    </header>
                                    <a target="_blank" href="caballeros/hades/espectros.php" class="image">
                                        <img src="images/galeria/big/espectros/espectros.jpg" alt="Espectros de Hades"/>
                                    </a> 
                                </section>
                            </div>
                            <div class="row">
                                <section class="u6">
                                    <header>
                                        <h3>Generales Marinos</h3>
                                    </header>
                                    <a target="_blank" href="caballeros/poseidon/generales_marinos.php" class="image">
                                        <img src="images/galeria/big/generales_marinos/generales_marinos.jpg" alt="Generales Marinos"/>
                                    </a> 
                                </section>   
                                <section class="u6">
                                    <header>
                                        <h3>Dioses Guerreros de Asgard</h3>
                                    </header>
                                    <a target="_blank" href="caballeros/odin/dioses_guerreros.php" class="image">
                                        <img src="images/galeria/big/dioses_guerreros/dioses_guerreros.jpg" alt="Dioses Guerreros"/>
                                    </a> 
                                </section>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>


        <!-- Features -->
        <?php
        include 'template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include 'template/footer.php';
        ?>

    </body>
</html>