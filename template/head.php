<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta charset="UTF-8">
<!--<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet" type="text/css" />-->
<!--[if lte IE 8]><script src="http://www.saintseiyasigma.com/js/html5shiv.js"></script><![endif]-->
<script src="http://www.saintseiyasigma.com/js/jquery-1.10.2.min.js"></script>
<script src="http://www.saintseiyasigma.com/js/jquery.dropotron.js"></script>
<script src="http://www.saintseiyasigma.com/js/skel.min.js"></script>
<script src="http://www.saintseiyasigma.com/js/skel-panels.min.js"></script>
<script src="http://www.saintseiyasigma.com/js/init.js"></script>

<noscript>
<link rel="stylesheet" href="http://www.saintseiyasigma.com/css/skel-noscript.css" />
<link rel="stylesheet" href="http://www.saintseiyasigma.com/css/style.css" />
<link rel="stylesheet" href="http://www.saintseiyasigma.com/css/style-desktop.css" />
<link rel="stylesheet" href="http://www.saintseiyasigma.com/css/style-noscript.css" />
</noscript>
<!--[if lte IE 8]><link rel="stylesheet" href="http://www.saintseiyasigma.com/css/ie8.css" /><![endif]--![endif]-->

<link href="http://www.saintseiyasigma.com/css/jplayer/blue.monday/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
<script src="http://www.saintseiyasigma.com/js/jplayer/jquery.jplayer.min.js"></script>


<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-37659676-5', 'saintseiyasigma.com');
    ga('send', 'pageview');

</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1375500552688346";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>