

<!-- Footer -->
<div id="footer">
    <div class="container">
        <div class="row">

            <!-- Tweets -->
            <section class="u4">
                <header>
                    <h2 class="icon icon-book circled"><span>Tweets</span></h2>
                </header>
                <ul class="divided">
                    <li>
                        <article class="tweet">
                            <h3><a href="http://www.saintseiyasigma.com/manga/next_dimension/volumen_8/capitulo_61/1.php">Next Dimension - Capítulo 61: Poema de Despedida a un amigo</a></h3>
                            <span class="timestamp"></span>
                        </article>
                    </li>
                    <li>
                        <article class="tweet">
                            <h3><a href="http://www.saintseiyasigma.com/manga/episodio_g/volumen_4/capitulo_17/1.php">Episodio G - Capítulo 17: Aquel que guia al más alla</a></h3>
                            <span class="timestamp"></span>
                        </article>
                    </li>
                    <li>
                        <article class="tweet">
                            <h3><a href="http://www.saintseiyasigma.com/manga/the_lost_canvas/volumen_2/capitulo_10/1.php">The Lost Canvas - Capítulo 10: Castillo Maléfico</a></h3>
                            <span class="timestamp"></span>
                        </article>
                    </li>
                </ul>
            </section>

            <!-- Posts -->
            <section class="u4">
                <header>
                    <h2 class="icon icon-file circled"><span>Posts</span></h2>
                </header>
                <ul class="divided">
                    <li>
                        <article class="post stub">
                            <header>
                                <h3><a href="http://www.saintseiyasigma.com/historia/temporada1/capitulo_4/acto0.php">Capítulo 4: Batalla en el Inframundo</a></h3>
                            </header>
                            <span class="timestamp"></span>
                        </article>
                    </li>
                    <li>
                        <article class="post stub">
                            <header>
                                <h3><a href="http://www.saintseiyasigma.com/historia/temporada1/capitulo_3/acto0.php">Capitulo 3: Entrando en el Reino de los Muertos</a></h3>
                            </header>
                            <span class="timestamp"></span>
                        </article>
                    </li>
                    <li>
                        <article class="post stub">
                            <header>
                                <h3><a href="http://www.saintseiyasigma.com/historia/temporada1/capitulo_2/acto0.php">Capítulo 2: El poder de un caballero plateado</a></h3>
                            </header>
                            <span class="timestamp"></span>
                        </article>
                    </li>
                </ul>
            </section>

            <!-- Photos -->
            <section class="u4">
                <header>
                    <h2 class="icon icon-camera circled"><span>Photos</span></h2>
                </header>
                <div class="row quarter no-collapse">
                    <div class="u6">
                        <a href="http://www.saintseiyasigma.com/galeria.php" class="image full">
                            <img src="http://www.saintseiyasigma.com/images/galeria/small/caballeros_dorados/dorados-header.jpg" alt="Caballeros de Oro" />
                        </a>
                    </div>
                    <div class="u6">
                        <a href="http://www.saintseiyasigma.com/galeria.php" class="image full"
                           ><img src="http://www.saintseiyasigma.com/images/galeria/small/generales_marinos/generales_marinos.jpg" alt="Generales Marinos" /></a>
                    </div>
                </div>
                <div class="row quarter no-collapse">
                    <div class="u6">
                        <a href="http://www.saintseiyasigma.com/galeria.php" class="image full">
                            <img src="http://www.saintseiyasigma.com/images/galeria/small/dioses_guerreros/dioses_guerreros.jpg" alt="Dioses Guerreros" />
                        </a>
                    </div>
                    <div class="u6">
                        <a href="http://www.saintseiyasigma.com/galeria.php" class="image full"
                           ><img src="http://www.saintseiyasigma.com/images/galeria/small/caballeros_de_plata/caballeros_de_plata.jpg" alt="Caballeros de Plata" /></a>
                    </div>
                </div>
                <div class="row quarter no-collapse">
                    <div class="u6">
                        <a href="http://www.saintseiyasigma.com/galeria.php" class="image full">
                            <img src="http://www.saintseiyasigma.com/images/galeria/small/caballeros_de_bronce/caballeros_de_bronce.jpg" alt="Caballeros de Bronce" />
                        </a>
                    </div>
                    <div class="u6">
                        <a href="http://www.saintseiyasigma.com/galeria.php" class="image full"
                           ><img src="http://www.saintseiyasigma.com/images/galeria/small/espectros/espectros.jpg" alt="Espectros de Hades" /></a>
                    </div>
                </div>
            </section>

        </div>
        <hr />
        <div class="row">
            <div class="u12">
                <div class="row">

                    <!-- Correo Electronico -->
                    <section class="contact u6">
                        <header>
                            <h3>Puedes enviarnos un mensaje a nuestro correo electronico</h3>
                        </header>
                        <p>Si quieres reportar alguno problema, hacer alguno pedido de adiciòn de manga, historia o caballero.</p>
                        <ul class="icons">
                            <li><a href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" class="icon icon-envelope"><span>Twitter</span></a>info@saintseiyasigma.com</li>
                        </ul>
                    </section>

                    <!-- Contact -->
                    <section class="contact u6">
                        <header>
                            <h3>Tambien estamos en las Redes Sociales</h3>
                        </header>
                        <p>Compartenos y dile a tus amigos de esta nueva historia.</p>
                        <ul class="icons">
                            <li><a href="https://twitter.com/saintseiyasigma" class="icon icon-twitter"><span>Twitter</span></a></li>
                            <li><a href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" class="icon icon-facebook"><span>Facebook</span></a></li>
                            <li><a href="http://www.pinterest.com/saintseiyasigma/" class="icon icon-pinterest"><span>Pinterest</span></a></li>
                            <li><a href="http://saintseiyasigma.tumblr.com/" class="icon icon-tumblr"><span>Tumblr</span></a></li>
                            <li><a href="https://plus.google.com/105136089288852777292" rel="publisher" class="icon icon-google-plus"><span>Google+</span></a></li>
                        </ul>
                    </section>
                </div>

                <div class="row">

                    <!-- Copyright -->
                    <div class="copyright u12">
                        <ul class="menu">
                            <li>&copy; CZHistoriaAlterna. All rights reserved.</li>
                            <li>Design by: <a target="_blank" href="http://qualixium.com">Qualixium</a></li>
                            <li>Legal: <a href="http://www.saintseiyasigma.com/disclaimer.php">Disclaimer</a></li>
                        </ul>
                    </div>

                </div>

            </div>
        </div>
    </div>

