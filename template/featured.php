<div class="leaderboard">
    <script type="text/javascript"><!--
    google_ad_client = "ca-pub-6835894736815485";
        /* SSLeaderBoard */
        google_ad_slot = "1201595250";
        google_ad_width = 728;
        google_ad_height = 90;
//-->
    </script>
    <script type="text/javascript"
            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
</div>
<!-- Features -->
<div class="wrapper style2">

    <section id="features" class="container special">
        <header>
            <h2>Ultimos post</h2>
            <span class="byline"></span>
        </header>
        <div class="row">
            <article class="u4 special">
                <a href="http://www.saintseiyasigma.com/historia/temporada1/capitulo4/acto0.php" class="image featured"><img src="http://www.saintseiyasigma.com/images/historia/temporada1/capitulo_3/albiore_vs_lune.jpg" alt="Abiore vs Lune" /></a>
                <header>
                    <h3><a href="http://www.saintseiyasigma.com/historia/temporada1/capitulo4/acto0.php">Capítulo 4: Batalla en el inframundo</a></h3>
                </header>
                <p>
                    Lune le dice a los caballeros que sabe que son Santos de Atena, pero que de todas formas mirará los pecados de Albiore primero para saber a qué infierno lo enviará.
                </p>
            </article>
            <article class="u4 special">
                <a href="http://www.saintseiyasigma.com/manga/next_dimension.php" class="image featured"><img src="http://www.saintseiyasigma.com/images/manga/next_dimension.jpg" alt="Manga Saint seiya Next Dimension" /></a>
                <header>
                    <h3><a href="http://www.saintseiyasigma.com/manga/next_dimension.php">Next Dimension Ya Esta Completo en la seccion de los Mangas</a></h3>
                </header>
                <p>
                   Inicia con un prólogo en el cual Hades se encuentra luchando contra Seiya, el santo de Pegaso del siglo XX: en ese momento, el dios recuerda que otro guerrero de la misma constelación, además de herirlo en la era del mito, fue su amigo en la Guerra Santa del siglo XVIII, hace 243 años. Así, la historia pasa a centrarse en ese siglo, donde los jóvenes Shion y Dohko son ascendidos de santos de bronce a santos de oro.
                </p>
            </article>

            <article class="u4 special">
                <a href="http://www.saintseiyasigma.com/manga/the_lost_canvas.php" class="image featured"><img src="http://www.saintseiyasigma.com/images/manga/the_lost_canvas.jpg" alt="Manga Saint seiya The Lost Canvas" /></a>
                <header>
                    <h3><a href="http://www.saintseiyasigma.com/manga/the_lost_canvas.php">Nuevo Manga añadido en SaintSeiyaSigma: The Lost Canvas</a></h3>
                </header>
                <p>
                    El Manga cuenta lo ocurrido en la anterior guerra santa contra Hades ocurrida antes del Saint Seiya original
                    donde se explica muchos cosas concernientes al mundo de Saint Seiya Los Caballeros del Zodiaco. 
                </p>
            </article>
        </div>
    </section>

</div>