<article id="main">
    <section>
        <header>
            <h2><a href="noticias/omega_73.php">Nuevo Capitulo de Saint seiya Omega: Cap 73 Tears of Equuleus!? Awakening Two Cloths!</a></h2>
            <span class="timestamp"> 2 de Octubre, 4:07 PM </span>                           
        </header>
        <a href="noticias/omega_73.php" class="image featured"><img src="images/noticias/omega_73.jpg" alt="Saint Seiya Omega 73" /></a>
        <p>
            En este capitulo se ve como la armadura de equuleus no quiere pelear, pero subaru la convense de
            Volver a la pelea. Tambien Athena se decide a ir al campo de batalla con los caballeros de oros.
            Y un Cameo de los Santos Legendarios y los santos originales de acero.
        </p>
    </section>
    <section>
        <header>
            <h2><a href="manga.php">Nuevo Manga añadido en SaintSeiyaSigma: The Lost Canvas</a></h2>
            <span class="timestamp"> 2 de Octubre, 1:07 PM </span>                           
        </header>
        <a href="manga.php" class="image featured"><img src="images/manga/the_lost_canvas.jpg" alt="The Lost Canvas" /></a>
        <p>
            Hemos añadido los primeros 5 capitulos del manga de The Lost Canvas,
            posteriormente continuaremos añadiendo más capitulos,
            hasta que los tengamos todos en el website.
        </p>
        <p>
            El Manga cuenta lo ocurrido en la anterior guerra santa contra Hades ocurrida antes del Saint Seiya original
            donde se explica muchos cosas concernientes al mundo de Saint Seiya Los Caballeros del Zodiaco.
        </p>
    </section>
    <section>
        <header>
            <h2><a href="galeria.php">Nuevas Imagenes añadidas en nuestra Galeria.</a></h2>
            <span class="timestamp"> 2 de Octubre, 12:55 PM </span>                           
        </header>
        <p>
            Hemos añadido muchas imagenes impactantes en nuestra Galería disfrutenlas.
        </p>
    </section>
    <section>
        <header>
            <h2><a href="historia.php">La Saga Sigma</a></h2>
            <span class="byline">
                Una Historia Alterna Creada por SaintSeiyaSigma.com
            </span>
            <span class="timestamp"> Octubre 1, 9:22 AM </span>                            
        </header>
        <a href="historia.php" class="image featured"><img src="images/historia/temporada1/capitulo1/dohko_en_5_picos.jpg" alt="Dohko en los 5 picos" /></a>
        <p>
            ¿Qué hubiera ocurrido si Saga no traiciona a Athena en la seria de saint seiya original?
        </p>
        <p>
            Para responder esta pregunta hemos creado la "Saga Sigma", una historia alterna para contar
            lo que hubiera ocurrido. En esta historia Aioros de Sagitario esta VIVO!!! y los
            caballeros de plata y oro que murieron en la serie original también estan vivos y al servicio
            de Athena. Pero esta guerra santa no es común. Esta vez los Dioses vienen decididos a 
            adueñarse de nuestro planeta.
        </p>
    </section>
    <section>
        <header>
            <h2><a href="manga.php">Nuevo Manga añadido en SaintSeiyaSigma: Episodio G</a></h2>
            <span class="timestamp"> SEP. 30, 3:22 PM </span>                           
        </header>
        <a href="manga.php" class="image featured"><img src="images/manga/episodio_g.jpg" alt="Episodio G" /></a>
        <p>
            Hemos añadido algunos capitulos del manga del Episodio G, posteriormente continuaremos añadiendo más capitulos,
            hasta que los tengamos todos en el website.
        </p>
        <p>
            La serie cuenta la historia de los santos de oro siete años antes del Saint Seiya original con Aioria de Leo
            como protagonista, narra los problemas a los que se enfrenta Aioria para ser aceptado como un santo de oro y
            su enfrentamiento junto a los otros santos contra los titanes de la mitología griega, quienes buscan liberar
            a Cronos, quien fue derrotado y aprisionado por Zeus. 
        </p>
    </section>
</article>
