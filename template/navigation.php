<!-- Nav -->
<nav id="nav">
    <ul>
        <li><a href="http://www.saintseiyasigma.com/index.php"><span class="icon icon-info-sign circled"></span>Inicio</a></li>
        <li><a href="http://www.saintseiyasigma.com/noticias"><span class="icon icon-file-text circled"></span>Noticias</a></li>
        <li><a href="http://www.saintseiyasigma.com/historia.php"><span class="icon icon-bookmark circled"></span>La Saga Sigma</a></li>
        <li><a href="http://www.saintseiyasigma.com/caballeros.php"><span class="icon icon-user circled"></span>Caballeros</a>
            <ul>
                <li>
                    <a href="http://www.saintseiyasigma.com/dioses/athena.php">Diosa athena</a>
                    <ul>
                        <li class="caballerodeoro"><a href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados.php">Caballeros Dorados</a></li>
                        <li class="caballerodeplata"><a href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata.php">Caballeros de Plata</a></li>
                        <li class="caballerodebronce"><a href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_bronce.php">Caballeros de Bronce</a></li>
                        <li class="caballerodeoroomega"><a href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega.php">Caballeros Dorados Omega</a></li>
                        <li class="caballerodeorothe_lost_canvas"><a href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas.php">Caballeros Dorados The Lost Canvas</a></li>
                    </ul>
                </li>
                <li class="dioses_guerreros">
                    <a href="http://www.saintseiyasigma.com/dioses/odin.php">Dios Odin</a>
                    <ul>
                        <li><a href="http://www.saintseiyasigma.com/caballeros/odin/dioses_guerreros.php">Dioses Guerreros</a></li>
                    </ul>
                </li>
                <li class="generales_marinos">
                    <a href="http://www.saintseiyasigma.com/dioses/poseidon.php">Dios Poseidon</a>
                    <ul>
                        <li><a href="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos.php">Generales Marinos</a></li>
                    </ul>
                </li>
                <li class="espectros">
                    <a href="http://www.saintseiyasigma.com/dioses/hades.php">Dios Hades</a>
                    <ul>
                        <li><a href="http://www.saintseiyasigma.com/caballeros/hades/dioses_gemelos.php">Dioses Gemelos</a></li>
                        <li><a href="http://www.saintseiyasigma.com/caballeros/hades/espectros.php">Espectros</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="http://www.saintseiyasigma.com/anime/omega.php"><span class="icon icon-facetime-video circled"></span>Anime</a>
            <ul>
                <li class="omega"><a href="http://www.saintseiyasigma.com/anime/omega.php">Omega &Omega;</a></li>              
                <li class="omega"><a href="http://www.saintseiyasigma.com/anime/the_lost_canvas.php">The Lost Canvas</a></li>              
            </ul>
        <li><a href="http://www.saintseiyasigma.com/manga.php"><span class="icon icon-book circled"></span>Manga</a>
            <ul>
                <li class="episodio_g"><a href="http://www.saintseiyasigma.com/manga/episodio_g.php">Episodio G</a></li>
                <li class="the_lost_canvas"><a href="http://www.saintseiyasigma.com/manga/the_lost_canvas.php">The Lost Canvas</a></li>
                <li class="next_dimension"><a href="http://www.saintseiyasigma.com/manga/next_dimension.php">Next Dimension</a></li>
            </ul>
        </li>
        <li><a href="http://www.saintseiyasigma.com/galeria.php"><span class="icon icon-picture circled"></span>Galeria</a></li>
    </ul>
</nav>
