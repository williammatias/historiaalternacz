<div class="fb-like-box" data-href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>

<hr class="first" />
<section>
    <header>
        <h3><a href="http://www.saintseiyasigma.com/caballeros.php">Caballeros</a></h3>
    </header>
    <p>
        En esta sección del website hemos colocado los ejércitos de cada Dios de la serie original
        de Saint Seiya.
    </p>
    <footer>
        <a href="http://www.saintseiyasigma.com/caballeros.php" class="button">Ver Caballeros</a>
    </footer>
</section>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6835894736815485";
    /* SSMediumRect */
    google_ad_slot = "5354423255";
    google_ad_width = 300;
    google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
        src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<hr />
<section>
    <header>
        <h3><a href="http://www.saintseiyasigma.com/manga.php">Nuevos capitulos de los Mangas esta semana</a></h3>
    </header>
    <p>
        Estaremos publicando mas capitulos y mas Mangas del mundo de Saint Seiya, dale like a nuestra
        pagina de Facebook y mantente atento al contenido que vamos publicando en saintseiyasigma.com
    </p>
    <div class="row half no-collapse">
        <div class="u4">
            <a href="http://www.saintseiyasigma.com/manga/next_dimension/volumen_8/capitulo_61/1.php" class="image featured"><img src="http://www.saintseiyasigma.com/images/manga/next_dimension/volumen_8/capitulo_61/1.jpg" alt="Next Dimension - Capitulo 61 - Poema a un amigo" /></a>
        </div>
        <div class="u8">
            <h3><a href="http://www.saintseiyasigma.com/manga/next_dimension/volumen_8/capitulo_61/1.php">Next Dimension - Capitulo 61: Poema de Despedida a un amigo</a></h3>
        </div>
    </div>
    <div class="row half no-collapse">
        <div class="u4">
            <a href="http://www.saintseiyasigma.com/manga/episodio_g/volumen_4/capitulo_17/1.php" class="image featured"><img src="http://www.saintseiyasigma.com/images/manga/episodio_g/volumen_4/capitulo_17/1.jpg" alt="Episodio G - Capitulo 17 - Aquel que guia al mas alla" /></a>
        </div>
        <div class="u8">
            <h3><a href="http://www.saintseiyasigma.com/manga/episodio_g/volumen_4/capitulo_17/1.php">Episodio G - Capitulo 17: Aquel que guia al mas alla</a></h3>

        </div>
    </div>
    <div class="row half no-collapse">
        <div class="u4">
            <a href="http://www.saintseiyasigma.com/manga/the_lost_canvas/volumen_2/capitulo_10/1.php" class="image featured"><img src="http://www.saintseiyasigma.com/images/manga/the_lost_canvas/volumen_2/capitulo_10/1.jpg" alt="The Lost Canvas - Capitulo 10 - Castillo Malefico" /></a>
        </div>
        <div class="u8">
            <h3><a href="http://www.saintseiyasigma.com/manga/the_lost_canvas/volumen_2/capitulo_10/1.php">The Lost Canvas - Capitulo 10: Castillo Malefico</a></h3>
        </div>
    </div>
    <footer>
        <a href="http://www.saintseiyasigma.com/manga.php" class="button">Ver Mangas</a>
    </footer>
</section>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-6835894736815485";
/* SSMangaAds */
google_ad_slot = "4573864053";
google_ad_width = 300;
google_ad_height = 600;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>