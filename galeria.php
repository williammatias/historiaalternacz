<!DOCTYPE HTML>
<html>
    <head>
        <title>Galeria de Imagenes - Saint Seiya - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Galeria de Imagenes de SaintSeiyaSigma.com" />
        <?php
        include 'template/head.php';
        ?>
        <link rel="stylesheet" href="css/gallery/demo-styles.css" />
        <link rel="stylesheet" href="css/gallery/styles.css" />
    </head>
    <body class="galeria">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1 id="logo">Galeria Sigma</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include 'template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <p class="tips"> Puede utilizar las teclas de flecha para navegar entre las imágenes en la vista de diapositivas, y la tecla Shift para cambiar de nuevo a partir de diapositivas a vista de cuadrícula</p>
                        <header>
                            <h2>Galeria de Saint Seiya Sigma</h2>
                            <span class="byline">
                                <script type="text/javascript"><!--
                          google_ad_client = "ca-pub-6835894736815485";
                                    /* SSespectroLeaderBoard */
                                    google_ad_slot = "7629905257";
                                    google_ad_width = 728;
                                    google_ad_height = 90;
//-->
                                </script>
                                <script type="text/javascript"
                                        src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                </script>
                                <div class="fb-like-box" data-href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                            </span>
                        </header>
                        <div class="row">
                            <div class="u3">
                                <a target="_blank" href="galerias/caballeros_de_oro.php">
                                    <h3>Caballeros Dorados</h3>
                                    <img src="images/galeria/small/caballeros_dorados/dorados-header.jpg" alt="Caballeros Dorados" />
                                </a>
                            </div>
                            <div class="u3">
                                <a target="_blank" href="galerias/caballeros_de_plata.php">
                                    <h3>Caballeros De Plata</h3>
                                    <img src="images/galeria/small/caballeros_de_plata/elite-plata_01.jpg" alt="Caballeros de Plata" />
                                </a>
                            </div>
                            <div class="u3">
                                <a target="_blank" href="galerias/caballeros_de_bronce.php">
                                    <h3>Caballeros De Bronce</h3>
                                    <img src="images/galeria/small/caballeros_de_bronce/bronce_01.jpg" alt="Caballeros de Bronce" />
                                </a>
                            </div>
                            <div class="u3">
                                <a target="_blank" href="galerias/lugares.php">
                                    <h3>Lugares</h3>
                                    <img src="images/galeria/small/lugares/el-reloj-de-fuego.jpg" alt="Reloj de Fuego 12 Casas" />
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="u3">
                                <a target="_blank" href="galerias/generales_marinos.php">
                                    <h3>Generales Marinos</h3>
                                    <img src="images/galeria/small/generales_marinos/generales_marinos.jpg" alt="Generales Marinos" />
                                </a>
                            </div>
                            <div class="u3">
                                <a target="_blank" href="galerias/dioses_guerreros.php">
                                    <h3>Dioses Guerreros</h3>
                                    <img src="images/galeria/small/dioses_guerreros/dioses_guerreros.jpg" alt="Dioses Guerreros" />
                                </a>
                            </div>
                            <div class="u3">
                                <a target="_blank" href="galerias/espectros_de_hades.php">
                                    <h3>Espectros de Hades</h3>
                                    <img src="images/galeria/small/espectros/espectros.jpg" alt="Espectros de Hades" />
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Features -->
        <?php
        include 'template/featured.php';
        ?>


        <!-- Footer -->
        <?php
        include 'template/footer.php';
        ?>

        <script src="js/gallery/plugins.js"></script>
        <script src="js/gallery/scripts.js"></script>
        <script>
                                    $(document).ready(function() {
                                        $('#gallery-container').sGallery({
                                            fullScreenEnabled: true
                                        });
                                    });
        </script>
    </body>
</html>