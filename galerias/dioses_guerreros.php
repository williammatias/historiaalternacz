<!DOCTYPE HTML>
<html>
    <head>
        <title>Galeria de Dioses Guerreros - Saint Seiya - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Galeria de los Caballeros de oro de SaintSeiyaSigma.com" />
        <?php
        include '../template/head.php';
        ?>
        <link rel="stylesheet" href="../css/gallery/demo-styles.css" />
        <link rel="stylesheet" href="../css/gallery/styles.css" />
        
        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/galeria/small/dioses_guerreros/dioses_guerreros.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/galeria/small/dioses_guerreros/dioses_guerreros.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/galeria/small/dioses_guerreros/dioses_guerreros.jpg"/>
    </head>
    <body class="galeria">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../galeria.php" id="logo">Galeria de Dioses Guerreros</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <p class="tips"> Puede utilizar las teclas de flecha para navegar entre las imágenes en la vista de diapositivas, y la tecla Shift para cambiar de nuevo a partir de diapositivas a vista de cuadrícula</p>
                        <header>
                            <h2>Galeria de Saint Seiya Sigma</h2>
                            <span class="byline">
                                <script type="text/javascript"><!--
                          google_ad_client = "ca-pub-6835894736815485";
                                    /* SSespectroLeaderBoard */
                                    google_ad_slot = "7629905257";
                                    google_ad_width = 728;
                                    google_ad_height = 90;
//-->
                                </script>
                                <script type="text/javascript"
                                        src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                </script>
                            </span>
                        </header>
                        <div class="demo-wrapper">
                            <div id="gallery-container">

                                <ul class="items--small">
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/dioses_guerreros.jpg" alt="Dioses Guerreros" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/mime_01.jpg" alt="Mime de Benetnash Eta" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/syd_01.jpg" alt="Syd de Mizar Zeta" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/hagen_01.jpg" alt="Hagen de Merak Beta" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/siegfried.jpg" alt="Siedfried de Dubhe Alfa" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/thor.jpg" alt="Thor de Phecda Gamma" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/mime.jpg" alt="Mime de Benetnash Eta" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/hagen.jpg" alt="Hagen de Merak Beta" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/syd.jpg" alt="Syd de Mizar Zeta" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/bud.jpg" alt="Bud de Alcor Zeta" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/fenrir.jpg" alt="Fenrir de Alioth Epsilon" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/dioses_guerreros/alberich.jpg" alt="Alberich de Megrez Delta" /></a></li>
                                </ul>
                                <ul class="items--big">
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/dioses_guerreros.jpg"><img src="../images/galeria/big/dioses_guerreros/dioses_guerreros.jpg" alt="Dioses Guerreros" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/mime_01.jpg"><img src="../images/galeria/big/dioses_guerreros/mime_01.jpg" alt="Mime de Benetnash Eta" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/syd_01.jpg"><img src="../images/galeria/big/dioses_guerreros/syd_01.jpg" alt="Syd de Mizar Zeta" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/hagen_01.jpg"><img src="../images/galeria/big/dioses_guerreros/hagen_01.jpg" alt="Hagen de Merak Beta" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/siegfried.jpg"><img src="../images/galeria/big/dioses_guerreros/siegfried.jpg" alt="Siedfried de Dubhe Alfa" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/thor.jpg"><img src="../images/galeria/big/dioses_guerreros/thor.jpg" alt="Thor de Phecda Gamma" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/mime.jpg"><img src="../images/galeria/big/dioses_guerreros/mime.jpg" alt="Mime de Benetnash Eta" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/hagen.jpg"><img src="../images/galeria/big/dioses_guerreros/hagen.jpg" alt="Hagen de Merak Beta" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/syd.jpg"><img src="../images/galeria/big/dioses_guerreros/syd.jpg" alt="Syd de Mizar Zeta" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/bud.jpg"><img src="../images/galeria/big/dioses_guerreros/bud.jpg" alt="Bud de Alcor Zeta" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/fenrir.jpg"><img src="../images/galeria/big/dioses_guerreros/fenrir.jpg" alt="Fenrir de Alioth Epsilon" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/dioses_guerreros/alberich.jpg"><img src="../images/galeria/big/dioses_guerreros/alberich.jpg" alt="Alberich de Megrez Delta" /></a></li>
                                </ul>
                                <div class="controls">
                                    <span class="control icon-gallery-arrow-left" data-direction="previous"></span> 
                                    <span class="control icon-gallery-arrow-right" data-direction="next"></span> 
                                    <span class="grid icon-gallery-grid"></span>
                                    <span class="fs-toggle icon-gallery-fullscreen"></span>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Features -->
        <?php
        include '../template/featured.php';
        ?>


        <!-- Footer -->
        <?php
        include '../template/footer.php';
        ?>

        <script src="../js/gallery/plugins.js"></script>
        <script src="../js/gallery/scripts.js"></script>
        <script>
                                    $(document).ready(function() {
                                        $('#gallery-container').sGallery({
                                            fullScreenEnabled: true
                                        });
                                    });
        </script>
    </body>
</html>