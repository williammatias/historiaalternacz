<!DOCTYPE HTML>
<html>
    <head>
        <title>Galeria de Caballeros de Oro - Saint Seiya - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Galeria de los Caballeros de oro de SaintSeiyaSigma.com" />
        <?php
        include '../template/head.php';
        ?>
        <link rel="stylesheet" href="../css/gallery/demo-styles.css" />
        <link rel="stylesheet" href="../css/gallery/styles.css" />
    </head>
    <body class="galeria">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../galeria.php" id="logo">Galeria de Caballeros de oro</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <p class="tips"> Puede utilizar las teclas de flecha para navegar entre las imágenes en la vista de diapositivas, y la tecla Shift para cambiar de nuevo a partir de diapositivas a vista de cuadrícula</p>
                        <header>
                            <h2>Galeria de Saint Seiya Sigma</h2>
                            <span class="byline">
                                <script type="text/javascript"><!--
                          google_ad_client = "ca-pub-6835894736815485";
                                    /* SSespectroLeaderBoard */
                                    google_ad_slot = "7629905257";
                                    google_ad_width = 728;
                                    google_ad_height = 90;
//-->
                                </script>
                                <script type="text/javascript"
                                        src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                </script>
                            </span>
                        </header>
                        <div class="demo-wrapper">
                            <div id="gallery-container">

                                <ul class="items--small">
                                    <li class="item"><a href="#"><img src="../images/galeria/small/caballeros_dorados/dorados-header.jpg" alt="Caballeros Dorados" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/caballeros_dorados/dorados-hades.jpg" alt="Caballeros Dorados Saga de Hades" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/caballeros_dorados/dorados_constelaciones.jpg" alt="Constelaciones de Caballeros Dorados" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/caballeros_dorados/dorados_01.jpg" alt="Caballeros Dorados" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/caballeros_dorados/mu_de_aries.jpg" alt="Mu de Aries" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/caballeros_dorados/exclamacion_de_athena.jpg" alt="Exclamacion de Athena" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/caballeros_dorados/all-gold-saint.jpg" alt="Caballeros de Oro" /></a></li>
                                    <li class="item"><a href="#"><img src="../images/galeria/small/caballeros_dorados/shaka-y-aioria.jpg" alt="Shaka de Virgo y Aioria de Leo" /></a></li>
                                </ul>
                                <ul class="items--big">
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/caballeros_dorados/dorados-header.jpg"><img src="../images/galeria/big/caballeros_dorados/dorados-header.jpg" alt="Caballeros Dorados" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/caballeros_dorados/dorados-hades.jpg"><img src="../images/galeria/big/caballeros_dorados/dorados-hades.jpg" alt="Caballeros Dorados Saga de Hades" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/caballeros_dorados/dorados_constelaciones.jpg"><img src="../images/galeria/big/caballeros_dorados/dorados_constelaciones.jpg" alt="Constelaciones Caballeros Dorados" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/caballeros_dorados/dorados_01.jpg"><img src="../images/galeria/big/caballeros_dorados/dorados_01.jpg" alt="Caballeros De Oro" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/caballeros_dorados/mu_de_aries.jpg"><img src="../images/galeria/big/caballeros_dorados/mu_de_aries.jpg" alt="Mu de Aries" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/caballeros_dorados/exclamacion_de_athena.jpg"><img src="../images/galeria/big/caballeros_dorados/exclamacion_de_athena.jpg" alt="Exclamacion de Athena" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/caballeros_dorados/all-gold-saint.jpg"><img src="../images/galeria/big/caballeros_dorados/all-gold-saint.jpg" alt="Caballeros Dorados" /></a></li>
                                    <li class="item--big"><a target="_blank" href="../images/galeria/big/caballeros_dorados/shaka-y-aioria.jpg"><img src="../images/galeria/big/caballeros_dorados/shaka-y-aioria.jpg" alt="Shaka de Virgo y Aioria de Leo" /></a></li>
                                </ul>
                                <div class="controls">
                                    <span class="control icon-gallery-arrow-left" data-direction="previous"></span> 
                                    <span class="control icon-gallery-arrow-right" data-direction="next"></span> 
                                    <span class="grid icon-gallery-grid"></span>
                                    <span class="fs-toggle icon-gallery-fullscreen"></span>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Features -->
        <?php
        include '../template/featured.php';
        ?>


        <!-- Footer -->
        <?php
        include '../template/footer.php';
        ?>

        <script src="../js/gallery/plugins.js"></script>
        <script src="../js/gallery/scripts.js"></script>
        <script>
                                    $(document).ready(function() {
                                        $('#gallery-container').sGallery({
                                            fullScreenEnabled: true
                                        });
                                    });
        </script>
    </body>
</html>