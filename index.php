
<!DOCTYPE HTML>
<html>
    <head>
        <title>Saint Seiya - Caballeros del Zodiaco - SaintSeiyaSigma.com</title>        
        <meta name="description" content="SaintSeiyaSigma.com la serie saint seiya vista desde el mejor punto de vista..." />

        <?php
        include 'template/head.php';
        ?>
        
    </head>
    <body class="homepage">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">La Saga Sigma</a></h1>
                    <hr />
                    <span class="byline">¿Qué hubiera ocurrido si saga no hubiera traicionado a Athena?</span>
                </header>
                <footer>
                    <a href="historia.php" class="button circled scrolly">Leer</a>
                </footer>
            </div>

            <!-- Nav -->
            <?php
            include 'template/navigation.php';
            ?>

        </div>
        


<div class="fb-like-box" data-href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>

        <!-- Features -->
        <?php
        include 'template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include 'template/footer.php';
        ?>
    </body>
</html>