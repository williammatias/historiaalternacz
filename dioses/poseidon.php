<!DOCTYPE HTML>
<html>
    <head>
        <title>Poseidon - Dios de los Mares - SaintSeiyaSigma.com</title>
        <meta name="description" content="Poseidón es el dios de los mares..." />

        <?php
        include '../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/poseidon.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/poseidon.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/poseidon.jpg"/>

    </head>
    <body class="dios_poseidon">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1 id="logo">Poseidon</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../images/caballeros/poseidon/armadura_poseidon.jpg">Escama de Poseidon</a></h3>
                                        <a href="../images/caballeros/poseidon/armadura_poseidon.jpg" class="image">
                                            <img src="../images/caballeros/poseidon/armadura_poseidon.jpg" alt="Armadura de Poseidon" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,77 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2> <a href="../images/caballeros/poseidon/poseidon.jpg">Dios Poseidon</a></h2>
                                        <span class="byline">
                                            Dios de los Mares
                                        </span>
                                    </header>
                                    <a href="../images/caballeros/poseidon/poseidon.jpg" class="image saint">
                                        <img src="../images/caballeros/poseidon/poseidon.jpg" alt="Poseidon" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/dioses/poseidon.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/dioses/poseidon.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 59 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Aries</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡El Emperador de los Mares engullendo la faz de la tierra!
                                        </p>
                                        <p>
                                            Uno de los 12 Dioses principales. Se ha disputado con Athena el dominio de la 
                                            superficie terrestre. Poseidón siempre reencarnó en los descendientes de la familia
                                            Solo, una millonaria potencia de comercio marítimo. Poseidón despertó no por voluntad
                                            propia sino porque Kanon accidentalmente encontró su tridente. Planeaba crear una
                                            utopía en la Tierra después de desaparecer a los seres humanos a través de un diluvio.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../template/footer_ad.php';
        ?>

    </body>
</html>