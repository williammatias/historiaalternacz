<!DOCTYPE HTML>
<html>
    <head>
        <title>Hades - Dios del Inframundo - SaintSeiyaSigma.com</title>
        <meta name="description" content="Hades es el Dios del Reino de los muertos..." />

        <?php
        include '../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/hades.png"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/hades.png"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/hades.png"/>

    </head>
    <body class="dios_hades">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1 id="logo">Dios Hades</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../images/caballeros/hades/hades_sapuri.png">Hades Sapuri</a></h3>
                                        <a href="../images/caballeros/hades/hades_sapuri.png" class="image">
                                            <img src="../images/caballeros/hades/hades_sapuri.png" alt="Hades Sapuri" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,84 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../images/caballeros/hades/hades.png">Dios Hades</a></h2>
                                        <span class="byline">
                                        </span>
                                    </header>
                                    <a href="../images/caballeros/hades/hades.png" class="image saint">
                                        <img src="../images/caballeros/hades/hades.png" alt="Dios Hades" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/dioses/hades.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/dioses/hades.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 73 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Inframundo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            UNA MIRADA LIMPIA LLENA DE UNA PROFUNDA TRISTEZA. EL DIOS DEL INFRAMUNDO QUE NO CONOCE
                                            LO QUE ES EL AMOR.
                                        </p>
                                        <p>
                                            Dios que comanda el Inframundo. Disgustado con todo el mal causado por la humanidad, planea
                                            purificar la superficie terrestre. Desde la Era de los Dioses su espíritu encarna en el cuerpo
                                            físico de algún humano para intentar invadir la Tierra, pero todos los intentos fueron frustrados
                                            por Athena. Fue Aprisionado en la época de la Guerra Santa por la Athena antecesora, pero
                                            volvió al mundo actual. Ahora, su plan es exterminar a todos los seres vivos de la Tierra
                                            a través del "Gran Eclipse".
                                        </p>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../template/footer_ad.php';
        ?>

    </body>
</html>