<!DOCTYPE HTML>
<html>
    <head>
        <title>Kiki de Aries - Caballero de Oro Omega - SaintSeiyaSigma.com</title>
        <meta name="description" content="Era el antiguo discípulo de Mu de Aries y posee poderes de telequinesis y teletransportación, habilidades que le 
                                            fueron enseñadas por su maestro." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/kiki.png"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/kiki.png"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/kiki.png"/>

    </head>
    <body class="caballerodeoroomega kiki">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_omega.php" id="logo">Caballero de Oro Omega</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="info">
                                        <strong>Maestro:</strong>
                                        <span><a target="_blank" href="../caballeros_dorados/mu.php">Mu de Aries</a></span>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>1 de Abril</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_omega/kiki.png">Kiki de Aries</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_omega.php">Caballero de Oro en Omega</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_omega/kiki.png" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_omega/kiki.png" alt="Kiki de Aries" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega/kiki.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega/kiki.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Jamir, Tibet</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Aries</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                            Era el antiguo discípulo de Mu de Aries y posee poderes de telequinesis y teletransportación, habilidades que le 
                                            fueron enseñadas por su maestro. En la serie es el encargado de reparar las armaduras de la nueva generación 
                                            de caballeros, Kiki tiene a una alegre y pequeña aprendiz llamada Raki, que pertenece a la misma raza que su 
                                            maestro, al principio Kiki no quería ser encontrado pero debido a las acciones de Marte al asesinar a Aria decidió
                                            fingir estar del lado de Marte asumiendo su cargo de Caballero de Dorado de Aries.
                                        </p>
                                        <p>
                                            Kiki es el Caballero Dorado de Aries y el guardián del primer templo, el Templo de Aries. Se hace llamar
                                            El Caballero Dorado de la Regeneración y el Clima. En la segunda temporada aparece en Jamir rindiéndole ofrendas
                                            a todos los caballeros caídos en la guerra contra Apsu y también aparece reparando las armaduras de Sōma y Yuna, 
                                            más tarde durante el asedio al Palacio Belda, Kiki junto a Harbinger y Fudō participan en la guerra contra Palas.
                                            Su procedencia es igual a la de su maestro Mu de Aries, Lemuria. Su cosmos-elemento es tierra.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>