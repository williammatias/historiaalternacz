<!DOCTYPE HTML>
<html>
    <head>
        <title>Paradox de Geminis - Caballero de Oro Omega - SaintSeiyaSigma.com</title>
        <meta name="description" content="Fue el Caballero Dorado Femenino de Géminis y guardiana del tercer templo, el Templo de Géminis. Paradox al igual que su
                                            predecesor, Saga padece de la doble personalidad." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/paradox.png"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/paradox.png"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/paradox.png"/>

    </head>
    <body class="caballerodeoroomega paradox">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_omega.php" id="logo">Caballero de Oro Omega</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="info">
                                        <strong>Lugar de Entrenamiento:</strong>
                                        <span>Santuario de Athena, Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Cosmo-Elemento:</strong>
                                        <span>Viento</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_omega/paradox.png">Paradox de Géminis</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_omega.php">Caballero de Oro en Omega</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_omega/paradox.png" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_omega/paradox.png" alt="Paradox de Géminis" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega/paradox.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega/paradox.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Inglaterra</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Geminis</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            Fue el Caballero Dorado Femenino de Géminis y guardiana del tercer templo, el Templo de Géminis. Paradox al igual que su
                                            predecesor, Saga padece de la doble personalidad; su primera personalidad se llama Paradox del Amor y es una persona 
                                            tranquila y amorosa, esa personalidad se hace llamar El Caballero Dorado de el Amor y el Destino; su segunda personalidad
                                            se llama Paradox del Odio y es una persona totalmente diferente en apariencia, esta personalidad tiende a ser demasiado 
                                            violenta a tal punto de atacar sin piedad a sus enemigos, esta personalidad se hace llamar El Caballero Dorado 
                                            de el Odio y la Muerte.
                                        </p>
                                        <p>
                                            En la segunda temporada, sin que ningún caballero supiera de su paradero, Paradox se encontraba 
                                            encarcelada en Cabo Sunion pero fue con la ayuda de la Palasiana de Primer Nivel llamada Galia,
                                            que le ofrece unirse al ejercito de Palas a cambio de su libertad, Paradox acepta gustosamente
                                            convirtiéndose así en una Palasiana de Segundo Nivel. Su procedencia es de Inglaterra. 
                                            Su cosmos-elemento es viento
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>