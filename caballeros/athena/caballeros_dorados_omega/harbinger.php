<!DOCTYPE HTML>
<html>
    <head>
        <title>Harbinger de Tauro - Caballero de Oro Omega - SaintSeiyaSigma.com</title>
        <meta name="description" content="Es el Caballero Dorado de Tauro y el guardián del segundo templo, el Templo de Tauro. Se hace llamar El Caballero
                                            Dorado de la Fuerza y la Destrucción." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/harbinger.png"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/harbinger.png"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/harbinger.png"/>

    </head>
    <body class="caballerodeoroomega harbinger">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_omega.php" id="logo">Caballero de Oro Omega</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="info">
                                        <strong>Lugar de Entrenamiento:</strong>
                                        <span>Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Cosmo-Elemento:</strong>
                                        <span>Rayo</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_omega/kiki.png">Harbinger de Tauro</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_omega.php">Caballero de Oro en Omega</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_omega/harbinger.png" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_omega/harbinger.png" alt="Harbinger de Tauro" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega/harbinger.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega/harbinger.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Nueva York, (Estados Unidos)</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Tauro</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            Es el Caballero Dorado de Tauro y el guardián del segundo templo, el Templo de Tauro. Se hace llamar El Caballero
                                            Dorado de la Fuerza y la Destrucción. Harbinger es una persona sádica que le gusta el sonido de los huesos romperse,
                                            su apariencia es muy parecida a la de Hasgard de Tauro, también al igual que sus predecesores es una persona terca
                                            y muy testadura. Al principio mostró ser enemigo de los Caballeros pero más tarde él mismo decidió ayudarlos.
                                        </p>
                                        <p>
                                            En la segunda temporada, Athena le confía su Kamei al caballero dorado y Harbinger avergonzado acepta la misión, 
                                            por lo tanto él se convierte en el guardián de la armadura de Athena y más tarde va al Palacio Belda, junto Kiki
                                            y Fudō para participar en la guerra contra Palas. Su procedencia es de Estados Unidos. Su cosmos-elemento es trueno.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>