<!DOCTYPE HTML>
<html>
    <head>
        <title>Genbu de Libra - Caballero de Oro Omega - SaintSeiyaSigma.com</title>
        <meta name="description" content="Fue el Caballero Dorado de Libra y el guardián del séptimo templo, el Templo de Libra. Se hace llamar 
                                            El Caballero Dorado de el Equilibrio y la Armonía." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/genbu.png"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/genbu.png"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_omega/genbu.png"/>

    </head>
    <body class="caballerodeoroomega genbu">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_omega.php" id="logo">Caballero de Oro Omega</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="info">
                                        <strong>Maestro:</strong>
                                        <span>Dohko</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de Entrenamiento:</strong>
                                        <span>5 Picos</span>
                                    </div>
                                    <div class="info">
                                        <strong>Cosmo-Elemento:</strong>
                                        <span>Agua</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_omega/genbu.png">Genbu de Libra</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_omega.php">Caballero de Oro en Omega</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_omega/genbu.png" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_omega/genbu.png" alt="Genbu de Libra" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega/genbu.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega/genbu.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>China</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Libra</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            Fue el Caballero Dorado de Libra y el guardián del séptimo templo, el Templo de Libra. Se hace llamar 
                                            El Caballero Dorado de el Equilibrio y la Armonía. Genbu es el último discípulo del maestro Dohko de Libra
                                            y el condiscípulo de Shiryū a quien lo sustituiría como Caballero Dorado de Libra en la guerra contra Marte,
                                            peleó contra Tokisada de Acuario resultando Genbu ganador. 
                                        </p>
                                        <p>
                                            En la segunda temporada, Genbu hace una breve aparición en el Santuario ya que Atenea convocó a todos sus 
                                            caballeros para avisarles que inició una nueva guerra santa contra su hermana menor Palas, luego aparece
                                            para pelear en Palestra contra el palasiano de segundo nivel y discípulo de Hyperion, Aegir del Brazo Fantasma,
                                            al principio Genbu tuvo la ventaja en la pelea pero luego de que Hyperion le prestara su arma a su discípulo,
                                            rápidamente Genbu cae en desventaja aún a pesar de haber usado la Espada de Libra, Genbu muere al ser casi 
                                            cortado en dos luego de que Aegir amenazara con asesinar a los caballeros de Palestra, antes de morir Genbu
                                            estalla su cosmos hasta el infinito para destruir la Espada de la Destrucción pero solo consigue hacerle una 
                                            pequeña grieta a la espada en cambio una de las espadas de Libra (la que él usó) fue destruida y, en sus 
                                            momentos de agonía se despide de los jóvenes caballeros. Su procedencia es de China. Su cosmos-elemento es agua.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>