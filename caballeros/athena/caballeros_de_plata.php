<!DOCTYPE HTML>
<html>
    <head>
        <title>Caballeros de Plata - Saint Seiya Sigma</title>
        <meta name="description" content="Los caballeros de plata se encuentran en un rango..." />
        
        <?php
        include '../../template/head.php';
        ?>
    </head>
    <body class="caballerodeplata">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">Caballeros de Plata</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <a href="caballeros_de_plata/agora.php">
                                        <div class="saint_box info">                                        
                                            <strong>Agora de Loto</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/albiore.php">
                                        <div class="saint_box info">                                        
                                            <strong>Albiore de Cefeo</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/algethi.php">
                                        <div class="saint_box info">                                        
                                            <strong>Algethi de Heracles</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_de_plata/aracne.php">
                                        <div class="saint_box info">                                        
                                            <strong>Aracne de Tarantula</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/argol.php">
                                        <div class="saint_box info">                                        
                                            <strong>Argol de Perseo</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/asterion.php">
                                        <div class="saint_box info">                                        
                                            <strong>Asterion de Perros de Caza</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_de_plata/babel.php">
                                        <div class="saint_box info">                                        
                                            <strong>Babel de Centauro</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/capella.php">
                                        <div class="saint_box info">                                        
                                            <strong>Capella de Auriga</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/dante.php">
                                        <div class="saint_box info">                                        
                                            <strong>Dante de Cerbero</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_de_plata/dio.php">
                                        <div class="saint_box info">                                        
                                            <strong>Dio de Mosca</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/jamian.php">
                                        <div class="saint_box info">                                        
                                            <strong>Jamian de Cuervo</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/misty.php">
                                        <div class="saint_box info">                                        
                                            <strong>Misty de Lagarto</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_de_plata/moses.php">
                                        <div class="saint_box info">                                        
                                            <strong>Moses de Ballena</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/orpheo.php">
                                        <div class="saint_box info">                                        
                                            <strong>Orpheo de la Lira</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/plotemy.php">
                                        <div class="saint_box info">                                        
                                            <strong>Plotemy de Flecha</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_de_plata/shiva.php">
                                        <div class="saint_box info">                                        
                                            <strong>Shiva de Pavo Real</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_plata/sirius.php">
                                        <div class="saint_box info">                                        
                                            <strong>Sirius de Can Mayor</strong>
                                        </div>                                        
                                    </a>
                                </div>
                            </section>

                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../template/footer_ad.php';
        ?>

    </body>
</html>