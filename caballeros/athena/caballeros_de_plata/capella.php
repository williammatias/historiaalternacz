<!DOCTYPE HTML>
<html>
    <head>
        <title>Capella de Auriga - Caballero de Plata - SaintSeiyaSigma.com</title>
        <meta name="description" content="Capella de Auriga es el Santo de Plata de de Saint Seiya..." />
        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/capella.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/capella.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/capella.jpg"/>

    </head>
    <body class="caballerodeplata capella">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_de_plata.php" id="logo">Caballero de Plata</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/plata/armadura_auriga.jpg">Armadura de Auriga</a></h3>
                                        <a href="../../../images/caballeros/athena/plata/armadura_auriga.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/plata/armadura_auriga.jpg" alt="Armadura de Auriga" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>21 de Agosto</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>17</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1.83 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/plata/capella.jpg">Capella de Auriga</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_de_plata.php">Caballero de Plata</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/plata/capella.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/plata/capella.jpg" alt="Capella de Auriga" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/capella.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/capella.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 83 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Santuario de Atena, Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Auriga</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Leo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡Nadie puede escapar a la eterna persecusión de su disco!
                                        </p>
                                        <p>
                                            Es un santo de plata que utiliza como arma un disco energizado con su cosmo.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            Su disco es simplemente un recurso para amenazar al enemigo. Su verdadero golpe 
                                            secreto nunca fue revelado.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>