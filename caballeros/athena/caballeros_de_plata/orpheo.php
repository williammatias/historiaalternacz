<!DOCTYPE HTML>
<html>
    <head>
        <title>Orfeo de la Lira - Caballero de Plata - SaintSeiyaSigma.com</title>
        <meta name="description" content="Orpheo de la Lira es el Santo de Plata de la Lira de Saint Seiya..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/orpheo.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/orpheo.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/orpheo.jpg"/>


        <script type="text/javascript">
               

//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/orfeo_soundtrack.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });


            });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>

    </head>
    <body class="caballerodeplata orfeo">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_de_plata.php" id="logo">Caballero de Plata</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">
            <div id="jquery_jplayer_1" class="jp-jplayer"></div>

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/plata/armadura_lira.jpg">Armadura de la Lira</a></h3>
                                        <a href="../../../images/caballeros/athena/plata/armadura_lira.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/plata/armadura_lira.jpg" alt="Armadura de la Lira" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>15 de Noviembre</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>19</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,73 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/plata/orpheo.jpg">Orfeo de la Lira</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_de_plata.php">Caballero de Plata</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/plata/orpheo.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/plata/orpheo.jpg" alt="Orfeo de la Lira" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/orpheo.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/orpheo.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 60 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>El Santuario de Atena, Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Lyra</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Escorpio</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            Los sonidos de su lira son tristes acordes de amor.
                                        </p>
                                        <p>
                                            Un legendario Santo de Plata que había desaparecido hace tiempo, cuya fuerza se creía
                                            que era superior a la de los Santos de Oro. Era un hábil tocador de lira. Va para
                                            el Mundo de los Muertos para buscar a su amada Eurídice, pero Pandora le gusta tanto el
                                            sonido de su lira que termina convirtiendo a Eurídice en piedra para detenerlos en el infierno.
                                        </p>
                                        <p>
                                            A partir de eso, Orpheo decide permanecer por voluntad propia al lado de su amada en el
                                            Mundo de los Muertos siendo considerado traidor por el Santuario. Al final decide volver
                                            a luchar por Athena.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            No usa técnicas de lucha corporal, prefiere atacar con su lira. El sonido de las cuerdas
                                            de su lira paraliza al enemigo y causa daños a su mente. El golpe con el que logra dormir
                                            al enemigo consigue alcanzar hasta a los dioses.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>