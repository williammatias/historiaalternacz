<!DOCTYPE HTML>
<html>
    <head>
        <title>Aracne de Tarantula - Caballero de Plata - SaintSeiyaSigma.com</title>
        <meta name="description" content="Aracne de Tarantula es el Santo de Plata de Tarantula de Saint Seiya..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/aracne.jpeg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/aracne.jpeg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/aracne.jpeg"/>

    </head>
    <body class="caballerodeplata albiore">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_de_plata.php" id="logo">Caballero de Plata</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/plata/armadura_tarantula.jpg">Armadura de Tarantula</a></h3>
                                        <a href="../../../images/caballeros/athena/plata/armadura_tarantula.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/plata/armadura_tarantula.jpg" alt="Armadura de Tarantula" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>12 de Enero</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>17</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1.90 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/plata/aracne.jpeg">Aracne de Tarantula</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_de_plata.php">Caballero de Plata</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/plata/aracne.jpeg" class="image saint">
                                        <img src="../../../images/caballeros/athena/plata/aracne.jpeg" alt="Aracne de Tarantula" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/aracne.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/aracne.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 88 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Turquia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Mar Negro, Turquia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Tarantula</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Capricornio</span>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>