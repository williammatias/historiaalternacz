<!DOCTYPE HTML>
<html>
    <head>
        <title>Jamian de Cuervo - Caballero de Plata - SaintSeiyaSigma.com</title>
        <meta name="description" content="Jamian de Cuervo es el Santo de Plata de Cuervo de Saint Seiya..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/jamian.png"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/jamian.png"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/plata/jamian.png"/>

    </head>
    <body class="caballerodeplata jamian">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_de_plata.php" id="logo">Caballero de Plata</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/plata/armadura_cuervo.png">Armadura de Cuervo</a></h3>
                                        <a href="../../../images/caballeros/athena/plata/armadura_cuervo.png" class="image">
                                            <img src="../../../images/caballeros/athena/plata/armadura_cuervo.png" alt="Armadura de Cuervo" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>27 de Febrero</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>17</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1.66 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/plata/jamian.png"">Jamian de Cuervo</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_de_plata.php">Caballero de Plata</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/plata/jamian.png" class="image saint">
                                        <img src="../../../images/caballeros/athena/plata/jamian.png" alt="Jamian de Cuervo" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/jamian.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/jamian.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 56 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Reino Unido</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Escocia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Corbus</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Acuario</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡Alas negras que cubren el cielo! Un asesino que surge entre las aves.
                                        </p>
                                        <p>
                                            Santo de Plata que controla los cuervos. La Cloth de la constelación del cuervo 
                                            tiene una estructura leve debido a la naturaleza aérea de su constelación. 
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            Consigue volar por los cielos con la ayuda de sus cuervos. ¡El ataque que cae de los cielos es
                                            tremendamente fuerte!
                                        </p>
                                        <p>
                                            Su técnica es la "Ráfaga del Ala Negra" con la cual los cuervos energizados con su cosmo
                                            cubren y atrapan al enemigo.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>