<!DOCTYPE HTML>
<html>
    <head>
        <title>Caballeros de Bronce - Saint Seiya Sigma</title>
        <meta name="description" content="Los caballeros de Bronce son los de más bajo rango
              en la caballería de athena..." />
        
        <?php
        include '../../template/head.php';
        ?>
    </head>
    <body class="caballerodebronce">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">Caballeros de Bronce</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <a href="caballeros_de_bronce/seiya.php" class="seiya">
                                        <div class="info">                                        
                                            <strong>Seiya de Pegaso</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_bronce/shun.php" class="shun">
                                        <div class="info">                                        
                                            <strong>Shun de Andromeda</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_bronce/geki.php" class="geki">
                                        <div class="info">                                        
                                            <strong>Geki del Oso</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_de_bronce/shiryu.php" class="shiryu">
                                        <div class="info">                                        
                                            <strong>Shiryu del Dragon</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_bronce/ikki.php" class="ikki">
                                        <div class="info">                                        
                                            <strong>Ikki del Fenix</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_bronce/ichi.php" class="ichi">
                                        <div class="info">                                        
                                            <strong>Ichi de Hidra</strong>
                                        </div>                                       
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_de_bronce/hyoga.php" class="hyoga">
                                        <div class="info">                                        
                                            <strong>Hyoga del Cisne</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_bronce/ban.php" class="ban">
                                        <div class="info">                                        
                                            <strong>Ban de Leon Menor</strong>
                                        </div>                                        
                                    </a>
                                    <a href="caballeros_de_bronce/jabu.php" class="jabu">
                                        <div class="info">                                        
                                            <strong>Jabu del Unicornio</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_de_bronce/nachi.php" class="nachi">
                                        <div class="info">                                        
                                            <strong>Nachi del Lobo</strong>
                                        </div>                                        
                                    </a>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../template/footer_ad.php';
        ?>

    </body>
</html>