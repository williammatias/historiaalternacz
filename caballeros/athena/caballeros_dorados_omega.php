<!DOCTYPE HTML>
<html>
    <head>
        <title>Caballeros de Oro Omega - Saint Seiya Sigma</title>
        <meta name="description" content="Los Caballeros Dorados son los caballeros más fuertes de la caballeria
              de Athena." />

        <?php
        include '../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/galeria/big/caballeros_dorados_omega/all-gold-saint.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/galeria/big/caballeros_dorados_omega/all-gold-saint.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/galeria/big/caballeros_dorados_omega/all-gold-saint.jpg"/>

    </head>
    <body class="caballerodeoroomega">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">Caballeros de Oro Omega</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <div class="row">
                                <div class="u4"><div></div></div>
                                <div class="share u4">
                                    <!--Facebook-->
                                    <div id="fb-root"></div>
                                    <script>(function(d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id))
                                                return;
                                            js = d.createElement(s);
                                            js.id = id;
                                            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>

                                    <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                    <!--Twitter-->

                                    <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_omega.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                    <script>!function(d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (!d.getElementById(id)) {
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "https://platform.twitter.com/widgets.js";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }
                                        }(document, "script", "twitter-wjs");
                                    </script>

                                    <!-- Place this tag where you want the +1 button to render. -->
                                    <div class="g-plusone" data-size="tall"></div>

                                    <!-- Place this tag after the last +1 button tag. -->
                                    <script type="text/javascript">
                                        window.___gcfg = {lang: 'es'};

                                        (function() {
                                            var po = document.createElement('script');
                                            po.type = 'text/javascript';
                                            po.async = true;
                                            po.src = 'https://apis.google.com/js/plusone.js';
                                            var s = document.getElementsByTagName('script')[0];
                                            s.parentNode.insertBefore(po, s);
                                        })();
                                    </script>
                                </div>
                                <div class="u4"></div>
                            </div>

                            <section class="row">
                                <div class="u4">  
                                    <a href="caballeros_dorados_omega/kiki.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/mu_half.png" alt="Mu de Aries"/>-->
                                        <div class="info">                                        
                                            <strong>Kiki de Aries</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/harbinger.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/aldebaran_half.png" alt="Aldebarán de Tauro"/>-->
                                        <div class="info">
                                            <strong>Harbinger de Tauro</strong>
                                        </div>
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/paradox.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/saga_half.png" alt="Saga de Géminis"/>-->
                                        <div class="info">
                                            <strong>Paradox de Géminis</strong>
                                        </div>
                                    </a>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/schiller.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/mascara_mortal_half.png" alt="Mascara Mortal de Cáncer"/>-->
                                        <div class="info">
                                            <strong>Schiller de Cáncer</strong>
                                        </div>
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/micenas.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/aioria_half.png" alt="Aioria de Leo"/>-->
                                        <div class="info">
                                            <strong>Micenas de Leo</strong>
                                        </div>
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/fudo.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/shaka_half.png" alt="Shaka de Virgo"/>-->
                                        <div class="info">                                        
                                            <strong>Fudō de Virgo</strong>
                                        </div>                                        
                                    </a>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/genbu.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/dohko_half.png" alt="Dohko de Libra"/>-->
                                        <div class="info">
                                            <strong>Genbu de Libra</strong>
                                        </div>
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/sonia.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/milo_half.png" alt="Milo de Escorpio"/>-->
                                        <div class="info">
                                            <strong>Sonia de Escorpio</strong>
                                        </div>
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/seiya.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/aioros_half.png" alt="Aioros de Sagitario"/>-->
                                        <div class="info">
                                            <strong>Seiya de Sagitario</strong>
                                        </div>
                                    </a>   
                                </div>
                            </section>
                            <section class="row">
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/ionia.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/shura_half.png" alt="Shura de Capricornio"/>-->
                                        <div class="info">
                                            <strong>Ionia de Capricornio</strong>
                                        </div>
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/tokisada.php" class="image boxsaint">
                                        <!--<img class="featured" src="../../images/caballeros/athena/dorados/half/camus_half.png" alt="Camus de Acuario"/>-->
                                        <div class="info">
                                            <strong>Tokisada de Acuario</strong>
                                        </div>
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="caballeros_dorados_omega/amor.php" class="image boxsaint">
<!--                                        <img class="featured" src="../../images/caballeros/athena/dorados/half/afrodita_half.png" alt="Afrodita de Piscis"/>-->
                                        <div class="info">
                                            <strong>Amor de Piscis</strong>
                                        </div>
                                    </a>
                                </div>
                            </section>

                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../template/footer_ad.php';
        ?>

    </body>
</html>