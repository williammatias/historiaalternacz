<!DOCTYPE HTML>
<html>
    <head>
        <title>Mu de Aries - Caballero de Oro - SaintSeiyaSigma.com</title>
        <meta name="description" content="Mu de Aries es el caballero dorado que proteje el primero de los 12 templos del Santuario..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/mu.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/mu.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/mu.jpg"/>

    </head>
    <body class="caballerodeoro mu">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados.php" id="logo">Caballero de Oro</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados/armadura_aries.jpg">Armadura De Aries</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados/armadura_aries.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados/armadura_aries.jpg" alt="Armadura De Aries" />
                                        </a>
                                    </div>
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Personalidad</h3>
                                        </header>
                                        <p>
                                            Mu de Aries es el caballero dorado que proteje el primero de los 12 templos del Santuario.
                                            Es un caballero tranquilo que solo pelea si es estrictamente necesario. A pesar de su
                                            actitud serena este caballero posee un cosmos muy violento, especialmente cuando el carnero dorado
                                            muestra sus cuernos.
                                        </p>

                                        <p>
                                            Es el unico caballero con la capacidad de reparar las armaduras. Fue entrenado en Jamir por su maestro
                                            Shion (El caballero de Aries de la anterior guerra santa). Su unico discipulo es kiki.
                                        </p>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados/mu.jpg">Mu de Aries</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados.php"><a href="../caballeros_dorados.php">Caballero de Oro</a></a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados/mu.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados/mu.jpg" alt="Mu de Aries" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/mu.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/mu.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>27 de Marzo</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>20</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,82 m (6 pies 0 pulg)</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 75kg (165 lb)</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Frontera de China e India</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Jamir</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Aries</span>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>