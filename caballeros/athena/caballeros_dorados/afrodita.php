<!DOCTYPE HTML>
<html>
    <head>
        <title>Afrodita de Piscis - <a href="../caballeros_dorados.php">Caballero de Oro</a> - SaintSeiyaSigma.com</title>
        <meta name="description" content="Afrodita de Piscis es el guardian del doceavo templo del santuario de Athena..." />
        
        <?php
        include '../../../template/head.php';
        ?>
        
        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/afrodita.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/afrodita.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/afrodita.jpg"/>
        
    </head>
    <body class="caballerodeoro afrodita">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados.php" id="logo"><a href="../caballeros_dorados.php">Caballero de Oro</a></a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados/armadura_piscis.jpg">Armadura de Piscis</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados/armadura_piscis.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados/armadura_piscis.jpg" alt="Armadura de Piscis" />
                                        </a>
                                    </div>
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Personalidad</h3>
                                        </header>
                                        <p>
                                            Afrodita de Piscis es el guardian del doceavo templo del santuario de Athena el cual es el 
                                            ultimo de los 12 templos. El camino que conduce desde su templo hasta la recamara del patriarca
                                            puede ser cubierto por las rosas demoniacas de este caballero. Este caballero es muy orgulloso
                                            de su rango entre los caballeros de athena y de lo que puede hacer con su poder.
                                        </p>

                                        <p>
                                            A pesar de su belleza este caballero dorado tiene un poder terrible.
                                        </p>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados/afrodita.jpg">Afrodita de Piscis</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_dorados.php"><a href="../caballeros_dorados.php">Caballero de Oro</a></a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados/afrodita.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados/afrodita.jpg" alt="Afrodita de Piscis" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/afrodita.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/afrodita.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>10 de Marzo</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>22</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,83 m</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 72 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Suecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Groenlandia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Piscis</span>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>