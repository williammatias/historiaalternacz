<!DOCTYPE HTML>
<html>
    <head>
        <title>Shura de Capricornio - Caballero de Oro - SaintSeiyaSigma.com</title>
        <meta name="description" content="Shura de Capricornio es el caballero que proteje la decima casa del santuario..." />
        
        <?php
        include '../../../template/head.php';
        ?>
        
        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/shura.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/shura.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/shura.jpg"/>
        
    </head>
    <body class="caballerodeoro shura">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados.php" id="logo">Caballero de Oro</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados/armadura_capricornio.jpg">Armadura De Capricornio</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados/armadura_capricornio.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados/armadura_capricornio.jpg" alt="Armadura De Capricornio" />
                                        </a>
                                    </div>
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Personalidad</h3>
                                        </header>
                                        <p>
                                            Shura de Capricornio es el caballero que proteje la decima casa del santuario de Athena.
                                            Shura es el caballero mas fiel a Athena, por eso le fue otorgada la legendaria espada excalibur.
                                            Se dice que su brazo derecho es capaz de cortar todo incluso el acero.
                                        </p>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados/shura.jpg">Shura de Capricornio</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_dorados.php">Caballero de Oro</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados/shura.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados/shura.jpg" alt="Shura de Capricornio" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/shura.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/shura.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>12 de Enero</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>23</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,86 m</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 83 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>España</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Pirineos, España</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Capricornio</span>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>