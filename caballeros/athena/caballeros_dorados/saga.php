<!DOCTYPE HTML>
<html>
    <head>
        <title>Saga de Geminis - Caballero de Oro - SaintSeiyaSigma.com</title>
        <meta name="description" content="Saga de Geminis es el caballero que proteje el tercer templo de los 12 que conforman el 
                                            Santuario..." />
        
        <?php
        include '../../../template/head.php';
        ?>
        
        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/saga.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/saga.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/saga.jpg"/>
        
    </head>
    <body class="caballerodeoro saga">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados.php" id="logo">Caballero de Oro</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados/armadura_geminis.jpg">Armadura De Géminis</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados/armadura_geminis.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados/armadura_geminis.jpg" alt="Armadura De Géminis" />
                                        </a>
                                    </div>
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Personalidad</h3>
                                        </header>
                                        <p>
                                            Saga de Geminis es el caballero que proteje el tercer templo de los 12 que conforman el 
                                            Santuario. Es un caballero que segun dicen posee doble personalidad. Este caballero es muy
                                            respetado dentro del santuario por su gran poder, por eso muchos le llaman el semidios. Se
                                            dice tambien de saga que posee una personalidad buena y una mala.
                                        </p>

                                        <p>
                                            Tiene un hermano de gran poder llamado Kanon.
                                        </p>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados/saga.jpg">Saga de Géminis</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_dorados.php">Caballero de Oro</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados/saga.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados/saga.jpg" alt="Saga de Geminis" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/saga.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/saga.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>30 de Mayo</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>28</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,88 m</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 87kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Santuario de athena, Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Géminis</span>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>