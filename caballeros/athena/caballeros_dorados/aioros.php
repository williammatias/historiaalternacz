<!DOCTYPE HTML>
<html>
    <head>
        <title>Aioros de Sagitario - Caballero de Oro - SaintSeiyaSigma.com</title>
        <meta name="description" content="Aioros de Sagitario es el caballero que guarda el noveno templo del Santuario de Athena..." />
        
        <?php
        include '../../../template/head.php';
        ?>
        
        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/aioros.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/aioros.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados/aioros.jpg"/>
        
    </head>
    <body class="caballerodeoro aioros">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados.php" id="logo">Caballero de Oro</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados/armadura_sagitario.jpg">Armadura De Sagitario</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados/armadura_sagitario.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados/armadura_sagitario.jpg" alt="Armadura De Sagitario" />
                                        </a>
                                    </div>
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Personalidad</h3>
                                        </header>
                                        <p>
                                            Aioros de Sagitario es el caballero que guarda el noveno templo del Santuario de Athena.
                                            Es un caballero muy leal a athena. Entrenó a su hermano Aioria de Leo en el santuario.
                                            Se dice que su flecha dorada es capaz de enfrentar a los Dioses.
                                        </p>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados/aioros.jpg">Aioros de Sagitario</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_dorados.php">Caballero de Oro</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados/aioros.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados/aioros.jpg" alt="Aioros de Sagitario" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/aioros.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados/aioros.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>30 de Noviembre</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>14 (cuando murio)</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,87 m</span>
                                    </div>          
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 85 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Santuario de Atena, Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Sagitario</span>
                                    </div>
                                </div>
                            </section>                      
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>