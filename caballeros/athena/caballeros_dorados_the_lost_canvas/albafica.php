<!DOCTYPE HTML>
<html>
    <head>
        <title>Albafica de Piscis - Caballero de Oro The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="Es el santo de piscis en Lost Canvas. Sus técnicas son Royal Demon Rose, Piranha Rose, Bloody Rose,
                                           las tres técnicas utilizadas en la serie original por Afrodita de Piscis; y Crimson Thorn , 
                                           Espinas carmesí)." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/albafica.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/albafica.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/albafica.jpg"/>

    </head>
    <body class="caballerodeorothe_lost_canvas albafica">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_the_lost_canvas.php" id="logo">Caballero de Oro The Los Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_piscis.jpg">Armadura De Piscis</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_piscis.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_piscis.jpg" alt="Armadura De Piscis The Lost Canvas" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>20</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de Entrenamiento:</strong>
                                        <span>Santuario de Athena, Grecia</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/albafica.jpg">Albafica de Piscis</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_the_lost_canvas.php">Caballero de Oro en The Lost Canvas</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/albafica.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/albafica.jpg" alt="Albafica de Piscis" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/albafica.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/albafica.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Piscis</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                           Es el santo de piscis en Lost Canvas. Sus técnicas son Royal Demon Rose, Piranha Rose, Bloody Rose,
                                           las tres técnicas utilizadas en la serie original por Afrodita de Piscis; y Crimson Thorn , 
                                           Espinas carmesí), con esta técnica Albafika controla su sangre para crear muchas espinas con 
                                           su sangre envenenada y atacar al enemigo.
                                        </p>
                                        <p>
                                           A pesar de incapacidad para interactuar con las personas, debido al veneno que corre por su sangre, 
                                           Albafika desarrollo un gran cariño hacia la aldea de Rodorio que se encuentra cerca del Santuario. 
                                           Durante la guerra contra Hades, el santo de oro decidió expandir el jardín de rosas demoníacas, que 
                                           se encuentra comúnmente entre la casa de Piscis y la sala del Patriarca, al rededor de todo el Santuario;
                                           es ahí donde lo encuentra el grupo de Minos de Grifo cuando invade. Varios espectros mueren por el poder 
                                           de las rosas de Albafika hasta que Niobe de Profundo se ofrece para pelear. Niobe es inmune al veneno de 
                                           las rosas y utiliza su poder para podrir el jardín, por lo que Albafika tiene que utilizar su técnica más
                                           poderosa para vencerlo. Luego de vencer a Niobe, Albafika es atacado por Minos, quien destruye el jardín 
                                           y captura al santo de oro con sus hilos. Minos se da cuenta de que Albafika intenta proteger una aldea cercana
                                           y ordena a sus soldados seguir hacia Rodorio para destruirlo, pero son destruidos por las rosas blancas de Albafika.
                                           Albafika intenta detener a Minos con la misma técnica que venció a Niobe, pero Minos se defiende con sus alas 
                                           y lo deja quebrado y vencido antes de dirigirse a destruir la aldea. Albafika se recupera lo suficiente para llegar
                                           a la aldea y detener la pelea entre Minos y Shion, quien había venido a proteger la aldea, el santo de Piscis sacrifica
                                           su vida para perforar a Minos con una rosa llena de su sangre envenenada, lo que le causa la muerte.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>