<!DOCTYPE HTML>
<html>
    <head>
        <title>Hasgard de Tauro - Caballero de Oro The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="Hasgard se enfrentó a varios espectros y tuvo un enfrentamiento fuerte con Kagaho, el cual derrota Hasgard
                                            pero no lo mata al ver que no había maldad en el corazón de Kagaho." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/hasgard.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/hasgard.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/hasgard.jpg"/>

    </head>
    <body class="caballerodeorothe_lost_canvas hasgard">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_the_lost_canvas.php" id="logo">Caballero de Oro The Los Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_tauro.jpg">Armadura De Tauro</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_tauro.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_tauro.jpg" alt="Armadura De Tauro The Lost Canvas" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>28</span>
                                    </div>
                                    <div class="info">
                                        <strong>Alumnos:</strong>
                                        <span>Teneo, Celina, Salo</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/hasgard.jpg">Hasgard de Tauro</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_the_lost_canvas.php">Caballero de Oro en The Lost Canvas</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/hasgard.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/hasgard.jpg" alt="Hasgard de Tauro" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/hasgard.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/hasgard.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Tauro</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                            En la serie Lost Canvas podemos observar al antiguo Aldebaran de la pasada era mitológica,
                                            de nombre "Hasgard" y siendo llamado como todo caballero de Oro de Tauro "Aldebaran".
                                            Éste se encuentra entrenando a sus 3 discípulos Saro, Serinsa y Teneo. 
                                        </p>
                                        <p>
                                            Hasgard se enfrentó a varios espectros y tuvo un enfrentamiento fuerte con Kagaho, el cual derrota Hasgard
                                            pero no lo mata al ver que no había maldad en el corazón de Kagaho. Hasgard lleva una relación de amistad 
                                            con Tenma de Pegaso, el cual en cierto momento fue blanco de ataque para los espectros comandados por Pandora.
                                            Durante un ataque manda a 2 espectros a atacar a Tenma en el santuario, quien caía en profundo sueño junto
                                            a los discípulos de Tauro. Hasgard no cayó en sus técnicas y enfrentó solo a los dos espectros los cuales 
                                            lo paralizaron y mataron, no sin antes llevarse consigo a esos espectros también. 
                                            Tras la muerte de Aldebaran, Serinsa dice a Teneo que él será en un futuro el próximo caballero de Oro
                                            de Tauro.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>