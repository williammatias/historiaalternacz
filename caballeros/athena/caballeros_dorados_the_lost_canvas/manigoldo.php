<!DOCTYPE HTML>
<html>
    <head>
        <title>Manigoldo de Cancer - Caballero de Oro The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="Es el santo de cáncer en Lost Canvas. Su primera aparición es cuando encierra a Tenma en una prisión por órdenes
                                            del Patriarca Sage (el cual es su maestro) con el fin de proteger a Tenma y evitar que escape a su destino." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/manigoldo.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/manigoldo.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/manigoldo.jpg"/>

    </head>
    <body class="caballerodeorothe_lost_canvas manigoldo">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_the_lost_canvas.php" id="logo">Caballero de Oro The Los Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_cancer.jpg">Armadura De Cancer</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_cancer.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_cancer.jpg" alt="Armadura De Cancer The Lost Canvas" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>25</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de Entrenamiento:</strong>
                                        <span>Santuario de Athena, Grecia</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/manigoldo.jpg">Manigoldo de Cancer</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_the_lost_canvas.php">Caballero de Oro en The Lost Canvas</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/manigoldo.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/manigoldo.jpg" alt="Manigoldo de Cancer" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/manigoldo.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/manigoldo.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Cancer</span>
                                    </div>
                                    <div class="info">
                                        <strong>Maestro:</strong>
                                        <span>Sage</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                            Es el santo de cáncer en Lost Canvas. Su primera aparición es cuando encierra a Tenma en una prisión por órdenes
                                            del Patriarca Sage (el cual es su maestro) con el fin de proteger a Tenma y evitar que escape a su destino. 
                                            Luego de que Tenma escape de la prisión y se marche con Yato y Yuzuriha, el Santo de Cáncer lo sigue hasta el 
                                            Bosque de Thanatos, ya que ahora es el guardián de Tenma y sus compañeros. Mientras los jóvenes santos de bronce
                                            estaban en aprietos por el terrible poder de Verónica de Druj Nazu (Un espectro que recibió el poder inmortal de
                                            Thanatos para poder terminar con Tenma), el Santo de Cáncer incinera su alma usando el Sekishiki Souen, venciéndole
                                            sin muchos problemas. Luego sigue el cosmos de Thanatos hasta su Castillo, haciendo una gran aparición cayendo sobre
                                            el juego de ajedrez en el cual Hypnos y Thanatos se encontraban jugando, muy osado y valiente el Santo de Cáncer no 
                                            duda en desafiar a los dioses.
                                        </p>
                                        <p>
                                            Hypnos, que detesta las batallas sin sentido y la sangre, prefiere retirarse no sin antes advertir a su hermano 
                                            Thanatos que se encargue, por tener la culpa de la llegada del Santo de Cáncer. En ese instante Manigoldo entabla 
                                            una agonizante lucha frente a Thanatos, el dios de la muerte, quien tan solo pretende jugar con él sin tener que 
                                            utilizar mayores habilidades; en determinado momento se abre una dimensión, como la que conecta con el Campo de Elíseos,
                                            que absorbe a Manigoldo a punto de hacerlo pedazos, en ese instante hace la aparición el Patriarca del Santuario y 
                                            también maestro del Santo de Cáncer para salvarlo y acompañarle en la lucha frente al terrible dios de la muerte, 
                                            Manigoldo demuestra tener un poder muy grande al herir un poco al dios.
                                        </p>
                                        <p>
                                            Finalmente muere destrozado sacrificándose para destruir el cuerpo mortal de Thanatos, se le ve por última vez cuando 
                                            Shion llega a la casa de Cáncer y ve a Manigoldo con el casco del patriarca Sage, quien le pide que se lo de a su
                                            maestro(Harukei), finalmente su alma desaperece y solo queda la armadura de Cáncer en el templo. Como dato de personalidad,
                                            Manigoldo de Cáncer ha sido el caballero más arrogante, chistoso, burlón y osado entre todos los santos dorados de la 
                                            antigua era. Su seiyū es Daisuke Ono.
                                        </p>
                                        <p>
                                            Técnicas: Sekishiki Meikai Ha (Ondas Infernales), Acubens (Pinza de Cangrejo), Sekishiki Souen (Flamas Infernales),
                                            Sekishiki Konsoha (Sepultura de almas Infernal)
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>