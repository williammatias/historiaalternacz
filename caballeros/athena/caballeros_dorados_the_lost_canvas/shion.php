<!DOCTYPE HTML>
<html>
    <head>
        <title>Shion de Aries - Caballero de Oro The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="Caballero de Aries, guardián de la primera casa. Amigo y compañero muy cercano a Dohko de Libra. 
                                            Su maestro fue Hakurei de Altar." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/shion.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/shion.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/shion.jpg"/>

    </head>
    <body class="caballerodeorothe_lost_canvas shion">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_the_lost_canvas.php" id="logo">Caballero de Oro The Los Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_aries.jpg">Armadura De Aries</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_aries.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_aries.jpg" alt="Armadura De Aries" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de Entrenamiento:</strong>
                                        <span>Jamir</span>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>30 de Marzo</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/shion.jpg">Shion de Aries</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_the_lost_canvas.php">Caballero de Oro en The Lost Canvas</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/shion.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/shion.jpg" alt="Shion de Aries" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/shion.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/shion.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Lemuria</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Aries</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                            Caballero de Aries, guardián de la primera casa. Amigo y compañero muy cercano a Dohko de Libra. 
                                            Su maestro fue Hakurei de Altar. Tiene la capacidad de leer la historia (batallas) detrás de las
                                            armaduras que toca (lo cual le trae un placer a Shion), por ello en su juventud se le manifestó
                                            Lune de Balrog ofreciéndole ser el guardián de los libros que contenían la historia de la humanidad
                                            a cambio de destruir completamente aquellas armaduras que estaba reparando, lo cual comenzó a hacer;
                                            siendo éste el mayor crimen que pudo cometer Shion. Shion rechazó la oferta de Lune al ver que estaba
                                            apunto de destruir la armadura de su mentor "Hakurei de Altar", al ver Lune que se arrepintió lo hirió
                                            de gravedad. (Capítulos del 168 al 171 del Manga). Sus ataques principales son "Stardust Revolution" 
                                            y "Crystal Wall".
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>