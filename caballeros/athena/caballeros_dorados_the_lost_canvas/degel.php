<!DOCTYPE HTML>
<html>
    <head>
        <title>Degel de Acuario - Caballero de Oro The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="Dégel de Acuario Degel es el santo de acuario en 
                                           Lost Canvas. Aparece frente al castillo de Hades en Italia y calma al exaltado kardia." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/degel.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/degel.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/degel.jpg"/>

    </head>
    <body class="caballerodeorothe_lost_canvas degel">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_the_lost_canvas.php" id="logo">Caballero de Oro The Los Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_acuario.jpg">Armadura De Acuario</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_acuario.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_acuario.jpg" alt="Armadura De Acuario The Lost Canvas" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Blue Graad</span>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>7 de Febrero</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>22</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/degel.jpg">Degel de Acuario</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_the_lost_canvas.php">Caballero de Oro en The Lost Canvas</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/degel.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/degel.jpg" alt="Degel de Acuario" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/degel.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/degel.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Maestro:</strong>
                                        <span>Krest de Acuario</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Acuario</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                           Dégel de Acuario Degel es el santo de acuario en 
                                           Lost Canvas. Aparece frente al castillo de Hades en Italia y calma al exaltado kardia, diciéndole
                                           que la barrera tiene que desaparecer antes o seria el fin, mientras Hakurei llega al centro de la 
                                           barrera. Es quien envía a entrenar a Tenma con el demonio de la Isla de Kanon. Athena le encomendó
                                           la misión de ir a Bluegard a pedirle ayuda a Poseidón para poder atacar el Lost Canvas e invita a 
                                           Kardia de Escorpio a acompañarle en la misión. Cuando llega a Bluegard se encuentra con un amigo de
                                           la infancia llamado Unity, el señor de Bluegard, quien encamina a Kardia y a Degel a una habitación
                                           al fondo de la Biblioteca de Bluegard donde se encuentra la entrada a la Atlántida, pueblo de Poseidon.
                                        </p>
                                        <p>
                                            Ya en ese pueblo Unity es asesinado por Rhadamanthys, que junto con Pandora van a buscar a Poseidon,
                                           Degel los confronta pero es Kardia quien se queda a pelear con el Kyoto, provocando que Dégel parta 
                                           para detener a Pandora. Luego de sentir la muerte de Kardia y la de Radamanthys, Dégel entra al recinto
                                           donde para su sorpresa, ve a Pandora inconsciente.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>