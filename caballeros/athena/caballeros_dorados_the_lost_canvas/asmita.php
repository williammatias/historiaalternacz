<!DOCTYPE HTML>
<html>
    <head>
        <title>Asmita de Virgo - Caballero de Oro The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="Asmita es el santo de Virgo durante Lost Canvas, aparece en la Séptima prisión del Hades, al parecer su cosmos se 
                                           ha transportado ahí y ya sabía de la existencia del octavo sentido." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/asmita.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/asmita.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/asmita.jpg"/>

    </head>
    <body class="caballerodeorothe_lost_canvas asmita">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_the_lost_canvas.php" id="logo">Caballero de Oro The Los Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_virgo.jpg">Armadura De Virgo</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_virgo.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_virgo.jpg" alt="Armadura De Virgo The Lost Canvas" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>21</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>India</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/asmita.jpg">Asmita de Virgo</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_the_lost_canvas.php">Caballero de Oro en The Lost Canvas</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/asmita.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/asmita.jpg" alt="Asmita de Virgo" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/asmita.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/asmita.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Virgo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                           Asmita es el santo de Virgo durante Lost Canvas, aparece en la Séptima prisión del Hades, al parecer su cosmos se 
                                           ha transportado ahí y ya sabía de la existencia del octavo sentido. Cuestiona a Tenma de su lealtad a Atenea 
                                           y su razón de ser en este mundo, ya que él le revela al chico que es la reencarnación del santo de Pegaso legendario
                                           y que por causa suya es que Hades elige un cuerpo humano para ser su reencarnación, ya que el Pegaso original fue el
                                           único humano que logró golpearlo. A causa de esto, Tenma se siente culpable de que Alone se haya convertido en Hades
                                           y quiere más que nunca salvarlo. Un duelo final se entabla entre Asmita y Tenma, al final reconoce a Tenma como un 
                                           auténtico santo, dejando como misión a Tenma recolectar 108 frutos del Árbol de la Vida (el único ser viviente que crece
                                           en el Mekai).
                                        </p>
                                        <p>
                                            Luego de acabar con varios espectros que iban tras Tenma, con su sangre repara la armadura de este. Se sacrifica al 
                                            elevar su cosmo para crear el rosario de las 108 cuentas que atraparán los espíritus de los 108 espectros de Hades, 
                                            impidiendo revivirles, encomendándole la nueva misión de proteger a Athena. Asmita es ciego de nacimiento. Su espíritu
                                            aparece hablando con el demonio de la Isla Kanon. Puede usar las técnicas: Kahn, Tenma Kōfuku (Capitulación del Demonio),
                                            Riku dō rinne (Los 6 mundos), Tenkuja Chimimoryo (Horda de Espíritus) y Tenbu Hōrin (El Tesoro del Cielo).
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>