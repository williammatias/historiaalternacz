<!DOCTYPE HTML>
<html>
    <head>
        <title>Regulus de Leo - Caballero de Oro The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="Es el joven Santo de Leo en Lost Canvas, discípulo (al igual que Yato de Unicornio) del Santo 
                                           Sísifo de Sagitario. Hace su primera aparición en el capítulo 114 del manga." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/regulus.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/regulus.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/regulus.jpg"/>

    </head>
    <body class="caballerodeorothe_lost_canvas regulus">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_the_lost_canvas.php" id="logo">Caballero de Oro The Los Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>15</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Grecia</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/regulus.jpg">Regulus de Leo</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_the_lost_canvas.php">Caballero de Oro en The Lost Canvas</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/regulus.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/regulus.jpg" alt="Regulus de Leo" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/regulus.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/regulus.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Maestro:</strong>
                                        <span>Sisifo</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Leo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                           Es el joven Santo de Leo en Lost Canvas, discípulo (al igual que Yato de Unicornio) del Santo 
                                           Sísifo de Sagitario. Hace su primera aparición en el capítulo 114 del manga, en Jamil, donde es 
                                           enviado por su maestro Sísifo para proteger el Barco, que los llevará al Lost Canvas y a los Santos
                                           de Bronce que lo están reparando. Tuvo su primer combate enfrentando a la temida Violate de Behemont, 
                                           la cual es la mano derecha y compañera sentimental de Aiacos de Garuda.
                                        </p>
                                        <p>
                                            Como dato de personalidad, Regulus de Leo, es el santo más joven entre los doce santos de oro, prácticamente de la 
                                            generación de Yato y Tenma; posee una actitud juvenil, ingenua, inocente y poco sentimental hacia el enemigo, sin 
                                            realmente analizar a fondo las situaciones en las que se ve envuelto; le fascina el combate y superar a su rival y 
                                            a sí mismo. Posee como habilidad muy especial, la de poder imitar el nivel de combate y técnicas de sus rivales y compañeros.
                                            Podría ser el caballero dorado más poderoso en la historia del Lost Canvas.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>