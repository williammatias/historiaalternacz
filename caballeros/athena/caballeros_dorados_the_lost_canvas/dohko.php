<!DOCTYPE HTML>
<html>
    <head>
        <title>Dohko de Libra - Caballero de Oro The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="Al ser uno de los protagonistas, el papel de Dohko es vital en este manga. Una vez que recibió su 
                                           armadura se dirigió a Italia, donde se dice estaba a punto de renacer Hades." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/dohko.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/dohko.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/dorados_the_lost_canvas/dohko.jpg"/>

    </head>
    <body class="caballerodeorothe_lost_canvas dohko">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_dorados_the_lost_canvas.php" id="logo">Caballero de Oro The Los Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_libra.jpg">Armadura De Libra</a></h3>
                                        <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_libra.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/armadura_libra.jpg" alt="Armadura De Libra The Lost Canvas" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>18</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>China</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/dorados_the_lost_canvas/dohko.jpg">Dohko de Libra</a></h2>
                                        <span class="byline">                                            
                                            <a href="../caballeros_dorados_the_lost_canvas.php">Caballero de Oro en The Lost Canvas</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/dorados_the_lost_canvas/dohko.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/dorados_the_lost_canvas/dohko.jpg" alt="Dohko de Libra" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/dohko.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_dorados_the_lost_canvas/dohko.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de Entrenamiento:</strong>
                                        <span>Rozan</span>
                                    </div>
                                    <div class="info">
                                        <strong>Alumnos:</strong>
                                        <span>Tenma, Shiryu, Okko, Genbu</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Libra</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u12">  
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripcion</h3>
                                        </header>
                                        <p>
                                           Al ser uno de los protagonistas, el papel de Dohko es vital en este manga. Una vez que recibió su 
                                           armadura se dirigió a Italia, donde se dice estaba a punto de renacer Hades. Cuando se encuentra
                                           con el espectro de Cíclope y otros dos más Masei de Hades, el Santo no tiene problemas al 
                                           eliminarlos y se dirige al pueblo más cercano cuando se encuentra con el joven Tenma, el cual 
                                           despierta su cosmo para destruir una gigantesca roca y así cambiar el curso del río que iba a 
                                           inundar su aldea. El Santo de Libra se sorprende y rescata al muchacho que estaba a punto de ser
                                           tragado por la corriente del río. Más adelante le habla y lo convence para que lo acompañe a Grecia
                                           y así se convierta en un Santo de Atenea. Con el correr del tiempo, Dohko se convierte en el 
                                           maestro de Tenma y le enseña lo más importante para convertirse en Santo, hasta que llega el día
                                           en que Tenma se transforma en el nuevo Santo de Pegaso.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>