<!DOCTYPE HTML>
<html>
    <head>
        <title>Ikki del Fenix - Caballero de Bronce - SaintSeiyaSigma.com</title>
        <meta name="description" content="Ikki del Fenix, es el Santo de Bronce del Fenix de Saint Seiya..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/bronce/ikki.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/bronce/ikki.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/bronce/ikki.jpg"/>

    </head>
    <body class="caballerodebronce ikki">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_de_bronce.php" id="logo">Caballero de Bronce</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/bronce/armadura_fenix.jpg">Armadura del Fenix</a></h3>
                                        <a href="../../../images/caballeros/athena/bronce/armadura_fenix.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/bronce/armadura_fenix.jpg" alt="Armadura del Fenix" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>15 de Agosto</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>15</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,75 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/bronce/ikki.jpg">Ikki del Fenix</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_de_bronce.php">Caballero de Bronce</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/bronce/ikki.jpg" class="image saint">
                                        <img src="../../../images/caballeros/athena/bronce/ikki.jpg" alt="Ikki del Fenix" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/ikki.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/ikki.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 62 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Japon</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Isla de la Reina Muerte</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Phoenix</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Leo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡La solitaria ave inmortal que abandonó sus lágrimas y su bondad en el infierno!
                                        </p>
                                        <p>
                                            Hermano de Shun de Andromeda. En la Isla de la Reina muerte, donde fué entrenado,
                                            conoció a Esmeralda, su gran amor, muerta por Guilty quién entreno a Ikki. Posee
                                            la armadura del Fénix la cual puede resurgir de las Cenizas.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            Posee una fuerza mucho más allá de las capacidades de un <a href="../caballeros_de_bronce.php">Caballero de Bronce</a> y ha logrado
                                            triunfar en innumerables batallas con el batir de sus alas. También sabe usar técnicas
                                            psíquicas contra sus enemigos.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>