<!DOCTYPE HTML>
<html>
    <head>
        <title>Hyoga del Cisne - Caballero de Bronce - SaintSeiyaSigma.com</title>
        <meta name="description" content="Hyoga Del Cisne, es el Santo de Bronce del Cisne de Saint Seiya..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/athena/bronce/hyoga.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/bronce/hyoga.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/athena/bronce/hyoga.jpg"/>

    </head>
    <body class="caballerodebronce hyoga">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../caballeros_de_bronce.php" id="logo">Caballero de Bronce</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/athena/bronce/armadura_cisne.jpg">Armadura del Cisne</a></h3>
                                        <a href="../../../images/caballeros/athena/bronce/armadura_cisne.jpg" class="image">
                                            <img src="../../../images/caballeros/athena/bronce/armadura_cisne.jpg" alt="Armadura del Cisne" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>23 de Enero</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>14</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,73 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/athena/bronce/hyoga.png">Hyoga del Cisne</a></h2>
                                        <span class="byline">
                                            <a href="../caballeros_de_bronce.php">Caballero de Bronce</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/athena/bronce/hyoga.png" class="image saint">
                                        <img src="../../../images/caballeros/athena/bronce/hyoga.png" alt="Hyoga del Cisne" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/hyoga.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/athena/caballeros_de_plata/hyoga.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 60 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Rusia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Siberia, Rusia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Cygnus</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Acuario</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡Danza cisne! Lleva el frío del cero absoluto sobre tus alas.
                                        </p>
                                        <p>
                                            En las tierras frías de los glaciares eternos al este de Siberia, Hyoga se convierte
                                            en Santo teniendo como maestro a Cristal. Siguiendo las enseñanzas de su maestro, trata
                                            de mantenerse racional ante todo, pero tiene un lado emocional que no consigue abandonar.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            El Polvo de Diamantes es una de las técnicas más usadas por hyoga y su potencia va aumentando
                                            en cada lucha, al punto de conseguir derrotar a un espectro con un solo golpe.
                                        </p>
                                        <p>
                                            Luchando contra el mismo Camus de Acuario (El maestro de su maestro), Hyoga adquiere la máxima
                                            técnica de hielo: La Ejecución Aurora.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>