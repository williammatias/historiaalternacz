<!DOCTYPE HTML>
<html>
    <head>
        <title>Mime de Benetnasch Eta - Dios Guerrero de Asgard - SaintSeiyaSigma.com</title>
        <meta name="description" content="Mime de Benetnasch Eta es uno de los Dioses Guerreros 
              de Asgard" />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/odin/dioses_guerreros/mime.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/odin/dioses_guerreros/mime.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/odin/dioses_guerreros/mime.jpg"/>
        <script type="text/javascript">


//<![CDATA[
            $(document).ready(function() {

                $("#jquery_jplayer_1").jPlayer({
                    ready: function() {
                        $(this).jPlayer("setMedia", {
                            mp3: "http://www.saintseiyasigma.com/music/OrpheoRequeum.mp3"
                        });
                        play();
                    },
                    ended: function() {
                        play();
                    },
                    swfPath: "../../../js/jplayer",
                    supplied: "mp3",
                    wmode: "window",
                    smoothPlayBar: true,
                    keyEnabled: true
                });


    });

            function play() {
                $("#jquery_jplayer_1").jPlayer("play");
            }
//]]>
        </script>
    </head>
    <body class="dioses_guerreros mime">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../dioses_guerreros.php" id="logo">Dioses Guerreros de Asgard</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">
            <div id="jquery_jplayer_1" class="jp-jplayer"></div>

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/odin/dioses_guerreros/armadura_benetnash_eta.jpg">God Robe de Benetnasch Eta</a></h3>
                                        <a href="../../../images/caballeros/odin/dioses_guerreros/armadura_benetnash_eta.jpg" class="image">
                                            <img src="../../../images/caballeros/odin/dioses_guerreros/armadura_benetnash_eta.jpg" alt="God Robe de Benetnasch Eta" />
                                        </a>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/odin/dioses_guerreros/mime.jpg">Mime de Benetnasch Eta</a></h2>
                                        <span class="byline">
                                            <a href="../dioses_guerreros.php">Dios Guerrero</a>
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/odin/dioses_guerreros/mime.jpg" class="image saint">
                                        <img src="../../../images/caballeros/odin/dioses_guerreros/mime.jpg" alt="Mime de Benetnasch Eta" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/odin/dioses_guerreros/mime.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/odin/dioses_guerreros/mime.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Estrella:</strong>
                                        <span>Eta</span>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>