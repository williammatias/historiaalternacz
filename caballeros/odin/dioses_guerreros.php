<!DOCTYPE HTML>
<html>
    <head>
        <title>Dioses Guerreros de Asgard - Caballeros del Zodiaco</title>
        <meta name="description" content="Los Dioses guerreros de Asgard estan al servicio..." />
        
        <?php
        include '../../template/head.php';
        ?>
    </head>
    <body class="dioses_guerreros">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">Dioses Guerreros de Asgard</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <a href="dioses_guerreros/thor.php">
                                        <div class="info saint_box">                                        
                                            <strong>Thor de Phecda Gamma</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="dioses_guerreros/mime.php">
                                        <div class="info saint_box">                                        
                                            <strong>Mime de Benetnasch Eta</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="dioses_guerreros/hagen.php">
                                        <div class="info saint_box">                                        
                                            <strong>Hagen de Merak Beta</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="dioses_guerreros/siegfried.php">
                                        <div class="info saint_box">                                        
                                            <strong>Siegfried de Dubhe Alfa</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="dioses_guerreros/fenrir.php">
                                        <div class="info saint_box">                                        
                                            <strong>Fenrir de Alioth Epsilon</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="dioses_guerreros/syd.php">
                                        <div class="info saint_box">                                        
                                            <strong>Syd de Mizar Zeta</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="dioses_guerreros/bud.php">
                                        <div class="info saint_box">                                        
                                            <strong>Bud de Alcor Zeta</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="dioses_guerreros/alberich.php">
                                        <div class="info saint_box">                                        
                                            <strong>Alberich de Megrez Delta</strong>
                                        </div>                                        
                                    </a>
                                </div>
                            </section>

                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../template/footer_ad.php';
        ?>

    </body>
</html>