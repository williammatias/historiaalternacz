<!DOCTYPE HTML>
<html>
    <head>
        <title>Radamanthys de Wyvern - Juez del Infierno - SaintSeiyaSigma.com</title>
        <meta name="description" content="Radamanthys es uno de los 3 Jueces del Infierno..." />
        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/radamanthys.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/radamanthys.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/radamanthys.jpg"/>

    </head>
    <body class="espectros radamanthys">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../espectros.php" id="logo">Espectros de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/hades/espectros/armadura_wyvern.jpg">Sapuri de Wyvern</a></h3>
                                        <a href="../../../images/caballeros/hades/espectros/armadura_wyvern.jpg" class="image">
                                            <img src="../../../images/caballeros/hades/espectros/armadura_wyvern.jpg" alt="Armadura de Wyvern" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>30 de Octubre</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>23</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,89 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="#">Radamanthys de Wyvern</a></h2>
                                        <span class="byline">
                                            Juez del Infierno
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/hades/espectros/radamantys.jpg" class="image saint">
                                        <img src="../../../images/caballeros/hades/espectros/radamantys.jpg" alt="Radamanthys de Wyvern" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/hades/espectros/radamanthys.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/hades/espectros/radamanthys.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Estrella:</strong>
                                        <span>Estrella Celeste de la Ferocidad</span>
                                    </div>
                                    <div class="info">
                                        <strong>Rango:</strong>
                                        <span>Juez del Infierno / Espectro Celeste</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 84 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Inglaterra - Islas Fellows</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Escorpio</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            LAS ALAS NEGRAS SON UNA INVITACIÓN A LA MUERTE. EL DRAGÓN ALADO QUE COMANDA EL MUNDO DE LOS MUERTOS.
                                        </p>
                                        <p>
                                            Uno de los Tres Jueces del Inframundo, los más poderosos entre los 108 espectros existentes.
                                            Sus actitudes parecen ser motivadas por razones emocionales, pero eso es porque él actúa de
                                            acuerdo a su intuición, desarrollada en años de batalla. Hades confía en él y sus subordinados
                                            también.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            El golpe lanzado por su puño drecho dejaría fuera de combate hasta a un Caballero de Oro.
                                            Sólo uno de los Tres Jueces de los espectros podría someter al enemigo sin darle chance
                                            de contraatacar.
                                        </p>
                                        <p>
                                            Su gran poder de combate crea golpes mortales.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>