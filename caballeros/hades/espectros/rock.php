<!DOCTYPE HTML>
<html>
    <head>
        <title>Rock de Golem - Espectro de Hades - SaintSeiyaSigma.com</title>
        <meta name="description" content="Rock de Golem es uno de los 108 espectros de hades..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/rock.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/rock.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/rock.jpg"/>

    </head>
    <body class="espectros rock">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../espectros.php" id="logo">Espectros de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/hades/espectros/armadura_golem.jpg">Sapuri de Golem</a></h3>
                                        <a href="../../../images/caballeros/hades/espectros/armadura_golem.jpg" class="image">
                                            <img src="../../../images/caballeros/hades/espectros/armadura_golem.jpg" alt="Sapuri de Golem" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>17 de Junio</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>21</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,95 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="#">Rock de Golem</a></h2>
                                        <span class="byline">
                                            Estrella Celeste del Angulo
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/hades/espectros/rock.jpg" class="image saint">
                                        <img src="../../../images/caballeros/hades/espectros/rock.jpg" alt="Rock de Golem" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/hades/espectros/rock.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/hades/espectros/rock.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Rango:</strong>
                                        <span>Espectro Celeste</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 114 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Madagascar</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Geminis</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡UN GUERRERO PODEROSO QUE LANZA ENORMES PIEDRAS CON SU PODER MENTAL!
                                        </p>
                                        <p>
                                            Guardián de la Tercera Prisión. Su Gran Fuerza es comparable a la de Ivan de Troll. Su ataque
                                            favorito es el de lanzar piedras grandes. Era considerado invencible como guardián de la Tercera
                                            Prisión, pero murió en un abrir y cerrar de ojos.
                                        </p>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>