<!DOCTYPE HTML>
<html>
    <head>
        <title>Ivan de Troll - Espectro de Hades - SaintSeiyaSigma.com</title>
        <meta name="description" content="Ivan de Troll es uno de los 108 espectros de hades..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/ivan.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/ivan.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/ivan.jpg"/>

    </head>
    <body class="espectros ivan">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../espectros.php" id="logo">Espectros de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/hades/espectros/armadura_troll.jpg">Sapuri de Troll</a></h3>
                                        <a href="../../../images/caballeros/hades/espectros/armadura_troll.jpg" class="image">
                                            <img src="../../../images/caballeros/hades/espectros/armadura_troll.jpg" alt="Sapuri de Troll" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>30 de Agosto</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>21</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,92 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="#">Ivan de Troll</a></h2>
                                        <span class="byline">
                                            Estrella Celeste de la Derrota
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/hades/espectros/ivan.jpg" class="image saint">
                                        <img src="../../../images/caballeros/hades/espectros/ivan.jpg" alt="Ivan de Troll" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/hades/espectros/ivan.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/hades/espectros/ivan.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Rango:</strong>
                                        <span>Espectro Celeste</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 109 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Rusia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Virgo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡EL BRAZO FUERTE EN ACCIÓN! EL GUERRERO MÁS FUERTE FISICAMENTE DEL MUNDO DE LOS MUERTOS!
                                        </p>
                                        <p>
                                            Guardían de la tercera prisión, posee la mayor fuerza y energía de todos los espectros y las
                                            utiliza para la lucha cuerpo a cuerpo. Tiene gran resistencia a impactos y golpes.
                                        </p>
                                        <p>
                                            Acabó siendo derrotado antes de decir su nombre. Pero si hubiera sido un combate prolongado, 
                                            hasta el mismo Hyoga hubiera tenido trabajo para derrotarlo. Por eso mismo, decidió acabar
                                            rápido con el asunto.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Técnica</h3>
                                        </header>
                                        <p>
                                            Máxima Reestructuración
                                        </p>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>