<!DOCTYPE HTML>
<html>
    <head>
        <title>Minos de Grifo - Juez del Infierno - SaintSeiyaSigma.com</title>
        <meta name="description" content="Minos de Grifo es uno de los 3 Jueces del Infierno..." />
        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/minos.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/minos.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/minos.jpg"/>

    </head>
    <body class="espectros minos">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../espectros.php" id="logo">Espectros de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/hades/espectros/armadura_grifo.jpg">Sapuri de Grifo</a></h3>
                                        <a href="../../../images/caballeros/hades/espectros/armadura_grifo.jpg" class="image">
                                            <img src="../../../images/caballeros/hades/espectros/armadura_grifo.jpg" alt="Armadura de Grifo" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>25 de Marzo</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>23</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,84 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="#">Minos de Grifo</a></h2>
                                        <span class="byline">
                                            Juez del Infierno
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/hades/espectros/minos.jpg" class="image saint">
                                        <img src="../../../images/caballeros/hades/espectros/minos.jpg" alt="Minos de Grifo" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/hades/espectros/minos.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/hades/espectros/minos.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Estrella:</strong>
                                        <span>Estrella Celeste de la Nobleza</span>
                                    </div>
                                    <div class="info">
                                        <strong>Rango:</strong>
                                        <span>Juez del Infierno / Espectro Celeste</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 72 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Oslo, Noruega</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Aries</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            UNA SOMBRA SINIESTRA APROXIMÁNDOSE... ¡UN MAESTRO DE LAS MARIONETAS, MANIPULANDO LA VIDA!
                                        </p>
                                        <p>
                                            Uno de los Tres Jueces del Inframundo. Ya que poseen el mismo nivel de poderes, él, Radamanthys
                                            y Aiacos son compañeros de lucha pero también rivales entre sí. Es calmado y Racional y siempre
                                            calcula el próximo paso dependiendo de la situación.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            Tiene habilidades psiquicas que permiten que luche sin usar sus puños a pesar de que el poder
                                            destructivo de su golpe es capaz de despedazar hasta la barrera más fuerte.
                                        </p>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>