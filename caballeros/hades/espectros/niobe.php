<!DOCTYPE HTML>
<html>
    <head>
        <title>Niobe de Deep - Espectro de Hades - SaintSeiyaSigma.com</title>
        <meta name="description" content="Niobe de Deep es uno de los 108 espectros de hades..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/niobe.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/niobe.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/niobe.jpg"/>

    </head>
    <body class="espectros niobe">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../espectros.php" id="logo">Espectros de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/hades/espectros/armadura_deep.jpg">Sapuri de Deep</a></h3>
                                        <a href="../../../images/caballeros/hades/espectros/armadura_deep.jpg" class="image">
                                            <img src="../../../images/caballeros/hades/espectros/armadura_deep.jpg" alt="Sapuri de Deep" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>3 de Agosto</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>25</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,81 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="#">Niobe de Deep</a></h2>
                                        <span class="byline">
                                            Estrella Terrestre de la Oscuridad
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/hades/espectros/niobe.jpg" class="image saint">
                                        <img src="../../../images/caballeros/hades/espectros/niobe.jpg" alt="Niobe de Deep" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/hades/espectros/niobe.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/hades/espectros/niobe.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Ejercito:</strong>
                                        <span>Wyvern</span>
                                    </div>
                                    <div class="info">
                                        <strong>Rango:</strong>
                                        <span>Espectro Terrestre</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 80 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Bolivia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Leo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            LA FRAGANCIA DEMONÍACA QUE ENCANTA EL AMBIENTE
                                        </p>
                                        <p>
                                            Espectro que invadió el Santuario junto con Saga y los otros Santos de Oro obedeciendo las
                                            órdenes de Radamanthys. A pesar de derrotar a Aldebarán de Tauro, él también cayó. 
                                        </p>
                                        <p>
                                            Ya que mató a un caballero de Oro, se dice que era un personaje considerablemente fuerte.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            El aroma que exhala de su Surplice anestesia el sistema nervioso central del Oponente.
                                        </p>
                                        <p>
                                            Sus técnicas son: Fragancia Profunda y Perfume de Muerte.
                                        </p>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>