<!DOCTYPE HTML>
<html>
    <head>
        <title>Aiacos de Garuda - Juez del Infierno - SaintSeiyaSigma.com</title>
        <meta name="description" content="Aiacos de Garuda es uno de los 3 Jueces del Infierno..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/aiacos.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/aiacos.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/aiacos.jpg"/>

    </head>
    <body class="espectros aiacos">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../espectros.php" id="logo">Espectros de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/hades/espectros/armadura_garuda.jpg">Sapuri de Garuda</a></h3>
                                        <a href="../../../images/caballeros/hades/espectros/armadura_garuda.jpg" class="image">
                                            <img src="../../../images/caballeros/hades/espectros/armadura_garuda.jpg" alt="Sapuri de Garuda" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>6 de Julio</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>22</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,86 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/hades/espectros/aiacos.jpg">Aiacos de Garuda</a></h2>
                                        <span class="byline">
                                            Juez del Infierno
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/hades/espectros/aiacos.jpg" class="image saint">
                                        <img src="../../../images/caballeros/hades/espectros/aiacos.jpg" alt="Aiacos de Garuda" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/hades/espectros/aiacos.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/hades/espectros/aiacos.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Estrella:</strong>
                                        <span>Estrella Celeste de la Valentia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Rango:</strong>
                                        <span>Juez del Infierno / Espectro Celeste</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 85 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Nepal</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Cancer</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡LA GARRA DE GARUDA ATACA! UN FUERTE GUERRERO COMANDANDO EL MUNDO DE LOS MUERTOS.
                                        </p>
                                        <p>
                                            Uno de los más poderosos Espectros del Inframundo, él junto con Radamanthys y Minos son
                                            llamados los Tres Jueces del Inframundo.
                                        </p>
                                        <p>
                                            Su orgullo es sustentado por todo el poder que esconde.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            Puede usar técnicas con las manos para amenazar al oponente pero prefiere las técnicas
                                            de lanzamiento, que aprovechan la flexibilidad e impulso de su cuerpo.
                                        </p>
                                        <p>
                                            La técnica principal Ilusión Galáctica lanza al enemigo hacia lo alto y no hay escapatoria.
                                            Esta técnica de Aiacos restringe los movimientos del enemigo girándolo rápidamente e
                                            impactándolo contra el suelo.
                                        </p>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>