<!DOCTYPE HTML>
<html>
    <head>
        <title>Caronte de Aqueronte - Espectro de Hades - SaintSeiyaSigma.com</title>
        <meta name="description" content="Caronte de Aqueronte es el encargado de transportar las
              almas de los recien muertos..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/caronte.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/caronte.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/espectros/caronte.jpg"/>

    </head>
    <body class="espectros caronte">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../espectros.php" id="logo">Espectros de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/hades/espectros/armadura_aqueronte.jpg">Sapuri de Aqueronte</a></h3>
                                        <a href="../../../images/caballeros/hades/espectros/armadura_aqueronte.jpg" class="image">
                                            <img src="../../../images/caballeros/hades/espectros/armadura_aqueronte.jpg" alt="Sapuri de Aqueronte" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>10 de Febrero</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>38</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,83 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="#">Caronte de Aqueronte</a></h2>
                                        <span class="byline">
                                            Estrella Celeste del Intervalo
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/hades/espectros/caronte.jpg" class="image saint">
                                        <img src="../../../images/caballeros/hades/espectros/caronte.jpg" alt="Caronte de Aqueronte" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/hades/espectros/caronte.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/hades/espectros/caronte.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Rango:</strong>
                                        <span>Espectro Celeste</span>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 65 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Napoles, Italia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Acuario</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            GUARDIÁN DEL RÍO ENTRE LA VIDA Y LA MUERTE. EL INFIERNO DEPENDE DE SU PRECIO.
                                        </p>
                                        <p>
                                            Guardián del Río Aqueronte que separa el mundo real del Mundo de los Muertos. Es ávaro y
                                            engaña fácilmente a las personas, pero por otro lado es leal a algunas cosas, mostrando
                                            así bastante semejanza con los seres humanos comunes. Se conmueve con el corazón puro de Shun
                                            pero no cambia su postura.
                                        </p>
                                        <p>
                                            La manera "perdida" con que mantiene su postura hasta el final es digna de palmas. Le gusta cantar
                                            mientras trabaja: "Y de nuevo... lo hago..."
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Técnica</h3>
                                        </header>
                                        <ul>
                                            <li>Remo Giratorio</li>
                                            <li>Corriente Final Trituradora</li>
                                        </ul>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>