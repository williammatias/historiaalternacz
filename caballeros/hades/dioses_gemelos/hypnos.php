<!DOCTYPE HTML>
<html>
    <head>
        <title>Hypnos Dios del Sueño - Dios Gemelo de Hades - SaintSeiyaSigma.com</title>
        <meta name="description" content="Hypnos es el Dios del Sueño leal al Dios del Inframundo Hades..." />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/hades/dioses_gemelos/hypnos.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/dioses_gemelos/hypnos.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/hades/dioses_gemelos/hypnos.jpg"/>

    </head>
    <body class="dioses_gemelos hypnos">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../dioses_gemelos.php" id="logo">Dioses Gemelos de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/hades/dioses_gemelos/armadura_hipnos.jpg">Sapuri de Hypnos</a></h3>
                                        <a href="../../../images/caballeros/hades/dioses_gemelos/armadura_hipnos.jpg" class="image">
                                            <img src="../../../images/caballeros/hades/dioses_gemelos/armadura_hipnos.jpg" alt="Sapuri de Hypnos" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>13 de Junio</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,92 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/hades/dioses_gemelos/hipnos.jpg">Hypnos</a></h2>
                                        <span class="byline">
                                            Dios del Sueño
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/hades/dioses_gemelos/hipnos.jpg" class="image saint">
                                        <img src="../../../images/caballeros/hades/dioses_gemelos/hipnos.jpg" alt="Hypnos Dios del Sueño" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/hades/dioses_gemelos/hypnos.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/hades/dioses_gemelos/hypnos.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 88 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Signo:</strong>
                                        <span>Geminis</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            SILENCIOSO DIOS DEMONÍACO QUE INVITA A LOS SERES HUMANOS A UN SUEÑO MÁS PROFUNDO QUE LA MUERTE.
                                        </p>
                                        <p>
                                            Un dios de ojos y cabellos dorados que habita en el paraíso de los Campos Elíseos, escondido
                                            detrás del Muro de los Lamentos. Su técnica "Adormecimiento Eterno" restringe el Cosmos del 
                                            enemigo y lo invita a un sueño eterno sin retorno. Como asesores directos de Hades, él y Thanatos
                                            manipulan a Pandora y todos los otros espectros, planeando diezmar a los seres humanos
                                            de la Tierra. A pesar de despreciar a los humanos, no los subestima.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            Retira todo sentido de dolor de la persona y la debilita poco a poco, llevándolo a un estado de 
                                            sueño profundo. Aquel que duerme, nunca jamás despierta. También posee habilidades como guerrero.
                                        </p>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>