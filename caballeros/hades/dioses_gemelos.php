<!DOCTYPE HTML>
<html>
    <head>
        <title>Jueces del Infierno - Saint Seiya Sigma</title>
        <meta name="description" content="Los Dioses Gemelos son los Dioses subordinados a..." />
        
        <?php
        include '../../template/head.php';
        ?>
    </head>
    <body class="dioses_gemelos">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">Dioses Gemelos</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u6">
                                    <a href="dioses_gemelos/hypnos.php" class="image">
                                        <img src="../../images/caballeros/hades/hypnos.jpg" alt="Hypnos Dios de los Sueños"/>
                                        <div class="info saint_box">                                        
                                            <strong>Dios Hypnos</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u6">
                                    <a href="dioses_gemelos/thanatos.php" class="image">
                                        <img src="../../images/caballeros/hades/thanatos.jpg" alt="Thanatos Dios de la Muerte"/>
                                        <div class="info saint_box">                                        
                                            <strong>Dios Thanatos</strong>
                                        </div>                                        
                                    </a>
                                </div>
                            </section>

                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../template/footer_ad.php';
        ?>

    </body>
</html>