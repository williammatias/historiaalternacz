<!DOCTYPE HTML>
<html>
    <head>
        <title>Espectros de Hades - Saint Seiya - SaintSeiyaSigma.com</title>
        <meta name="description" content="Los espectros de hades corresponden a las 108 estrellas malignas..." />
        
        <?php
        include '../../template/head.php';
        ?>
    </head>
    <body class="espectros">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">Espectros de Hades</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <a href="espectros/aiacos.php" class="image">
                                        <img src="../../images/caballeros/hades/aiacos.jpg" alt="Aiacos de Garuda"/>
                                        <div class="info saint_box">                                        
                                            <strong>Aiacos de Garuda</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/minos.php" class="image">
                                        <img src="../../images/caballeros/hades/minos.jpg" alt="Minos de Grifo"/>
                                        <div class="info saint_box">                                        
                                            <strong>Minos de Grifo</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/radamanthys.php" class="image">
                                        <img src="../../images/caballeros/hades/radamantys.jpg" alt="Radamanthys de Wyvern"/>
                                        <div class="info saint_box">                                        
                                            <strong>Radamanthys de Wyvern</strong>
                                        </div>                                        
                                    </a>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u4">
                                    <a href="espectros/violate.php">
                                        <div class="info saint_box">                                        
                                            <strong>Violate de Behemoth</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/stand.php">
                                        <div class="info saint_box">                                        
                                            <strong>Stand de Escarabajo Mortal</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/caronte.php">
                                        <div class="info saint_box">                                        
                                            <strong>Caronte de Aqueronte</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/cube.php">
                                        <div class="info saint_box">                                        
                                            <strong>Cube de Dullahan</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/giganto.php">
                                        <div class="info saint_box">                                        
                                            <strong>Giganto de Cíclope</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/gordon.php">
                                        <div class="info saint_box">                                        
                                            <strong>Gordon de Minotauro</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/ivan.php">
                                        <div class="info saint_box">                                        
                                            <strong>Ivan de Troll</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/lune.php">
                                        <div class="info saint_box">                                        
                                            <strong>Lune de Balrog</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/mills.php">
                                        <div class="info saint_box">                                        
                                            <strong>Mills de Elfo</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/myu.php">
                                        <div class="info saint_box">                                        
                                            <strong>Myu de Papillon</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/niobe.php">
                                        <div class="info saint_box">                                        
                                            <strong>Niobe de Deep</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/ochs.php">
                                        <div class="info saint_box">                                        
                                            <strong>Ochs de Gorgon</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/pharaoh.php">
                                        <div class="info saint_box">                                        
                                            <strong>Pharaoh de Esfinge</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/phlegyas.php">
                                        <div class="info saint_box">                                        
                                            <strong>Phlegyas de Licaon</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/queen.php">
                                        <div class="info saint_box">                                        
                                            <strong>Queen de Alraune</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/raimi.php">
                                        <div class="info saint_box">                                        
                                            <strong>Raimi de Gusano</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/rock.php">
                                        <div class="info saint_box">                                        
                                            <strong>Rock de Golem</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/sylphid.php">
                                        <div class="info saint_box">                                        
                                            <strong>Sylphid de Basilisco</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/valentine.php">
                                        <div class="info saint_box">                                        
                                            <strong>Valentine de la Harpia</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="espectros/zelos.php">
                                        <div class="info saint_box">                                        
                                            <strong>Zelos de Rana</strong>
                                        </div>                                        
                                    </a>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../template/footer_ad.php';
        ?>

    </body>
</html>