<!DOCTYPE HTML>
<html>
    <head>
        <title>Sorrento de Sirena - General Marino de Poseidon - SaintSeiyaSigma.com</title>
        <meta name="description" content="Sorrento de Sirena es uno de los 7 Generales Marinos del Dios Poseidon" />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/sorrento.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/sorrento.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/sorrento.jpg"/>

    </head>
    <body class="generales_marinos sorrento">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../generales_marinos.php" id="logo">Generales Marinos</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/poseidon/generales_marinos/armadura_sirena.jpg">Escama de Sirena</a></h3>
                                        <a href="../../../images/caballeros/poseidon/generales_marinos/armadura_sirena.jpg" class="image">
                                            <img src="../../../images/caballeros/poseidon/generales_marinos/armadura_sirena.jpg" alt="Escama de Sirena" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>10 de Septiembre</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>16</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,78 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/poseidon/generales_marinos/sorrento.jpg">Sorrento de Sirena</a></h2>
                                        <span class="byline">
                                            Guardián del Pilar del Océano Atlantico Sur
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/poseidon/generales_marinos/sorrento.jpg" class="image saint">
                                        <img src="../../../images/caballeros/poseidon/generales_marinos/sorrento.jpg" alt="Sorrento de Sirena" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos/sorrento.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos/sorrento.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 75 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Austria</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Virgo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            El sonido que sale de su flauta son bellos y tristes acordes de muerte.
                                        </p>
                                        <p>
                                            Uno de los generales marinos. Su flauta priva al enemigo de sus fuerzas, su poder 
                                            fue capaz de acorralar hasta al mismo Aldebarán. El proteje el pilar del Océano Atlántico Sur.
                                            Dueño de un corazón puro, creía que purificar la faz de la tierra era el camino
                                            para que poseidón pudiera crear una utopía en la tierra. Sin embargo, cuando conoce el gran
                                            amor de Athena, percibe que era mentira. Al final, vuelve a ser una persona normal y
                                            viaja por el mundo para ayudar a las victimas de las lluvias.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            No usa ningún tipo de técnica de combate ni armas. Simplemente el sonido de su flauta y su fenomenal talento musical son suficientes
                                            para atacar al enemigo. Aunque puede utilizar su flauta como arma y atacar fisicamente al enemigo
                                            con ella. La melodía de su flauta entra en la mente del adversario y destruye su cerebro.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>