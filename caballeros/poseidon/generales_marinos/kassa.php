<!DOCTYPE HTML>
<html>
    <head>
        <title>Kassa de Lymnades - General Marino de Poseidon - SaintSeiyaSigma.com</title>
        <meta name="description" content="Kassa de Lymnades es uno de los 7 Generales Marinos del Dios Poseidon" />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/kassa.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/kassa.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/kassa.jpg"/>

    </head>
    <body class="generales_marinos isaac">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../generales_marinos.php" id="logo">Generales Marinos</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/poseidon/generales_marinos/armadura_lymnades.jpg">Escama de Lymnades</a></h3>
                                        <a href="../../../images/caballeros/poseidon/generales_marinos/armadura_lymnades.jpg" class="image">
                                            <img src="../../../images/caballeros/poseidon/generales_marinos/armadura_lymnades.jpg" alt="Escama de Lymnades" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>19 de Agosto</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>21</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,68 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/poseidon/generales_marinos/kassa.jpg">Kassa de Lymnades</a></h2>
                                        <span class="byline">
                                            Guardián del Pilar del Océano Antartico
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/poseidon/generales_marinos/kassa.jpg" class="image saint">
                                        <img src="../../../images/caballeros/poseidon/generales_marinos/kassa.jpg" alt="Kassa de Lymnades" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos/kassa.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos/kassa.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 49 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Portugal</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Leo</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            Un imperdonable demonio de las aguas que juega con el corazón de la personas.
                                        </p>
                                        <p>
                                            General Marino que proteje el pilar del Océano Antártico. Su Escama representa un
                                            monstruo de las aguas de la Mitología Griega. Tiene el poder de leer la mente de una
                                            persona y descubrir a su ser más querido imitándolo no solo físicamente sino también
                                            psicológicamente.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            Una de las presencias más amenazadoras entre los Generales Marinos no por sus habilidades
                                            de lucha sino porque no mide consecuencias para obtener lo que quiere. Su técnica engaña a
                                            adversario y lo deja tumbado.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>