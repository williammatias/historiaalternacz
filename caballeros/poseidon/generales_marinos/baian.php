<!DOCTYPE HTML>
<html>
    <head>
        <title>Baian el Caballo Marino - General Marino de Poseidon - SaintSeiyaSigma.com</title>
        <meta name="description" content="Baian de Caballo Marino es uno de los 7 Generales Marinos
              del Dios Poseidon" />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/baian.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/baian.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/baian.jpg"/>

    </head>
    <body class="generales_marinos baian">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../generales_marinos.php" id="logo">Generales Marinos</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/poseidon/generales_marinos/armadura_hipo_campo.jpg">Escama de Caballo Marino</a></h3>
                                        <a href="../../../images/caballeros/poseidon/generales_marinos/armadura_hipo_campo.jpg" class="image">
                                            <img src="../../../images/caballeros/poseidon/generales_marinos/armadura_hipo_campo.jpg" alt="Escama de Caballo Marino" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>7 de Mayo</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>18</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,81 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/poseidon/generales_marinos/baian.jpg">Baian de Caballo Marino</a></h2>
                                        <span class="byline">
                                            Guardián del Pilar del Océano Pacifico Norte
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/poseidon/generales_marinos/baian.jpg" class="image saint">
                                        <img src="../../../images/caballeros/poseidon/generales_marinos/baian.jpg" alt="Baian el Caballo Marino" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos/baian.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos/baian.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 78 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Canada</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Tauro</span>
                                    </div>

                                </div>

                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡El Dueño del soplo divino capaz de repeler cualquier tipo de ataque enemigo!
                                        </p>
                                        <p>
                                            Combate contra Seiya para proteger el Pilar del Océano Pacífico Norte. Para defenderse,
                                            crea muros de aire girando sus manos a una velocidad cercana a la de la luz, pero ese
                                            truco fue eliminado por Seiya, más habilidoso que nunca después de la batalla contra
                                            los Santos de Oro. Baian jamás se enfrentó a un enemigo más fuerte que él. En cambio,
                                            Seiya siempre ha combatido contra adversarios más fuertes que él. Ésa fue la diferencia
                                            que determinó la victoria entre los dos.
                                        </p>
                                        <p>

                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            Manipula corrientes de aire y consigue levantar al oponente con un simple soplo
                                            y crea muros de aire para protegerse del ataque del enemigo. Esas técnicas de lucha
                                            eran admiradas por todos los otros Generales Marinos como técnicas divinas.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>