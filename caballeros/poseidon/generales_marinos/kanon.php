<!DOCTYPE HTML>
<html>
    <head>
        <title>Kanon de Dragon Marino - General Marino de Poseidon - SaintSeiyaSigma.com</title>
        <meta name="description" content="Kanon de Dragon Marino es uno de los 7 Generales Marinos del Dios Poseidon" />

        <?php
        include '../../../template/head.php';
        ?>

        <meta itemprop="image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/kanon.jpg"/>       
        <meta property="og:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/kanon.jpg"/>  
        <meta name="twitter:image" content="http://www.saintseiyasigma.com/images/caballeros/poseidon/generales_marinos/kanon.jpg"/>

    </head>
    <body class="generales_marinos kanon">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="../generales_marinos.php" id="logo">Generales Marinos</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">  
                                    <div class="cloth">
                                        <h3><a href="../../../images/caballeros/poseidon/generales_marinos/armadura_dragon_marino.jpg">Escama del Dragon Marino</a></h3>
                                        <a href="../../../images/caballeros/poseidon/generales_marinos/armadura_dragon_marino.jpg" class="image">
                                            <img src="../../../images/caballeros/poseidon/generales_marinos/armadura_dragon_marino.jpg" alt="Escama del Dragon Marino" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <strong>Fecha de Nacimiento:</strong>
                                        <span>30 de Mayo</span>
                                    </div>
                                    <div class="info">
                                        <strong>Edad:</strong>
                                        <span>28</span>
                                    </div>
                                    <div class="info">
                                        <strong>Altura:</strong>
                                        <span>1,88 m</span>
                                    </div>
                                </div>
                                <div class="u4 centered">
                                    <header>
                                        <h2><a href="../../../images/caballeros/poseidon/generales_marinos/kanon.jpg">Kanon de Dragon Marino</a></h2>
                                        <span class="byline">
                                            Guardián del Pilar del Océano Atlántico Norte
                                        </span>
                                    </header>
                                    <a href="../../../images/caballeros/poseidon/generales_marinos/kanon.jpg" class="image saint">
                                        <img src="../../../images/caballeros/poseidon/generales_marinos/kanon.jpg" alt="Kanon de Dragon Marino" />
                                    </a>
                                </div>
                                <div class="u4">
                                    <div class="info">
                                        <strong>Popularidad:</strong>
                                        <!--Facebook-->
                                        <div id="fb-root"></div>
                                        <script>(function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (d.getElementById(id))
                                                    return;
                                                js = d.createElement(s);
                                                js.id = id;
                                                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                                fjs.parentNode.insertBefore(js, fjs);
                                            }(document, 'script', 'facebook-jssdk'));</script>

                                        <div class="fb-like" data-href="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos/kanon.php" data-width="450" data-layout="box_count" data-show-faces="true" data-send="false"></div>

                                        <!--Twitter-->

                                        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://www.saintseiyasigma.com/caballeros/poseidon/generales_marinos/kanon.php" data-via="saintseiyasigma" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
                                        <script>!function(d, s, id) {
                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                if (!d.getElementById(id)) {
                                                    js = d.createElement(s);
                                                    js.id = id;
                                                    js.src = "https://platform.twitter.com/widgets.js";
                                                    fjs.parentNode.insertBefore(js, fjs);
                                                }
                                            }(document, "script", "twitter-wjs");
                                        </script>

                                        <!-- Place this tag where you want the +1 button to render. -->
                                        <div class="g-plusone" data-size="tall"></div>

                                        <!-- Place this tag after the last +1 button tag. -->
                                        <script type="text/javascript">
                                            window.___gcfg = {lang: 'es'};

                                            (function() {
                                                var po = document.createElement('script');
                                                po.type = 'text/javascript';
                                                po.async = true;
                                                po.src = 'https://apis.google.com/js/plusone.js';
                                                var s = document.getElementsByTagName('script')[0];
                                                s.parentNode.insertBefore(po, s);
                                            })();
                                        </script>
                                    </div>
                                    <div class="info">
                                        <strong>Peso:</strong>
                                        <span> 87 kg</span>
                                    </div>
                                    <div class="info">
                                        <strong>Procedencia:</strong>
                                        <span>Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Lugar de entrenamiento:</strong>
                                        <span>Santuario de Atena, Grecia</span>
                                    </div>
                                    <div class="info">
                                        <strong>Constelación:</strong>
                                        <span>Géminis</span>
                                    </div>
                                </div>
                            </section>
                            <section class="row">
                                <div class="u6">
                                    <div class="info personalidad">
                                        <header>
                                            <h3>Descripción</h3>
                                        </header>
                                        <p>
                                            ¡El usurpador del fondo del mar engañando hasta a los Dioses!
                                        </p>
                                        <p>
                                            Hermano Gemelo de Saga. Preocupado por la maldad de su corazón, Saga lo aprisiona
                                            en el calabozo de Cabo Sunion. Por azares del destino, Kanon termina liberando el alma 
                                            de Poseidón, el emperador de los Mares, quien había sido caputarado por Athena. 
                                            A partir de ese momento, se convierte en el General Marino del Dragón Marino y comanda 
                                            a los otros Marinos con planes de conquistar tanto la Tierra como el Mar. Sus poderes son
                                            tan fuertes como los de su hermano Saga.
                                        </p>
                                    </div>
                                </div>
                                <div class="u6">
                                    <div class="info Habilidad_Combate">
                                        <header>
                                            <h3>Habilidad de Combate</h3>
                                        </header>
                                        <p>
                                            De la misma forma que su hermano gemelo Saga, Kanon usa ataques que manipulan las mentes
                                            de las personas y el espacio temporal. También aprendió trucos para lanzar al oponente a
                                            otra dimensión y hacer que las dimensiones se desvirtúen.
                                        </p>
                                    </div>
                                </div>
                            </section>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Footer -->
        <?php
        include '../../../template/footer_ad.php';
        ?>

    </body>
</html>