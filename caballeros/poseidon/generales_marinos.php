<!DOCTYPE HTML>
<html>
    <head>
        <title>Generales Marinos de Poseidon - Caballeros del Zodiaco</title>
        <meta name="description" content="Los Generales Marinos del Dios Poseion son los guardianes..." />
        
        <?php
        include '../../template/head.php';
        ?>
    </head>
    <body class="generales_marinos">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a href="#" id="logo">Generales Marinos</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <section class="row">
                                <div class="u4">
                                    <a href="generales_marinos/kanon.php">
                                        <div class="info saint_box">                                        
                                            <strong>Kanon de Dragon Marino</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="generales_marinos/io.php">
                                        <div class="info saint_box">                                        
                                            <strong>Io de Escila</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="generales_marinos/baian.php">
                                        <div class="info saint_box">                                        
                                            <strong>Baian de Caballo Marino</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="generales_marinos/isaac.php">
                                        <div class="info saint_box">                                        
                                            <strong>Isaac de Kraken</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="generales_marinos/krishna.php">
                                        <div class="info saint_box">                                        
                                            <strong>Krishna de Krisaor</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="generales_marinos/kassa.php">
                                        <div class="info saint_box">                                        
                                            <strong>Kassa de Lymnades</strong>
                                        </div>                                        
                                    </a>
                                </div>
                                <div class="u4">
                                    <a href="generales_marinos/sorrento.php">
                                        <div class="info saint_box">                                        
                                            <strong>Sorrento de Sirena</strong>
                                        </div>                                        
                                    </a>
                                </div>
                            </section>

                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../../template/footer_ad.php';
        ?>

    </body>
</html>