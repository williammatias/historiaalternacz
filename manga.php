<!DOCTYPE HTML>
<html>
    <head>
        <title>Mangas Saint Seiya - SaintSeiyaSigma.com</title>
        <meta name="description" content="Estos son los mangas de Saint Seiya" />

        <?php
        include 'template/head.php';
        ?>
        
        
    </head>
    <body class="mangas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1>Mangas de Saint Seiya</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include 'template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <header>
                                <h2>Mangas Disponibles</h2>
                                <span class="byline">
                                    <script type="text/javascript"><!--
                                        google_ad_client = "ca-pub-6835894736815485";
                                        /* SSLeaderBoard */
                                        google_ad_slot = "1201595250";
                                        google_ad_width = 728;
                                        google_ad_height = 90;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>
                                    <div class="fb-like-box" data-href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                                </span>
                            </header>
                            <div class="row">
                                <article class="u4 special">
                                    <header>
                                        <h3><a href="manga/the_lost_canvas.php">The Lost Canvas</a></h3>
                                    </header>
                                    <a href="manga/the_lost_canvas.php" class="image featured"><img src="images/manga/the_lost_canvas.jpg" alt="Manga del Episodio G" /></a>
                                    <p>
                                        El Manga cuenta lo ocurrido en la anterior guerra santa contra Hades ocurrida antes del Saint Seiya original
                                        donde se explica muchos cosas concernientes al mundo de Saint Seiya Los Caballeros del Zodiaco. 
                                    </p>
                                </article>
                                <article class="u4 special">
                                    <header>
                                        <h3><a href="manga/episodio_g.php">Episodio G</a></h3>
                                    </header>
                                    <a href="manga/episodio_g.php" class="image featured"><img src="images/manga/episodio_g.jpg" alt="Manga del Episodio G" /></a>
                                    <p>
                                        La serie cuenta la historia de los santos de oro siete años antes del Saint Seiya original con Aioria de Leo
                                        como protagonista, narra los problemas a los que se enfrenta Aioria para ser aceptado como un santo de oro y
                                        su enfrentamiento junto a los otros santos contra los titanes de la mitología griega, quienes buscan liberar
                                        a Cronos, quien fue derrotado y aprisionado por Zeus. 
                                    </p>
                                </article>
                                <article class="u4 special">
                                    <header>
                                        <h3><a href="manga/next_dimension.php">Next Dimension</a></h3>
                                    </header>
                                    <a href="manga/next_dimension.php" class="image featured"><img src="images/manga/next_dimension.jpg" alt="Manga del Episodio G" /></a>
                                    <p>
                                        Inicia con un prólogo en el cual Hades se encuentra luchando contra Seiya, el santo de Pegaso del siglo XX: en ese momento, 
                                        el dios recuerda que otro guerrero de la misma constelación, además de herirlo en la era del mito, 
                                        fue su amigo en la Guerra Santa del siglo XVIII, hace 243 años. Así, la historia pasa a centrarse en ese siglo,
                                        donde los jóvenes Shion y Dohko son ascendidos de santos de bronce a santos de oro.
                                    </p>
                                </article>
                            </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include 'template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include 'template/footer.php';
        ?>

    </body>
</html>