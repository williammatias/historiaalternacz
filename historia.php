<!DOCTYPE HTML>
<html>
    <head>
        <title>La Saga Sigma - Saint Seiya Historia Alterna - SaintSeiyaSigma.com</title>
        <meta name="description" content="La Saga Sima es una historia alterna creada por SaintSeiyaSigma.com que..." />

        <?php
        include 'template/head.php';
        ?>


    </head>
    <body class="chapter_infierno">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1 id="logo">La Saga Sigma Capitulos</h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include 'template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style3">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <header>
                                <h2>Primera Temporada: Chapter Infierno</h2>
                                <span class="byline">
                                    <script type="text/javascript"><!--
                          google_ad_client = "ca-pub-6835894736815485";
                                        /* SSespectroLeaderBoard */
                                        google_ad_slot = "7629905257";
                                        google_ad_width = 728;
                                        google_ad_height = 90;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>
                                    <div class="fb-like-box" data-href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                                </span>
                            </header>
                            <div class="row">
                                <article class="u4 special">
                                    <header>
                                        <h3><a target="_blank" href="historia/temporada1/capitulo_1/acto0.php">Capítulo 1: El comienzo de la guerra Santa</a></h3>
                                    </header>
                                    <a target="_blank" href="historia/temporada1/capitulo_1/acto0.php" class="image featured"><img src="images/historia/temporada1/capitulo1/hades_vs_athena.jpg" alt="Hades VS Athena" /></a>
                                    <p>
                                        La mayor amenaza que ha vivido el mundo esta a punto de comenzar. Dohko de Libra en los 5 picos en China lo
                                        presiente, se pone de pie lentamente y pronuncia las siguientes palabras observando el sello de athena que
                                        tenía a Hades encerrado: “El Momento ha llegado”.
                                    </p>
                                </article>
                                <article class="u4 special">
                                    <header>
                                        <h3><a target="_blank" href="historia/temporada1/capitulo_2/acto0.php">Capítulo 2: El poder de un caballero plateado</a></h3>
                                    </header>
                                    <a target="_blank" href="historia/temporada1/capitulo_2/acto0.php" class="image featured"><img src="images/historia/temporada1/capitulo_2/88Caballeros.jpg" alt="Constelaciones Saint Seiya Los Caballeros del Zodiaco" /></a>
                                    <p>
                                        El patriarca envía un nuevo caballero al campo de Batalla...
                                    </p>
                                </article>
                                <article class="u4 special">
                                    <header>
                                        <h3><a target="_blank" href="historia/temporada1/capitulo_3/acto0.php">Capitulo 3: Entrando en el Reino de los Muertos</a></h3>
                                    </header>
                                    <a target="_blank" href="historia/temporada1/capitulo_3/acto0.php" class="image featured"><img src="images/historia/temporada1/capitulo_3/inframundo.jpg" alt="Mapa del Inframundo" /></a>
                                    <p>
                                        Los caballeros de plata se preparan para entrar al mundo de los muertos. ¿Podrán salvar al mundo y salir con Vida?
                                    </p>
                                </article>
                            </div>      

                            <div class="row">
                                <article class="u4 special">
                                    <header>
                                        <h3><a target="_blank" href="historia/temporada1/capitulo_4/acto0.php">Capítulo 4: Batalla en el inframundo</a></h3>
                                    </header>
                                    <a target="_blank" href="historia/temporada1/capitulo_4/acto0.php" class="image featured"><img src="images/historia/temporada1/capitulo_3/albiore_vs_lune.jpg" alt="Albiore en el inframundo vs lune" /></a>
                                    <p>
                                        Lune le dice a los caballeros que sabe que son Santos de Atena, pero que de todas formas 
                                        mirará los pecados de Albiore primero para saber a qué infierno lo enviará. 
                                    </p>
                                </article>
                                <article class="u4 special">
                                    <header>
                                    </header>
                                    <p>
                                    </p>
                                </article>
                                <article class="u4 special">
                                    <header>
                                    </header>
                                    <p>
                                    </p>
                                </article>
                            </div> 
                        </article>
                    </div>
                </div>
                <hr />
                <div class="row ad">
                    <div class="u3"><span><h1>saint seiya los caballeros del zodiaco</h1></span></div>
                    <div class="u6">
                        <script type="text/javascript"><!--
                            google_ad_client = "ca-pub-6835894736815485";
                            /* SSLargeRectangle */
                            google_ad_slot = "7248128858";
                            google_ad_width = 336;
                            google_ad_height = 280;
//-->
                        </script>
                        <script type="text/javascript"
                                src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                        </script>
                    </div>
                    <div class="u3"><span><h1>saint seiya los caballeros del zodiaco</h1></span></div>
                </div>
            </div>

        </div>

        <!-- Features -->
        <?php
        include 'template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include 'template/footer.php';
        ?>

    </body>
</html>