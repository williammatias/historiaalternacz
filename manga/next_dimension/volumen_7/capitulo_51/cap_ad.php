<!DOCTYPE HTML>
<html>
    <head>
        <title>Next Dimension - Capitulo 51 Cisne y Dragon - SaintSeiyaSigma.com</title>
        <meta name="description" content="Su argumento narra los eventos posteriores a la batalla contra Hades en el siglo XX, junto con los sucesos de la Guerra Santa del siglo XVIII..." />


        <?php
        include '../../../../template/head.php';
        ?>

        <?php
        include './cap_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = '../capitulo_52/1.php';
            }
            function previous() {
                window.location = '21.php';
            }
            function restartCap() {
                window.location = '1.php';
            }

//<![CDATA[
            $(document).ready(function() {
                var element = document.getElementById('historia');
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer().on("swiperight", function() {
                    previous();
                });

            });
//]]>
        </script>
    </head>
    <body class="manga next_dimension adpage">

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <header>
                                <div class="bar row">
                                    <div class="u5">                                    
                                        <a id="return" class="u1"  href="../../../next_dimension.php">
                                            <span class="icon icon-home circled"></span>
                                        </a>
                                        <div class="u11">
                                            <h3 class="volumen">Next Dimension - Volumen 7</h3>
                                            <h3 class="capitulo">Capítulo 51: Cisne y Dragon</h3>
                                        </div>             
                                    </div>             
                                    <div class="u3">
                                        <?php
                                        include './cap_share.php';
                                        ?>
                                    </div> 
                                    <div class="u2">
                                        <h3 class="pagina">Fin del Capitulo</h3>
                                    </div>
                                    <div class="u2 next_previous">
                                        <span class="icon icon-arrow-left circled" onclick="previous();"></span>
                                    </div>    
                                    <div class="clear"></div>
                                </div>
                            </header>
                            <!-- AdTemplate -->
                            <?php
                            include '../../../adtemplate.php';
                            ?>    
                        </article>
                    </div>
                </div>

            </div>
        </div>

        <!-- Footer -->
        <?php
        include '../../../../template/footer.php';
        ?>    

    </body>
</html>