<!DOCTYPE HTML>
<html>
    <head>
        <title>Next Dimension - Capitulo 45 Broma de Los Dioses - SaintSeiyaSigma.com</title>
        <meta name="description" content="Su argumento narra los eventos posteriores a la batalla contra Hades en el siglo XX, junto con los sucesos de la Guerra Santa del siglo XVIII..." />


        <?php
        include '../../../../template/head.php';
        ?>

        <?php
        include './cap_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = '3.php';
            }
            function previous() {
                window.location = '1.php';
            }

//<![CDATA[
            $(document).ready(function() {
                var element = document.getElementById('historia');
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer().on("swiperight", function() {
                    previous();
                });

            });
//]]>
        </script>
    </head>
    <body class="manga next_dimension capitulo_45">

        <!-- Main -->
        <div id="historia" class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <div class="bar row">
                                <div class="u5">                                    
                                    <a id="return" class="u1"  href="../../../next_dimension.php">
                                        <span class="icon icon-home circled"></span>
                                    </a>
                                    <div class="u11">
                                        <h3 class="volumen">Next Dimension - Volumen 6</h3>
                                        <h3 class="capitulo">Capítulo 45: Broma de los Dioses</h3>
                                    </div>             
                                </div>             
                                <div class="u3">
                                    <?php
                                    include './cap_share.php';
                                    ?>
                                </div> 
                                <div class="u2">
                                    <h3 class="pagina">Pagina 2</h3>
                                </div>
                                <div class="u2 next_previous">
                                    <span class="icon icon-arrow-left circled" onclick="previous();"></span>
                                    <span class="icon icon-arrow-right circled" onclick="next();"></span>
                                </div>    
                                <div class="clear"></div>
                            </div>
                            <a href="#" class="image featured u12">
                                <img src="../../../../images/manga/next_dimension/volumen_6/capitulo_45/2.jpg" alt="Next Dimension - Capitulo 45 - Pagina 2" />
                            </a>
                        </article>
                    </div>
                </div>

            </div>
        </div>

        <!-- Footer -->
        <?php
        include '../../../../template/footer_ad.php';
        ?>    

    </body>
</html>