<!DOCTYPE HTML>
<html>
    <head>
        <title>Manga The Lost Canvas - SaintSeiyaSigma.com</title>
        <meta name="description" content="El Manga cuenta lo ocurrido en la anterior 
              guerra santa contra Hades ocurrida antes del Saint Seiya original
              donde se explica muchos cosas concernientes al mundo de Saint Seiya Los Caballeros del Zodiaco. " />
        <link rel="stylesheet" href="../css/jquery-ui.css" />

        <?php
        include '../template/head.php';
        ?>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
            $(function() {
                $("#accordion").accordion();
            });
        </script>
    </head>
    <body class="manga_the_lost_canvas">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a target="_blank" href="#" id="logo">Saint Seiya The Lost Canvas</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <header>
                                <h2>The Lost Canvas: Chapters</h2>
                                <span class="byline">
                                    <script type="text/javascript"><!--
                                        google_ad_client = "ca-pub-6835894736815485";
                                        /* SSLeaderBoard */
                                        google_ad_slot = "1201595250";
                                        google_ad_width = 728;
                                        google_ad_height = 90;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>
                                    <div class="fb-like-box" data-href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                                </span>
                            </header>


                            <div id="accordion">
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya The Lost Canvas - Volumen 1</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_1/capitulo_1/1.php">Capitulo 1:  Los dias perdidos</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_1/capitulo_1/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_1/capitulo_1/1.jpg" alt="The lost Canvas - Capitulo 1 - Los Dias perdidos" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_1/capitulo_2/1.php">Capítulo 2: Promesa</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_1/capitulo_2/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_1/capitulo_2/1.jpg" alt="The lost Canvas - Capitulo 2 - Promesa" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_1/capitulo_3/1.php">Capítulo 3: Pulsera Floral del Reencuentro</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_1/capitulo_3/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_1/capitulo_3/1.jpg" alt="The lost Canvas - Capitulo 3 - Pulsera Floral del Reencuentro" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_1/capitulo_4/1.php">Capítulo 4: Derrota</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_1/capitulo_4/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_1/capitulo_4/1.jpg" alt="The lost Canvas - Capitulo 4 - Derrota" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_1/capitulo_5/1.php">Capítulo 5: La Catedral</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_1/capitulo_5/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_1/capitulo_5/1.jpg" alt="The lost Canvas - Capítulo 5 - La Catedral" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_1/capitulo_6/1.php">Capítulo 6: Despertar</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_1/capitulo_6/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_1/capitulo_6/1.jpg" alt="The lost Canvas - Capítulo 6 - Despertar" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya The Lost Canvas - Volumen 2</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_2/capitulo_7/1.php">Capitulo 7:  Camaradas</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_2/capitulo_7/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_2/capitulo_7/1.jpg" alt="The lost Canvas - Capitulo 7 - Camaradas" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_2/capitulo_8/1.php">Capítulo 8: Reunión</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_2/capitulo_8/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_2/capitulo_8/1.jpg" alt="The lost Canvas - Capitulo 8 - Reunión" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_2/capitulo_9/1.php">Capítulo 9: Separación</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_2/capitulo_9/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_2/capitulo_9/1.jpg" alt="The lost Canvas - Capitulo 9 - Separación" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="the_lost_canvas/volumen_2/capitulo_10/1.php">Capítulo 10: Castillo Malefico</a></h3>
                                            </header>
                                            <a target="_blank" href="the_lost_canvas/volumen_2/capitulo_10/1.php" class="image featured"><img src="../images/manga/the_lost_canvas/volumen_2/capitulo_10/1.jpg" alt="The lost Canvas - Capitulo 10 - Castillo Maléfico" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../template/footer.php';
        ?>

    </body>
</html>