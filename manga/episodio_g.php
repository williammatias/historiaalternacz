<!DOCTYPE HTML>
<html>
    <head>
        <title>Manga Episodio G - SaintSeiyaSigma.com</title>
        <meta name="description" content="La serie cuenta la historia de los 
              santos de oro siete años antes del Saint Seiya original con Aioria de Leo
              como protagonista." />
        <link rel="stylesheet" href="../css/jquery-ui.css" />

        <?php
        include '../template/head.php';
        ?>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
            $(function() {
                $("#accordion").accordion();
            });
        </script>
    </head>
    <body class="manga_episodio_g">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a target="_blank" href="#" id="logo">Saint Seiya Episodio G</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <header>
                                <h2>Episodio G: Chapters</h2>
                                <span class="byline">
                                    <script type="text/javascript"><!--
                                        google_ad_client = "ca-pub-6835894736815485";
                                        /* SSLeaderBoard */
                                        google_ad_slot = "1201595250";
                                        google_ad_width = 728;
                                        google_ad_height = 90;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>
                                    <div class="fb-like-box" data-href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                                </span>
                            </header>



                            <div id="accordion">
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 10</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_10/capitulo_38/1.php">Capitulo 38: Aquel que elige a los dioses</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_10/capitulo_38/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_10/capitulo_38/1.jpg" alt="Episodio G - Capitulo 38 - Aquel que elige a los dioses" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_10/capitulo_39/1.php">Capitulo 39: Aquel que roba las Almas</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_10/capitulo_39/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_10/capitulo_39/1.jpg" alt="Episodio G - Capitulo 39 - Aquel que roba las Almas" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_10/capitulo_40/1.php">Capitulo 40: Aquel del Acero de Muerte</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_10/capitulo_40/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_10/capitulo_40/1.jpg" alt="Episodio G - Capitulo 40 - Aquel del Acero de Muerte" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_10/capitulo_41/1.php">Capitulo 41: Aquel que tiene Fragancia</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_10/capitulo_41/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_10/capitulo_41/1.jpg" alt="Episodio G - Capitulo 41 - Aquel que tiene Fragancia" /></a>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_10/capitulo_42/1.php">Capitulo 42: Aquel que crea el camino a la Victoria</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_10/capitulo_42/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_10/capitulo_42/1.jpg" alt="Episodio G - Capitulo 42 - Aquel que crea el camino a la Victoria" /></a>
                                        </article>
                                    </div>
                                </div>
                                
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 9</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_9/capitulo_34/1.php">Capitulo 34: Aquel que condena</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_9/capitulo_34/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_9/capitulo_34/1.jpg" alt="Episodio G - Capitulo 34 - Aquel que condena" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_9/capitulo_35/1.php">Capitulo 35: Aquel que tiene gran Fuerza</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_9/capitulo_35/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_9/capitulo_35/1.jpg" alt="Episodio G - Capitulo 35 - Aquel que tiene gran Fuerza" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_9/capitulo_36/1.php">Capitulo 36: Aquel que crea el Flujo</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_9/capitulo_36/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_9/capitulo_36/1.jpg" alt="Episodio G - Capitulo 36 - Aquel que crea el Flujo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_9/capitulo_37/1.php">Capitulo 37: Aquel que Hace Fluir</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_9/capitulo_37/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_9/capitulo_37/1.jpg" alt="Episodio G - Capitulo 37 - Aquel que Hace Fluir" /></a>
                                        </article>
                                    </div>
                                </div>

                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 8</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_8/capitulo_30/1.php">Capitulo 30: El que confronta</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_8/capitulo_30/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_8/capitulo_30/1.jpg" alt="Episodio G - Capitulo 30 - El que confronta" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_8/capitulo_31/1.php">Capitulo 31: El que esta sellado</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_8/capitulo_31/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_8/capitulo_31/1.jpg" alt="Episodio G - Capitulo 31 - El que esta sellado" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_8/capitulo_32/1.php">Capitulo 32: El que Esclaviza los Truenos</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_8/capitulo_32/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_8/capitulo_32/1.jpg" alt="Episodio G - Capitulo 32 - El que Esclaviza los Truenos" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_8/capitulo_33/1.php">Capitulo 33: El Ser que no posee alas</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_8/capitulo_33/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_8/capitulo_33/1.jpg" alt="Episodio G - Capitulo 33 - El Ser que no posee alas" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>

                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 7</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_7/capitulo_26/1.php">Capitulo 26: Aquel que Desciende</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_7/capitulo_26/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_7/capitulo_26/1.jpg" alt="Episodio G - Capitulo 26 - Aquel que Desciende" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_7/capitulo_27/1.php">Capitulo 27: Aquellos que iluminan el Simbolo</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_7/capitulo_27/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_7/capitulo_27/1.jpg" alt="Episodio G - Capitulo 27 - Aquellos que iluminan el Simbolo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_7/capitulo_28/1.php">Capitulo 28: Aquel que corta las Estrellas</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_7/capitulo_28/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_7/capitulo_28/1.jpg" alt="Episodio G - Capitulo 28 - Aquel que corta las Estrellas" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_7/capitulo_29/1.php">Capitulo 29: Aquel que Otorga la Espada Sagrada</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_7/capitulo_29/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_7/capitulo_29/1.jpg" alt="Episodio G - Capitulo 29 - Aquel que Otorga la Espada Sagrada" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>

                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 6</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_6/capitulo_22/1.php">Capitulo 22: Aquel que supera al Destino</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_6/capitulo_22/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_6/capitulo_22/1.jpg" alt="Episodio G - Capitulo 22 - Aquel que supera al Destino" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_6/capitulo_23/1.php">Capitulo 23: El que sostiene mi ser</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_6/capitulo_23/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_6/capitulo_23/1.jpg" alt="Episodio G - Capitulo 23 - El que sostiene mi ser" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_6/capitulo_24/1.php">Capitulo 24: Aquel que se encuentra con los dioses</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_6/capitulo_24/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_6/capitulo_24/1.jpg" alt="Episodio G - Capitulo 24 - Aquel que se encuentra con los dioses" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_6/capitulo_25/1.php">Capitulo 25: Aquel que se vuelve legendario</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_6/capitulo_25/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_6/capitulo_25/1.jpg" alt="Episodio G - Capitulo 25 - Aquel que se vuelve legendario" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 5</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_5/capitulo_18/1.php">Capitulo 18: Aquel que responde al Llamado</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_5/capitulo_18/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_5/capitulo_18/1.jpg" alt="Episodio G - Capitulo 18 - Aquel que responde al llamado" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_5/capitulo_19/1.php">Capitulo 19: Aquel ser antiguo</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_5/capitulo_19/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_5/capitulo_19/1.jpg" alt="Episodio G - Capitulo 19 - Aquel ser antiguo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_5/capitulo_20/1.php">Capitulo 20: Aquel que hereda</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_5/capitulo_20/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_5/capitulo_20/1.jpg" alt="Episodio G - Capitulo 20 - Aquel que Hereda" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_5/capitulo_21/1.php">Capitulo 21: Aquel que alaba al Destino</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_5/capitulo_21/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_5/capitulo_21/1.jpg" alt="Episodio G - Capitulo 21 - Aquel que alaba al Destino" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>

                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 4</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_4/capitulo_14/1.php">Capitulo 14: Aquel que se Agiganta</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_4/capitulo_14/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_4/capitulo_14/1.jpg" alt="Episodio G - Capitulo 14 - Aquel que se Agiganta" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_4/capitulo_15/1.php">Capitulo 15: Aquel del Colmillo Dorado</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_4/capitulo_15/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_4/capitulo_15/1.jpg" alt="Episodio G - Capitulo 15 - Aquel del Colmillo Dorado" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_4/capitulo_16/1.php">Capitulo 16: Aquel que se convierte en Demonio</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_4/capitulo_16/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_4/capitulo_16/1.jpg" alt="Episodio G - Capitulo 16 - Aquel que se convierte en Demonio" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_4/capitulo_17/1.php">Capitulo 17: Aquel que guia al mas alla</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_4/capitulo_17/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_4/capitulo_17/1.jpg" alt="Episodio G - Capitulo 17 - Aquel que guia al mas alla" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 3</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_3/capitulo_10/1.php">Capitulo 10:  Aries</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_3/capitulo_10/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_3/capitulo_10/1.jpg" alt="Episodio G - Capitulo 10 - Aries" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_3/capitulo_11/1.php">Capitulo 11:  Aquel que abre las dimensiones</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_3/capitulo_11/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_3/capitulo_11/1.jpg" alt="Episodio G - Capitulo 11 -  Aquel que abre las dimensiones" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_3/capitulo_12/1.php">Capitulo 12: Aquel que libera gran Poder</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_3/capitulo_12/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_3/capitulo_12/1.jpg" alt="Episodio G - Capitulo 12 - Aquel que libera gran Poder" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_3/capitulo_13/1.php">Capitulo 13: Aquel que impone Justicia</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_3/capitulo_13/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_3/capitulo_13/1.jpg" alt="Episodio G - Capitulo 13 - Aquel que impone Justicia" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 2</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_2/capitulo_6/1.php">Capítulo 6: Aquel Negro como el Ebano</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_2/capitulo_6/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_2/capitulo_6/1.jpg" alt="Episodio G - Capítulo 6 - Aquel Negro como el Ebano" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_2/capitulo_7/1.php">Capitulo 7: Aquellos que Obedecen al Viento</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_2/capitulo_7/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_2/capitulo_7/1.jpg" alt="Episodio G - Capitulo 7 - Aquellos que Obedecen al Viento" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_2/capitulo_8/1.php">Capitulo 8: Aquel que quebranta huesos</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_2/capitulo_8/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_2/capitulo_8/1.jpg" alt="Episodio G - Capitulo 8 - Aquel que quebranta huesos" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_2/capitulo_9/1.php">Capitulo 9: Quien Repara Armaduras</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_2/capitulo_9/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_2/capitulo_9/1.jpg" alt="Episodio G - Capitulo 9 - Quien Repara Armaduras" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Episodio G - Volumen 1</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_1/capitulo_1/1.php">Capitulo 1: Prologo</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_1/capitulo_1/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_1/capitulo_1/1.jpg" alt="Episodio G - Capitulo 1 - Prologo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_1/capitulo_2/1.php">Capítulo 2: El Joven de Oro</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_1/capitulo_2/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_1/capitulo_2/1.jpg" alt="Episodio G - Capitulo 2 - El Joven de Oro" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_1/capitulo_3/1.php">Capitulo 3: El Ser que se Lamenta</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_1/capitulo_3/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_1/capitulo_3/1.jpg" alt="Episodio G - Capitulo 3 - El Ser que se Lamenta" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_1/capitulo_4/1.php">Capitulo 4: Aquel que desgarra la Luz</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_1/capitulo_4/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_1/capitulo_4/1.jpg" alt="Episodio G - Capitulo 4 - Aquel que desgarra la Luz" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank" href="episodio_g/volumen_1/capitulo_5/1.php">Capitulo 5: Aquel que se Reune</a></h3>
                                            </header>
                                            <a target="_blank" href="episodio_g/volumen_1/capitulo_5/1.php" class="image featured"><img src="../images/manga/episodio_g/volumen_1/capitulo_5/1.jpg" alt="Episodio G - Capitulo 5 - Aquel que se Reune" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>

                            </div>

                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../template/footer.php';
        ?>

    </body>
</html>