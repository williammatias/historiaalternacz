<!DOCTYPE HTML>
<html>
    <head>
        <title>Next Dimension Manga - SaintSeiyaSigma.com</title>
        <meta name="description" content="Inicia con un prólogo en el cual Hades  recuerda que otro guerrero de la constelaciónn de pegaso, además de herirlo en la era del mito, 
              fue su amigo en la Guerra Santa del siglo XVIII, hace 243 años. Así, la historia pasa a centrarse en ese siglo,
              donde los jóvenes Shion y Dohko son ascendidos de santos de bronce a santos de oro. " />
        <link rel="stylesheet" href="../css/jquery-ui.css" />

        <?php
        include '../template/head.php';
        ?>
        <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
        <script>
            $(function() {
                $("#accordion").accordion();
            });
        </script>
    </head>
    <body class="next_dimension">

        <!-- Header -->
        <div id="header">

            <!-- Inner -->
            <div class="inner">
                <header>
                    <h1><a target="_blank"href="#" id="logo">Next Dimension de Saint Seiya</a></h1>
                </header>
            </div>

            <!-- Nav -->
            <?php
            include '../template/navigation.php';
            ?>

        </div>

        <!-- Main -->
        <div class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">
                        <article id="main" class="special">
                            <header>
                                <h2>Next Dimension: Chapters</h2>
                                <span class="byline">
                                    <script type="text/javascript"><!--
                                        google_ad_client = "ca-pub-6835894736815485";
                                        /* SSLeaderBoard */
                                        google_ad_slot = "1201595250";
                                        google_ad_width = 728;
                                        google_ad_height = 90;
//-->
                                    </script>
                                    <script type="text/javascript"
                                            src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                                    </script>
                                    <div class="fb-like-box" data-href="https://www.facebook.com/pages/Saint-Seiya-Sigma/444882938963306" data-width="The pixel width of the plugin" data-height="The pixel height of the plugin" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="false"></div>
                                </span>
                            </header>


                            <div id="accordion">
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Next Dimension - Volumen 1</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_1/1.php">Capitulo 1: Dohko y Shion</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_1/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_1/1.jpg" alt="Next Dimension - Capitulo 1 - Prologo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_2/1.php">Capitulo 2: Alone</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_2/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_2/1.jpg" alt="Next Dimension - Capitulo 2 - Alone" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_3/1.php">Capitulo 3: Tenma</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_3/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_3/1.jpg" alt="Next Dimension - Capitulo 3 - Tenma" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_4/1.php">Capitulo 4: Amigo</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_4/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_4/1.jpg" alt="Next Dimension - Capitulo 4 - Amigo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_5/1.php">Capitulo 5: Espada de Hades</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_5/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_5/1.jpg" alt="Next Dimension - Capitulo 5 - Espada de Hades" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_6/1.php">Capitulo 6: Hades Despierta</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_6/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_6/1.jpg" alt="Next Dimension - Capitulo 6 - Hades Despierta" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_7/1.php">Capitulo 7: Castillo de Hades</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_7/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_7/1.jpg" alt="Next Dimension - Capitulo 7 - Castillo de Hades" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_8/1.php">Capitulo 8: Espectros</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_8/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_8/1.jpg" alt="Next Dimension - Capitulo 8 - Espectros" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_9/1.php">Capitulo 9: Barrera</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_9/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_9/1.jpg" alt="Next Dimension - Capitulo 9 - Barrera" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_10/1.php">Capitulo 10: Suikyo</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_10/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_10/1.jpg" alt="Next Dimension - Capitulo 10 - Suikyo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_11/1.php">Capitulo 11: Calidez</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_11/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_11/1.jpg" alt="Next Dimension - Capitulo 11 - Calidez" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_1/capitulo_12/1.php">Capitulo 12: El Agua de Crateris</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_1/capitulo_12/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_1/capitulo_12/1.jpg" alt="Next Dimension - Capitulo 12 - Suikyo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Next Dimension - Volumen 2</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_2/capitulo_13/1.php">Capitulo 13: Izou y Ox</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_2/capitulo_13/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_2/capitulo_13/1.jpg" alt="Next Dimension - Capitulo - 13 Izou y Ox" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_2/capitulo_14/1.php">Capitulo 14: Cadena de Flores</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_2/capitulo_14/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_2/capitulo_14/1.jpg" alt="Next Dimension - Capitulo 14 - Cadena de Flores" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_2/capitulo_15/1.php">Capitulo 15: El Templo de la Luna</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_2/capitulo_15/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_2/capitulo_15/1.jpg" alt="Next Dimension - Capitulo 15 - El Templo de la Luna" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_2/capitulo_16/1.php">Capitulo 16: Mientras haya amor</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_2/capitulo_16/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_2/capitulo_16/1.jpg" alt="Next Dimension - Capitulo 16 - Mientras haya amor" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_2/capitulo_17/1.php">Capitulo 17: La Scoumoune</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_2/capitulo_17/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_2/capitulo_17/1.jpg" alt="Next Dimension - Capitulo 17 - La Scoumoune" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_2/capitulo_18/1.php">Capitulo 18: Ave Fenix</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_2/capitulo_18/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_2/capitulo_18/1.jpg" alt="Next Dimension - Capitulo 18 - Ave Fenix" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_2/capitulo_19/1.php">Capitulo 19 - El portal de espacio-tiempo</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_2/capitulo_19/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_2/capitulo_19/1.jpg" alt="Next Dimension - Capitulo 19 - El portal de espacio-tiempo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Next Dimension - Volumen 3</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_3/capitulo_20/1.php">Capitulo 20: Natividad de Athena</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_3/capitulo_20/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_3/capitulo_20/1.jpg" alt="Next Dimension - Capitulo 20 - Natividad de Athena" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_3/capitulo_21/1.php">Capitulo 21: La Daga del Patriarca</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_3/capitulo_21/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_3/capitulo_21/1.jpg" alt="Next Dimension - Capitulo 21 - La Daga del Patriarca" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_3/capitulo_22/1.php">Capitulo 22: Al lado de Athena</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_3/capitulo_22/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_3/capitulo_22/1.jpg" alt="Next Dimension - Capitulo 22 - Al lado de Athena" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_3/capitulo_23/1.php">Capitulo 23: El enemigo esta en el Santuario</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_3/capitulo_23/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_3/capitulo_23/1.jpg" alt="Next Dimension - Capitulo 23 - El enemigo esta en el Santuario" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_3/capitulo_24/1.php">Capitulo 24: Muro de Cristal</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_3/capitulo_24/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_3/capitulo_24/1.jpg" alt="Next Dimension - Capitulo 24 - Muro de Cristal" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_3/capitulo_25/1.php">Capitulo 25: Lagrimas de Sangre</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_3/capitulo_25/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_3/capitulo_25/1.jpg" alt="Next Dimension - Capitulo 25 - Lagrimas de Sangre" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_3/capitulo_26/1.php">Capitulo 26: La Guia de Athena</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_3/capitulo_26/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_3/capitulo_26/1.jpg" alt="Next Dimension - Capitulo 26 - La Guia de Athena" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>

                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Next Dimension - Volumen 4</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_4/capitulo_27/1.php">Capitulo 27: Combate a Muerte en Aries</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_4/capitulo_27/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_4/capitulo_27/1.jpg" alt="Next Dimension - Capitulo 27 Combate a Muerte en Aries" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_4/capitulo_28/1.php">Capitulo 28: Suishou</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_4/capitulo_28/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_4/capitulo_28/1.jpg" alt="Next Dimension - Capitulo 28 - Suishou" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_4/capitulo_29/1.php">Capitulo 29: Lanzas de Hielo</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_4/capitulo_29/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_4/capitulo_29/1.jpg" alt="Next Dimension - Capitulo 29 - Lanzas de Hielo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_4/capitulo_30/1.php">Capitulo 30: Angel Caido de un Ala</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_4/capitulo_30/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_4/capitulo_30/1.jpg" alt="Next Dimension - Capitulo 30 - Mientras haya amor" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_4/capitulo_31/1.php">Capitulo 31: El Laberinto de Geminis</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_4/capitulo_31/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_4/capitulo_31/1.jpg" alt="Next Dimension - Capitulo 31 - El Laberinto de Geminis" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_4/capitulo_32/1.php">Capitulo 32: Premonicion de un combate a Muerte</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_4/capitulo_32/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_4/capitulo_32/1.jpg" alt="Next Dimension - Capitulo 32 - Premonicion de un combate a Muerte" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_4/capitulo_33/1.php">Capitulo 33 Combate a muerte en Geminis</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_4/capitulo_33/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_4/capitulo_33/1.jpg" alt="Next Dimension - Capitulo 33 - Combate a muerte en Geminis" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>

                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Next Dimension - Volumen 5</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_5/capitulo_34/1.php">Capitulo 34: Abel de Geminis</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_5/capitulo_34/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_5/capitulo_34/1.jpg" alt="Next Dimension - Capitulo 34 - Abel de Geminis" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_5/capitulo_35/1.php">Capitulo 35: Cain y Abel</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_5/capitulo_35/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_5/capitulo_35/1.jpg" alt="Next Dimension -Capitulo 35 - Cain y Abel" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_5/capitulo_36/1.php">Capitulo 36: Deathtoll, El Fabricante de Ataudes</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_5/capitulo_36/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_5/capitulo_36/1.jpg" alt="Next Dimension - Capitulo 36 - Deathtoll, El Fabricante de Ataudes" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_5/capitulo_37/1.php">Capitulo 37: Omerta</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_5/capitulo_37/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_5/capitulo_37/1.jpg" alt="Next Dimension - Capitulo 37 - Omerta" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_5/capitulo_38/1.php">Capitulo 38: El Sacrificio del Puño del Emperador Demoniaco</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_5/capitulo_38/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_5/capitulo_38/1.jpg" alt="Next Dimension - Capitulo 38 - El Sacrificio del Puño del Emperador Demoniaco" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_5/capitulo_39/1.php">Capitulo 39: Aquel Calido Dia</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_5/capitulo_39/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_5/capitulo_39/1.jpg" alt="Next Dimension - Capitulo 39 - Aquel Calido Dia" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_5/capitulo_40/1.php">Capitulo 40: Caminando</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_5/capitulo_40/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_5/capitulo_40/1.jpg" alt="Next Dimension - Capitulo 40 - Caminando" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>

                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Next Dimension - Volumen 6</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_6/capitulo_41/1.php">Capitulo 41: Por ese dia...</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_6/capitulo_41/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_6/capitulo_41/1.jpg" alt="Next Dimension - Capitulo 41 - Por ese dia..." /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_6/capitulo_42/1.php">Capitulo 42: Estrellas Resucitadoras</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_6/capitulo_42/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_6/capitulo_42/1.jpg" alt="Next Dimension - Capitulo 42 - Estrellas Resucitadoras" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_6/capitulo_43/1.php">Capitulo 43: Kaiser de Leo</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_6/capitulo_43/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_6/capitulo_43/1.jpg" alt="Next Dimension - Capitulo 43 - Kaiser de Leo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_6/capitulo_44/1.php">Capitulo 44: Puño de Luz</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_6/capitulo_44/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_6/capitulo_44/1.jpg" alt="Next Dimension - Capitulo 44 - Puño de Luz" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_6/capitulo_45/1.php">Capitulo 45: Broma de Los Dioses</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_6/capitulo_45/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_6/capitulo_45/1.jpg" alt="Next Dimension - Capitulo 45 - Broma de Los Dioses" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_6/capitulo_46/1.php">Capitulo 46: Laberinto de los Dioses</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_6/capitulo_46/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_6/capitulo_46/1.jpg" alt="Next Dimension - Capitulo 46 - Laberinto de los Dioses" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_6/capitulo_47/1.php">Capitulo 47: Compasion de Guerrero</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_6/capitulo_47/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_6/capitulo_47/1.jpg" alt="Next Dimension - Capitulo 47 - Compasion de Guerrero" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                <h3><span class="icon icon-arrow-down"></span>Saint Seiya Next Dimension - Volumen 7</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_7/capitulo_48/1.php">Capitulo 48: Flama de la Amistad</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_7/capitulo_48/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_7/capitulo_48/1.jpg" alt="Next Dimension - Capitulo 48 - Flama de la Amistad" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_7/capitulo_49/1.php">Capitulo 49: La puerta de la Amistad</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_7/capitulo_49/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_7/capitulo_49/1.jpg" alt="Next Dimension - Capitulo 49 - La puerta de la Amistad" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_7/capitulo_50/1.php">Capitulo 50: Gran Luz</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_7/capitulo_50/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_7/capitulo_50/1.jpg" alt="Next Dimension - Capitulo 50 - Gran Luz" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_7/capitulo_51/1.php">Capitulo 51: Cisne y Dragon</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_7/capitulo_51/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_7/capitulo_51/1.jpg" alt="Next Dimension - Capitulo 51 - Cisne y Dragon" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_7/capitulo_52/1.php">Capitulo 52: El Tigre y el Dragon, Maestro y Discipulo</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_7/capitulo_52/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_7/capitulo_52/1.jpg" alt="Next Dimension - Capitulo 52 - El Tigre y el Dragon, Maestro y Discipulo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_7/capitulo_53/1.php">Capitulo 53: Sucesor</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_7/capitulo_53/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_7/capitulo_53/1.jpg" alt="Next Dimension - Capitulo 53 - Sucesor" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_7/capitulo_54/1.php">Capitulo 54 - Templo Demoniaco</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_7/capitulo_54/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_7/capitulo_54/1.jpg" alt="Next Dimension - Capitulo 54 - Templo Demoniaco" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                                 <h3><span class="icon icon-arrow-down"></span>Saint Seiya Next Dimension - Volumen 8</h3>
                                <div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_8/capitulo_55/1.php">Capitulo 55: El Decimo Tercer Santo de Oro</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_8/capitulo_55/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_8/capitulo_55/1.jpg" alt="Next Dimension - Capitulo 55 - El Decimo Tercer Santo de Oro" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_8/capitulo_56/1.php">Capitulo 56: Amigo Mio</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_8/capitulo_56/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_8/capitulo_56/1.jpg" alt="Next Dimension - Capitulo 56 - Amigo Mio" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_8/capitulo_57/1.php">Capitulo 57: Tenbo Horin</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_8/capitulo_57/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_8/capitulo_57/1.jpg" alt="Next Dimension - Capitulo 57 - Tenbo Horin" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_8/capitulo_58/1.php">Capitulo 58: Ohm</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_8/capitulo_58/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_8/capitulo_58/1.jpg" alt="Next Dimension - Capitulo 58 - Ohm" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_8/capitulo_59/1.php">Capitulo 59: Imprudencia</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_8/capitulo_59/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_8/capitulo_59/1.jpg" alt="Next Dimension - Capitulo 59 - Imprudencia" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_8/capitulo_60/1.php">Capitulo 60: Cosmo de un Dios</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_8/capitulo_60/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_8/capitulo_60/1.jpg" alt="Next Dimension - Capitulo 60 - Cosmo de un Dios" /></a>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                    <div class="row">
                                        <article class="u4 special">
                                            <header>
                                                <h3><a target="_blank"href="next_dimension/volumen_8/capitulo_61/1.php">Capitulo 61: Poema a un Amigo</a></h3>
                                            </header>
                                            <a target="_blank"href="next_dimension/volumen_8/capitulo_61/1.php" class="image featured"><img src="../images/manga/next_dimension/volumen_8/capitulo_61/1.jpg" alt="Next Dimension - Capitulo 61 - Poema a un Amigo" /></a>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                        <article class="u4 special">
                                            <header>
                                            </header>
                                            <p>
                                            </p>
                                        </article>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
                <hr />
            </div>

        </div>

        <!-- Features -->
        <?php
        include '../template/featured.php';
        ?>

        <!-- Footer -->
        <?php
        include '../template/footer.php';
        ?>

    </body>
</html>