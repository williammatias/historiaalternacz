<!DOCTYPE HTML>
<html>
    <head>
        <title>The Lost Canvas - Capitulo 1  Los dias perdidos  - SaintSeiyaSigma.com</title>
        <meta name="description" content="The Lost Canvas es un manga spin-off de Saint Seiya que cuenta los hechos ocurridos en la anterior guerra santa contra hades llevada..." />
                

        <?php
        include '../../../../template/head.php';
        ?>

        <?php
        include './cap_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = '16.php';
            }
            function previous() {
                window.location = '14.php';
            }

//<![CDATA[
            $(document).ready(function() {
                var element = document.getElementById('historia');
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer().on("swiperight", function() {
                    previous();
                });

            });
//]]>
        </script>
    </head>
    <body class="manga the_lost_canvas capitulo_1">

        <!-- Main -->
        <div id="historia" class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <div class="bar row">
                                <div class="u5">                                    
                                    <a id="return" class="u1"  href="../../../the_lost_canvas.php">
                                        <span class="icon icon-home circled"></span>
                                    </a>
                                    <div class="u11">
                                        <h3 class="volumen">The Lost Canvas - Volumen 1</h3>
                                        <h3 class="capitulo">Capítulo 1: Los dias perdidos</h3>
                                    </div>             
                                </div>             
                                <div class="u3">
                                    <?php
                                    include './cap_share.php';
                                    ?>
                                </div> 
                                <div class="u2">
                                    <h3 class="pagina">Pagina 15</h3>
                                </div>
                                <div class="u2 next_previous">
                                    <span class="icon icon-arrow-left circled" onclick="previous();"></span>
                                    <span class="icon icon-arrow-right circled" onclick="next();"></span>
                                </div>    
                                <div class="clear"></div>
                            </div>
                            <a href="#" class="image featured u12">
                                <img src="../../../../images/manga/the_lost_canvas/volumen_1/capitulo_1/15.jpg" alt="The Lost Canvas Capitulo 1 Pagina 15" />
                            </a>
                        </article>
                    </div>
                </div>

            </div>
        </div>

        <!-- Footer -->
        <?php
        include '../../../../template/footer_ad.php';
        ?>    

    </body>
</html>