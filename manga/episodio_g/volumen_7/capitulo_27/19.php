<!DOCTYPE HTML>
<html>
    <head>
        <title>Episodio G - Capitulo 27 Aquel que desciende - SaintSeiyaSigma.com</title>
        <meta name="description" content="La serie cuenta la historia de los santos de oro siete años antes del Saint Seiya original con Aioria de Leo como protagonista, narra los problemas a los que se enfrenta Aioria..." />


        <?php
        include '../../../../template/head.php';
        ?>

        <?php
        include './cap_head.php';
        ?>

        <script type="text/javascript">
            $(document).keyup(function(e) {
                // handle cursor keys
                if (event.keyCode === 37) {
                    previous();
                } else if (event.keyCode === 39) {
                    next();
                }
            });
            function next() {
                window.location = '20.php';
            }
            function previous() {
                window.location = '18.php';
            }

//<![CDATA[
            $(document).ready(function() {
                var element = document.getElementById('historia');
                Hammer(element).on("swipeleft", function() {
                    next();
                });

                Hammer().on("swiperight", function() {
                    previous();
                });

            });
//]]>
        </script>
    </head>
    <body class="manga episodio_g capitulo_27">

        <!-- Main -->
        <div id="historia" class="wrapper style1">

            <div class="container">
                <div class="row">
                    <div class="u12 skel-cell-mainContent" id="content">                        

                        <article id="main" class="special">
                            <div class="bar row">
                                <div class="u5">                                    
                                    <a id="return" class="u1"  href="../../../episodio_g.php">
                                        <span class="icon icon-home circled"></span>
                                    </a>
                                    <div class="u11">
                                        <h3 class="volumen">Episodio G - Volumen 7</h3>
                                        <h3 class="capitulo">Capítulo 27: Aquellos que iluminan el Simbolo</h3>
                                    </div>             
                                </div>             
                                <div class="u3">
                                    <?php
                                    include './cap_share.php';
                                    ?>
                                </div> 
                                <div class="u2">
                                    <h3 class="pagina">Pagina 19</h3>
                                </div>
                                <div class="u2 next_previous">
                                    <span class="icon icon-arrow-left circled" onclick="previous();"></span>
                                    <span class="icon icon-arrow-right circled" onclick="next();"></span>
                                </div>    
                                <div class="clear"></div>
                            </div>
                            <a href="#" class="image featured u12">
                                <img src="../../../../images/manga/episodio_g/volumen_7/capitulo_27/19.jpg" alt="Episodio G - Capitulo 27 - Pagina 19" />
                            </a>
                        </article>
                    </div>
                </div>

            </div>
        </div>

        <!-- Footer -->
        <?php
        include '../../../../template/footer_ad.php';
        ?>    

    </body>
</html>